QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    lp_dali/lp_dali_master.cpp \
    lp_pc_protocol/lp_pc_protocol.cpp \
    main.cpp \
    usb/hidapi/hid.c \
    usb/mUSBHID.cpp \
    usb/usbdate_decode.cpp \
    widget.cpp

HEADERS += \
    lp_common/bit/bitfield.h \
    lp_dali/lp_dali_102_command.h \
    lp_dali/lp_dali_207_command.h \
    lp_dali/lp_dali_209_command.h \
    lp_dali/lp_dali_master.h \
    lp_pc_protocol/lp_pc_protocol.h \
    usb/hidapi/elf.h \
    usb/hidapi/hidapi.h \
    usb/hidapi/hidapi_cfgmgr32.h \
    usb/hidapi/hidapi_hidclass.h \
    usb/hidapi/hidapi_hidpi.h \
    usb/hidapi/hidapi_hidsdi.h \
    usb/hidapi/hidapi_winapi.h \
    usb/mUSBHID.h \
    usb/usbdate_decode.h \
    widget.h

FORMS += \
    widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/usb/ -llibusb-1.0
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/usb/ -llibusb-1.0
else:unix:!macx: LIBS += -L$$PWD/usb/ -llibusb-1.0

INCLUDEPATH += $$PWD/usb
DEPENDPATH += $$PWD/usb

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/usb/libusb-1.0.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/usb/libusb-1.0.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/usb/libusb-1.0.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/usb/libusb-1.0.lib
else:unix:!macx: PRE_TARGETDEPS += $$PWD/usb/libusb-1.0.a
