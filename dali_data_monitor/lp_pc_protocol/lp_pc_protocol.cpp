
#include "lp_pc_protocol/lp_pc_protocol.h"


lp_pc_command_check::lp_pc_command_check(lp_pdataPack_t pdatapack)
{

  pdatapack->dataHeader.opcode = LP_PC_COM_DALI_DOWN_CHECK_E;
  pdatapack->dataHeader.dataLen = sizeof(lp_dataHeader_t);
  pdatapack->buf = NULL;

}

lp_pc_command_check::~lp_pc_command_check()
{

}


lp_pc_command_extend_check::lp_pc_command_extend_check(lp_pdataPack_t pdatapack)
{
  pdatapack->dataHeader.opcode = LP_PC_COM_DALI_DOWN_EXTEND_CHECK_E;
  pdatapack->dataHeader.dataLen = sizeof(lp_dataHeader_t);
  pdatapack->buf = NULL;
}

lp_pc_command_extend_check::~lp_pc_command_extend_check()
{

}


lp_pc_command_search_again::lp_pc_command_search_again(lp_pdataPack_t pdatapack)
{
    pdatapack->dataHeader.opcode = LP_PC_COM_DALI_DOWN_SEARCH_AGAIN_E;
    pdatapack->dataHeader.dataLen = sizeof(lp_dataHeader_t);
    pdatapack->buf = NULL;
}

lp_pc_command_search_again::~lp_pc_command_search_again()
{

}


 lp_pc_command_read_info::lp_pc_command_read_info(lp_pdataPack_t pdatapack,int addr)
 {
     pdatapack->dataHeader.opcode = LP_PC_COM_DALI_DOWN_READ_INFO_E;
     pdatapack->dataHeader.dataLen = sizeof(lp_dataHeader_t)+1;
     pdatapack->buf = NULL;
     pdatapack->arr_buf.append((uint8_t)addr);
 }

 lp_pc_command_read_info::~lp_pc_command_read_info()
 {

 }


 lp_pc_command_write_info::lp_pc_command_write_info(lp_pdataPack_t pdatapack,int addr)
 {
     pdatapack->dataHeader.opcode = LP_PC_COM_DALI_DOWN_WRITE_INFO_E;
     pdatapack->dataHeader.dataLen = sizeof(lp_dataHeader_t)+1;
     pdatapack->buf = NULL;
     pdatapack->arr_buf.append((uint8_t)addr);
 }

 lp_pc_command_write_info::~lp_pc_command_write_info()
 {

 }

 lp_pc_command_set_addr::lp_pc_command_set_addr(lp_pdataPack_t pdatapack,int origin_addr,int target_addr)
 {
     pdatapack->dataHeader.opcode = LP_PC_COM_DALI_DOWN_SET_ADDR_E;
     pdatapack->dataHeader.dataLen = sizeof(lp_dataHeader_t)+2;
     pdatapack->buf = NULL;
     pdatapack->arr_buf.append((uint8_t)origin_addr);
     pdatapack->arr_buf.append((uint8_t)target_addr);
 }

 lp_pc_command_set_addr::~lp_pc_command_set_addr()
 {

 }


 lp_pc_command_set_maxlight::lp_pc_command_set_maxlight(lp_pdataPack_t pdatapack,int addr,int maxlight)
 {
     pdatapack->dataHeader.opcode = LP_PC_COM_DALI_DOWN_SET_MAXLIGHT_E;
     pdatapack->dataHeader.dataLen = sizeof(lp_dataHeader_t)+2;
     pdatapack->buf = NULL;
     pdatapack->arr_buf.append((uint8_t)addr);
     pdatapack->arr_buf.append((uint8_t)maxlight);

 }

 lp_pc_command_set_maxlight::~lp_pc_command_set_maxlight()
 {

 }


 lp_pc_command_set_minlight::lp_pc_command_set_minlight(lp_pdataPack_t pdatapack,int addr,int minlight)
 {
     pdatapack->dataHeader.opcode = LP_PC_COM_DALI_DOWN_SET_MINLIGHT_E;
     pdatapack->dataHeader.dataLen = sizeof(lp_dataHeader_t)+2;
     pdatapack->buf = NULL;
     pdatapack->arr_buf.append((uint8_t)addr);
     pdatapack->arr_buf.append((uint8_t)minlight);
 }

lp_pc_command_set_minlight::~lp_pc_command_set_minlight()
{

}


lp_pc_command_active_maxlight::lp_pc_command_active_maxlight(lp_pdataPack_t pdatapack,int addr)
{
    pdatapack->dataHeader.opcode = LP_PC_COM_DALI_DOWN_ACTIVE_MAXLIGHT_E;
    pdatapack->dataHeader.dataLen = sizeof(lp_dataHeader_t)+1;
    pdatapack->buf = NULL;
    pdatapack->arr_buf.append((uint8_t)addr);
}

lp_pc_command_active_maxlight::~lp_pc_command_active_maxlight()
{


}


lp_pc_command_set_dapc::lp_pc_command_set_dapc(lp_pdataPack_t pdatapack,int addr,int level)
{
    pdatapack->dataHeader.opcode = LP_PC_COM_DALI_DOWN_SET_DAPC_E;
    pdatapack->dataHeader.dataLen = sizeof(lp_dataHeader_t)+2;
    pdatapack->buf = NULL;
    pdatapack->arr_buf.append((uint8_t)addr);
    pdatapack->arr_buf.append((uint8_t)level);

}

lp_pc_command_set_dapc::~lp_pc_command_set_dapc()
{


}

lp_pc_command_set_off_light::lp_pc_command_set_off_light(lp_pdataPack_t pdatapack,int addr)
{
    pdatapack->dataHeader.opcode = LP_PC_COM_DALI_DOWN_SET_OFF_LIGHT_E;
    pdatapack->dataHeader.dataLen = sizeof(lp_dataHeader_t)+1;
    pdatapack->buf = NULL;
    pdatapack->arr_buf.append((uint8_t)addr);

}


lp_pc_command_set_off_light::~lp_pc_command_set_off_light()
{


}












