


#ifndef _LP_PC_PROTOCOL_
#define _LP_PC_PROTOCOL_

#include <QtCore/QtGlobal>
#include <QByteArray>

#ifdef __cplusplus
extern "C" {
#endif

//#include "config/lp_config.h"

typedef struct _lp_dataHeader
{
    short dataLen; //两个字节后面数据的长度
    short opcode;

}lp_dataHeader_t,*lp_pdataHeader_t;


//DALI 0X1000  开始
//up 上行命令
//down 下行命令
typedef enum 
{
	LP_PC_COM_DALI_UP_LOG_E = 0x1000, 
    LP_PC_COM_DALI_UP_ORDER_E,
    LP_PC_COM_DALI_DOWN_CHECK_E,
    LP_PC_COM_DALI_DOWN_EXTEND_CHECK_E,
    LP_PC_COM_DALI_DOWN_SEARCH_AGAIN_E,
    LP_PC_COM_DALI_UP_REPORT_DEV_E,
    LP_PC_COM_DALI_UP_REPORT_INFO_E,
    LP_PC_COM_DALI_DOWN_READ_INFO_E,
    LP_PC_COM_DALI_DOWN_WRITE_INFO_E,
    LP_PC_COM_DALI_DOWN_SET_ADDR_E,
    LP_PC_COM_DALI_DOWN_SET_MAXLIGHT_E,
    LP_PC_COM_DALI_DOWN_SET_MINLIGHT_E,
    LP_PC_COM_DALI_DOWN_ACTIVE_MAXLIGHT_E,
    LP_PC_COM_DALI_DOWN_SET_DAPC_E,
    LP_PC_COM_DALI_DOWN_SET_OFF_LIGHT_E,

    
}lp_pc_commond_dali_e;


typedef struct _lp_dataPack
{
    lp_dataHeader_t dataHeader;
    uint8_t* buf;
    QByteArray arr_buf;

}lp_dataPack_t,*lp_pdataPack_t;

typedef struct _lp_usbOrigin
{
    uint8_t buf[64];

}lp_usbOrigin_t,*lp_pusbOrigin_t;

class lp_pc_command_check {

public:
  lp_pc_command_check(lp_pdataPack_t pdatapack);
  ~lp_pc_command_check();

};

class lp_pc_command_extend_check {

public:
    lp_pc_command_extend_check(lp_pdataPack_t pdatapack);
    ~lp_pc_command_extend_check();

};

class lp_pc_command_search_again {

public:
    lp_pc_command_search_again(lp_pdataPack_t pdatapack);
    ~lp_pc_command_search_again();

};

class lp_pc_command_read_info {

public:
    lp_pc_command_read_info(lp_pdataPack_t pdatapack,int addr);
    ~lp_pc_command_read_info();

};

class lp_pc_command_write_info {

public:
    lp_pc_command_write_info(lp_pdataPack_t pdatapack,int addr);
    ~lp_pc_command_write_info();

};

class lp_pc_command_set_addr {

public:
    lp_pc_command_set_addr(lp_pdataPack_t pdatapack,int origin_addr,int target_addr);
    ~lp_pc_command_set_addr();

};

//MAXLIGHT
class lp_pc_command_set_maxlight {

public:
    lp_pc_command_set_maxlight(lp_pdataPack_t pdatapack,int addr,int maxlight);
    ~lp_pc_command_set_maxlight();

};

class lp_pc_command_set_minlight {

public:
    lp_pc_command_set_minlight(lp_pdataPack_t pdatapack,int addr,int minlight);
    ~lp_pc_command_set_minlight();

};


//LP_PC_COM_DALI_DOWN_SET_ACTIVE_MAXLIGHT_E,
class lp_pc_command_active_maxlight {

public:
    lp_pc_command_active_maxlight(lp_pdataPack_t pdatapack,int addr);
    ~lp_pc_command_active_maxlight();

};
//LP_PC_COM_DALI_DOWN_SET_DAPC_E,
class lp_pc_command_set_dapc {

public:
    lp_pc_command_set_dapc(lp_pdataPack_t pdatapack,int addr,int level);
    ~lp_pc_command_set_dapc();

};

//LP_PC_COM_DALI_DOWN_SET_OFF_LIGHT_E,
class lp_pc_command_set_off_light {

public:
    lp_pc_command_set_off_light(lp_pdataPack_t pdatapack,int addr);
    ~lp_pc_command_set_off_light();

};


#ifdef __cplusplus
}
#endif

#endif

























