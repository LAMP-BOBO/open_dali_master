#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    setWindowTitle("LAMP_BOBO DALI测试工具");
    ui->tabWidget->setTabText(0,"LOG");
    ui->tabWidget->setTabText(1,"MASTER");

    ui->pBt_addr->setDisabled(true);
    ui->pBt_type->setDisabled(true);
    ui->pBt_version->setDisabled(true);
    ui->pBt_status->setDisabled(true);
    ui->pBt_activelevel->setDisabled(true);
    ui->verticalSlider_dacplevel->setMinimum(0);
    ui->verticalSlider_dacplevel->setMaximum(254);
    ui->lineEdit_dacplevel->setAlignment(Qt::AlignCenter);

    // 创建一个正则表达式，用于匹配0-254之间的数字
    QRegExp rx("([01]?\\d?\\d|2[0-4]\\d|25[0-4])");
    // 创建一个 QRegExpValidator 对象
    QRegExpValidator* validator = new QRegExpValidator(rx, this);

    ui->lineEdit_dacplevel->setValidator(validator);
    ui->lineEdit_maxlevel->setValidator(validator);
    ui->lineEdit_minlevel->setValidator(validator);

    /*listview*/
    m_stringListModel = new QStringListModel();
    ui->listView_dev->setModel(m_stringListModel);

    m_usb = new mUSBHID();
    m_pthread = new QThread();
    m_usb->moveToThread(m_pthread);

    connect(m_usb , SIGNAL(destroyed()), m_pthread , SLOT(quit()));//业务对象释放后，线程退出
    connect(m_pthread , SIGNAL(finished()), m_pthread , SLOT(deleteLater()));//子线程对象退出则结束，发出结束信号，释放子线程对象
    connect(m_usb , SIGNAL(endUsbTask()), this , SLOT(waitUsbConnect()));

    m_usb_decode = new usbData_decode();
    m_usbdecode_pthread = new QThread();
    m_usb_decode->moveToThread(m_usbdecode_pthread);

    connect(m_usb_decode , SIGNAL(destroyed()), m_usbdecode_pthread , SLOT(quit()));//业务对象释放后，线程退出
    connect(m_usbdecode_pthread , SIGNAL(finished()), m_usbdecode_pthread , SLOT(deleteLater()));//子线程对象退出则结束，发出结束信号，释放子线程对象
    connect(this,&Widget::startUsbdecode, m_usb_decode,&usbData_decode::Usbdata_decode);

    connect(m_usb_decode,&usbData_decode::sendShowText, this ,&Widget::showText);
    connect(m_usb_decode,&usbData_decode::sendAddDev, this,&Widget::addDevListview);
    connect(m_usb_decode,&usbData_decode::sendInfo, this,&Widget::setDev_info);

    connect(this, SIGNAL(startUsbTask(int,int)), m_usb, SLOT(startUsb(int,int)));
    connect(m_usb,&mUSBHID::sendData_push,this,&Widget::revData_push);

    m_timer.setInterval(1000);
    m_timer.setTimerType(Qt::TimerType::PreciseTimer);
    m_timer.setSingleShot(false);
    connect(&m_timer,SIGNAL(timeout()),this,SLOT(timeOut()));

    //libusb_init(NULL);
    hid_init();

    QSettings setting("config.ini",QSettings::IniFormat);
    vid = setting.value("vid").toInt();
    pid = setting.value("pid").toInt();
    if(vid == 0 || pid == 0)
    {
        QSettings setting("config.ini",QSettings::IniFormat);
        setting.setValue("vid",0);
        setting.setValue("pid",0);
        QMessageBox errBox(QMessageBox::Information, tr("提示消息"),"未设置VID和PID",QMessageBox::Ok);
        errBox.exec();
    }
    else
    {
        m_timer.start();
    }
}

Widget::~Widget()
{
    delete ui;
}

void Widget::waitUsbConnect()
{
    m_timer.start();
    ui->label->setText("设备未连接");
    ui->label->setStyleSheet("QLabel{background-color:rgb(255,0,0);}");  //设置样式表

}

void Widget::timeOut()
{
//    libusb_device ** m_ppdevs_list;
//    libusb_device *dev;
//    int i = 0;
//    hid_device *handle;

//    libusb_get_device_list(NULL,&m_ppdevs_list);
//    while((dev = m_ppdevs_list[i++]) != NULL)
//    {
//        struct libusb_device_descriptor desc;
//        int r = libusb_get_device_descriptor(dev,&desc);
//        if(r < 0)
//        {
//            qDebug() << "failed to get device descriptor.";
//            break;
//        }

//        if(desc.idVendor == vid && desc.idProduct == pid)
//        {
//            qDebug()<<"设备连接成功!";
//            ui->label->setText("设备已连接");
//            ui->label->setStyleSheet("QLabel{background-color:rgb(0,255,0);}");  //设置样式表
//            m_timer.stop();
//            daliMasterConnectFlag = true;
//            libusb_free_device_list(m_ppdevs_list,1);
//            m_pthread->start();
//            emit startUsbTask(vid,pid);
//            return ;
//        }
//    }
//    libusb_free_device_list(m_ppdevs_list,1);

    hid_device_info *devs = hid_enumerate(vid, pid);
    if(devs != NULL){
         qDebug()<<"设备连接成功!";
         ui->label->setText("设备已连接");
         ui->label->setStyleSheet("QLabel{background-color:rgb(0,255,0);}");
         m_timer.stop();
         daliMasterConnectFlag = true;
         m_pthread->start();
         emit startUsbTask(vid,pid);

         m_usbdecode_pthread->start();
         emit startUsbdecode();

         return;
    }
    else{
        qDebug() << "failed to get device descriptor.";
    }

}

void Widget::on_pushButton_clear_released()
{
    ui->textBrowser->clear();
}

void Widget::showText(int index,QString text)
{
    if(index == 0)
    {
        ui->textBrowser->append("<font color=black>"+text+"</font>" "<font color=black> </font>");
        ui->textBrowser->append("");
    }
    else if(index == 1)
    {
        ui->textBrowser->append("<font color=red>"+text+"</font>" "<font color=black> </font>");
        ui->textBrowser->append("");
    }
    else if(index == 2)
    {
        ui->textBrowser->append("<font color=blue>"+text+"</font>" "<font color=black> </font>");
        ui->textBrowser->append("");
    }
    else if(index == 3)
    {
        ui->textBrowser->append("<font color=orange>"+text+"</font>" "<font color=black> </font>");
        ui->textBrowser->append("");
    }
    else if(index == 4)
    {
        ui->textBrowser->append("<font color=purple>"+text+"</font>" "<font color=black> </font>");
        ui->textBrowser->append("");
    }
    else if(index == 5)
    {
        ui->textBrowser->append("<font color=green>"+text+"</font>" "<font color=black> </font>");
        ui->textBrowser->append("");
    }
}

void Widget::addDevListview(int addr)
{
    QString str;
    str.append("Dev_Addr:"+QString::number(addr));


    qDebug("Dev_Addr:%d",addr);
    m_stringListModel->insertRow(m_stringListModel->rowCount());
    QModelIndex index=m_stringListModel->index(m_stringListModel->rowCount()-1,0);
    m_stringListModel->setData(index,str,Qt::DisplayRole);
    ui->listView_dev->setCurrentIndex(index);

    m_index_addr.insert(index,addr);
}

void Widget::revData_push(QByteArray arr)
{
    m_usb_decode->usbData_push(arr);
}

void  Widget::setDev_info(QByteArray info)
{
    uint8_t* rev_buf = (uint8_t*)info.data();
    lp_pdali_devices_data_t pdev = (lp_pdali_devices_data_t)rev_buf;
    qDebug("addr:0x%x,status:0x%x",pdev->addr,pdev->status);

    ui->lineEdit_addr->setText(QString::number(pdev->addr,10));
    ui->lineEdit_type->setText(QString::number(pdev->device_type,10));
    ui->lineEdit_status->setText("0x"+QString::number(pdev->status,16));
    ui->lineEdit_activelevel->setText(QString::number(pdev->lightness,10));
    ui->lineEdit_maxlevel->setText(QString::number(pdev->max_level,10));
    ui->lineEdit_minlevel->setText(QString::number(pdev->min_level,10));
    ui->lineEdit_version->setText(QString::number(pdev->version_number,10));

}

void Widget::on_pBt_check_released()
{
    lp_dataPack_t data;
    lp_pc_command_check pack(&data);
    QByteArray arr;

    //qDebug("data opcode:0x%x len:%d\n",data.dataHeader.opcode,data.dataHeader.dataLen);
    arr.append((char*)&data.dataHeader,sizeof(data.dataHeader));

    m_usb->usbSend_data(arr);
}


void Widget::on_pBt_extend_check_released()
{
    lp_dataPack_t data;
    lp_pc_command_extend_check pack(&data);
    QByteArray arr;

    //qDebug("data opcode:0x%x len:%d\n",data.dataHeader.opcode,data.dataHeader.dataLen);
    arr.append((char*)&data.dataHeader,sizeof(data.dataHeader));

    m_usb->usbSend_data(arr);
}

void Widget::on_pBt_search_again_released()
{
    lp_dataPack_t data;
    lp_pc_command_search_again pack(&data);
    QByteArray arr;

    //qDebug("data opcode:0x%x len:%d\n",data.dataHeader.opcode,data.dataHeader.dataLen);
    arr.append((char*)&data.dataHeader,sizeof(data.dataHeader));

   m_usb->usbSend_data(arr);
}

void Widget::on_pBt_enquiry_released()
{

    lp_dataPack_t data;
    int addr = ui->lineEdit_addr->text().toInt();
    lp_pc_command_read_info pack(&data,addr);
    QByteArray arr;

    //qDebug("data opcode:0x%x len:%d\n",data.dataHeader.opcode,data.dataHeader.dataLen);
    arr.append((char*)&data.dataHeader,sizeof(data.dataHeader));
    arr.append(data.arr_buf);

   m_usb->usbSend_data(arr);
}

void Widget::on_pBt_write_released()
{
    lp_dataPack_t data;
    int addr = ui->lineEdit_addr->text().toInt();
    int maxlevel = ui->lineEdit_maxlevel->text().toInt();
    int minlevel = ui->lineEdit_minlevel->text().toInt();

    lp_pc_command_write_info pack(&data,addr);
    QByteArray arr;

    //qDebug("data opcode:0x%x len:%d\n",data.dataHeader.opcode,data.dataHeader.dataLen);
    arr.append((char*)&data.dataHeader,sizeof(data.dataHeader));
    arr.append(data.arr_buf);
    arr.append(maxlevel);
    arr.append(minlevel);


   m_usb->usbSend_data(arr);

}


void Widget::on_listView_dev_indexesMoved(const QModelIndexList &indexes)
{
     Q_UNUSED(indexes);
}


void Widget::on_listView_dev_pressed(const QModelIndex &index)
{
    int addr = m_index_addr.value(index);
    ui->lineEdit_addr->setText(QString::number(addr,10));
    //qDebug("addr: %d \n",addr);
}


void Widget::on_pBt_maxlevel_released()
{

    lp_dataPack_t data;
    int addr = ui->lineEdit_addr->text().toInt();
    int maxlight = ui->lineEdit_maxlevel->text().toInt();
    lp_pc_command_set_maxlight pack(&data,addr,maxlight);
    QByteArray arr;

    //qDebug("data opcode:0x%x len:%d\n",data.dataHeader.opcode,data.dataHeader.dataLen);
    arr.append((char*)&data.dataHeader,sizeof(data.dataHeader));
    arr.append(data.arr_buf);

   m_usb->usbSend_data(arr);
}

void Widget::on_pBt_minlevel_released()
{
    lp_dataPack_t data;
    int addr = ui->lineEdit_addr->text().toInt();
    int minlight = ui->lineEdit_minlevel->text().toInt();
    lp_pc_command_set_minlight pack(&data,addr,minlight);
    QByteArray arr;

    //qDebug("data opcode:0x%x len:%d\n",data.dataHeader.opcode,data.dataHeader.dataLen);
    arr.append((char*)&data.dataHeader,sizeof(data.dataHeader));
    arr.append(data.arr_buf);

   m_usb->usbSend_data(arr);
}


void Widget::on_pBt_setmaxlevel_released()
{
    lp_dataPack_t data;
    int addr = ui->lineEdit_addr->text().toInt();
    lp_pc_command_active_maxlight pack(&data,addr);
    QByteArray arr;

    //qDebug("data opcode:0x%x len:%d\n",data.dataHeader.opcode,data.dataHeader.dataLen);
    arr.append((char*)&data.dataHeader,sizeof(data.dataHeader));
    arr.append(data.arr_buf);

   m_usb->usbSend_data(arr);
}


void Widget::on_pBt_turnoff_released()
{
    lp_dataPack_t data;
    int addr = ui->lineEdit_addr->text().toInt();
    lp_pc_command_set_off_light pack(&data,addr);
    QByteArray arr;

    //qDebug("data opcode:0x%x len:%d\n",data.dataHeader.opcode,data.dataHeader.dataLen);
    arr.append((char*)&data.dataHeader,sizeof(data.dataHeader));
    arr.append(data.arr_buf);

   m_usb->usbSend_data(arr);
}

void Widget::on_verticalSlider_dacplevel_valueChanged(int value)
{
    ui->lineEdit_dacplevel->setText(QString::number(value));
    lp_dataPack_t data;
    int addr = ui->lineEdit_addr->text().toInt();
    lp_pc_command_set_dapc pack(&data,addr,value);
    QByteArray arr;

    //qDebug("data opcode:0x%x len:%d\n",data.dataHeader.opcode,data.dataHeader.dataLen);
    arr.append((char*)&data.dataHeader,sizeof(data.dataHeader));
    arr.append(data.arr_buf);

   m_usb->usbSend_data(arr);
}

void Widget::on_lineEdit_dacplevel_editingFinished()
{
    int level = ui->lineEdit_dacplevel->text().toInt();
    if(level>254 || level < 0){
        QMessageBox errBox(QMessageBox::Information, tr("提示消息"),"输入数据无效",QMessageBox::Ok);
        errBox.exec();
        return;
    }

    ui->verticalSlider_dacplevel->setValue(level);

}

























