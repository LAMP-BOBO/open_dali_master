#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QtCore/QtGlobal>
#include "mUSBHID.h"
#include "lp_dali/lp_dali_master.h"
#include "lp_pc_protocol/lp_pc_protocol.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

signals:
    void startUsbTask(int,int);
    void startUsbdecode(void);

public slots:
    void timeOut();
    void waitUsbConnect();
    void showText(int index, QString text);
    void addDevListview(int addr);
    void revData_push(QByteArray arr);
    void setDev_info(QByteArray arr);

private slots:
    void on_pushButton_clear_released();
    void on_pBt_check_released();
    void on_pBt_extend_check_released();
    void on_pBt_search_again_released();
    void on_pBt_enquiry_released();
    void on_pBt_write_released();

    void on_listView_dev_indexesMoved(const QModelIndexList &indexes);

    void on_listView_dev_pressed(const QModelIndex &index);

    void on_pBt_maxlevel_released();

    void on_pBt_minlevel_released();

    void on_pBt_setmaxlevel_released();

    void on_pBt_turnoff_released();

    void on_verticalSlider_dacplevel_valueChanged(int value);

    void on_lineEdit_dacplevel_editingFinished();

private:
    Ui::Widget *ui;
    QThread * m_pthread;
    mUSBHID * m_usb;

    usbData_decode * m_usb_decode;
    QThread * m_usbdecode_pthread;

    QTimer m_timer;
    bool daliMasterConnectFlag = false;
    int pid=0;
    int vid=0;

    QStringListModel * m_stringListModel;
    QMap<QModelIndex,int> m_index_addr; //listview index mapping addr
};
#endif // WIDGET_H
