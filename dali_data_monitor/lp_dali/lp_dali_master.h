


#ifndef _LP_DALI_MASTER_
#define _LP_DALI_MASTER_

#include <QtCore/QtGlobal>
#include <QVector>
#include "lp_common/bit/bitfield.h"


typedef struct _lp_dali_devices_data_t {
    uint8_t addr;
    uint8_t device_type;//设备类型
    uint8_t status;//设备状态
    uint8_t lightness;//设备亮度
    uint8_t version_number;
    uint8_t min_level;
    uint8_t max_level;
}lp_dali_devices_data_t,*lp_pdali_devices_data_t;


class lp_dali_master {

public:
    lp_dali_master();
    ~lp_dali_master();
    void lp_dali_write_device_list(uint32_t* device_list,QByteArray& arr);
private:
    uint8_t  m_device_num = 0;
    uint32_t m_device_list[BITFIELD_BLOCK_COUNT(64)];
    QVector<lp_dali_devices_data_t> m_dev_info;

};



#endif



















