






/* define to prevent recursive inclusion -------------------------------------*/
#ifndef _LP_DALI_102_COMMAND_
#define _LP_DALI_102_COMMAND_

#ifdef __cplusplus
extern "C" {
#endif

/* includes ------------------------------------------------------------------*/
/******************************************
协议:IEC62386-102
来源:Table 15  Standard_Commonds
NO
if a query is asked where the answer is NO, there will be no response, such that the sender of
the query will conclude “no backward frame" following subclause 8.2.5 of IEC 62386-101:2014
YES
if a query is asked where the answer is YES, the response will be a backward frame
containing the value of MASK
******************************************/
/*Command reference:11.3.1  References:9.4  9.7.3  9.8*/
#define DALI_STANDARD_COMMOND_DAPC  

/*Command reference:11.3.2  References:9.7.2*/
#define DALI_STANDARD_COMMOND_OFF			 0x00//直接关闭灯具

/*Command reference:11.3.3  References:9.7.3*/
#define DALI_STANDARD_COMMOND_UP				 0x01//将灯具调亮200ms

/*Command reference:11.3.4  References:9.7.3*/
#define DALI_STANDARD_COMMOND_DOWN				 0x02//将灯具调灭200ms

/*Command reference:11.3.5  References:9.7.2*/
#define DALI_STANDARD_COMMOND_STEP_UP				 0x03//亮度等级励，若当前亮度丰 或者预订的最大等级，亮度无变匍

/*Command reference:11.3.6  References:9.7.2*/
#define DALI_STANDARD_COMMOND_STEP_DOWN				 0x04//亮度等级?，若当前亮度丰 或者预订的最小等级，亮度无变匍

/*Command reference:11.3.7  References:9.7.2  9.14.2*/
#define DALI_STANDARD_COMMOND_RECALL_MAX_LEVEL				 0x05//亮度等级调整到最大，无需渐变

/*Command reference:11.3.8  References:9.7.2  9.14.2*/
#define DALI_STANDARD_COMMOND_RECALL_MIN_LEVEL			 0x06//亮度等级调整到最小，无需渐变

/*Command reference:11.3.9  References:9.7.2*/
#define DALI_STANDARD_COMMOND_STEP_DOWN_AND_OFF				 0x07//亮度逐步调低,若当前亮度等级为预订的最低等级，则关?

/*Command reference:11.3.10  References:9.7.2*/
#define DALI_STANDARD_COMMOND_ON_AND_STEP_UP			 0x08//亮度等级逐步调高,若当前灯具为关闭状态，则调整到预订的最低亮?

/*Command reference:11.3.11  References:9.8*/
#define DALI_STANDARD_COMMOND_ENABLE_DAPC_SEQUENCE   0x09	//使能DAPC控制

/*Command reference:11.3.12  References:9.7.3*/
#define DALI_STANDARD_COMMOND_GO_TO_LAST_ACTIVE_LEVEL 0x0A///返回上一个亮度

/*Command reference:11.3.13  References:9.7.3  9.19*/
#define DALI_STANDARD_COMMOND_GO_TO_SCENE               0x10///10+scene

/*Command reference:11.4.2  References:9.11.1  10*/
#define DALI_STANDARD_COMMOND_RESET				     0x20//复位灯具的默认参数，如预订的最大亮度等级等

/*Command reference:11.4.3  */
#define DALI_STANDARD_COMMOND_STORE_ACTUAL_LEVEL_IN_DTR0 0x21//将灯具的当前亮度等级存储在DTR不

/*Command reference:11.4.4  References:9.17  10*/
#define DALI_STANDARD_COMMOND_SAVE_PERSISTENT_VARIABLES   0x22 //保存变量命令

/*Command reference:11.4.5  References:9.9.4*/
#define DALI_STANDARD_COMMOND_SET_OPERATING_MODE   0x23 //设置操作模式

/*Command reference:11.4.6  References:9.11.2*/
#define DALI_STANDARD_COMMOND_RESET_MEMORY_BANK    0x24 //重置内存

/*Command reference:11.4.7  References:9.14.2*/
#define DALI_STANDARD_COMMOND_IDENTIFY_DEVICE    0x25	//设备身份识别

/*Command reference:11.4.8  References:9.6*/
#define DALI_STANDARD_COMMOND_SET_MAX_LEVEL			 0x2A//将DTR中的值，设置为预订的最大亮度

/*Command reference:11.4.9  References:9.6*/
#define DALI_STANDARD_COMMOND_SET_MIN_LEVEL			 0x2B//将DTR中的值，设置为预订的最小亮度

/*Command reference:11.4.10  References:9.12*/
#define DALI_STANDARD_COMMOND_SET_SYSTEM_FAILURE_LEVEL	 0x2C//将DTR中的值，设置为系统失效时的亮度

/*Command reference:11.4.11  References:9.13*/
#define DALI_STANDARD_COMMOND_SET_POWER_ON_LEVEL	 0x2D//将DTR中的值，设置为灯具上电时的默认亮度

/*Command reference:11.4.12  References:9.5.2*/
#define DALI_STANDARD_COMMOND_SET_FADE_TIME 0x2E//将DTR中的值，设置为亮度变化时

/*Command reference:11.4.13  References:9.5.3*/
#define DALI_STANDARD_COMMOND_SET_FADE_RATE 0x2F//将DTR中的值，设置为亮度变化速率

/*Command reference:11.4.14  References:9.5.4*/
//Fade time = extendedFadeTimeBase * extendedFadeTimeMultiplier 扩展的延时时间
#define DALI_STANDARD_COMMOND_SET_EXTENDED_FADE_TIME 0x30

/*Command reference:11.4.14  References:9.19*/
#define DALI_STANDARD_COMMOND_SET_SCENE	 0x40//40+scene  将DTR中的值，设置为选择的场景亮度等?

/*Command reference:11.4.16  References:9.19*/
#define DALI_STANDARD_COMMOND_REMOVE_FROM_SCENE				 0x50//50+scene  删除该场?

/*Command reference:11.4.17  References:9.19*/
#define DALI_STANDARD_COMMOND_ADD_TO_GROUP			 0x60//60+group  将该灯具设置加入到某一个组

/*Command reference:11.4.18*/
#define DALI_STANDARD_COMMOND_REMOVE_FROM_GROUP			 0x70//70+group  将该灯具从指定组中删降

/*Command reference:11.4.19  References:9.14.4*/
#define DALI_STANDARD_COMMOND_SET_SHORT_ADDRESS     0x80//DTR中的数据作为该灯具的新地址

/*Command reference:11.4.20  References 9.10.5*/
#define DALI_STANDARD_COMMOND_ENABLE_WRITE_MEMORY     0x81//使能写

/*Command reference:11.5.2  References:9.16*/
#define DALI_STANDARD_COMMOND_QUERY_STATUS			 0x90//查询当前灯具的状态

/*Command reference:11.5.3*/
#define DALI_STANDARD_COMMOND_QUERY_CONTROL_GEAR_PRESENT		 0x91//查询当前灯具是否在工位,有地址回复YES

/*Command reference:11.5.4*/
#define DALI_STANDARD_COMMOND_QUERY_LAMP_FAILURE			 0x92//The answer shall be YES if “controlGearFailure” is TRUE and NO otherwise.

/*Command reference:11.5.6*/
#define DALI_STANDARD_COMMOND_QUERY_LAMP_POWER_ON		 0x93//The answer shall be YES if “lampOn” is TRUE and NO otherwise.

/*Command reference:11.5.7*/
#define DALI_STANDARD_COMMOND_QUERY_LIMIT_ERROR    		0x94//The answer shall be YES if “limitError” is TRUE and NO otherwise

/*Command reference:11.5.8*/
#define DALI_STANDARD_COMMOND_QUERY_RESET_STATE          0x95//The answer shall be YES if “resetState” is TRUE and NO otherwise.

/*Command reference:11.5.9  References:9.14.2*/
#define DALI_STANDARD_COMMOND_QUERY_MISSING_SHORT_ADDRESS	 0x96//The answer shall be YES if “shortAddress” is equal to MASK and NO otherwise

/*Command reference:11.5.10*/
#define DALI_STANDARD_COMMOND_QUERY_VERSION_NUMBER           0x97//查看当前 DALI 的版服

/*Command reference:11.5.11  References:9.10*/
#define DALI_STANDARD_COMMOND_QUERY_CONTENT_DTR0			 0x98//读取 DTR 中的数据

/*Command reference:11.5.12  References:9.18*/
#define DALI_STANDARD_COMMOND_QUERY_DEVICE_TYPE		        0x99//返回设备类型

/*Command reference:11.5.13*/
#define DALI_STANDARD_COMMOND_QUERY_PHYSICAL_MINIMUM		0x9A//返回物理最小等

/*Command reference:11.5.15*/
#define DALI_STANDARD_COMMOND_QUERY_POWER_FAILURE		 0x9B//The answer shall be YES if “powerCycleSeen” is TRUE and NO otherwise.

/*Command reference:11.5.16  References:9.10*/
#define DALI_STANDARD_COMMOND_QUERY_CONTENT_DTR1			 0x9C//读取 DTR1 中的数据

/*Command reference:11.5.17*/
#define DALI_STANDARD_COMMOND_QUERY_CONTENT_DTR2			 0x9D//读取 DTR2 中的数据

/*Command reference:11.5.18  References:9.9.4*/
#define DALI_STANDARD_COMMOND_QUERY_OPEARTING_MODE			 0x9E //读取操作模式

/*Command reference:11.5.19*/
#define DALI_STANDARD_COMMOND_QUERY_LIGHT_SOURCE_TYPE		 0x9F //读取光源类型

/*Command reference:11.5.20*/
#define DALI_STANDARD_COMMOND_QUERY_ACTUAL_LEVEL			 0xA0//返回当前灯光等级

/*Command reference:11.5.21*/
#define DALI_STANDARD_COMMOND_QUERY_MAX_LEVEL		 0xA1//返回灯光限制的最大等

/*Command reference:11.5.22*/
#define DALI_STANDARD_COMMOND_QUERY_MIN_LEVEL		 0xA2//返回灯光限制的最小等

/*Command reference:11.5.23  References:9.13*/
#define DALI_STANDARD_COMMOND_QUERY_POWER_ON_LEVEL		 0xA3//返回通电级别

/*Command reference:11.5.24  References:9.12*/
#define DALI_STANDARD_COMMOND_QUERY_SYSTEM_FAILURE_LEVEL	0xA4//返回系统故障等级

/*Command reference:11.5.25*/
#define DALI_STANDARD_COMMOND_QUERY_FADE_TIME_RATE	        0xA5//返回淡入淡出时间X，淡入淡出率Y

/*Command reference:11.5.27  References:9.9*/
#define DALI_STANDARD_COMMOND_QUERY_MANUFACTURER_SPECIFIC_MODE	 0xA6 //读取是否在运行在特殊模式

/*Command reference:11.5.13  References:9.18*/
#define DALI_STANDARD_COMMOND_QUERY_NEXT_DEVICE_TYPE		 0xA7 //读取设备的下个配置类型

/*Command reference:11.5.26  References:9.5.4*/
#define DALI_STANDARD_COMMOND_QUERY_EXTENDED_FADE_TIME		 0xA8 //读取额外的渐变时间

/*Command reference:11.5.4 */
#define DALI_STANDARD_COMMOND_QUERY_CONTROL_GEAR_FAILURE     0xAA //读取是否故障

/*Command reference:11.5.28  References:9.19*/
#define DALI_STANDARD_COMMOND_QUERY_SCENE_LEVEL		 0xB0//B0+scene  返回场景的灯光级

/*Command reference:11.5.29*/
#define DALI_STANDARD_COMMOND_QUERY_GROUP_0_7		 0xC0//位模式返回属性哪个组  (0-7)

/*Command reference:11.5.30*/
#define DALI_STANDARD_COMMOND_QUERY_GROUP_8_15		 0xC1//位模式返回属性哪个组  (8-15)

/*Command reference:11.5.31*/
#define DALI_STANDARD_COMMOND_QUERY_RANDOM_ADDRESS_H		 0xC2//返回随机地址高位

/*Command reference:11.5.32*/
#define DALI_STANDARD_COMMOND_QUERY_RANDOM_ADDRESS_M		 0xC3//返回随机地址中位

/*Command reference:11.5.33*/
#define DALI_STANDARD_COMMOND_QUERY_RANDOM_ADDRESS_L		 0xC4//返回随机地址低位

/*Command reference:11.5.34  References:9.10*/
#define DALI_STANDARD_COMMOND_READ_MEMORY_LOCATION		 0xC5//读取存储单元

//Application extended commands Device 1 				0xE0 – 0xFE 	224 – 254? ? ? ? ? 9.18 	11.6
//这些命令是特殊设备保留的命令比如DT6 和 DT8
//使用ENABLE DEVICE TYPE (data) 进行使能特殊设备

/*Command reference:11.6.2*/
#define DALI_STANDARD_COMMOND_QUERY_EXTENDED_VERSION_NUMBER    0xFF  //查询版本

/******************************************
协议:IEC62386-102
来源:Table 16  Special Commands
******************************************/
/*Command reference:11.7.1  References:9.14.2*/
#define DALI_SPECIAL_COMMOND_TERMINATE                      0xA1//终止所有的特殊命令

/*Command reference:11.7.3  References:9.10*/
#define DALI_SPECIAL_COMMOND_DTR0							0xA3//将 XX 存储到 DTR 中

/*Command reference:11.7.4  References:9.14.2*/
#define DALI_SPECIAL_COMMOND_INITIALISE						0xA5//初始化地址

/*Command reference:11.7.5  References:9.14.2*/
#define DALI_SPECIAL_COMMOND_RANDOMISE          			0xA7//生成新的随机地址

/*Command reference:11.7.6  References:9.14.2*/
#define DALI_SPECIAL_COMMOND_COMPARE						0xA9//将随机地址与搜索地址进行比较 

/*Command reference:11.7.7  References:9.14.2*/
#define DALI_SPECIAL_COMMOND_WITHRAW						0xAB//退出比较命仍

/*Command reference:11.7.19*/
#define DALI_SPECIAL_COMMOND_PING							0xAD//退出比较命仍

/*Command reference:11.7.8  References:9.14.2*/
#define DALI_SPECIAL_COMMOND_SEARCHADDRH					0xB1//将值HH存储为搜索地址的高位

/*Command reference:11.7.9  References:9.14.2*/
#define DALI_SPECIAL_COMMOND_SEARCHADDRM					0xB3//将值MM存储为搜索地址的中位

/*Command reference:11.7.10  References:9.14.2*/
#define DALI_SPECIAL_COMMOND_SEARCHADDRL					0xB5//将值LL存储为搜索地址的低位

/*Command reference:11.7.11  References:9.14.2*/
#define DALI_SPECIAL_COMMOND_PROGRAM_SHORT_ADDRESS			0xB7//用短地址编程选定的从服

/*Command reference:11.7.12  References:9.14.2*/
#define DALI_SPECIAL_COMMOND_VERIFY_SHORT_ADDRESS           0xB9//检查所选从机是否有短地址

/*Command reference:11.7.13  References:9.14.2*/
#define DALI_SPECIAL_COMMOND_QUERY_SHORT_ADDRESS          	0xBB//所选从机返回其短地址

/*Command reference:11.7.14  References:9.14.2*/
#define DALI_SPECIAL_COMMOND_ENABLE_DEVICE_TYPE             0xC1//使能设备类型

/*Command reference:11.7.15  References:9.10*/
#define DALI_SPECIAL_COMMOND_DTR1							0xC3 //将xx存储到DTR1中

/*Command reference:11.7.16*/
#define DALI_SPECIAL_COMMOND_DTR2							0xC5 //将xx存储到DTR2中

/*Command reference:11.7.17  References:9.10*/
#define DALI_SPECIAL_COMMOND_WRITE_MEMORY_LOCATION			0xC7 //写数据到本地存储器

/*Command reference:11.7.18  References:9.10*/
#define DALI_SPECIAL_COMMOND_WRITE_MEMORY_LOCATION_NO_REPLY	0xC9 


#ifdef __cplusplus
}
#endif

#endif
























