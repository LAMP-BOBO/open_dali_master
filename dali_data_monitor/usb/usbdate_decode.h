#ifndef USBDATE_DECODE_H
#define USBDATE_DECODE_H

#include <QObject>
#include <QThread>
#include <QQueue>
#include <QTime>
#include <QMutexLocker>
#include "lp_pc_protocol/lp_pc_protocol.h"
#include "lp_dali/lp_dali_102_command.h"
#include "lp_dali/lp_dali_207_command.h"
#include "lp_dali/lp_dali_209_command.h"
#include "lp_dali/lp_dali_master.h"

class usbData_decode : public QObject
{
    Q_OBJECT
public:
    explicit usbData_decode(QObject *parent = nullptr);
    void usbData_push(QByteArray arr);
    int usbData_pop(QByteArray& arr);

signals:
    void sendlogData(QByteArray);
    void sendShowText(int,QString);
    void sendAddDev(int);
    void sendInfo(QByteArray);

public slots:
    void Usbdata_decode(void);
    void revlogData(QByteArray buff);

private:
    void dali_decode(uint8_t address, uint8_t commond);
    void dali_handle_spec_commond(uint8_t address, uint8_t commond, QString *text);
    void dali_handle_standard_commond(uint8_t commond, QString *text);
    void revData_handle(lp_pdataPack_t dataPack);

    QQueue<QByteArray> m_rev_queue;

    int mcolor;
    QTime m_current_time;
    int m_last_msec;
    lp_dali_master m_dali_master;
    QMutex  m_lock;
};

#endif // USBDATE_DECODE_H
