#include "mUSBHID.h"


mUSBHID::mUSBHID(QObject *parent) : QObject(parent)
{
    //connect(this,&mUSBHID::sendlogData,this,&mUSBHID::usbSend_data); //线程启动之后就不在响应槽函数了
}


void mUSBHID::usbSend_data(QByteArray buff)
{

    uint8_t buf_temp[65] = {0}; //第一个字节为报告ID 0x00
    uint8_t* send_buf = (uint8_t*)buff.data();
    int send_num = buff.size();
    int send_already = 0;

    qDebug("send_num: %d\n", send_num);
    if(m_pdev_handle == nullptr)
    {
        qDebug()<<"m_pdev_handle is null";
        return;
    }

     while(send_num){
            memset(buf_temp,0x00,sizeof(buf_temp));
            if(send_num>=64){
                  memcpy(buf_temp+1,(uint8_t*)(send_buf+send_already),64);
                  int err = hid_write(m_pdev_handle,buf_temp,sizeof(buf_temp));
                  if(err != -1)
                  {
                      send_num -= 64;
                      send_already +=64;
                  }
                  else {
                     hid_close(m_pdev_handle);
                     qDebug("libusb_interrupt_transfer err : %d\n", err);
                     emit endUsbTask();
                     return ;
                  }
            }
            else
            {
                memcpy(buf_temp+1,(uint8_t*)(send_buf+send_already),send_num);
                //qDebug("buf_temp 0x%x 0x%x 0x%x 0x%x\n",buf_temp[0],buf_temp[1],buf_temp[2],buf_temp[3]);

                int err = hid_write(m_pdev_handle,buf_temp,sizeof(buf_temp));
                if(err != -1)
                {
                    send_num = 0;
                    send_already += send_num;
                    qDebug("usb data send finish\n");
                }
                else {
                   hid_close(m_pdev_handle);
                   qDebug("libusb_interrupt_transfer err : %d\n", err);
                   emit endUsbTask();
                   return ;
                }

            }
        }
}



void mUSBHID::startUsb(int vid, int pid)
{
//    int status = 0;
//    m_pdev_handle = libusb_open_device_with_vid_pid(nullptr,vid,pid);
//    if(m_pdev_handle == nullptr)
//    {
//        qDebug()<<"open device failed:"<<vid<<pid;
//        return;
//    }
//    libusb_set_auto_detach_kernel_driver(m_pdev_handle, 1);
//    status = libusb_claim_interface(m_pdev_handle, 0);
//    if (status != LIBUSB_SUCCESS) {
//        libusb_close(m_pdev_handle);
//        qDebug()<<"libusb_claim_interface failed:"<<libusb_error_name(status)<<endl;
//        return;
//    }
//    qDebug()<<"open_dev dev success";
//    m_usbdecode_pthread->start();
//    emit startUsbdecode();

//    uint8_t buf_temp[64];
//    int transferred = 0;
//    lp_usbOrigin_t dataUsb;
//    int send_num = 0;

//    while(1){
//        memset(buf_temp,0x00,sizeof(buf_temp)); //变解析边接受会丢包
//        int err = libusb_interrupt_transfer(m_pdev_handle,0x81,buf_temp,64,&transferred,0xFFFFFFFF);
//        if (!err) {
//                //hid 设备必须固定字节数传输 64字节,不足64字节要补0
//                 if(transferred < 64){
//                    qDebug("transferred rev data error: %d",transferred);
//                    continue;
//                 }
//                 memcpy(dataUsb.buf,buf_temp,sizeof(buf_temp));
//                 m_usb_decode->usbData_push(&dataUsb);
//                 send_num++;
//                 qDebug("send num: %d",send_num);

//        } else if (err == LIBUSB_ERROR_TIMEOUT){
//            qDebug("libusb_interrupt_transfer timout\n");
//        } else {
//            libusb_close(m_pdev_handle);
//            qDebug("libusb_interrupt_transfer err : %d\n", err);
//            emit endUsbTask();
//            return ;
//        }
//     //QThread::msleep(10);
//     }

    m_pdev_handle = hid_open(vid,pid,NULL);
    if(m_pdev_handle == NULL)
    {
        qDebug()<<"open device failed:"<<vid<<pid;
        return;
    }
    //hid_set_nonblocking(m_pdev_handle,1); //设置非阻塞

    qDebug()<<"open_dev dev success";

    uint8_t buf_temp[64];
    int send_num = 0;


    while(1){
        memset(buf_temp,0x00,sizeof(buf_temp)); //变解析边接受会丢包
        int err = hid_read(m_pdev_handle,buf_temp,sizeof(buf_temp)); //默认阻塞
        if (err != -1) {
                //hid 设备必须固定字节数传输 64字节,不足64字节要补0
                 if(err < 64){
                    qDebug("transferred rev data error: %d",err);
                    continue;
                 }

                 //m_usb_decode->usbData_push(buf_temp,sizeof(buf_temp));
                 QByteArray buff;
                 for (int i = 0; i < err; i++)
                 {
                     buff.append(buf_temp[i]);
                 }
                 emit sendData_push(buff);
                 send_num++;
                 qDebug("Rev num: %d",send_num);

        }
        else {
            hid_close(m_pdev_handle);
            qDebug("libusb_interrupt_transfer err : %d\n", err);
            emit endUsbTask();
            return;
        }
     QThread::msleep(2);
     }
}
