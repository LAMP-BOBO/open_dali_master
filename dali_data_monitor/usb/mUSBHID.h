#ifndef MUSBHID_H
#define MUSBHID_H

#include <QObject>

//#include "libusb.h"
#include "usb/hidapi/hidapi.h"

#include <QtWidgets>
#include <QtCore/QtGlobal>
#include "lp_pc_protocol/lp_pc_protocol.h"
#include "lp_dali/lp_dali_102_command.h"
#include "lp_dali/lp_dali_207_command.h"
#include "lp_dali/lp_dali_209_command.h"
#include "usbdate_decode.h"

class mUSBHID : public QObject
{
    Q_OBJECT
public:
    explicit mUSBHID(QObject *parent = nullptr);
    void usbSend_data(QByteArray buff);

signals:
    void endUsbTask();
    void sendData_push(QByteArray arr);

public slots:
    void startUsb(int vid,int pid);

private:


    //libusb_device_handle * m_pdev_handle;
    hid_device * m_pdev_handle;
    QString tempText;

};

#endif // MUSBHID_H
