#include "usbdate_decode.h"
#include <qdebug.h>
#include <QMessageBox>

usbData_decode::usbData_decode(QObject *parent) : QObject(parent)
{
    m_current_time =QTime::currentTime();
    m_last_msec = m_current_time.msec();

     connect(this,&usbData_decode::sendlogData,this,&usbData_decode::revlogData);
}

/*
    address : 8 high bit
    commond : 8 low bit
    address type     :  describe
    short address       0AAA AAAS   AAAAAA=(0-63)
    Group address       100A AAAS   AAAA=(0-15)
    Broadcast address   1111 111S
    Special command     101C CCC1   CCCC=(command code)
    IEC 62386-209  command:272  1100 0001 0000 1000
*/
void usbData_decode::dali_decode(uint8_t address, uint8_t commond)
{
    QString text;
    mcolor = 0;
    if((address&0x80) == 0x80)//组地址  广播地址  特殊命令
    {
      if((address>>5) == 0x04)//组地址
      {
          uint8_t groupAddress = (address>>1)&0x0F;
          text.append("组地址:"+QString::number(groupAddress)+",数据为"+QString::number(commond,16));
          if((address&0x01) == 0x00)//调光
          {
              text.append("  调光命令,亮度="+QString::number(commond)+", 协议IEC62386-102 11.3.1");
          }
          else//DALI命令
          {
              dali_handle_standard_commond(commond, &text);
          }
      }
      else if((address>>1) == 0x7F)//广播地址
      {
          text.append("广播地址,数据为"+QString::number(commond,16));
          if((address&0x01) == 0x00)//调光
          {
              text.append("  调光命令,亮度="+QString::number(commond)+", 协议IEC62386-102 11.3.1");
          }
          else//DALI命令
          {
              dali_handle_standard_commond(commond, &text);
          }
      }
      else if(((address&0xE0) == 0xA0)||((address&0xE0) == 0xC0))//特殊命令
      {
          text.append("特殊命令,数据为"+QString::number(commond,16));
          dali_handle_spec_commond(address, commond,&text);
          //dali_handle_spec_commond(address, commond);
      }
    }
    else
    {
        uint8_t shortAddress = address>>1;
        text.append("单播地址:"+QString::number(shortAddress)+",数据为"+QString::number(commond,16));
        if((address&0x01) == 0x00)//调光
        {
            text.append("  调光命令,亮度="+QString::number(commond)+", 协议IEC62386-102 11.3.1");
        }
        else//DALI命令
        {
            //dali_handle_standard_commond(address, commond);
            dali_handle_standard_commond(commond, &text);
        }
    }
    emit sendShowText(mcolor,text);
}


void usbData_decode::dali_handle_spec_commond(uint8_t address,uint8_t commond,QString *text)
{
    Q_UNUSED(commond);

    switch(address)
    {
        case DALI_SPECIAL_COMMOND_TERMINATE:text->append("   特殊命令,终止所有指令,  协议IEC62386-102 参考:11.7.2,详细:9.14.2");break;
        case DALI_SPECIAL_COMMOND_DTR0:text->append("   特殊命令,将数据存储到DTR0中,  协议IEC62386-102 参考:11.7.3,详细:9.10");break;
        case DALI_SPECIAL_COMMOND_INITIALISE:text->append("   特殊命令,初始化设备,  协议IEC62386-102 参考:11.7.4,详细:9.14.2");break;
        case DALI_SPECIAL_COMMOND_RANDOMISE:text->append("   特殊命令,生成随机地址(3字节),  协议IEC62386-102 参考:11.7.5,详细:9.14.2");break;
        case DALI_SPECIAL_COMMOND_COMPARE:text->append("   特殊命令,比较随机地址和搜索地址,  协议IEC62386-102 参考:11.7.6,详细:9.14.2");break;
        case DALI_SPECIAL_COMMOND_WITHRAW:text->append("   特殊命令,退出比较命令,  协议IEC62386-102 参考:11.7.7,详细:9.14.2");break;
        case DALI_SPECIAL_COMMOND_SEARCHADDRH:text->append("   特殊命令,设置搜索地址高字节,  协议IEC62386-102 参考:11.7.7,详细:9.14.2");break;
        case DALI_SPECIAL_COMMOND_SEARCHADDRM:text->append("   特殊命令,设置搜索地址中字节,  协议IEC62386-102 参考:11.7.8,详细:9.14.2");break;
        case DALI_SPECIAL_COMMOND_SEARCHADDRL:text->append("   特殊命令,设置搜索地址低字节,  协议IEC62386-102 参考:11.7.9,详细:9.14.2");break;
        case DALI_SPECIAL_COMMOND_PROGRAM_SHORT_ADDRESS:text->append("   特殊命令,写入地址码,  协议IEC62386-102 参考:11.7.11,详细:9.14.2");break;
        case DALI_SPECIAL_COMMOND_VERIFY_SHORT_ADDRESS:text->append("   特殊命令,查询设备是否有地址码,  协议IEC62386-102 参考:11.7.12,详细:9.14.2");break;
        case DALI_SPECIAL_COMMOND_QUERY_SHORT_ADDRESS:text->append("   特殊命令,返回设备地址码,  协议IEC62386-102 参考:11.7.13,详细:9.14.2");break;
        //case DALI_SPECIAL_COMMOND_INTO_PHY_MODE:dali_spec_commond_into_phy_mode();break;//进入物理选择模式
        case DALI_SPECIAL_COMMOND_DTR1:text->append("   特殊命令,保存数据到DTR1,  协议IEC62386-102 参考:11.7.15,详细:9.10");break;
        case DALI_SPECIAL_COMMOND_DTR2:text->append("   特殊命令,保存数据到DTR2,  协议IEC62386-102 参考:11.7.16,详细:9.10");break;
        case DALI_SPECIAL_COMMOND_ENABLE_DEVICE_TYPE:text->append("   特殊命令,启动设备类型,  协议IEC62386-102 参考:11.7.14,详细:9.10");break;
        default:mcolor = 2;text->append("   特殊命令,未解析的数据");break;
    }
}


void usbData_decode::dali_handle_standard_commond(uint8_t commond, QString *text)
{
    //text->append("   标准命令"+QString::number(commond,16)+",  协议");
    switch(commond)
    {
        case DALI_STANDARD_COMMOND_OFF:text->append("   标准命令,直接关闭灯具,  协议IEC62386-102 参考:11.3.2,详细:9.7.2");break;
        case DALI_STANDARD_COMMOND_UP:text->append("   标准命令,调亮200毫秒,  协议IEC62386-102 参考:11.3.3,详细:9.7.3");break;
        case DALI_STANDARD_COMMOND_DOWN:text->append("   标准命令,调灭200毫秒,  协议IEC62386-102 参考:11.3.4,详细:9.7.4");break;
        case DALI_STANDARD_COMMOND_STEP_UP:text->append("   标准命令,灯具亮度+1,  协议IEC62386-102 参考:11.3.5,详细:9.7.2");break;
        case DALI_STANDARD_COMMOND_STEP_DOWN:text->append("   标准命令,灯具亮度-1,  协议IEC62386-102 参考:11.3.6,详细:9.7.2");break;
        case DALI_STANDARD_COMMOND_RECALL_MAX_LEVEL:text->append("   标准命令,将亮度调到预设的最大值,  协议IEC62386-102 参考:11.3.7,详细:9.7.2和9.14.2");break;
        case DALI_STANDARD_COMMOND_RECALL_MIN_LEVEL:text->append("   标准命令,将亮度调到预设的最小值,  协议IEC62386-102 参考:11.3.8,详细:9.7.2和9.14.2");break;
        case DALI_STANDARD_COMMOND_STEP_DOWN_AND_OFF:text->append("   标准命令,亮度逐渐减小,  协议IEC62386-102 参考:11.3.9,详细:9.7.2");break;
        case DALI_STANDARD_COMMOND_ON_AND_STEP_UP:text->append("   标准命令,亮度逐渐增加,  协议IEC62386-102 参考:11.3.10,详细:9.7.2");break;
        case DALI_STANDARD_COMMOND_RESET:text->append("   标准命令,灯具参数复位,  协议IEC62386-102 参考:11.4.2,详细:9.11.1和10");break;
        case DALI_STANDARD_COMMOND_STORE_ACTUAL_LEVEL_IN_DTR0:text->append("   标准命令,将当前亮度存在DTR中,  协议IEC62386-102 参考:11.4.3");break;
        case DALI_STANDARD_COMMOND_SET_MAX_LEVEL:text->append("   标准命令,设置最大亮度值,  协议IEC62386-102 参考:11.4.8,详细:9.6");break;
        case DALI_STANDARD_COMMOND_SET_MIN_LEVEL:text->append("   标准命令,设置最小亮度值,  协议IEC62386-102 参考:11.4.9,详细:9.6");break;
        case DALI_STANDARD_COMMOND_SET_SYSTEM_FAILURE_LEVEL:text->append("   标准命令,设置系统失效值,  协议IEC62386-102 参考:11.4.10,详细:9.12");break;
        case DALI_STANDARD_COMMOND_SET_POWER_ON_LEVEL:text->append("   标准命令,设置起亮值,  协议IEC62386-102 参考:11.4.11,详细:9.13");break;
        case DALI_STANDARD_COMMOND_SET_FADE_TIME:text->append("   标准命令,设置调光时间,  协议IEC62386-102 参考:11.4.13,详细:9.5和9.7.3");break;
        case DALI_STANDARD_COMMOND_SET_FADE_RATE:text->append("   标准命令,设置调光速率,  协议IEC62386-102 参考:11.4.14,详细:9.5.4");break;
        case DALI_STANDARD_COMMOND_SET_SHORT_ADDRESS:text->append("   标准命令,设置单播地址,  协议IEC62386-102 参考:11.4.19");break;
        case DALI_STANDARD_COMMOND_QUERY_STATUS:text->append("   标准命令,查询灯具状态,  协议IEC62386-102 参考:11.5.2,详细:9.16");break;
        case DALI_STANDARD_COMMOND_QUERY_CONTROL_GEAR_PRESENT:text->append("   标准命令,查询灯具是否正常,  协议IEC62386-102 参考:11.5.3");break;
        case DALI_STANDARD_COMMOND_QUERY_LAMP_FAILURE:text->append("   标准命令,查询灯具是否失效,  协议IEC62386-102 参考:11.5.4");break;
        case DALI_STANDARD_COMMOND_QUERY_LAMP_POWER_ON:text->append("   标准命令,查询灯具是否被操作,  协议IEC62386-102 参考:11.5.6");break;
        case DALI_STANDARD_COMMOND_QUERY_LIMIT_ERROR:text->append("   标准命令,查询灯具是否接收到超出限制的亮度,  协议IEC62386-102 参考:11.5.7");break;
        case DALI_STANDARD_COMMOND_QUERY_RESET_STATE:text->append("   标准命令,查询灯具是否在复位,  协议IEC62386-102 参考:11.5.8");break;
        case DALI_STANDARD_COMMOND_QUERY_MISSING_SHORT_ADDRESS:text->append("   标准命令,查询灯具是否缺少地址码,  协议IEC62386-102 参考:11.5.9,详细:9.14.2");break;
        case DALI_STANDARD_COMMOND_QUERY_VERSION_NUMBER:text->append("   标准命令,查询DALI版本,  协议IEC62386-102 参考:11.5.10,表9");break;
        case DALI_STANDARD_COMMOND_QUERY_CONTENT_DTR0:text->append("   标准命令,查询DTR0的值,  协议IEC62386-102 参考:11.5.11,详细:9.10");break;
        case DALI_STANDARD_COMMOND_QUERY_CONTENT_DTR1:text->append("   标准命令,查询DTR1的值,  协议IEC62386-102 参考:11.5.16,详细:9.10");break;
        case DALI_STANDARD_COMMOND_QUERY_CONTENT_DTR2:text->append("   标准命令,查询DTR2的值,  协议IEC62386-102 参考:11.5.17");break;
        case DALI_STANDARD_COMMOND_QUERY_DEVICE_TYPE:text->append("   标准命令,查询设备类型,  协议IEC62386-102 参考:11.5.12,详细:9.18");break;
        case DALI_STANDARD_COMMOND_QUERY_PHYSICAL_MINIMUM:text->append("   标准命令,查询硬件最小值,  协议IEC62386-102 参考:11.5.13");break;
        case DALI_STANDARD_COMMOND_QUERY_POWER_FAILURE:text->append("   标准命令,查询是否处于断电状态,  协议IEC62386-102 参考:11.5.15");break;
        case DALI_STANDARD_COMMOND_QUERY_ACTUAL_LEVEL:text->append("   标准命令,查询当前亮度,  协议IEC62386-102 参考:11.5.20");break;
        case DALI_STANDARD_COMMOND_QUERY_MAX_LEVEL:text->append("   标准命令,查询亮度最大值,  协议IEC62386-102 参考:11.5.21");break;
        case DALI_STANDARD_COMMOND_QUERY_MIN_LEVEL:text->append("   标准命令,查询亮度最小值,  协议IEC62386-102 参考:11.5.22");break;
        case DALI_STANDARD_COMMOND_QUERY_POWER_ON_LEVEL:text->append("   标准命令,查询起亮值,  协议IEC62386-102 参考:11.5.23,详细:9.13");break;
        case DALI_STANDARD_COMMOND_QUERY_SYSTEM_FAILURE_LEVEL:text->append("   标准命令,查询系统失效值,  协议IEC62386-102 参考:11.5.24,详细:9.12");break;
        case DALI_STANDARD_COMMOND_QUERY_FADE_TIME_RATE:text->append("   标准命令,查询调光时间和速率,  协议IEC62386-102 参考:11.5.25");break;
        case DALI_STANDARD_COMMOND_QUERY_GROUP_0_7:text->append("   标准命令,查询是否在组0-7,  协议IEC62386-102 参考:11.5.29");break;
        case DALI_STANDARD_COMMOND_QUERY_GROUP_8_15:text->append("   标准命令,查询是否在组8-15,  协议IEC62386-102 参考:11.5.30");break;
        case DALI_STANDARD_COMMOND_QUERY_RANDOM_ADDRESS_H:text->append("   标准命令,查询随机地址高字节,  协议IEC62386-102 参考:11.5.31");break;
        case DALI_STANDARD_COMMOND_QUERY_RANDOM_ADDRESS_M:text->append("   标准命令,查询随机地址中字节,  协议IEC62386-102 参考:11.5.32");break;
        case DALI_STANDARD_COMMOND_QUERY_RANDOM_ADDRESS_L:text->append("   标准命令,查询随机地址低字节,  协议IEC62386-102 参考:11.5.33");break;
        case DALI_STANDARD_COMMOND_READ_MEMORY_LOCATION:text->append("   标准命令,读取存储单元,  协议IEC62386-102 参考:11.5.34,详细:9.10");break;
        case DALI_EXTENDED_COMMOND_QUERY_DIMMER_CURVE:text->append("   标准命令,查询调光曲线,  协议IEC62386-207(DT6) 参考:9.3.1");break;
        case DALI_EXTENDED_COMMOND_SET_DIMMER_CURVE:text->append("   标准命令,设置调光曲线,  协议IEC62386-207(DT6) 参考:9.3.2");break;
        case DALI_EXTENDED_COMMOND_QUERY_COLOUR_VALUE:text->append("   标准命令,QUERY_COLOUR_VALUE,功能跟DTR里的值有关,  协议IEC62386-209(DT8) Command:250,Table:11");break;
        case DALI_EXTENDED_COMMOND_SET_COLOR_TEMP:text->append("   标准命令,设置临时色温(0忽略),  协议IEC62386-209(DT8) Command:231");break;
        case DALI_EXTENDED_COMMOND_ACTIVATE:text->append("   标准命令,激活色温,  协议IEC62386-209(DT8) Command:226 详细:9.12.5");break;
        case DALI_EXTENDED_COMMOND_SET_COLOR_TEMP_LIMIT:text->append("   标准命令,功能跟DTR2的值有关(0x00-0x03),  协议IEC62386-209(DT8) Command:242 详细:9.13");break;
        case DALI_STANDARD_COMMOND_QUERY_EXTENDED_VERSION_NUMBER:text->append("   标准命令,查询扩展版本号,跟Part2xx有关,  协议IEC62386-102 参考:11.6.2,详细:9.18");break;
        default:
            if(commond >= DALI_STANDARD_COMMOND_GO_TO_SCENE && commond <= DALI_STANDARD_COMMOND_GO_TO_SCENE+0x0F)
            {
                text->append("   标准命令,切换到场景"+QString::number(commond-DALI_STANDARD_COMMOND_GO_TO_SCENE)+",  协议IEC62386-102 参考:11.3.13,详细:9.7.3和9.19");
            }
            else if(commond >= DALI_STANDARD_COMMOND_SET_SCENE && commond <= DALI_STANDARD_COMMOND_SET_SCENE+0x0F)
            {
                text->append("   标准命令,保存到场景"+QString::number(commond-DALI_STANDARD_COMMOND_SET_SCENE)+",  协议IEC62386-102 参考:11.4.14,详细:9.19");
            }
            else if(commond >= DALI_STANDARD_COMMOND_REMOVE_FROM_SCENE && commond <= DALI_STANDARD_COMMOND_REMOVE_FROM_SCENE+0x0F)
            {
                text->append("   标准命令,删除场景"+QString::number(commond-DALI_STANDARD_COMMOND_REMOVE_FROM_SCENE)+",  协议IEC62386-102 参考:11.4.16,详细:9.19");
            }
            else if(commond >= DALI_STANDARD_COMMOND_QUERY_SCENE_LEVEL && commond <= DALI_STANDARD_COMMOND_QUERY_SCENE_LEVEL+0x0F)
            {
                text->append("   标准命令,查询场景"+QString::number(commond-DALI_STANDARD_COMMOND_QUERY_SCENE_LEVEL)+"亮度,  协议IEC62386-102 参考:11.5.28,详细:9.19");
            }
            else if(commond >= DALI_STANDARD_COMMOND_ADD_TO_GROUP && commond <= DALI_STANDARD_COMMOND_ADD_TO_GROUP+0x0F)
            {
                text->append("   标准命令,添加到组"+QString::number(commond-DALI_STANDARD_COMMOND_ADD_TO_GROUP)+",  协议IEC62386-102 参考:11.4.17,详细:9.19");
            }
            else if(commond >= DALI_STANDARD_COMMOND_REMOVE_FROM_GROUP && commond <= DALI_STANDARD_COMMOND_REMOVE_FROM_GROUP+0x0F)
            {
                text->append("   标准命令,从组"+QString::number(commond-DALI_STANDARD_COMMOND_REMOVE_FROM_GROUP)+"移除,  协议IEC62386-102 参考:11.4.18,详细:9.19");
            }
            else
            {
                mcolor = 2;
                text->append("   标准命令,未解析的数据");
            };break;
    }

}

void usbData_decode::revlogData(QByteArray buff) //脉冲先低后高
{
    QString data(buff);
    data.remove(data.size()-2,2); //去掉起始电平
    QStringList list = data.split(",");

    uint8_t pulseFlag[64];
    uint8_t daliBitData[24];
    uint16_t pulseIndex = 0;
    uint8_t daliBitDataIndex = 0;


    m_current_time =QTime::currentTime();
    int current_time_msec = m_current_time.msec();
    int rev_error = current_time_msec - m_last_msec;
    m_last_msec = current_time_msec;
    QString spaceTime = QString::number(rev_error);

    for(int i=0;i<list.size();i++)
    {
        int num = list[i].toInt();
        if(i%2) //低
        {
            if(num > 300 && num < 550)//416 += 10%
            {
                pulseFlag[pulseIndex++] = 1;
            }
            else if(num > 700 && num < 950)
            {
                pulseFlag[pulseIndex++] = 1;
                pulseFlag[pulseIndex++] = 1;
            }
            else
            {
                emit sendShowText(1,"解码错误1");
                emit sendShowText(1,buff);
                return ;
            }
        }
        else
        {
            if(num > 300 && num < 550)//416 += 10%
            {
                pulseFlag[pulseIndex++] = 0;
            }
            else if(num > 700 && num < 950)
            {
                pulseFlag[pulseIndex++] = 0;
                pulseFlag[pulseIndex++] = 0;
            }
            else
            {
                emit sendShowText(1,"解码错误2");
                emit sendShowText(1,buff);
                return ;
            }
        }
    }

    if(pulseIndex == 19)
    {
        pulseIndex--;
        emit sendShowText(1,"解码错误19,");
        //pulseFlag[pulseIndex++] = 1;
    }
    if(pulseIndex == 17)
    {
        pulseFlag[pulseIndex++] = 1;
    }
    if(pulseIndex == 33)
    {
        pulseFlag[pulseIndex++] = 1;
    }
    if(pulseIndex == 49)
    {
        pulseFlag[pulseIndex++] = 1;
    }


    for(int i=2;i<pulseIndex;i+=2)
    {
        if(pulseFlag[i]==0x00 && pulseFlag[i+1]==0x01)
        {
            daliBitData[daliBitDataIndex++] = 0x01;
        }
        else if(pulseFlag[i]==0x01 && pulseFlag[i+1]==0x00)
        {
            daliBitData[daliBitDataIndex++] = 0x00;
        }
        else
        {
            emit sendShowText(1,"解码错误3,pulseIndex="+QString::number(pulseIndex)+":"+QString::number(pulseFlag[i])+","+QString::number(pulseFlag[i+1])+",i="+QString::number(i));
            emit sendShowText(1,buff);
            return ;
        }
    }

    QByteArray daliData;
    if(daliBitDataIndex == 8)
    {
        uint8_t temp = (daliBitData[0]<<7)|(daliBitData[1]<<6)|(daliBitData[2]<<5)|(daliBitData[3]<<4)|(daliBitData[4]<<3)|(daliBitData[5]<<2)|(daliBitData[6]<<1)|(daliBitData[7]);
        daliData.append(temp);
    }
    else if(daliBitDataIndex == 16)
    {
        uint8_t temp = (daliBitData[0]<<7)|(daliBitData[1]<<6)|(daliBitData[2]<<5)|(daliBitData[3]<<4)|(daliBitData[4]<<3)|(daliBitData[5]<<2)|(daliBitData[6]<<1)|(daliBitData[7]);
        uint8_t temp1 = (daliBitData[8]<<7)|(daliBitData[9]<<6)|(daliBitData[10]<<5)|(daliBitData[11]<<4)|(daliBitData[12]<<3)|(daliBitData[13]<<2)|(daliBitData[14]<<1)|(daliBitData[15]);
        daliData.append(temp);
        daliData.append(temp1);
    }
    else if(daliBitDataIndex == 24)
    {
        uint8_t temp = (daliBitData[0]<<7)|(daliBitData[1]<<6)|(daliBitData[2]<<5)|(daliBitData[3]<<4)|(daliBitData[4]<<3)|(daliBitData[5]<<2)|(daliBitData[6]<<1)|(daliBitData[7]);
        uint8_t temp1 = (daliBitData[8]<<7)|(daliBitData[9]<<6)|(daliBitData[10]<<5)|(daliBitData[11]<<4)|(daliBitData[12]<<3)|(daliBitData[13]<<2)|(daliBitData[14]<<1)|(daliBitData[15]);
        uint8_t temp2 = (daliBitData[16]<<7)|(daliBitData[17]<<6)|(daliBitData[18]<<5)|(daliBitData[19]<<4)|(daliBitData[20]<<3)|(daliBitData[21]<<2)|(daliBitData[22]<<1)|(daliBitData[23]);
        daliData.append(temp);
        daliData.append(temp1);
        daliData.append(temp2);
    }

    if(!daliData.isEmpty())
    {
        sendShowText(0,daliData.toHex());
        if(daliData.size() == 2)
        {
            dali_decode((uint8_t)daliData[0], (uint8_t)daliData[1]);
        }
        else if(daliData.size() == 1)
        {
            emit sendShowText(3,"返回数据:"+daliData.toHex()+",时间:"+spaceTime+"ms");
        }
        else if(daliData.size() == 3)
        {
            emit sendShowText(4,"DALI主机数据:"+daliData.toHex());
        }
    }
    else
    {
        emit sendShowText(1,"解码错误4,daliBitDataIndex=" +QString::number(daliBitDataIndex));
        emit sendShowText(1,buff);
        return ;
    }
    emit sendShowText(0,buff);
}


void usbData_decode::usbData_push(QByteArray arr)
{

    {//锁的作用范围
        QMutexLocker locker(&m_lock);
        m_rev_queue.enqueue(arr);
    }
}

int usbData_decode::usbData_pop(QByteArray& arr)
{

    {//锁的作用范围
        QMutexLocker locker(&m_lock);
        if(m_rev_queue.size() > 0){

            arr = m_rev_queue.dequeue();
            return arr.size();
        }
        else
        {
            return -1;
        }

    }
}


void usbData_decode::revData_handle(lp_pdataPack_t pdataPack)
{
   QString buff;
   uint16_t temp_u16 = 0;
   uint32_t temp_u32[2] = {0};


    switch(pdataPack->dataHeader.opcode){
    case LP_PC_COM_DALI_UP_LOG_E:
       //emit sendShowText(1,"dataPack,len="+QString::number(dataPack.dataHeader.dataLen)+" dataPack opcode:0x"+QString::number(dataPack.dataHeader.opcode,16));
        if(pdataPack->buf != NULL){
            //qDebug()<<"dataHeader.dataLen"<<dataPack.dataHeader.dataLen-sizeof(lp_dataHeader_t)<<endl;
            for (int i = 0; i < (int)(pdataPack->dataHeader.dataLen-sizeof(lp_dataHeader_t)); i+=2)
            {
                temp_u16 = (pdataPack->buf[i+1]<<8)|pdataPack->buf[i];
                buff.append(QString::number(temp_u16));
                buff.append(", ");
            }
            //emit sendShowText(1,"dataPack data:" + test);
           emit sendlogData(buff.toLatin1());
           //delete[] pdataPack->buf;
            free(pdataPack->buf);
        }
        else{

            qDebug()<<"dataHeader.dataLen"<<pdataPack->dataHeader.dataLen-sizeof(lp_dataHeader_t)<<endl;
            uint8_t* rev_buf = (uint8_t*)pdataPack->arr_buf.data();
            for (int i = 0; i < (int)(pdataPack->dataHeader.dataLen-sizeof(lp_dataHeader_t)); i+=2)
            {
                temp_u16 = (rev_buf[i+1]<<8)|rev_buf[i];
                buff.append(QString::number(temp_u16));
                buff.append(", ");
            }
            emit sendlogData(buff.toLatin1());

        }

        break;
    case LP_PC_COM_DALI_UP_ORDER_E:
         if(pdataPack->buf != NULL){
             for (int i = 0; i < (int)(pdataPack->dataHeader.dataLen-sizeof(lp_dataHeader_t)); i++)
             {
                 buff.append("0X");
                 buff.append(QString::number(pdataPack->buf[i],16).toUpper());
                 buff.append(", ");
             }
             emit sendShowText(5,"Master send data:" + buff);
             dali_decode(pdataPack->buf[0],pdataPack->buf[1]);
             //delete[] pdataPack->buf;
             free(pdataPack->buf);
         }
         else{
             qDebug()<<"dataHeader.dataLen"<<pdataPack->dataHeader.dataLen-sizeof(lp_dataHeader_t)<<endl;
             uint8_t* rev_buf = (uint8_t*)pdataPack->arr_buf.data();
             for (int i = 0; i < (int)(pdataPack->dataHeader.dataLen-sizeof(lp_dataHeader_t)); i++)
             {
                 buff.append("0X");
                  buff.append(QString::number(rev_buf[i],16).toUpper());
                 buff.append(", ");
             }
             emit sendShowText(5,"Master send data:" + buff);
             dali_decode(rev_buf[0],rev_buf[1]);

         }
        break;
    case LP_PC_COM_DALI_UP_REPORT_DEV_E:
        if(pdataPack->buf != NULL){
            QByteArray arr;
            temp_u32[0] = *(uint32_t*)pdataPack->buf;
            temp_u32[1] = *(uint32_t*)(pdataPack->buf+4);
            //qDebug("len:%d 0x%x,0x%x",(int)(dataPack.dataHeader.dataLen-sizeof(lp_dataHeader_t)),temp_u32_1,temp_u32_2);
            m_dali_master.lp_dali_write_device_list(temp_u32,arr);
            for(int i=0;i<arr.size();i++)
            {
                emit sendAddDev(arr.at(i));
            }

            QMessageBox errBox(QMessageBox::Information, tr("消息"),"查找到设备："+QString::number(arr.size()),QMessageBox::Ok);
            //delete[] dataPack.buf;
            free(pdataPack->buf);
        }
        else
        {
            QByteArray arr;
            uint8_t* rev_buf = (uint8_t*)pdataPack->arr_buf.data();
            temp_u32[0] = *(uint32_t*)rev_buf;
            temp_u32[1] = *(uint32_t*)(rev_buf+4);
            //qDebug("len:%d 0x%x,0x%x",(int)(dataPack.dataHeader.dataLen-sizeof(lp_dataHeader_t)),temp_u32_1,temp_u32_2);
            m_dali_master.lp_dali_write_device_list(temp_u32,arr);
            for(int i=0;i<arr.size();i++)
            {
                emit sendAddDev(arr.at(i));
            }

        }
        break;

    case LP_PC_COM_DALI_UP_REPORT_INFO_E:
//            uint8_t* rev_buf = (uint8_t*)pdataPack->arr_buf.data();
//            lp_pdali_devices_data_t pdev = (lp_pdali_devices_data_t)rev_buf;
//            qDebug("addr:0x%x,status:0x%x",pdev->addr,pdev->status);
            emit sendInfo(pdataPack->arr_buf);

        break;

    }

}


void usbData_decode::Usbdata_decode(void)
{
    uint8_t buf_temp[64];
    uint8_t already_rev = 0;
    uint16_t rev_len = 0;
    QByteArray arr_temp;
    int rev_num = 0;
    lp_dataPack_t datapack;

    while(1){

        memset(buf_temp,0x00,sizeof(buf_temp));
        if(usbData_pop(arr_temp) > 0){
            uint8_t* rev_buf = (uint8_t*)arr_temp.data();
            memcpy(buf_temp,rev_buf,arr_temp.size());
            rev_num++;
            qDebug("data handle finish rev_num:%d %d\n",rev_num,arr_temp.size());

            if(already_rev == 0){

                rev_len = 0;
                lp_pdataHeader_t pdataHeader = (lp_pdataHeader_t)buf_temp;

                rev_len =  pdataHeader->dataLen;
                datapack.dataHeader.opcode = pdataHeader->opcode;
                datapack.dataHeader.dataLen = pdataHeader->dataLen;
                datapack.buf = NULL;
                datapack.arr_buf.clear();

                qDebug("rev_len :%d \n",rev_len);
                if(rev_len == 0){

                    qDebug("rev_len == 0 error ");
                    continue;
                }
                else if(rev_len <= 64)
                {
                    //一包就完成的部分
                    //datapack.buf = new uint8_t[pdataHeader->dataLen - sizeof(lp_dataHeader_t)];
//                     datapack.buf = (uint8_t*)malloc(pdataHeader->dataLen - sizeof(lp_dataHeader_t));
//                    if(datapack.buf == NULL){
//                        qDebug("new fail : %d",rev_len);
//                        continue;
//                    }
                    //memcpy(datapack.buf,(uint8_t*)(buf_temp+sizeof(lp_dataHeader_t)),64-sizeof(lp_dataHeader_t));
                    for(int i=0;i<(int)(rev_len - sizeof(lp_dataHeader_t));i++)
                    {
                        datapack.arr_buf.append((buf_temp+sizeof(lp_dataHeader_t))[i]);
                    }
                    already_rev = 0;
                    rev_len = 0;
                    revData_handle(&datapack);
                }
                else{

                    //datapack.buf = new uint8_t[pdataHeader->dataLen - sizeof(lp_dataHeader_t)];
//                    datapack.buf = (uint8_t*)malloc(pdataHeader->dataLen - sizeof(lp_dataHeader_t));
//                    if(datapack.buf == NULL){
//                        qDebug("new fail : %d",rev_len);
//                        continue;
//                    }
                    //memcpy(datapack.buf,(uint8_t*)(buf_temp+sizeof(lp_dataHeader_t)),64-sizeof(lp_dataHeader_t));
                    //数据超过一包，先收第一包
                    rev_len -=  64;
                    for(int i=0;i<(int)(64-sizeof(lp_dataHeader_t));i++)
                    {
                        datapack.arr_buf.append((buf_temp+sizeof(lp_dataHeader_t))[i]);
                    }
                    already_rev = 64-sizeof(lp_dataHeader_t);
                }

            }
            else{
                //已经接受了一部分数据包
                if(rev_len > 64){

                   //接收中间的包
                  //memcpy((uint8_t*)(datapack.buf+already_rev),(uint8_t*)(buf_temp),64);
                  for(int i=0;i<64;i++)
                  {
                        datapack.arr_buf.append(buf_temp[i]);
                  }
                  rev_len -= 64;
                  already_rev += 64;
               }
               else
               {
                  //接收最后一包
                  //memcpy((uint8_t*)(datapack.buf+already_rev),(uint8_t*)(buf_temp),rev_len);
                  for(int i=0;i<rev_len;i++)
                  {
                      datapack.arr_buf.append(buf_temp[i]);
                  }
                  rev_len = 0;
                  already_rev = 0;
                  revData_handle(&datapack);
               }

            }

          }

        QThread::msleep(2);
    }

}














