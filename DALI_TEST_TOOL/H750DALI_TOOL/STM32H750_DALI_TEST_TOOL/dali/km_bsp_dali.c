#include "km_bsp_dali.h"
#include "sys_log.h"
#include "main.h"
#include "tim.h"
#include "km_app_dali_master.h"
extern km_dali_master_ctrl_t dali_master_ctrl;
uint32_t sendBit[BITFIELD_BLOCK_COUNT(64)];
uint8_t sendDataStatus = 0xFF;
uint8_t sendDataIndex;
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == DALI_RX_Pin)
	{
		if(sendDataStatus == 0xFF)
		{
			uint32_t temp = __HAL_TIM_GET_COUNTER(&htim1);
			__HAL_TIM_SET_COUNTER(&htim1,0);
			if((dali_master_ctrl.pulseIndex == 0) && !HAL_GPIO_ReadPin(DALI_RX_GPIO_Port,DALI_RX_Pin))
			{
				return ;
			}
			dali_master_ctrl.rx_pulse(temp,&dali_master_ctrl);
		}
	}
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) //416us��ʱ��
{
	if(htim == &htim2)
	{
		if(sendDataStatus < sendDataIndex)
		{
			if(bitfield_get(sendBit, sendDataStatus))
			{
				HAL_GPIO_WritePin(DALI_TX_GPIO_Port,DALI_TX_Pin,GPIO_PIN_SET);
			}
			else
			{
				HAL_GPIO_WritePin(DALI_TX_GPIO_Port,DALI_TX_Pin,GPIO_PIN_RESET);
			}
			sendDataStatus++;
		}
		else
		{
			sendDataStatus = 0xFF;
			HAL_TIM_Base_Stop_IT(&htim2);
			dali_send_data_finish(&dali_master_ctrl);
		}
	}
}

static void dali_send_data(uint8_t *data,uint8_t length)
{
	//MYLOG_XB(data,length);
	HAL_TIM_Base_Start_IT(&htim2);
	bitfield_set(sendBit, 0);
	bitfield_clear(sendBit, 1);
	int index = 2;
	for(uint8_t i=0;i<length;i++)
    {
        for(uint8_t j=0;j<8;j++)
        {
          if((data[i]>>(7-j))&0x01)
          {
			  bitfield_set(sendBit, index++);
			  bitfield_clear(sendBit, index++);
          }
          else
          {
			  bitfield_clear(sendBit, index++);
			  bitfield_set(sendBit, index++);
          }
        }
    }
	bitfield_clear(sendBit, index++);
	bitfield_clear(sendBit, index++);
	sendDataIndex = index;
	sendDataStatus = 0;
}

void bsp_dali_send_data(uint8_t addr,uint8_t cmd)
{
	//MYLOG_XB(data,length);
	bitfield_set(sendBit, 0);
	bitfield_clear(sendBit, 1);
	int index = 2;
	uint8_t data[2];
	data[0] = addr;
	data[1] = cmd;
	for(uint8_t i=0;i<2;i++)
    {
        for(uint8_t j=0;j<8;j++)
        {
          if((data[i]>>(7-j))&0x01)
          {
			  bitfield_set(sendBit, index++);
			  bitfield_clear(sendBit, index++);
          }
          else
          {
			  bitfield_clear(sendBit, index++);
			  bitfield_set(sendBit, index++);
          }
        }
    }
	bitfield_clear(sendBit, index++);
	bitfield_clear(sendBit, index++);
	sendDataIndex = index;
	sendDataStatus = 0;
	HAL_TIM_Base_Start_IT(&htim2);
}

static void dali_send_error_data(void)
{
	//MYLOG_XB(data,length);
	bitfield_set(sendBit, 0);
	bitfield_clear(sendBit, 1);
	int index = 2;
	uint8_t data[2] = {0xAA,0x55};
	for(uint8_t i=0;i<1;i++)
    {
        for(uint8_t j=0;j<6;j++)
        {
          if((data[i]>>(7-j))&0x01)
          {
			  bitfield_set(sendBit, index++);
			  bitfield_clear(sendBit, index++);
          }
          else
          {
			  bitfield_clear(sendBit, index++);
			  bitfield_set(sendBit, index++);
          }
        }
    }
	bitfield_clear(sendBit, index++);
	bitfield_clear(sendBit, index++);
	sendDataIndex = index;
	sendDataStatus = 0;
	HAL_TIM_Base_Start_IT(&htim2);
}

void km_bsp_dali_init(void)
{
    dali_master_ctrl.send_dali_data = dali_send_data;
	dali_master_ctrl.send_frame_error_data = dali_send_error_data;
}

