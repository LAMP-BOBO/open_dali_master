#include "km_app_dali.h"
#include "sys_queue.h"
#include "km_app_dali_master.h"
#include "km_bsp_dali.h"
#include "usbd_custom_hid_if.h"
#include "sys_log.h"
#include "dali102_test_common.h"
#include "dali102_func_common.h"
static struct rt_thread usbThread,usbSendThread;
static uint8_t usbThread_stack[1024];
static uint8_t usbSendThread_stack[1024];


static rt_timer_t daliTimer;
uint8_t usbRevBuff[64];
uint8_t usbSendBuff[64];
static rt_mq_t usbSendQueue = RT_NULL;
static rt_mq_t usbRevQueue = RT_NULL;

km_dali_master_ctrl_t dali_master_ctrl;
send_ack_cb_t send_ack_cb = NULL;
uint32_t task_uuid = 0xFFFFFFFF;

void usb_send_data(uint8_t *data,uint16_t length)
{
	USBD_CUSTOM_HID_SendReport_FS(data, length);
}

void usb_rev_handle(uint8_t *buff, uint16_t size)
{
	rt_mq_send(usbRevQueue, buff, size);
}

void usb_send_handle(uint8_t *buff, uint16_t size)
{
	rt_mq_send(usbSendQueue, buff, size);
}

static void query_complete_cb(uint8_t send_data1,uint8_t send_data2,uint8_t send_type,uint8_t reply_type,uint8_t reply_data,uint32_t seq)
{
	//MYLOG("send = %x,%x, type = %d, data = %d",send_data1,send_data2,reply_type,reply_data);
	if(send_ack_cb != NULL)
	{
		send_ack_cb(send_data1,send_data2,send_type,reply_type,reply_data,seq);
	}
}

static void dali_timeout_handler(void * p_context)
{
    km_dali_time_handle(&dali_master_ctrl);
}

void load_dali_data(uint8_t addr,uint8_t cmd,uint8_t send_count,uint8_t send_atk,uint16_t send_end_delay)
{
	dali_load_send_data(addr,cmd,send_count,send_atk,send_end_delay,&dali_master_ctrl);
}

void load_dali_error_data(uint16_t send_end_delay)
{
	dali_load_send_error_data(send_end_delay,&dali_master_ctrl);
}

static void usbThread_entry(void *param)
{
	while (1)
	{
		if(rt_mq_recv(usbRevQueue, usbRevBuff, sizeof(usbRevBuff), RT_WAITING_FOREVER) == RT_EOK)
        {	
            //usb_send_data(usbRevBuff,sizeof(usbRevBuff));
			start_4_1_RESET_task();
			//usb_send_handle(usbRevBuff, sizeof(usbRevBuff));
        }
	}
}

static void usbSendThread_entry(void *param)
{
	while (1)
	{
		if(rt_mq_recv(usbSendQueue, usbSendBuff, sizeof(usbSendBuff), RT_WAITING_FOREVER) == RT_EOK)
        {	
            usb_send_data(usbSendBuff,sizeof(usbSendBuff));
			rt_thread_delay(5);
        }
	}
}

void km_dali_app_init(void)
{
    km_bsp_dali_init();
    dali_master_ctrl.query_complete_cb = query_complete_cb;
    km_dali_master_init(&dali_master_ctrl);
	usbRevQueue = rt_mq_create("usbRevMq",64,128,RT_IPC_FLAG_FIFO);
	usbSendQueue = rt_mq_create("usbSendMq",64,128,RT_IPC_FLAG_FIFO);
	rt_thread_init(&usbThread,"usbThread",usbThread_entry,RT_NULL,&usbThread_stack[0],sizeof(usbThread_stack),15 - 1, 5);
	rt_thread_init(&usbSendThread,"usbSendThread",usbSendThread_entry,RT_NULL,&usbSendThread_stack[0],sizeof(usbSendThread_stack),14 - 1, 5);
	rt_thread_startup(&usbThread);
	rt_thread_startup(&usbSendThread);
	daliTimer = rt_timer_create("daliTimer", dali_timeout_handler,RT_NULL, 1,RT_TIMER_FLAG_PERIODIC);
	rt_timer_start(daliTimer);
	
	//start_4_1_RESET_task(&send_ack_cb);
	task_uuid = 0;
}
