#ifndef _KM_BSP_DALI_H_
#define _KM_BSP_DALI_H_

#include <stdint.h>
#define GPIO_HIGH 0
#define GPIO_LOW  416
#include "tim.h"
void km_bsp_dali_init(void);
void HAL_TIM_PeriodElapsedHandle(TIM_HandleTypeDef *htim);
void bsp_dali_send_data(uint8_t addr,uint8_t cmd);
#endif
