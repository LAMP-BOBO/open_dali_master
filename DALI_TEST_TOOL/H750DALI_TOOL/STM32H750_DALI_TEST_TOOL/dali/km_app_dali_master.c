#include "km_app_dali_master.h"
#include "sys_log.h"

static void km_dali_rx_handle(km_dali_master_ctrl_t *ctrl)
{
    bool ioState[64];
    bool ioData[64];
    uint8_t ioStateIndex = 0;
    for(int i = 0;i < ctrl->pulseIndex;i++) //先高再低  //忽略0
    {
        if(ctrl->pulse[i] > 300 && ctrl->pulse[i] < 550)// 416 +- 10%   自身的误差+其他设备的误差
        {
            ioState[ioStateIndex++] = (i % 2)?true:false;
        }
        else if(ctrl->pulse[i] > 650 && ctrl->pulse[i] < 1000)// 833 +- 10%
        {
            ioState[ioStateIndex++] = (i % 2)?true:false;
            ioState[ioStateIndex++] = (i % 2)?true:false;
        }
        else
        {
            ctrl->revDaliAck = DALI_RX_ERROR_PLUSE;
            //__LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "pwm width fail = %d\n",ctrl->pulse[i]);//脉宽异常
            return;
        }
    }
    if(ioStateIndex % 2)//补最后一个高电平
    {
        ioState[ioStateIndex++] = true;
    }
    uint8_t bitCount = 0;
    for(int i = 2;i < ioStateIndex;i += 2)//起始位忽略
    {
        if(!ioState[i] && ioState[i+1])
        {
            ioData[bitCount++] = true;
        }
        else if(ioState[i] && !ioState[i+1])
        {
            ioData[bitCount++] = false;
        }                
        else
        {
            ctrl->revDaliAck = DALI_RX_ERROR_DECODE;
            return;
        }
    }
    if(bitCount == 8)
    {
        ctrl->revDaliData[0] = (ioData[0]<<7)|(ioData[1]<<6)|(ioData[2]<<5)|(ioData[3]<<4)|(ioData[4]<<3)|(ioData[5]<<2)|(ioData[6]<<1)|(ioData[7]);
        ctrl->revDaliAck = DALI_RX_BIT8;
    }
    else if(bitCount == 16)
    {
        ctrl->revDaliData[0] = (ioData[0]<<7)|(ioData[1]<<6)|(ioData[2]<<5)|(ioData[3]<<4)|(ioData[4]<<3)|(ioData[5]<<2)|(ioData[6]<<1)|(ioData[7]);
        ctrl->revDaliData[1] = (ioData[8]<<7)|(ioData[9]<<6)|(ioData[10]<<5)|(ioData[11]<<4)|(ioData[12]<<3)|(ioData[13]<<2)|(ioData[14]<<1)|(ioData[15]);	
        ctrl->revDaliAck = DALI_RX_BIT16;
    }
    else if(bitCount == 24)
    {
        ctrl->revDaliData[0] = (ioData[0]<<7)|(ioData[1]<<6)|(ioData[2]<<5)|(ioData[3]<<4)|(ioData[4]<<3)|(ioData[5]<<2)|(ioData[6]<<1)|(ioData[7]);
        ctrl->revDaliData[1] = (ioData[8]<<7)|(ioData[9]<<6)|(ioData[10]<<5)|(ioData[11]<<4)|(ioData[12]<<3)|(ioData[13]<<2)|(ioData[14]<<1)|(ioData[15]);	
        ctrl->revDaliData[2] = (ioData[16]<<7)|(ioData[17]<<6)|(ioData[18]<<5)|(ioData[19]<<4)|(ioData[20]<<3)|(ioData[21]<<2)|(ioData[22]<<1)|(ioData[23]);
        ctrl->revDaliAck = DALI_RX_BIT24;
    }
    else if(bitCount == 25)
    {
        ctrl->revDaliData[0] = (ioData[1]<<7)|(ioData[2]<<6)|(ioData[3]<<5)|(ioData[4]<<4)|(ioData[5]<<3)|(ioData[6]<<2)|(ioData[7]<<1)|(ioData[8]);
        ctrl->revDaliData[1] = (ioData[9]<<7)|(ioData[10]<<6)|(ioData[11]<<5)|(ioData[12]<<4)|(ioData[13]<<3)|(ioData[14]<<2)|(ioData[15]<<1)|(ioData[16]);	
        ctrl->revDaliData[2] = (ioData[17]<<7)|(ioData[18]<<6)|(ioData[19]<<5)|(ioData[20]<<4)|(ioData[21]<<3)|(ioData[22]<<2)|(ioData[23]<<1)|(ioData[24]);
        ctrl->revDaliAck = DALI_RX_BIT25;
    }
    else
    {
        ctrl->revDaliAck = DALI_RX_UNKNOWN_BIT;
    }
}

static void km_dali_rx_pulse(uint16_t pulse,km_dali_master_ctrl_t *master)
{
    master->pulse[master->pulseIndex++] = pulse;
    if(master->pulseIndex >= 64)
    {
        master->pulseIndex = 0;
    }
    master->pulseTimerCount = 5;//1.5ms超时
}

void dali_load_send_data(uint8_t data,uint8_t data2,uint8_t send_count,uint8_t ack,uint16_t end_daley,km_dali_master_ctrl_t *master)
{
    dali_packget_t packet;
	packet.send_type = 0;
	packet.send_count = send_count;
	packet.send_length = 2;
	packet.send_need_ack = ack;
	if(end_daley < 25)
		end_daley = 25;
	packet.send_end_delay = end_daley;
    packet.send_data[0] = data;
    packet.send_data[1] = data2;
    packet.send_seq = master->seq_index++;
    uint8_t res = queuePush(&master->daliSendQueue,(uint8_t *)&packet,sizeof(packet));
    if(res == 0)
    {
        
    }
    master->send_flag = 1;
}

void dali_load_send_error_data(uint16_t end_daley,km_dali_master_ctrl_t *master)
{
    dali_packget_t packet;
	packet.send_type = 1;
	packet.send_length = 2;
	packet.send_count = 1;
	if(end_daley < 25)
		end_daley = 25;
	packet.send_end_delay = end_daley;
    packet.send_seq = master->seq_index++;
    uint8_t res = queuePush(&master->daliSendQueue,(uint8_t *)&packet,sizeof(packet));
    if(res == 0)
    {
        
    }
    master->send_flag = 1;
}

uint32_t get_ctrl_send_seq(km_dali_master_ctrl_t *master)
{
    return master->seq_index;
}

void dali_send_data_finish(km_dali_master_ctrl_t *master)
{
	
}

static void dali_send_finish_handle(km_dali_master_ctrl_t *master)
{
    dali_packget_t daliPacket = master->dali_packet;
    uint8_t *rev_data = master->revDaliData;
    uint8_t *send_data = master->dali_packet.send_data;
    if(master->query_complete_cb != NULL && master->dali_packet.send_length != 0)
    {
        master->query_complete_cb(send_data[0],send_data[1],master->dali_packet.send_type,master->revDaliAck,master->revDaliData[0],daliPacket.send_seq);
    }
}

void km_dali_time_handle(km_dali_master_ctrl_t *master)
{
    if(master->send_flag)
    {
        master->timer_count++;
        if(master->timer_count >= master->dali_packet.send_end_delay)
        {
            master->timer_count = 0;
            dali_send_finish_handle(master);//25判断是否有数据 
            if(master->daliSendQueue.nodeCount > 0)
            {
                uint32_t length = queueData(&master->daliSendQueue,(uint8_t *)&master->dali_packet,sizeof(master->dali_packet),0);
                if(length > 0)
                {
                    master->revDaliAck = DALI_RX_NULL;
					if(master->dali_packet.send_type == 0)
					{
						master->send_dali_data((uint8_t *)&master->dali_packet.send_data,master->dali_packet.send_length);
					}
					else
					{
						master->send_frame_error_data();
					}
                    master->send_flag = 1;
                    master->dali_packet.send_count--;
                    if(master->dali_packet.send_count == 0)
                    {
                        queuePop(&master->daliSendQueue,NULL,0);
                    }
                    else
                    {
                        queueUpdateData(&master->daliSendQueue,(uint8_t *)&master->dali_packet,sizeof(master->dali_packet),0);
                    }
                }
            }
            else
            {
                master->send_flag = 0;
            }
        }
    }

    if(master->pulseTimerCount > 0)
    {
        master->pulseTimerCount--;
        if(master->pulseTimerCount == 0)
        {
            km_dali_rx_handle(master);
            master->pulseIndex = 0;
        }
    }
}

uint8_t km_dali_master_init(km_dali_master_ctrl_t *master)
{
    master->rx_pulse = km_dali_rx_pulse;
    queueInit(&master->daliSendQueue);
    if(master->send_dali_data == NULL)
    {
        return 0;
    }
    return 1;
}

