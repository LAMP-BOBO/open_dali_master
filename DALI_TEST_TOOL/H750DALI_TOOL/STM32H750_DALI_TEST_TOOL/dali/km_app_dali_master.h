#ifndef _KM_APP_DALI_MASTER_H_
#define _KM_APP_DALI_MASTER_H_

#include <stdint.h>
#include <stdlib.h>
#include "bitfield.h"
#include "sys_queue.h"
#include "km_app_dali_common.h"

typedef enum{
    DALI_RX_NULL,
    DALI_RX_BIT8,
    DALI_RX_BIT16,
    DALI_RX_BIT24,
    DALI_RX_BIT25,
    DALI_RX_UNKNOWN_BIT,//其他BIT
    DALI_RX_ERROR_PLUSE,//脉宽错误
    DALI_RX_ERROR_DECODE,//解码错误
    DALI_RX_END
}dali_rx_type_t;//DALI接收数据状态 

typedef struct __km_dali_master_ctrl_t km_dali_master_ctrl_t;
typedef struct __km_dali_master_data_t km_dali_master_data_t;
typedef struct __km_dali_devices_data_t km_dali_devices_data_t;
typedef struct __dali_packget_t dali_packget_t;

typedef void (*send_dali_data_cb_t)(uint8_t *data,uint8_t length);
typedef void (*send_frame_error_data_t)();
typedef void (*query_complete_cb_t)(uint8_t address,uint8_t cmd,uint8_t send_type,uint8_t type,uint8_t reply_data,uint32_t seq);

#define DALI_INVALID_DATA 0xFF
#define DALI_SEND_NEED_ACK  0x01
#define DALI_SEND_NO_ACK  0x00

#define DALI_DATA_8BIT(a) ((uint8_t)(a)|(1<<24))
#define DALI_DATA_16BIT(a,b) ((uint8_t)(a)|((uint8_t)(b)<<8)|(2<<24))
#define DALI_DATA_24BIT(a,b,c) ((uint8_t)(a)|((b)<<8)|((c)<<24)|(3<<24))

struct __km_dali_devices_data_t{
    uint8_t addr;
    uint8_t device_type;//设备类型
    uint8_t status;//设备状态
    uint8_t lightness;//设备亮度
    uint8_t version_number;
    uint8_t min_level;
    uint8_t max_level;
};

struct __dali_packget_t{ //8字节
	uint8_t send_type:1;//发送类型
    uint8_t send_count:4;//发送次数
    uint8_t send_length:2;//发送长度
    uint8_t send_need_ack:1;//是否需要应答
	uint16_t send_end_delay;//发送完成后等待的延时
    uint8_t send_data[2];//发送数据
    uint32_t send_seq:24;
};

struct __km_dali_master_ctrl_t{
    void (*rx_pulse)(uint16_t,km_dali_master_ctrl_t *);//接收脉冲
    send_dali_data_cb_t send_dali_data;
	send_frame_error_data_t send_frame_error_data;
    query_complete_cb_t query_complete_cb;
    dali_rx_type_t revDaliAck;
    QueueContext_t daliSendQueue;
    dali_packget_t dali_packet;
    uint8_t revDaliData[3];
    uint32_t seq_index;
    uint32_t timer_count;
    uint16_t pulse[64];
    uint8_t pulseIndex;
    uint8_t pulseTimerCount;
	
//    uint32_t searchTimerCount;
//    uint32_t group_type_flag[BITFIELD_BLOCK_COUNT(256)];
//    uint8_t temp_index;
//    uint8_t temp_index2;
//    uint8_t temp_data[255];
    uint8_t send_flag:1;
//    uint8_t send_flag:1;
//    uint8_t group_flag:1;
}; 
void dali_send_data_finish(km_dali_master_ctrl_t *master);
uint8_t km_dali_master_init(km_dali_master_ctrl_t *master);
void dali_load_send_data(uint8_t data,uint8_t data2,uint8_t send_count,uint8_t ack,uint16_t end_daley,km_dali_master_ctrl_t *master);
void dali_load_send_error_data(uint16_t end_daley,km_dali_master_ctrl_t *master);
void km_dali_time_handle(km_dali_master_ctrl_t *master);
uint32_t get_ctrl_send_seq(km_dali_master_ctrl_t *master);
#endif
