#ifndef _MYLOG_H_
#define _MYLOG_H_

#include "stdio.h"
#define DEBUG_PRINTF	0

#define MYLOG(format,...) do {if(DEBUG_PRINTF)printf("[%s:%d] "format" \r\n",__FUNCTION__,__LINE__,##__VA_ARGS__); } while(0)															 
#define MYLOG2(x)	do {if(DEBUG_PRINTF)printf("Assertion \"%s\" failed at line %d in %s\n",x, __LINE__, __FILE__); } while(0)
#define MYLOG_ERROR(x,format,...)	do { if (!(x)) {MYLOG(format,...); }} while(0)

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

//int main (int argc, char const *argv[]) {
//        
//        printf(ANSI_COLOR_RED     "This text is RED!"     ANSI_COLOR_RESET "\r\n");
//        printf(ANSI_COLOR_GREEN   "This text is GREEN!"   ANSI_COLOR_RESET "\r\n");
//        printf(ANSI_COLOR_YELLOW  "This text is YELLOW!"  ANSI_COLOR_RESET "\r\n");
//        printf(ANSI_COLOR_BLUE    "This text is BLUE!"    ANSI_COLOR_RESET "\r\n");
//        printf(ANSI_COLOR_MAGENTA "This text is MAGENTA!" ANSI_COLOR_RESET "\r\n");
//        printf(ANSI_COLOR_CYAN    "This text is CYAN!"    ANSI_COLOR_RESET "\r\n");

//        return 0;
//}


#endif

