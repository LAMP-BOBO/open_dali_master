
这个项目是DALI2 协议测试工具
下位机主要是透传上位机的命令

主要有两个工程H750 F401
其中H750 cube生成的工程 新版本中选择选择一个芯片型号V型还是Y型，目前大部分是V型
如果选错型号烧入代码后会导致芯片锁死无法烧入。
解锁的方法是用STlink 连接上板子然后使用 STM32CubeProgrammer 上位机点击connect的同时按下复位键
然后连接上以后全片擦除。

STM32CubeMX 生成的hid代码不能直接使用
需要设置一些参数 
usbd_desc.c文件中
1. pid vid 用来查找设备

2.
修改将usbd_conf.h
#define USBD_CUSTOMHID_OUTREPORT_BUF_SIZE      64
#define USBD_CUSTOM_HID_REPORT_DESC_SIZE       34
usbd_customhid.h文件中的发送与接收长度为64
#define CUSTOM_HID_EPIN_SIZE                  0x40
#define CUSTOM_HID_EPOUT_SIZE                 0x40

3.添加HID设备描述符
 usbd_custom_hid_if.c 中
0x06, 0x00, 0xff,              // USAGE_PAGE (Vendor Defined Page 1)
0x09, 0x01,                    // USAGE (Vendor Usage 1)
0xa1, 0x01,                    // COLLECTION (Application)
0x09, 0x01,                    //   USAGE (Vendor Usage 1)
0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
0x26, 0xff, 0x00,              //   LOGICAL_MAXIMUM (255)
0x95, 0x40,                    //   REPORT_COUNT (64)
0x75, 0x08,                    //   REPORT_SIZE (8)
0x81, 0x02,                    //   INPUT (Data,Var,Abs)
0x09, 0x01,                    //   USAGE (Vendor Usage 1)
0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
0x26, 0xff, 0x00,              //   LOGICAL_MAXIMUM (255)
0x95, 0x40,                    //   REPORT_COUNT (64)
0x75, 0x08,                    //   REPORT_SIZE (8)
0x91, 0x02,                    //   OUTPUT (Data,Var,Abs)
0xc0                           // END_COLLECTION

改完上述即可使用


sys_log.h文件中
如果没有用printf 要注释掉




























