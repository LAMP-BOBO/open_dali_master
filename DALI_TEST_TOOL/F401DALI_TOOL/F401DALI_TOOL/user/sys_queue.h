#ifndef _MYQUEUE_H_
#define _MYQUEUE_H_

#include "main.h"
#include "sys_mem_tlsf.h"

#define QUEUE_MALLOC(len)		sys_mem_malloc(len)
#define QUEUE_FREE(point)		sys_mem_free(point)

enum{
	QUEUE_SUCESS,
	QUEUE_FAIL
};

typedef struct structQueueBuff{
	uint8_t *data;
	uint16_t len;
}QueueBuff_t;

typedef struct structQueueList{
	QueueBuff_t buff;
	struct structQueueList *next;
}QueueNode_t;

typedef struct structQueueContext{
	QueueNode_t *head;
	QueueNode_t *end;
	uint8_t initFlag;
	uint16_t nodeCount;
}QueueContext_t;

void queueInit(QueueContext_t *pQueue);
uint8_t queueDestroy(QueueContext_t *pQueue);
uint8_t queueClear(QueueContext_t *pQueue);
uint8_t queuePush(QueueContext_t *pQueue,uint8_t *data,uint16_t len);
uint32_t getQueueCount(QueueContext_t *pQueue);
uint8_t queuePop(QueueContext_t *pQueue,uint8_t *data,uint16_t len);
#endif
