#include "sys_queue.h"
#include "string.h"
void queueInit(QueueContext_t *pQueue)
{
	pQueue->head = NULL;
	pQueue->end = NULL;
	pQueue->nodeCount = 0;
	pQueue->initFlag = 1;
}

uint8_t queueDestroy(QueueContext_t *pQueue)
{
	if(queueClear(pQueue) == QUEUE_SUCESS)
	{
		pQueue->initFlag = 0;
		return QUEUE_SUCESS;
	}
	return QUEUE_FAIL;
}

uint8_t queueClear(QueueContext_t *pQueue)
{
	QueueContext_t *q = pQueue;
	QueueNode_t *node = NULL;
	if(!pQueue->initFlag)
	{
		return QUEUE_FAIL;
	}
	if(q->head != NULL)
	{
		while(q->head->next != NULL)
		{
			node = q->head;
			QUEUE_FREE(node->buff.data);
			node->buff.data = NULL;
			q->nodeCount--;
			q->head = q->head->next;
			QUEUE_FREE(node);
		}
		QUEUE_FREE(q->head->buff.data);
		QUEUE_FREE(q->head);
		q->nodeCount--;
		q->head = NULL;
		q->end = NULL;
	}
	return QUEUE_SUCESS;
}

uint8_t queuePush(QueueContext_t *pQueue,uint8_t *data,uint16_t len)
{
	QueueNode_t *node = NULL;
	if(!pQueue->initFlag)
	{
		return QUEUE_FAIL;
	}
	node = QUEUE_MALLOC(sizeof(QueueNode_t));
	if(node != NULL)
	{
		node->buff.data = QUEUE_MALLOC(len);
		node->next = NULL;
		if(node->buff.data != NULL)
		{
			memcpy(node->buff.data, data, len);
			node->buff.len = len;
			if(pQueue->head == NULL)
			{
				pQueue->head = node;
				pQueue->end = pQueue->head;
			}
			else
			{
				pQueue->end->next = node;
				pQueue->end = node;
			}
			pQueue->nodeCount++;
			return QUEUE_SUCESS;
		}
		else
		{
			QUEUE_FREE(node);
		}
	}
	return QUEUE_FAIL;
}

uint32_t getQueueCount(QueueContext_t *pQueue)
{
	if(!pQueue->initFlag)
	{
		return QUEUE_FAIL;
	}
	return pQueue->nodeCount;
}

uint8_t queuePop(QueueContext_t *pQueue,uint8_t *data,uint16_t len)
{
	QueueNode_t *node = NULL;
	if(!pQueue->initFlag)
	{
		return QUEUE_FAIL;
	}
	node = pQueue->head;
	memcpy(data,node->buff.data,len);
	if(pQueue->head->next != NULL)
	{
		pQueue->head = pQueue->head->next;
	}
	else
	{
		pQueue->head = NULL;
		pQueue->end = NULL;
	}
	QUEUE_FREE(node->buff.data);
	QUEUE_FREE(node);
	pQueue->nodeCount--;
	return QUEUE_SUCESS;
}
