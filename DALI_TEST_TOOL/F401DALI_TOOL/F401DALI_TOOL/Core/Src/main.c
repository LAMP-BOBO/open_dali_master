/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "app_scheduler.h"
#include "app_timer.h"
#include "sys_mem_tlsf.h"
#include "sys_log.h"
#include "usbd_custom_hid_if.h"
#include "bitfield.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
typedef enum{
    DALI_RX_NULL,
    DALI_RX_BIT8,
    DALI_RX_BIT16,
    DALI_RX_BIT24,
    DALI_RX_BIT25,
    DALI_RX_UNKNOWN_BIT,//其他BIT
    DALI_RX_ERROR_PLUSE,//脉宽错误
    DALI_RX_ERROR_DECODE,//解码错误
    DALI_RX_END
}dali_rx_type_t;//DALI接收数据状态 
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
APP_TIMER_DEF(m_timer);
APP_TIMER_DEF(dali_timer);
static void led_time_out(void *p_context)
{
	HAL_GPIO_TogglePin(LED_GPIO_Port,LED_Pin);
}
/* USER CODE END PFP */
uint8_t usbSendBuff[64] = {'D','A','L','I'};
uint32_t sendBit[BITFIELD_BLOCK_COUNT(64)];
uint8_t sendDataStatus = 0xFF;
uint8_t sendDataIndex;
uint16_t pulse[64];
uint8_t pulseIndex;
uint8_t pulseTimerCount;
dali_rx_type_t revDaliAck;
uint8_t revDaliData[3];
uint8_t daliCmdRev;
void bsp_dali_send_data(uint8_t addr,uint8_t cmd);
void dali_send_error_data(void);
/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void usb_send_data(uint8_t *data,uint16_t length)
{
	USBD_CUSTOM_HID_SendReport_FS(data, length);
}

void usb_rev_handle(void *buff, uint16_t size)
{
	if(daliCmdRev == 0)
	{
		uint8_t *p = (uint8_t *)buff;
		if(memcmp("DALI",p,4) == 0)
		{
			if(p[4] == 0x00)
			{
				daliCmdRev = 1;
				dali_send_error_data();
			}
			else if(p[4] == 0x01)
			{
				daliCmdRev = 1;
				bsp_dali_send_data(p[5],p[6]);
			}
		}
	}
}

void usb_send_package(uint8_t ack)
{
	usbSendBuff[4] = 0x02;
	usbSendBuff[5] = (uint8_t)revDaliAck;
	usbSendBuff[6] = ack;
	usb_send_data(usbSendBuff,sizeof(usbSendBuff));
}

static void km_dali_rx_handle(void)
{
    bool ioState[64];
    bool ioData[64];
    uint8_t ioStateIndex = 0;
    for(int i = 0;i < pulseIndex;i++) //先高再低  //忽略0
    {
        if(pulse[i] > 300 && pulse[i] < 550)// 416 +- 10%   自身的误差+其他设备的误差
        {
            ioState[ioStateIndex++] = (i % 2)?true:false;
        }
        else if(pulse[i] > 650 && pulse[i] < 1000)// 833 +- 10%
        {
            ioState[ioStateIndex++] = (i % 2)?true:false;
            ioState[ioStateIndex++] = (i % 2)?true:false;
        }
        else
        {
            revDaliAck = DALI_RX_ERROR_PLUSE;
            //__LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "pwm width fail = %d\n",ctrl->pulse[i]);//脉宽异常
            return;
        }
    }
    if(ioStateIndex % 2)//补最后一个高电平
    {
        ioState[ioStateIndex++] = true;
    }
    uint8_t bitCount = 0;
    for(int i = 2;i < ioStateIndex;i += 2)//起始位忽略
    {
        if(!ioState[i] && ioState[i+1])
        {
            ioData[bitCount++] = true;
        }
        else if(ioState[i] && !ioState[i+1])
        {
            ioData[bitCount++] = false;
        }                
        else
        {
            revDaliAck = DALI_RX_ERROR_DECODE;
            return;
        }
    }
    if(bitCount == 8)
    {
        revDaliData[0] = (ioData[0]<<7)|(ioData[1]<<6)|(ioData[2]<<5)|(ioData[3]<<4)|(ioData[4]<<3)|(ioData[5]<<2)|(ioData[6]<<1)|(ioData[7]);
        revDaliAck = DALI_RX_BIT8;
    }
    else if(bitCount == 16)
    {
        revDaliData[0] = (ioData[0]<<7)|(ioData[1]<<6)|(ioData[2]<<5)|(ioData[3]<<4)|(ioData[4]<<3)|(ioData[5]<<2)|(ioData[6]<<1)|(ioData[7]);
        revDaliData[1] = (ioData[8]<<7)|(ioData[9]<<6)|(ioData[10]<<5)|(ioData[11]<<4)|(ioData[12]<<3)|(ioData[13]<<2)|(ioData[14]<<1)|(ioData[15]);	
        revDaliAck = DALI_RX_BIT16;
    }
    else if(bitCount == 24)
    {
        revDaliData[0] = (ioData[0]<<7)|(ioData[1]<<6)|(ioData[2]<<5)|(ioData[3]<<4)|(ioData[4]<<3)|(ioData[5]<<2)|(ioData[6]<<1)|(ioData[7]);
        revDaliData[1] = (ioData[8]<<7)|(ioData[9]<<6)|(ioData[10]<<5)|(ioData[11]<<4)|(ioData[12]<<3)|(ioData[13]<<2)|(ioData[14]<<1)|(ioData[15]);	
        revDaliData[2] = (ioData[16]<<7)|(ioData[17]<<6)|(ioData[18]<<5)|(ioData[19]<<4)|(ioData[20]<<3)|(ioData[21]<<2)|(ioData[22]<<1)|(ioData[23]);
        revDaliAck = DALI_RX_BIT24;
    }
    else if(bitCount == 25)
    {
        revDaliData[0] = (ioData[1]<<7)|(ioData[2]<<6)|(ioData[3]<<5)|(ioData[4]<<4)|(ioData[5]<<3)|(ioData[6]<<2)|(ioData[7]<<1)|(ioData[8]);
        revDaliData[1] = (ioData[9]<<7)|(ioData[10]<<6)|(ioData[11]<<5)|(ioData[12]<<4)|(ioData[13]<<3)|(ioData[14]<<2)|(ioData[15]<<1)|(ioData[16]);	
        revDaliData[2] = (ioData[17]<<7)|(ioData[18]<<6)|(ioData[19]<<5)|(ioData[20]<<4)|(ioData[21]<<3)|(ioData[22]<<2)|(ioData[23]<<1)|(ioData[24]);
        revDaliAck = DALI_RX_BIT25;
    }
    else
    {
        revDaliAck = DALI_RX_UNKNOWN_BIT;
    }
}

static void dali_time_out(void *p_context)
{
    if(pulseTimerCount > 0)
    {
        pulseTimerCount--;
        if(pulseTimerCount == 0)
        {
            km_dali_rx_handle();
			usb_send_package(revDaliData[0]);
            pulseIndex = 0;
        }
    }
}

void dali_send_data_finish(void)
{
	daliCmdRev = 0;
}

static void km_dali_rx_pulse(uint16_t data)
{
    pulse[pulseIndex++] = data;
    if(pulseIndex >= 64)
    {
        pulseIndex = 0;
    }
    pulseTimerCount = 5;//1.5ms超时
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == DALI_RX_Pin)
	{
		if(sendDataStatus == 0xFF)
		{
			uint32_t temp = __HAL_TIM_GET_COUNTER(&htim1);
			__HAL_TIM_SET_COUNTER(&htim1,0);
			if((pulseIndex == 0) && !HAL_GPIO_ReadPin(DALI_RX_GPIO_Port,DALI_RX_Pin))
			{
				return ;
			}
			km_dali_rx_pulse(temp);
		}
	}
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) //416us定时器
{
	if(htim == &htim2)
	{
		if(sendDataStatus < sendDataIndex)
		{
			if(bitfield_get(sendBit, sendDataStatus))
			{
				HAL_GPIO_WritePin(DALI_TX_GPIO_Port,DALI_TX_Pin,GPIO_PIN_SET);
			}
			else
			{
				HAL_GPIO_WritePin(DALI_TX_GPIO_Port,DALI_TX_Pin,GPIO_PIN_RESET);
			}
			sendDataStatus++;
		}
		else
		{
			sendDataStatus = 0xFF;
			HAL_TIM_Base_Stop_IT(&htim2);
			dali_send_data_finish();
		}
	}
}

void bsp_dali_send_data(uint8_t addr,uint8_t cmd)
{
	bitfield_set(sendBit, 0);
	bitfield_clear(sendBit, 1);
	int index = 2;
	uint8_t data[2];
	data[0] = addr;
	data[1] = cmd;
	for(uint8_t i=0;i<2;i++)
    {
        for(uint8_t j=0;j<8;j++)
        {
          if((data[i]>>(7-j))&0x01)
          {
			  bitfield_set(sendBit, index++);
			  bitfield_clear(sendBit, index++);
          }
          else
          {
			  bitfield_clear(sendBit, index++);
			  bitfield_set(sendBit, index++);
          }
        }
    }
	bitfield_clear(sendBit, index++);
	bitfield_clear(sendBit, index++);
	sendDataIndex = index;
	sendDataStatus = 0;
	HAL_TIM_Base_Start_IT(&htim2);
}

void dali_send_error_data(void)
{
	bitfield_set(sendBit, 0);
	bitfield_clear(sendBit, 1);
	int index = 2;
	uint8_t data[2] = {0xAA,0x55};
	for(uint8_t i=0;i<1;i++)
    {
        for(uint8_t j=0;j<6;j++)
        {
          if((data[i]>>(7-j))&0x01)
          {
			  bitfield_set(sendBit, index++);
			  bitfield_clear(sendBit, index++);
          }
          else
          {
			  bitfield_clear(sendBit, index++);
			  bitfield_set(sendBit, index++);
          }
        }
    }
	bitfield_clear(sendBit, index++);
	bitfield_clear(sendBit, index++);
	sendDataIndex = index;
	sendDataStatus = 0;
	HAL_TIM_Base_Start_IT(&htim2);
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  APP_SCHED_INIT(64,128);
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_USART1_UART_Init();
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN 2 */
  HAL_Delay(200);
  HAL_TIM_Base_Start(&htim1);
  app_timer_init();
  app_timer_create(&m_timer, APP_TIMER_MODE_REPEATED, led_time_out);
  app_timer_start(m_timer, 1000, NULL);
  app_timer_create(&dali_timer, APP_TIMER_MODE_REPEATED, dali_time_out);
  app_timer_start(dali_timer, 1, NULL);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  app_sched_execute();
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 25;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
