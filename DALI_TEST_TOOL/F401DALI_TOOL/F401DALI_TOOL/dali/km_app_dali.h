#ifndef _KM_APP_DALI_H_
#define _KM_APP_DALI_H_
#include <stdint.h>
#include <stdbool.h>
void km_dali_app_init(void);
void start_dali_search_task(void);
uint8_t dali_search_status(void);
#endif

