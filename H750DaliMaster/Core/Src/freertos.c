/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "usbd_custom_hid_if.h"
#include "lp_led/lp_dev_led.h"
#include "lp_key/lp_dev_key.h"
#include "lp_sys_os_mem/lp_sys_os_mem.h"
#include "lp_sys_os_sftimer/lp_sys_os_sftimer.h"
#include "lp_sys_os_tick/lp_sys_os_tick.h"
#include "lp_dali_stack/lp_dali_master.h"
#include "lp_sys_os_queue/lp_sys_os_queue.h"
#include "lp_pc_protocol/lp_pc_protocol.h"
#include "lp_pc_protocol/lp_pc_protocol_decode.h"
#include "lp_dali_stack/lp_dali_102_command.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
lp_u8_t auto_test_flag = 0;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
extern USBD_HandleTypeDef hUsbDeviceFS;
uint8_t usbSendBuff[64];

lp_sys_os_queue_t usb_sendQueue;
lp_sys_os_queue_t usb_revQueue;
/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .stack_size = 1024 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for ledTask */
osThreadId_t ledTaskHandle;
const osThreadAttr_t ledTask_attributes = {
  .name = "ledTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityLow,
};
/* Definitions for daliTask */
osThreadId_t daliTaskHandle;
const osThreadAttr_t daliTask_attributes = {
  .name = "daliTask",
  .stack_size = 512 * 4,
  .priority = (osPriority_t) osPriorityLow,
};
/* Definitions for tickTask */
osThreadId_t tickTaskHandle;
const osThreadAttr_t tickTask_attributes = {
  .name = "tickTask",
  .stack_size = 512 * 4,
  .priority = (osPriority_t) osPriorityLow,
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
osThreadId_t usbTaskHandle;
const osThreadAttr_t usbTask_attributes = {
  .name = "usbTask",
  .stack_size = 1024 * 4,
  .priority = (osPriority_t) osPriorityBelowNormal1,
};

osThreadId_t keyTaskHandle;
const osThreadAttr_t keyTask_attributes = {
  .name = "keyTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityLow,
};


/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);
void ledTask_hook(void *argument);
void daliTask_hook(void *argument);
void tickTask_hook(void *argument);
void usbTask_hook(void *argument);
void keyTask_hook(void *argument);

extern void MX_USB_DEVICE_Init(void);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
	#if LOG_SYSTEM_ENABLE
	Log_init("log_rtt");
	#endif
	
	lp_sys_os_mem_init();
	lp_sys_os_timer_func_init();
	
	lp_pdev_key  pkey = lp_getKEYDevice();
	pkey->Init(pkey);
  /* USER CODE END Init */

	
  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  usb_sendQueue = lp_sys_os_queue_create(sizeof(lp_dataPack_t),6,true);
  usb_revQueue = lp_sys_os_queue_create(sizeof(lp_usbOrigin_t),6,true);
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* creation of ledTask */
  ledTaskHandle = osThreadNew(ledTask_hook, NULL, &ledTask_attributes);

  /* creation of daliTask */
  daliTaskHandle = osThreadNew(daliTask_hook, NULL, &daliTask_attributes);

  /* creation of tickTask */
  tickTaskHandle = osThreadNew(tickTask_hook, NULL, &tickTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  usbTaskHandle = osThreadNew(usbTask_hook, NULL, &usbTask_attributes);
  keyTaskHandle = osThreadNew(keyTask_hook, NULL, &keyTask_attributes);
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  LOG_OUT("system init!\r\n");
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* init code for USB_DEVICE */
  MX_USB_DEVICE_Init();
	//usb 需要做插入检测
  /* USER CODE BEGIN StartDefaultTask */
  lp_err_t ret = LP_FAIL;
  lp_usbOrigin_t data;
  lp_dataPack_t tmep_pack;
  lp_u8_t header_len = sizeof(lp_dataHeader_t);
  lp_pdataHeader_t pdataheade;
  
  /* Infinite loop */
  for(;;)
  {
	if(lp_sys_os_queue_empty(usb_revQueue) != LP_OK){
		ret = lp_sys_os_queue_pop(usb_revQueue,&data);
		if(ret == LP_OK){
			LOG_OUT("USB rev a packer\r\n");
			pdataheade = (lp_pdataHeader_t)data.buf;
			if(pdataheade->dataLen < sizeof(lp_dataHeader_t)){
				//错误包
				LOG_OUT("USB rev error packer\r\n");
				continue;
			}else if(pdataheade->dataLen == sizeof(lp_dataHeader_t)){
				//没有数据包
				tmep_pack.dataHeader.dataLen = pdataheade->dataLen;
				tmep_pack.dataHeader.opcode = pdataheade->opcode;
				lp_pc_protocol_decode(tmep_pack);
			}
			else if(pdataheade->dataLen <= 64){
				//数据包小于1帧数据
				tmep_pack.buf = (lp_u8_t*)lp_sys_men_malloc(pdataheade->dataLen-sizeof(lp_dataHeader_t));
				if(tmep_pack.buf == NULL){
					LOG_OUT("USB rev lp_sys_men_malloc error\r\n");
					continue;
				}
				else{
					tmep_pack.dataHeader.dataLen = pdataheade->dataLen;
				    tmep_pack.dataHeader.opcode = pdataheade->opcode;
					memcpy(tmep_pack.buf,(lp_u8_t*)(data.buf+header_len),pdataheade->dataLen-sizeof(lp_dataHeader_t));
					lp_pc_protocol_decode(tmep_pack);
				}
			}
			else{
				//等待下一包
				
			}
		}
	}
    osDelay(5);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_ledTask_hook */
/**
* @brief Function implementing the ledTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_ledTask_hook */
void ledTask_hook(void *argument)
{
  /* USER CODE BEGIN ledTask_hook */
  lp_pdev_led      p_led1;
	
  p_led1 = lp_getLEDDevice(LED_RED);
  p_led1->Init(p_led1);
	
  /* Infinite loop */
  for(;;)
  {
	p_led1->Toggout(p_led1);
	  if(auto_test_flag){
		osDelay(100);
	  }
	  else{
		osDelay(1000);
	  }  
  }
  /* USER CODE END ledTask_hook */
}

/* USER CODE BEGIN Header_daliTask_hook */
/**
* @brief Function implementing the daliTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_daliTask_hook */
void daliTask_hook(void *argument)
{
  /* USER CODE BEGIN daliTask_hook */
	lp_u16_t time_count = 0;
	lp_u8_t  turn_off = 0;
	lp_err_t ret = LP_FAIL;
	lp_pdali_master_t dali_master = lp_getDALI_master(LP_DALI_MASTER0);
	dali_master->Init(dali_master);
	//dali_master->search_device(dali_master,true);

   lp_pdev_key  pkey = lp_getKEYDevice();
  /* Infinite loop */
  for(;;)
  {
    dali_master->read_log(dali_master);
	//读log发送给上位机
	  
	 lp_u8_t ret_key =  pkey->Read(pkey);
	//读按键
	 if(ret_key == LP_KEY_1_LONG)
	 {
		LOG_OUT("LP_KEY_1_LONG PREES \r\n");
	 }
	 else if(ret_key == LP_KEY_2_UP){
		LOG_OUT("LP_KEY_2_UP PREES \r\n");
	 }
	 else if(ret_key == LP_KEY_2_LONG)
	 {
		LOG_OUT("LP_KEY_2_LONG PREES \r\n");
		//进入自动老化测试模式
		 auto_test_flag = ~auto_test_flag; 
	 }
	 
	 if((time_count%1000)==0 && auto_test_flag){
		
		
		if(turn_off == 1){
			dali_master->send_cmd(dali_master,0x81,DALI_STANDARD_COMMOND_OFF,1,DALI_SEND_NO_ACK);
			turn_off = 0;
		}
		else{
			turn_off = 1;
			dali_master->send_cmd(dali_master,0x83,DALI_STANDARD_COMMOND_RECALL_MAX_LEVEL,1,DALI_SEND_NO_ACK);
		}
	 }
	 time_count++;
	 
	 
    osDelay(5);
  }
  /* USER CODE END daliTask_hook */
}

/* USER CODE BEGIN Header_tickTask_hook */
/**
* @brief Function implementing the tickTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_tickTask_hook */
void tickTask_hook(void *argument)
{
  /* USER CODE BEGIN tickTask_hook */
  /* Infinite loop */
  
  for(;;)
  {
	lp_sys_os_tick_hook();
	lp_timer_loop_handler();
    osDelay(1);
  }
  /* USER CODE END tickTask_hook */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
lp_err_t lp_usbhid_send(lp_u8_t *report, lp_u16_t len)
{
	lp_err_t ret = LP_FAIL;
	lp_u8_t usb_ret;
	static lp_u8_t usb_error = 0;
	
	if(usb_error > 10){
	
		return LP_FAIL;
	}
	
	usb_ret = USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS, (lp_u8_t*)report,len);
	if(usb_ret == USBD_OK)
	{
		LOG_OUT("usb send usb_ret:%d \r\n",usb_ret);
	}
	else if(usb_ret == USBD_BUSY){
		LOG_OUT("usb send usb_ret:%d \r\n",usb_ret);
		while(USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS, (lp_u8_t*)report,len) != USBD_OK);
	}
	else{
		usb_error++;
		LOG_OUT("usb send error :%d \r\n",usb_ret);
	}
	
	return ret;
}


void usbTask_hook(void *argument)
{
  lp_err_t ret = LP_FAIL;
  lp_dataPack_t datapack;
  lp_u8_t buf_temp[64] = {0x0};
  
  lp_u8_t header_len = sizeof(lp_dataHeader_t);
  lp_int_t data_len = 0;
  lp_u8_t already_send = 0;
	
  for(;;)
  {
	if(lp_sys_os_queue_empty(usb_sendQueue) != LP_OK){
		ret = lp_sys_os_queue_pop(usb_sendQueue,&datapack);
		if(ret == LP_OK){//hid 设备必须固定字节数传输 64字节
			LOG_OUT("usb send datapack.len:%d,datapack.opcode:0x%x\r\n",datapack.dataHeader.dataLen,datapack.dataHeader.opcode);
			//LOG_U16_ARRAY((lp_u16_t*)datapack.buf,(datapack.dataHeader.dataLen-header_len)/2);
			if(datapack.dataHeader.dataLen <= 64){
				memset(buf_temp,0x00,sizeof(buf_temp));
				memcpy(buf_temp,(lp_u8_t*)&datapack.dataHeader,header_len);
				if(datapack.buf != NULL){
					data_len = datapack.dataHeader.dataLen - header_len;
					memcpy(buf_temp+header_len,(lp_u8_t*)datapack.buf,data_len);
					//LOG_U16_ARRAY((lp_u16_t*)buf_temp,sizeof(buf_temp)/2);
					//while(USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS, (lp_u8_t*)buf_temp, sizeof(buf_temp)) != USBD_OK);
					lp_usbhid_send((lp_u8_t*)buf_temp,sizeof(buf_temp));
					lp_sys_os_men_free(datapack.buf);
				}
				else{
					lp_usbhid_send((lp_u8_t*)buf_temp,sizeof(buf_temp));
				}
			}
			else{
				
				memset(buf_temp,0x00,sizeof(buf_temp));
				memcpy(buf_temp,(lp_u8_t*)&datapack.dataHeader,header_len);
				memcpy((lp_u8_t*)(buf_temp+header_len),(lp_u8_t*)datapack.buf,64-header_len);
				//发送第一包
				//LOG_U16_ARRAY((lp_u16_t*)buf_temp,sizeof(buf_temp)/2);
				lp_usbhid_send((lp_u8_t*)buf_temp,sizeof(buf_temp));
				data_len = datapack.dataHeader.dataLen - 64;
				
				memset(buf_temp,0x00,sizeof(buf_temp));
				already_send = 64 - header_len; //60
				
				
				while(data_len)
				{
					memset(buf_temp,0x00,sizeof(buf_temp));
					if(data_len>64){
						memcpy((lp_u8_t*)buf_temp,(lp_u8_t*)(datapack.buf+already_send),64);
						LOG_U16_ARRAY((lp_u16_t*)buf_temp,sizeof(buf_temp)/2);
						lp_usbhid_send((lp_u8_t*)buf_temp,sizeof(buf_temp));
						data_len -=64;
						already_send +=64;						
					}
					else{
						
						memcpy((lp_u8_t*)buf_temp,(lp_u8_t*)(datapack.buf+already_send),data_len);
						//LOG_U16_ARRAY((lp_u16_t*)buf_temp,sizeof(buf_temp)/2);
						lp_usbhid_send((lp_u8_t*)buf_temp,sizeof(buf_temp));
						already_send +=data_len;
						data_len = 0;
					}

				}
				lp_sys_os_men_free(datapack.buf);
			}
			LOG_OUT("usb send datapack finish already_send:%d\r\n",already_send);
			
		}
		
	}
	osDelay(10);
  }

}


void keyTask_hook(void *argument)
{

	lp_pdev_key  pkey = lp_getKEYDevice();

	while(1)
	{
	
		pkey->Run10ms(pkey);
		osDelay(10);
	}
}
/* USER CODE END Application */

