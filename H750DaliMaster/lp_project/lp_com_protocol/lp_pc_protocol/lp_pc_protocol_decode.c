

#include "lp_pc_protocol/lp_pc_protocol_decode.h"
#include "lp_sys_os_queue/lp_sys_os_queue.h"
#include "lp_dali_stack/lp_dali_master.h"




lp_err_t lp_pc_protocol_decode(lp_dataPack_t datapack)
{
	lp_err_t ret = LP_OK;
	lp_u8_t addr = 0;
	lp_u8_t set_addr = 0;
	lp_u8_t level = 0;
	lp_u8_t maxlevel = 0;
	lp_u8_t minlevel = 0;
	lp_dali_devices_data_t dev_info;
	lp_pdali_master_t dali_master = lp_getDALI_master(LP_DALI_MASTER0);
	
	switch(datapack.dataHeader.opcode){
		case LP_PC_COM_DALI_DOWN_CHECK_E:
			LOG_OUT("LP_PC_COM_DALI_DOWN_CHECK_E \r\n");
			ret = dali_master->check_device(dali_master);
			break;
		case LP_PC_COM_DALI_DOWN_EXTEND_CHECK_E:
			LOG_OUT("LP_PC_COM_DALI_DOWN_EXTEND_CHECK_E \r\n");
			ret = dali_master->search_device(dali_master,false);
			break;
		case LP_PC_COM_DALI_DOWN_SEARCH_AGAIN_E:
			LOG_OUT("LP_PC_COM_DALI_DOWN_SEARCH_AGAIN_E \r\n");
			ret = dali_master->search_device(dali_master,true);
		
			break;
		case LP_PC_COM_DALI_DOWN_READ_INFO_E:
			addr  =  datapack.buf[0];
			LOG_OUT("LP_PC_COM_DALI_DOWN_READ_INFO_E addr:%d\r\n",addr);
			ret = dali_master->read_dev_info(dali_master,addr,&dev_info);
			ret = send_dali_dev_info((lp_u8_t*)&dev_info,sizeof(lp_dali_devices_data_t));
			ret = dali_master->get_device_info(dali_master,(addr<<1)|0X01); //获取新的数据
		
			break;	
		case LP_PC_COM_DALI_DOWN_WRITE_INFO_E:
			LOG_OUT("LP_PC_COM_DALI_DOWN_WRITE_INFO_E \r\n");
			addr  =  datapack.buf[0];
			maxlevel = datapack.buf[1];
			minlevel = datapack.buf[2];
			dev_info.addr = addr;
			dev_info.max_level = maxlevel;
			dev_info.min_level = minlevel;
			ret = dali_master->write_dev_info(dali_master,addr,&dev_info);
			break;
		
		case LP_PC_COM_DALI_DOWN_SET_ADDR_E:
			addr  =  datapack.buf[0];
			set_addr =  datapack.buf[1];
			LOG_OUT("LP_PC_COM_DALI_DOWN_SET_ADDR_E addr:%d set_addr:%d\r\n",addr,set_addr);
			break;
		case LP_PC_COM_DALI_DOWN_SET_MAXLIGHT_E:
			addr  =  datapack.buf[0];
			level = datapack.buf[1];
			LOG_OUT("LP_PC_COM_DALI_DOWN_SET_MAXLIGHT_E addr:%d maxlevel:%d\r\n",addr,level);
			
		   break;
	   case LP_PC_COM_DALI_DOWN_SET_MINLIGHT_E: 
		   addr  =  datapack.buf[0];
		   level = datapack.buf[1];
		   LOG_OUT("LP_PC_COM_DALI_DOWN_SET_MINLIGHT_E addr:%d minlevel:%d\r\n",addr,level);
		   break;
	   case LP_PC_COM_DALI_DOWN_ACTIVE_MAXLIGHT_E:
		   addr  =  datapack.buf[0];
		   LOG_OUT("LP_PC_COM_DALI_DOWN_ACTIVE_MAXLIGHT_E addr:%d\r\n",addr);
		   ret = dali_master->active_maxlevel(dali_master,addr);
		   break;
	   case LP_PC_COM_DALI_DOWN_SET_DAPC_E:
		   addr  =  datapack.buf[0];
		   level = datapack.buf[1];
	       LOG_OUT("LP_PC_COM_DALI_DOWN_SET_DAPC_E addr:%d level:%d\r\n",addr,level);
		   ret = dali_master->set_dacp(dali_master,addr,level);
		   break;
	   case LP_PC_COM_DALI_DOWN_SET_OFF_LIGHT_E:
		   addr  =  datapack.buf[0];
		   LOG_OUT("LP_PC_COM_DALI_DOWN_SET_OFF_LIGHT_E addr:%d \r\n",addr);
		   ret = dali_master->active_turnoff(dali_master,addr);
		   break;
		
		
		
		default:
			ret = LP_FAIL;
			break;
		
	}
	
	return ret;	
}





































