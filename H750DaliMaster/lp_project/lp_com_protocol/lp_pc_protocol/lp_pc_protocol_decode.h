

#ifndef _LP_PC_PROTOCOL_DECODE_
#define _LP_PC_PROTOCOL_DECODE_

#ifdef __cplusplus
extern "C" {
#endif

#include "config/lp_config.h"
#include "lp_pc_protocol/lp_pc_protocol.h"


lp_err_t lp_pc_protocol_decode(lp_dataPack_t datapack);

#ifdef __cplusplus
}
#endif

#endif

