

#include "lp_pc_protocol/lp_pc_protocol.h"
#include "lp_sys_os_queue/lp_sys_os_queue.h"

extern lp_sys_os_queue_t usb_sendQueue;


static lp_err_t lp_pc_send_command(lp_u16_t opcode,void* data,lp_u16_t len)
{
	lp_err_t ret = LP_OK;
	lp_dataPack_t datapack;
	
	datapack.dataHeader.opcode = opcode;
	if(data != NULL){
		
		datapack.buf = (lp_u8_t*)lp_sys_men_malloc(len);
		if(datapack.buf == NULL){
			LOG_OUT("lp_pc_send_command error \r\n");
			ret = LP_FAIL;
		}
		else{
			memcpy(datapack.buf,(lp_u8_t*)data,len);
			//LOG_U16_ARRAY((lp_u16_t*)datapack.buf,len/2);
			datapack.dataHeader.dataLen = len+sizeof(lp_dataHeader_t); //header + data
			ret  = lp_sys_os_queue_push(usb_sendQueue,&datapack);
			if(ret != LP_OK){
			LOG_OUT("lp_pc_send_command push queue fail \r\n");	
			}
		}
	}
	
	return ret;
}


//send dali master command
lp_err_t send_dali_master_command(lp_u8_t* data,lp_u16_t len)
{
	return lp_pc_send_command(LP_PC_COM_DALI_UP_ORDER_E,data,len);
}

//send dali log command
lp_err_t send_dali_log_command(lp_u8_t* data,lp_u16_t len)
{
	return lp_pc_send_command(LP_PC_COM_DALI_UP_LOG_E,data,len);
}

//send searched dev num
//64bit data,1 express addr have dev
//0~63bit
lp_err_t send_dali_searched_dev(lp_u8_t* data,lp_u16_t len)
{
	return lp_pc_send_command(LP_PC_COM_DALI_UP_REPORT_DEV_E,data,len);
}


//send dev info
//
//
lp_err_t send_dali_dev_info(lp_u8_t* data,lp_u16_t len)
{
	return lp_pc_send_command(LP_PC_COM_DALI_UP_REPORT_INFO_E,data,len);
}

















