# lower_computer_protocol

## 下位机通讯协议：

数据头：

```c
struct DataHeader
{
	lp_u16_t dataLen; //两个字节后面数据的长度
	lp_u16_t opcode;
};
lp_u8_t data //根据不同的opcode数据格式可以不一样
```



## opcode指令

```c
// DALI 0X1000  开始
//up 上行命令
//down 下行命令
typedef enum 
{
	LP_PC_COM_DALI_UP_LOG_E = 0x1000, 
    LP_PC_COM_DALI_UP_ORDER_E,
    LP_PC_COM_DALI_DOWN_CHECK_E,
    LP_PC_COM_DALI_DOWN_EXTEND_CHECK_E,
    LP_PC_COM_DALI_DOWN_SEARCH_AGAIN_E,
    LP_PC_COM_DALI_UP_REPORT_DEV_E,
    LP_PC_COM_DALI_DOWN_ENQUIRE_INFO_E,
    LP_PC_COM_DALI_UP_REPORT_INFO_E,
    
}lp_pc_commond_dali_e;

```

## 指令详细说明

### LP_PC_COM_DALI_UP_LOG_E

```c
lp_u16_t data[] //脉冲时间长度
    
```

### LP_PC_COM_DALI_UP_ORDER_E

```c
lp_u8_t data[] //主机发送什么指令就上报什么指令
```



### LP_PC_COM_DALI_DOWN_CHECK_E

```c
//无数据
//启动下位机设备检查功能
//检测完毕后自动上报设备
```

### LP_PC_COM_DALI_DOWN_EXTEND_CHECK_E

```c
//无数据
//启动下位机设备扩展检查功能
//检测完毕后自动上报设备
```

### LP_PC_COM_DALI_DOWN_SEARCH_AGAIN_E

```c
//无数据
//启动下位机设备重新枚举功能
//检测完毕后自动上报设备
```

### LP_PC_COM_DALI_UP_REPORT_DEV_E

```c
lp_u32_t data[2] //两个32位的数据表示dali地址对应的地址，0表示无设备，1表示该地址有设备
```



### LP_PC_COM_DALI_DOWN_ENQUIRE_INFO_E

```c
lp_u8_t data[] //dali地址
//实时查询
//启动下位机设备查询功能
//检测完毕后自动上报数据
```

### LP_PC_COM_DALI_UP_REPORT_INFO_E

```c
lp_u8_t addr;
lp_u8_t device_type;//设备类型
lp_u8_t status;//设备状态
lp_u8_t lightness;//设备亮度
lp_u8_t version_number;
lp_u8_t min_level;
lp_u8_t max_level;
    
```









