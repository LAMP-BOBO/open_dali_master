


#ifndef _LP_PC_PROTOCOL_
#define _LP_PC_PROTOCOL_

#ifdef __cplusplus
extern "C" {
#endif

#include "config/lp_config.h"


typedef struct _lp_dataHeader
{
    lp_u16_t dataLen; //两个字节后面数据的长度
    lp_u16_t opcode;

}lp_dataHeader_t,*lp_pdataHeader_t;

//DALI 0X1000  开始
//up 上行命令
//down 下行命令
typedef enum 
{
	LP_PC_COM_DALI_UP_LOG_E = 0x1000, 
    LP_PC_COM_DALI_UP_ORDER_E,
    LP_PC_COM_DALI_DOWN_CHECK_E,
    LP_PC_COM_DALI_DOWN_EXTEND_CHECK_E,
    LP_PC_COM_DALI_DOWN_SEARCH_AGAIN_E,
    LP_PC_COM_DALI_UP_REPORT_DEV_E,
    LP_PC_COM_DALI_UP_REPORT_INFO_E,
    LP_PC_COM_DALI_DOWN_READ_INFO_E,
    LP_PC_COM_DALI_DOWN_WRITE_INFO_E,
	LP_PC_COM_DALI_DOWN_SET_ADDR_E,
    LP_PC_COM_DALI_DOWN_SET_MAXLIGHT_E,
    LP_PC_COM_DALI_DOWN_SET_MINLIGHT_E,
    LP_PC_COM_DALI_DOWN_ACTIVE_MAXLIGHT_E,
    LP_PC_COM_DALI_DOWN_SET_DAPC_E,
    LP_PC_COM_DALI_DOWN_SET_OFF_LIGHT_E,
    
}lp_pc_commond_dali_e;


typedef struct _lp_dataPack
{
    lp_dataHeader_t dataHeader;
    lp_u8_t* buf;

}lp_dataPack_t,*lp_pdataPack_t;

typedef struct _lp_usbOrigin
{
    lp_u8_t buf[64];

}lp_usbOrigin_t,*lp_pusbOrigin_t;


lp_err_t send_dali_master_command(lp_u8_t* data,lp_u16_t len);
lp_err_t send_dali_log_command(lp_u8_t* data,lp_u16_t len);
lp_err_t send_dali_searched_dev(lp_u8_t* data,lp_u16_t len);
lp_err_t send_dali_dev_info(lp_u8_t* data,lp_u16_t len);

#ifdef __cplusplus
}
#endif

#endif

























