






/* define to prevent recursive inclusion -------------------------------------*/
#ifndef _LP_COMMUNI_TASK_
#define _LP_COMMUNI_TASK_

#ifdef __cplusplus
extern "C" {
#endif

/* includes ------------------------------------------------------------------*/

#include "config/lp_config.h"

/* exported functions ------------------------------------------------------- */
lp_err_t lp_communi_task_init(void);
lp_err_t lp_communi_task_start(void);

void aero_dali_time_dimmer(uint8_t level, uint8_t fadeIndex,lp_u16_t tempTC);
void aero_dali_practical_dimmer(uint8_t level,lp_u16_t tempTC);

#ifdef __cplusplus
}
#endif

#endif


