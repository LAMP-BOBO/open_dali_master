
#include "lp_main_task.h"
#include "lp_sys_os_mem/lp_sys_os_mem.h"
#include "lp_sys_os_sftimer/lp_sys_os_sftimer.h"
#include "lp_sys_os_flash/lp_sys_os_flash.h"
#include "lp_led_task.h"
#include "lp_dali_task.h"
#include "lp_communi_task.h"
#include "lp_storage_task.h"





void at32_board_init(void);
lp_err_t lp_board_init(void)
{
	
	lp_err_t ret = LP_OK;
	
	#if LOG_SYSTEM_ENABLE
	Log_init("log_rtt");
	#endif
	
	lp_sys_os_mem_init();
	
	at32_board_init();

	lp_sys_os_timer_func_init();
	ret = lp_led_task_init();
	ret = lp_dali_task_init();
	ret = lp_communi_task_init();
	ret = lp_storage_task_init();
	LOG_OUT("system init!\r\n");
	
	
	
	return ret;
}


void lp_main_task(void)
{
	lp_board_init();
	
	lp_led_task_start();
	lp_communi_task_start();
	lp_dali_task_start();
	lp_storage_task_start();
	while(1)
	{
		lp_timer_loop_handler();
	}
}
























