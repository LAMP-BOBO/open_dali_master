

#include "lp_led_task.h"
#include "lp_dali/aero_dali_command.h"
#include "lp_sys_os_sftimer/lp_sys_os_sftimer.h"
#include "lp_led/lp_dev_led.h"


lp_pdev_led      p_led1;
struct lp_timer* timer_led_task = NULL;

void led_task_timeout_callback(struct lp_timer* timer)
{
	if((dali_get_short_addr()>=0) && (dali_get_short_addr()<63))
	{
		if(dali_get_level()>0){
			p_led1->Turn_on(p_led1);
		}else{
			p_led1->Turn_off(p_led1);
		}
	}
	else{
		p_led1->Toggout(p_led1);
	}	
}


lp_err_t lp_led_task_init(void)
{
	lp_err_t ret = LP_OK;
	p_led1 = lp_getLEDDevice(LED_RED);

	p_led1->Init(p_led1);
	timer_led_task = lp_sys_os_timer_create(led_task_timeout_callback);
    if (timer_led_task == NULL)
    {
        LOG_OUT("timer_led_task create error!\n");
		ret = LP_FAIL;
        return ret;
    }
	
	return ret;
}


lp_err_t lp_led_task_start(void)
{
	return lp_sys_os_timer_start(timer_led_task, TIMER_MODE_LOOP, 1000);
}











