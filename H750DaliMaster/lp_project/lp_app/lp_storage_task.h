

/* define to prevent recursive inclusion -------------------------------------*/
#ifndef _LP_STORAGE_TASK_
#define _LP_STORAGE_TASK_

#ifdef __cplusplus
extern "C" {
#endif

/* includes ------------------------------------------------------------------*/

#include "config/lp_config.h"

//eeprom
#define LP_EEPROM_INIT_INDEX 0
#define LP_EEPROM_ADDR_INDEX 1
#define LP_EEPROM_INIT_OK  	 0x6688
#define LP_EEPROM_INIT_NO  	 0xFFFF

//flash
#define	LP_DALI_DATA_FLASH_ADDR			0x8000000 + 1024 * 51
#define	LP_DALI_SCENE_FLASH_ADDR		0x8000000 + 1024 * 52

typedef enum
{
	LP_STORAGE_IDLE_E = 0x00,
	LP_STORAGE_UPDATA_E,
	LP_STORAGE_ALL_RESET_E,
	

	
} lp_storage_state_e;

/* exported functions ------------------------------------------------------- */
lp_err_t lp_dali_addr_updata(void);
lp_err_t lp_dali_data_updata(void);
lp_err_t lp_dali_scene_updata(void);
lp_err_t lp_dali_all_updata(void);
lp_err_t lp_dali_flash_restore_factory(void);
lp_err_t lp_dali_reset_random_addr(void);

lp_err_t lp_storage_task_init(void);
lp_err_t lp_storage_task_start(void);

#ifdef __cplusplus
}
#endif

#endif






















