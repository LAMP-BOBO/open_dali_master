

#include "lp_storage_task.h"
#include "lp_sys_os_cpu/lp_sys_os_cpu.h"
#include "lp_sys_os_sftimer/lp_sys_os_sftimer.h"
#include "lp_sys_os_serial/lp_sys_os_serial.h"
#include "lp_sys_os_random/lp_sys_os_random.h"
#include "lp_sys_os_dali/lp_sys_os_dali.h"
#include "lp_dali/aero_dali_command.h"
#include "lp_sys_os_flash/lp_sys_os_flash.h"
#include "lp_sys_os_random/lp_sys_os_random.h"

struct lp_timer* timer_storage_task = NULL;


lp_err_t lp_dali_read_data(pdali_data_t dali_data)
{
	lp_err_t ret = LP_OK;
	
	LOG_OUT("lp_dali_read_data len %d\r\n",sizeof(dali_data_t)/2);
	ret = lp_sys_os_flash_read(LP_DALI_DATA_FLASH_ADDR,(lp_u16_t*)dali_data,sizeof(dali_data_t)/2);
	if(ret != LP_OK){
		LOG_OUT("lp_dali_read_data fail\r\n");
	}
	
	return ret;
}

lp_err_t lp_dali_write_data(pdali_data_t dali_data)
{
	lp_err_t ret = LP_OK;
	
	LOG_OUT("lp_dali_write_data len %d\r\n",sizeof(dali_data_t)/2);
	ret = lp_sys_os_flash_write(LP_DALI_DATA_FLASH_ADDR,(lp_u16_t*)dali_data,sizeof(dali_data_t)/2);
	if(ret != LP_OK){
		LOG_OUT("lp_dali_read_data fail\r\n");
	}
	
	return ret;
}


lp_err_t lp_dali_read_scene(pdali_scene_data_t dali_scene)
{
	lp_err_t ret = LP_OK;
	
	LOG_OUT("lp_dali_read_scene len %d\r\n",sizeof(dali_scene_data_t)/2);
	ret = lp_sys_os_flash_read(LP_DALI_SCENE_FLASH_ADDR,(lp_u16_t*)dali_scene,sizeof(dali_scene_data_t)/2);
	if(ret != LP_OK){
		LOG_OUT("lp_dali_read_scene fail\r\n");
	}
	
	return ret;
}


lp_err_t lp_dali_write_scene(pdali_scene_data_t dali_scene)
{
	lp_err_t ret = LP_OK;
	
	LOG_OUT("lp_dali_write_scene len %d\r\n",sizeof(dali_scene_data_t)/2);
	ret = lp_sys_os_flash_write(LP_DALI_SCENE_FLASH_ADDR,(lp_u16_t*)dali_scene,sizeof(dali_scene_data_t)/2);
	if(ret != LP_OK){
		LOG_OUT("lp_dali_read_scene fail\r\n");
	}
	
	return ret;
}

lp_err_t lp_dali_reset_random_addr(void)
{
	lp_err_t ret = LP_OK;
	lp_u8_t random_data[3] = {0};
	
	
	ret = lp_sys_os_random_produce();
	ret = lp_sys_os_random_read(random_data,3);
	if(ret == LP_OK){
		dali_set_random_addr(random_data[0],random_data[1],random_data[2]);
	}
	else{
		ret = LP_FAIL;
		LOG_OUT("DALI lp_dali_reset_random_addr fail\r\n");
	}
	
	return ret;
}

lp_err_t lp_dali_data_storage_init(void)
{
	lp_err_t ret = LP_OK;
	lp_u16_t init_flag = 0x00;
	lp_u16_t dali_addr = 0xff;
	
	ret = lp_sys_os_eeprom_init();
	ret = lp_sys_os_eeprom_read(LP_EEPROM_INIT_INDEX,&init_flag);
	if(LP_EEPROM_INIT_OK == init_flag){
		LOG_OUT("dev flash is old!\r\n");
		pdali_data_t pdali_data = dali_get_pdata();
		ret =  lp_dali_read_data(pdali_data);
		
		pdali_scene_data_t pdali_scene = dali_get_pscene();
		ret =  lp_dali_read_scene(pdali_scene);
		
		ret = lp_sys_os_eeprom_read(LP_EEPROM_ADDR_INDEX,&dali_addr);
		dali_set_short_addr((lp_u8_t)dali_addr);
	}
	else{
		LOG_OUT("dev flash is new!\r\n");
		aero_dali_reset_data();
		lp_dali_reset_random_addr();
		lp_dali_all_updata();
		
	}
	
	return ret;
}

lp_err_t lp_dali_flash_restore_factory(void)
{
	lp_err_t ret = LP_OK;
	
	ret = lp_sys_os_eeprom_write(LP_EEPROM_INIT_INDEX,LP_EEPROM_INIT_NO);
	//mcu must reset
	lp_sys_os_SystemReset();
	return ret;
}


static lp_u8_t dali_addr_storage_flag = LP_STORAGE_IDLE_E;
static lp_u8_t dali_data_storage_flag = LP_STORAGE_IDLE_E;
static lp_u8_t dali_scene_storage_flag = LP_STORAGE_IDLE_E;
static lp_u8_t dali_init_storage_flag = LP_STORAGE_IDLE_E;

void storage_task_timeout_callback(struct lp_timer* timer)
{
	lp_err_t ret = LP_OK;
	
	if(dali_addr_storage_flag == LP_STORAGE_UPDATA_E)
	{
		lp_u16_t dali_addr = (lp_u16_t)dali_get_short_addr();
		ret = lp_sys_os_eeprom_write(LP_EEPROM_ADDR_INDEX,dali_addr);
		if(ret == LP_OK){
			dali_addr_storage_flag = LP_STORAGE_IDLE_E;
		}else{
			LOG_OUT("ERROR! dali_addr_storage_flag\r\n");
		}
	}
	
	if(dali_data_storage_flag == LP_STORAGE_UPDATA_E)
	{
		pdali_data_t pdali_data = dali_get_pdata();
		ret =  lp_dali_write_data(pdali_data);
		if(ret == LP_OK){
			dali_data_storage_flag = LP_STORAGE_IDLE_E;
		}else{
			LOG_OUT("ERROR! dali_data_storage_flag\r\n");
		}
	}
	
	if(dali_scene_storage_flag == LP_STORAGE_UPDATA_E)
	{
		pdali_scene_data_t pdali_scene = dali_get_pscene();
		ret =  lp_dali_write_scene(pdali_scene);
		if(ret == LP_OK){
			dali_scene_storage_flag = LP_STORAGE_IDLE_E;
		}else{
			LOG_OUT("ERROR! dali_scene_storage_flag\r\n");
		}
	}
	
	if(dali_init_storage_flag == LP_STORAGE_UPDATA_E)
	{
		ret = lp_sys_os_eeprom_write(LP_EEPROM_INIT_INDEX,LP_EEPROM_INIT_OK);
		if(ret == LP_OK){
			dali_init_storage_flag = LP_STORAGE_IDLE_E;
		}else{
			LOG_OUT("ERROR! dali_init_storage_flag\r\n");
		}
	}
}

lp_err_t lp_dali_addr_updata(void)
{
	dali_addr_storage_flag = LP_STORAGE_UPDATA_E;
	return LP_OK;
}

lp_err_t lp_dali_data_updata(void)
{

	dali_data_storage_flag = LP_STORAGE_UPDATA_E;
	lp_sys_os_timer_restart(timer_storage_task);
	return LP_OK;
}
	
lp_err_t lp_dali_scene_updata(void)
{
	dali_scene_storage_flag = LP_STORAGE_UPDATA_E;
	lp_sys_os_timer_restart(timer_storage_task);
	return LP_OK;
}

lp_err_t lp_dali_all_updata(void)
{
	dali_addr_storage_flag = LP_STORAGE_UPDATA_E;
	dali_data_storage_flag = LP_STORAGE_UPDATA_E;
	dali_scene_storage_flag = LP_STORAGE_UPDATA_E;
	dali_init_storage_flag = LP_STORAGE_UPDATA_E;
	
	return LP_OK;
}


lp_err_t lp_storage_task_init(void)
{
	lp_err_t ret = LP_OK;
	
	ret = lp_sys_os_random_init();
	ret = lp_dali_data_storage_init();
	timer_storage_task = lp_sys_os_timer_create(storage_task_timeout_callback);
    if (timer_storage_task == NULL)
    {
        LOG_OUT("timer_storage_task create error!\n");
		ret = LP_FAIL;
        return ret;
    }
	
	return ret;
}


lp_err_t lp_storage_task_start(void)
{
	return lp_sys_os_timer_start(timer_storage_task, TIMER_MODE_LOOP,2000);
}





