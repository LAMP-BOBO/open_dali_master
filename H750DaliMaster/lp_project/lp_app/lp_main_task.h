

/* define to prevent recursive inclusion -------------------------------------*/
#ifndef _LP_MAIN_TASK_
#define _LP_MAIN_TASK_

#ifdef __cplusplus
extern "C" {
#endif

/* includes ------------------------------------------------------------------*/

#include "config/lp_config.h"

/* exported functions ------------------------------------------------------- */
void lp_main_task(void);

#ifdef __cplusplus
}
#endif

#endif










