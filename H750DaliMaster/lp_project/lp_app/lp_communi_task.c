



#include "lp_communi_task.h"
#include "lp_storage_task.h"
#include "lp_dali/aero_dali_command.h"
#include "lp_serial/lp_dev_serial.h"
#include "lp_sys_os_sftimer/lp_sys_os_sftimer.h"
#include "lp_sys_os_serial/lp_sys_os_serial.h"
#include "lp_sys_os_random/lp_sys_os_random.h"
#include "lp_sys_os_boot/lp_sys_os_boot.h"

const lp_u16_t dimmerCurve[256]={0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
	2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,
	6,6,6,6,6,6,7,7,7,7,8,8,8,8,8,9,9,9,9,10,10,10,11,11,11,12,12,12,12,13,13,14,14,14,
	15,15,16,16,17,17,18,18,19,19,20,20,21,21,22,23,23,24,25,25,26,27,27,28,29,30,31,32,
	32,33,34,35,36,37,38,39,40,42,43,44,45,46,48,49,50,52,53,55,56,58,60,61,63,65,66,68,
	70,72,74,76,78,81,83,85,88,90,93,95,98,100,103,106,109,112,115,118,122,125,128,132,136,
	140,143,147,151,156,160,165,169,174,178,183,189,194,199,205,210,216,222,228,235,241,248,
	255,262,269,277,284,292,300,309,317,326,335,344,354,364,374,384,395,406,417,429,440,452,
	465,478,491,505,519,533,548,563,579,595,611,628,646,663,682,701,720,740,761,782,803,825,
	848,872,896,921,946,973,1000,1000};


lp_pdev_serial lp_dev_serial_dim = NULL;
struct lp_timer* timer_communi_task = NULL;

lp_err_t lp_dev_serial_handle(lp_u8_t* recBuff,lp_u16_t recLen);

lp_u8_t	read_buf[32] = {0};
lp_u16_t read_len = 0;

void communi_task_timeout_callback(struct lp_timer* timer)
{
	lp_err_t ret = LP_FAIL;

	if(lp_dev_serial_dim != NULL)
	{
		ret = lp_dev_serial_dim->Read(lp_dev_serial_dim,read_buf,&read_len);
		if(ret == LP_OK){
			LOG_OUT("communi_task ret %d len %d buf %s!\r\n",ret,read_len,read_buf);
			//ret = lp_dev_serial_dim->Write(lp_dev_serial_dim,read_buf,read_len);
			ret =  lp_dev_serial_handle(read_buf,read_len);
		}
		
	}
	
}


lp_err_t lp_communi_task_init(void)
{
	lp_err_t ret = LP_OK;

	lp_dev_serial_dim =  lp_getSERIALDevice(UART1_DIM);
	if(lp_dev_serial_dim == NULL)
	{
		LOG_OUT("lp_dev_serial_dim get error!\n");
		ret = LP_FAIL;
        return ret;
	}
	
	lp_dev_serial_dim->Init(lp_dev_serial_dim);
	lp_dev_serial_dim->Open(lp_dev_serial_dim);
	timer_communi_task = lp_sys_os_timer_create(communi_task_timeout_callback);
    if (timer_communi_task == NULL)
    {
        LOG_OUT("timer_communi_task create error!\n");
		ret = LP_FAIL;
        return ret;
    }
	
	return ret;
}



lp_err_t lp_communi_task_start(void)
{
	return lp_sys_os_timer_start(timer_communi_task, TIMER_MODE_LOOP, 50);
}


lp_err_t send_dali_short_addr(lp_u8_t  addr)
{
	lp_u8_t sendata[10];
	lp_err_t ret = LP_OK;
	
	memset(sendata,0,10);
	sendata[0] = 0xAA;
    sendata[1] = 0xAF;
    sendata[2] = 0x13;
    sendata[3] = 0x05;
    sendata[4] = 0x0c;
	sendata[5] = addr;
    for(int i=0;i<9;i++)
    {
        sendata[9] += sendata[i];
    }
	
	ret = lp_dev_serial_dim->Write(lp_dev_serial_dim,sendata,sizeof(sendata));
	if(ret != LP_OK){
		LOG_OUT("send_dali_short_addr fail\n");
		ret = LP_FAIL;
	}
	
	return ret;
}

lp_err_t send_dimmer_data(lp_u8_t level,lp_u16_t tempTC)
{
	lp_u8_t sendata[10];
	lp_err_t ret = LP_OK;
	
    sendata[0] = 0xAA;
    sendata[1] = 0xAF;
    sendata[2] = 0x13;
    sendata[3] = 0x05;
    sendata[4] = 0x01;
    sendata[5] = dimmerCurve[level];
    sendata[6] = dimmerCurve[level]>>8;

	uint16_t color = MIREK_CHANG_TC(tempTC);
	if(color > PHY_COOL_TC)
	{
		color = PHY_COOL_TC;
	}
	else if(color < PHY_WARM_TC)
	{
		color = PHY_WARM_TC;
	}
	
    sendata[7] = color;
    sendata[8] = color>>8;
    sendata[9] = 0x00;
    for(int i=0;i<9;i++)
    {
        sendata[9] += sendata[i];
    }
	
	ret = lp_dev_serial_dim->Write(lp_dev_serial_dim,sendata,sizeof(sendata));
	if(ret != LP_OK){
		LOG_OUT("send_dali_short_addr fail\n");
		ret = LP_FAIL;
	}
	
	return ret;
}

void aero_dali_time_dimmer(uint8_t level, uint8_t fadeIndex,lp_u16_t tempTC) //fadeTime dimmer
{
	send_dimmer_data(level,tempTC);
}

void aero_dali_practical_dimmer(uint8_t level,lp_u16_t tempTC)
{
	send_dimmer_data(level,tempTC);
}

lp_err_t lp_dev_serial_handle(lp_u8_t* recBuff,lp_u16_t recLen)
{
		lp_err_t ret = LP_OK;
	    if((recBuff[0]==0xAA)&&(recBuff[1]==0xAF)&&(recBuff[2]==0x0B)&&(recBuff[3]==0x02)&&(recBuff[4]==0x09)&&(recLen==0x07))
		{
			uint8_t frame = recBuff[0]+recBuff[1]+recBuff[2]+recBuff[3]+recBuff[4]+recBuff[5];
			if(frame == recBuff[6]){
				LOG_OUT("enter boot mode!\n");
				lp_iap_system_reset();
			}
		}
		else 
		if((recBuff[0]==0xAA)&&(recBuff[1]==0xAF)&&(recBuff[2]==0x13)&&(recBuff[3]==0x04)&&(recBuff[4]==0x0a)&&(recLen==0x09))
		{
			lp_u8_t frame = recBuff[0]+recBuff[1]+recBuff[2]+recBuff[3]+recBuff[4]+recBuff[5]+recBuff[6]+recBuff[7];
			if(frame == recBuff[8])	//设置随机地址
			{
				LOG_OUT("set random 0x%x 0x%x 0x%x!\n",recBuff[5],recBuff[6],recBuff[7]);
				dali_set_random_addr(recBuff[5],recBuff[6],recBuff[7]);
			}
			
		}
		else if((recBuff[0]==0xAA)&&(recBuff[1]==0xAF)&&(recBuff[2]==0x13)&&(recBuff[3]==0x01)&&(recBuff[4]==0x0b)&&(recLen==0x06))
		{
			lp_u8_t frame = recBuff[0]+recBuff[1]+recBuff[2]+recBuff[3]+recBuff[4];
			if(frame == recBuff[5]) //请求短地址
			{
				LOG_OUT("get short addr !\n");
				lp_u8_t addr =  dali_get_short_addr();
				ret = send_dali_short_addr(addr);
			}
			
		}
		else if((recBuff[0]==0xAA)&&(recBuff[1]==0xAF)&&(recBuff[2]==0x13)&&(recBuff[3]==0x02)&&(recBuff[4]==0x0d)&&(recLen==0x07))
		{
			lp_u8_t frame = recBuff[0]+recBuff[1]+recBuff[2]+recBuff[3]+recBuff[4]+recBuff[5];
			if(frame == recBuff[6]) //设置短地址
			{
				LOG_OUT("set short addr 0x%x!\n",recBuff[5]);
				dali_set_short_addr(recBuff[5]);
			}
			
		}
		else if((recBuff[0]==0xAA)&&(recBuff[1]==0xAF)&&(recBuff[2]==0x13)&&(recBuff[3]==0x01)&&(recBuff[4]==0x0f)&&(recLen==0x06))
		{
			lp_u8_t frame = recBuff[0]+recBuff[1]+recBuff[2]+recBuff[3]+recBuff[4];
			if(frame == recBuff[5]) //恢复dali出厂设置
			{
				LOG_OUT("Restore Factory!\n");
				ret = lp_dali_flash_restore_factory();
			}
			
		}
		
	return ret;
}





























