


/* define to prevent recursive inclusion -------------------------------------*/
#ifndef _LP_LED_TASK_
#define _LP_LED_TASK_

#ifdef __cplusplus
extern "C" {
#endif

/* includes ------------------------------------------------------------------*/

#include "config/lp_config.h"

/* exported functions ------------------------------------------------------- */
lp_err_t lp_led_task_init(void);
lp_err_t lp_led_task_start(void);

#ifdef __cplusplus
}
#endif

#endif
































