

#include "lp_dali_task.h"
#include "lp_storage_task.h"
#include "lp_sys_os_sftimer/lp_sys_os_sftimer.h"
#include "lp_sys_os_serial/lp_sys_os_serial.h"
#include "lp_sys_os_random/lp_sys_os_random.h"
#include "lp_sys_os_dali/lp_sys_os_dali.h"
#include "lp_dali/aero_dali_command.h"
#include "lp_sys_os_flash/lp_sys_os_flash.h"

#include "at32f421_aero_dali_board.h"

struct lp_timer* timer_dali_task = NULL;


void dali_task_timeout_callback(struct lp_timer* timer)
{
	lp_err_t ret = LP_FAIL;
	lp_u8_t dali_buf[3] = {0};
	lp_u16_t dali_len = 0;
	uint8_t retrunData = 0;
	
	ret = lp_sys_os_dali_read(LP_DALI_BUS0_E,dali_buf,&dali_len);
	if(ret == LP_OK){
		
		uint8_t sendFlag = dali_decode(dali_buf[0],dali_buf[1]);
		if(sendFlag){
			 //Insert Random Delay
			 retrunData = dali_get_returnData();
			 ret = lp_sys_os_dali_write(LP_DALI_BUS0_E,&retrunData,1);
			 if(ret != LP_OK){
				LOG_OUT("DALI send data 0x%x fail\r\n",retrunData);
			 }
		}
	}
}


lp_err_t lp_dali_task_init(void)
{
	lp_err_t ret = LP_OK;
	
	lp_sys_os_dali_init(LP_DALI_BUS0_E);
	timer_dali_task = lp_sys_os_timer_create(dali_task_timeout_callback);
    if (timer_dali_task == NULL)
    {
        LOG_OUT("timer_communi_task create error!\n");
		ret = LP_FAIL;
        return ret;
    }
	
	return ret;
}


lp_err_t lp_dali_task_start(void)
{
	return lp_sys_os_timer_start(timer_dali_task, TIMER_MODE_LOOP,3);
}

























