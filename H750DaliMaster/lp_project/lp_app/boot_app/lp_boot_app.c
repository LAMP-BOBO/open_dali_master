

#include "lp_boot_app.h"
#include "lp_sys_os_mem/lp_sys_os_mem.h"
#include "lp_sys_os_sftimer/lp_sys_os_sftimer.h"
#include "lp_sys_os_flash/lp_sys_os_flash.h"
#include "lp_serial/lp_dev_serial.h"
#include "lp_led/lp_dev_led.h"
#include "lp_sys_os_boot/lp_sys_os_boot.h"
#include "lp_sys_os_boot/lp_sys_os_boot_updata.h"

#include "at32f421_aero_dali_board.h"
#include "at32f421_clock.h"


lp_err_t lp_boot_task_init(void);
lp_err_t lp_boot_task_start(void);
void at32_board_init(void);

lp_err_t lp_board_init(void)
{
	
	lp_err_t ret = LP_OK;
	
	#if LOG_SYSTEM_ENABLE
	Log_init("log_rtt");
	#endif
	
	lp_sys_os_mem_init();
	at32_board_init();
	lp_sys_os_timer_func_init();
	
	
	ret = lp_boot_task_init();
	LOG_OUT("system init!\r\n");

	return ret;
}


void lp_main_task(void)
{
	lp_board_init();

	lp_boot_task_start();
	while(1)
	{
		lp_timer_loop_handler();
	}
}



lp_pdev_serial lp_dev_serial_dim = NULL;
lp_pdev_led      p_led1;
struct lp_timer* timer_boot_task = NULL;
struct lp_timer* timer_enter_boot = NULL;
lp_u8_t waitState = 0;


lp_err_t lp_dev_serial_handle(lp_u8_t* recBuff,lp_u16_t recLen)
{
		lp_err_t err = LP_FAIL;
	
		lp_sys_os_timer_restart(timer_enter_boot);
		lp_u8_t res = lp_boot_revDataHandle(recBuff,recLen);
		if(res == LP_PACKAGE_OK_E)
		{
			if(waitState)
			{
				waitState = 2;
				//usart_send((uint8_t *)"ok",2);
				p_led1->Toggout(p_led1);
				err = lp_dev_serial_dim->Write(lp_dev_serial_dim,(lp_u8_t *)"ok",2);
			}
		}
		else if(res == LP_PACKAGE_FINISH_E)
		{
			if(waitState)
			{
				//startUpdataFirmware();
				//usart_send((uint8_t *)"sucess",6);
				err = lp_dev_serial_dim->Write(lp_dev_serial_dim,(lp_u8_t *)"sucess",6);
				waitState = 0;
				
			}
		}
		else if(res == LP_PACKAGE_WAIT_E)
		{
			waitState = 1;
			//usart_send((uint8_t *)"ack",3);
			err = lp_dev_serial_dim->Write(lp_dev_serial_dim,(lp_u8_t *)"ack",3);
		}
		else if(res == LP_PACKAGE_RESET_E)
		{
			//EnterUserProg(APP_START_ADDR);
			lp_app_jumpboot_system_reset();
		}
		else if(res == LP_PACKAGE_PCB_ERROR_E)
		{
			waitState = 0;
			//enterAppTimer = 1000 + systemTimerCount;
			lp_sys_os_timer_restart(timer_enter_boot);
		}
		else
		{
			if(waitState)
			{
				//usart_send((uint8_t *)"err",3);
				err = lp_dev_serial_dim->Write(lp_dev_serial_dim,(lp_u8_t *)"err",3);
				waitState = 0;
			}
		}
		
		return err;
}

lp_u8_t	read_buf[512] = {0};
lp_u16_t read_len = 0;


static void boot_task_timeout_callback(struct lp_timer* timer)
{
	lp_err_t ret = LP_FAIL;

	if(lp_dev_serial_dim != NULL)
	{
		ret = lp_dev_serial_dim->Read(lp_dev_serial_dim,read_buf,&read_len);
		if(ret == LP_OK){
			//LOG_OUT("boot_task ret %d len %d buf %s!\r\n",ret,read_len,read_buf);
			//ret = lp_dev_serial_dim->Write(lp_dev_serial_dim,read_buf,read_len);
			ret =  lp_dev_serial_handle(read_buf,read_len);
		}
		
	}
	
}

static void enter_boot_timeout_callback(struct lp_timer* timer)
{
	lp_iap_system_reset();
}

lp_err_t lp_boot_task_init(void)
{
	lp_err_t ret = LP_OK;

	p_led1 = lp_getLEDDevice(LED_RED);
	p_led1->Init(p_led1);
	
	lp_dev_serial_dim =  lp_getSERIALDevice(UART1_DIM);
	if(lp_dev_serial_dim == NULL)
	{
		LOG_OUT("lp_dev_serial_dim get error!\n");
		ret = LP_FAIL;
        return ret;
	}
	
	lp_dev_serial_dim->Init(lp_dev_serial_dim);
	lp_dev_serial_dim->Open(lp_dev_serial_dim);
	timer_boot_task = lp_sys_os_timer_create(boot_task_timeout_callback);
    if (timer_boot_task == NULL)
    {
        LOG_OUT("timer_boot_task create error!\n");
		ret = LP_FAIL;
        return ret;
    }
	
	timer_enter_boot = lp_sys_os_timer_create(enter_boot_timeout_callback);
	if (timer_enter_boot == NULL)
    {
        LOG_OUT("timer_boot_task create error!\n");
		ret = LP_FAIL;
        return ret;
    }
	
	return ret;
}



lp_err_t lp_boot_task_start(void)
{
	//开一个2s定时器复位进入boot模式
	lp_sys_os_timer_start(timer_enter_boot, TIMER_MODE_SINGLE,2000);
	return lp_sys_os_timer_start(timer_boot_task, TIMER_MODE_LOOP,5);
}





/**
  * @brief  main function.
  * @param  none
  * @retval none
  */
int main(void)
{
	lp_err_t ret = LP_OK;
	/*use hick clock*/
	system_clock_config();

	/*****USER CODE******/
	 ret = lp_isUpgradeFirmware(APP_FIRMWARE_ADDR);
	 if(ret == LP_FAIL)waitState = 1;
	 lp_main_task();
	/*****USER CODE******/
}































