

/* define to prevent recursive inclusion -------------------------------------*/
#ifndef _LP_BOOT_APP_
#define _LP_BOOT_APP_

#ifdef __cplusplus
extern "C" {
#endif

/* includes ------------------------------------------------------------------*/

#include "config/lp_config.h"


/* exported functions ------------------------------------------------------- */
void lp_main_task(void);

#ifdef __cplusplus
}
#endif

#endif












