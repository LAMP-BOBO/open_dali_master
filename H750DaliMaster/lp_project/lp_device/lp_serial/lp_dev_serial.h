#ifndef _LP_DEV_SERIAL_H
#define _LP_DEV_SERIAL_H

#ifdef __cplusplus
extern "C" {
#endif


#include "config/lp_config.h"


#define UART1_DIM 	0

typedef enum
{
  LP_USART_DATA_8BITS                       = 0x00, /*!< usart data size is 8 bits */
  LP_USART_DATA_9BITS                       = 0x01  /*!< usart data size is 9 bits */
} lp_usart_data_bit_num_type;


typedef enum
{
  LP_USART_STOP_1_BIT                       = 0x00, /*!< usart stop bits num is 1 */
  LP_USART_STOP_0_5_BIT                     = 0x01, /*!< usart stop bits num is 0.5 */
  LP_USART_STOP_2_BIT                       = 0x02, /*!< usart stop bits num is 2 */
  LP_USART_STOP_1_5_BIT                     = 0x03  /*!< usart stop bits num is 1.5 */
} lp_usart_stop_bit_num_type;


typedef struct _lp_dev_serial {
	
	lp_u8_t   which;
	lp_u32_t  bund;
	lp_u8_t	  data_bit;
	lp_u8_t	  stop_bit;
	lp_u8_t	  send_delay;

	lp_err_t (*Init)(struct _lp_dev_serial *ptLEDDevice);
	lp_err_t (*Open)(struct _lp_dev_serial *ptLEDDevice);
	lp_err_t (*Read)(struct _lp_dev_serial *ptLEDDevice,lp_u8_t *data,lp_u16_t* len);
	lp_err_t (*Write)(struct _lp_dev_serial *ptLEDDevice,lp_u8_t *data,lp_u16_t len);
	lp_err_t (*Close)(struct _lp_dev_serial *ptLEDDevice);


}lp_dev_serial, *lp_pdev_serial;


lp_pdev_serial lp_getSERIALDevice(lp_u8_t which);

#ifdef __cplusplus
}
#endif

#endif


