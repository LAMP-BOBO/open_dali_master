

#include "lp_dev_serial.h"
#include "lp_sys_os_serial/lp_sys_os_serial.h"



lp_err_t lp_SERIALDevices_Init(struct _lp_dev_serial *ptSERIALDevices)
{
	lp_err_t ret = LP_OK;
	switch(ptSERIALDevices->which){
	case UART1_DIM:
			ret = lp_sys_os_serial_init(LP_UART1_E,ptSERIALDevices->bund,ptSERIALDevices->data_bit,ptSERIALDevices->stop_bit);
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}
	
lp_err_t lp_SERIALDevices_Open(struct _lp_dev_serial *ptSERIALDevices)
{
	lp_err_t ret = LP_OK;
	switch(ptSERIALDevices->which){
	case UART1_DIM:
		ret = lp_sys_os_serial_open(ptSERIALDevices->which,ptSERIALDevices->send_delay);

	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}

lp_err_t lp_SERIALDevices_Read(struct _lp_dev_serial *ptSERIALDevices,lp_u8_t *data,lp_u16_t* len)
{
	lp_err_t ret = LP_FAIL;
	lp_u8_t data_temp[32] = {0};
	lp_u16_t data_len = 0;
	
	if(data == NULL || len==NULL ){
		ret = LP_FAIL;
		return ret;
	}
	
	switch(ptSERIALDevices->which){
	case UART1_DIM:
			if(lp_sys_os_serial_read(ptSERIALDevices->which,data_temp,&data_len) == LP_OK)
			{
				*len = data_len;
				memcpy(data,data_temp,data_len);
				ret = LP_OK;
				//LOG_OUT("lp_SERIALDevices_Read ret %d len %d buf %s!\r\n",ret,data_len,data_temp);
			}
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}

lp_err_t lp_SERIALDevices_Write(struct _lp_dev_serial *ptSERIALDevices,lp_u8_t *data,lp_u16_t len)
{
	lp_err_t ret = LP_OK;
	
	if(data == NULL || len==NULL ){
		ret = LP_FAIL;
		return ret;
	}
	
	switch(ptSERIALDevices->which){
	case UART1_DIM:
		ret =  lp_sys_os_serial_wirte(ptSERIALDevices->which,data,len);
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}

lp_err_t lp_SERIALDevices_Close(struct _lp_dev_serial *ptSERIALDevices)
{
	lp_err_t ret = LP_OK;
	switch(ptSERIALDevices->which){
	case UART1_DIM:
			ret =  lp_sys_os_serial_close(ptSERIALDevices->which);
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}


static lp_dev_serial g_tSERIALDevices[] = {
	
	{UART1_DIM,230400,LP_USART_DATA_8BITS ,LP_USART_STOP_1_BIT,100,lp_SERIALDevices_Init,lp_SERIALDevices_Open,lp_SERIALDevices_Read,lp_SERIALDevices_Write,lp_SERIALDevices_Close},
};



lp_pdev_serial lp_getSERIALDevice(lp_u8_t which)
{
	if (which >= UART1_DIM && which <= UART1_DIM)
		return &g_tSERIALDevices[which];
	else
		return NULL;
}




















