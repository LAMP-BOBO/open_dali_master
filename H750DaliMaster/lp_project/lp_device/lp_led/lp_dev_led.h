


#ifndef _LP_DEV_LED_H
#define _LP_DEV_LED_H

#ifdef __cplusplus
extern "C" {
#endif


#include "config/lp_config.h"


#define LED_RED 	0

typedef enum
{
	LP_LED_OFF_E = 0x00,
	LP_LED_ON_E= 0x01,
	
} lp_led_state_e;

//0 turn on, 1 turn off  is polarity 0, or 1 turn on, 0 turn off  is polarity 1
typedef enum
{
	LP_LED_POLARITY_LOW_E = 0x00,
	LP_LED_POLARITY_HIGH_E= 0x01,
	
} lp_led_polarity_e;


typedef struct _lp_dev_led {
	
	lp_u8_t  which;
	lp_u8_t	 polarity;  
	lp_err_t (*Init)(struct _lp_dev_led *ptLEDDevice);
	lp_err_t (*Turn_on)(struct _lp_dev_led *ptLEDDevice);
	lp_err_t (*Turn_off)(struct _lp_dev_led *ptLEDDevice);
	lp_err_t (*Toggout)(struct _lp_dev_led *ptLEDDevice);

}lp_dev_led, *lp_pdev_led;


lp_pdev_led lp_getLEDDevice(lp_u8_t which);

#ifdef __cplusplus
}
#endif

#endif





























