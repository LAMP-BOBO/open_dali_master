

#include "lp_led/lp_dev_led.h"
#include "lp_sys_os_gpio/lp_sys_os_gpio.h"





static lp_err_t lp_LEDDeviceInit(struct _lp_dev_led *ptLEDDevice)
{
	lp_err_t ret = LP_OK;
	switch(ptLEDDevice->which){
	case LED_RED:
		ret = lp_sys_os_gpio_init(LP_GPIO_LED_R_E);
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	return ret;
}


static lp_err_t lp_LEDDevice_turn_on(struct _lp_dev_led *ptLEDDevice)
{
	lp_err_t ret = LP_OK;
	switch(ptLEDDevice->which){
	case LED_RED:
		if(ptLEDDevice->polarity ==  LP_LED_POLARITY_LOW_E){
			ret =  lp_sys_os_gpio_write(LP_GPIO_LED_R_E,LP_GPIO_LOW_E);
		}
		else if(ptLEDDevice->polarity ==  LP_LED_POLARITY_HIGH_E)
		{
			ret =  lp_sys_os_gpio_write(LP_GPIO_LED_R_E,LP_GPIO_HIGH_E);
		}
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}

static lp_err_t lp_LEDDevice_turn_off(struct _lp_dev_led *ptLEDDevice)
{
	lp_err_t ret = LP_OK;
	switch(ptLEDDevice->which){
	case LED_RED:
		
		if(ptLEDDevice->polarity ==  LP_LED_POLARITY_LOW_E){
			ret =  lp_sys_os_gpio_write(LP_GPIO_LED_R_E,LP_GPIO_HIGH_E);
		}
		else if(ptLEDDevice->polarity ==  LP_LED_POLARITY_HIGH_E)
		{
			ret =  lp_sys_os_gpio_write(LP_GPIO_LED_R_E,LP_GPIO_LOW_E);
		}
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}



static lp_err_t lp_LEDDeviceToggout(struct _lp_dev_led *ptLEDDevice)
{
	lp_err_t ret = LP_OK;
	switch(ptLEDDevice->which){
	case LED_RED:
		ret = lp_sys_os_gpio_toggout(LP_GPIO_LED_R_E);
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}




static lp_dev_led g_tLEDDevices[] = {
	
	{LED_RED, LP_LED_POLARITY_LOW_E ,lp_LEDDeviceInit, lp_LEDDevice_turn_on,lp_LEDDevice_turn_off,lp_LEDDeviceToggout},
};



lp_pdev_led lp_getLEDDevice(lp_u8_t which)
{
	if (which >= LED_RED && which <= LED_RED)
		return &g_tLEDDevices[which];
	else
		return NULL;
}























