

#ifndef _LP_DEV_KEY_H
#define _LP_DEV_KEY_H

#ifdef __cplusplus
extern "C" {
#endif


#include "config/lp_config.h"


/* Button renaming */
typedef enum
{
	LP_KEY_NONE = 0,			

	LP_KEY_1_DOWN,				
	LP_KEY_1_UP,				
	LP_KEY_1_LONG,				

	LP_KEY_2_DOWN,				
	LP_KEY_2_UP,				
	LP_KEY_2_LONG,

	LP_KEY1_2_DOWN,				
	LP_KEY1_2_UP,				
	LP_KEY1_2_LONG,	
	
}LP_KEY_ENUM_E;


typedef struct _lp_dev_key {
	  
	lp_err_t (*Init)(struct _lp_dev_key *ptKEYDevice);
	lp_u8_t (*Read)(struct _lp_dev_key *ptKEYDevice);
	void (*Run10ms)(struct _lp_dev_key *ptKEYDevice);
	void (*Run1ms)(struct _lp_dev_key *ptKEYDevice);
	
}lp_dev_key, *lp_pdev_key;

lp_pdev_key lp_getKEYDevice(void);

#ifdef __cplusplus
}
#endif

#endif


























