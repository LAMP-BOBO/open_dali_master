

#include "lp_key/lp_dev_key.h"
#include "lp_sys_os_key/lp_sys_os_key.h"



static lp_err_t lp_KEYDevice_Init(struct _lp_dev_key *ptKEYDevice)
{
	lp_err_t ret = LP_OK;
	
	ret = lp_sys_os_key_init();
	
	return ret;
}

static lp_u8_t lp_KEYDevice_Read(struct _lp_dev_key *ptKEYDevice)
{
	
	return lp_GetKey();
	
}

static void lp_KEYDevice_Run10ms(struct _lp_dev_key *ptKEYDevice)
{
	lp_KeyScan10ms();
}

static void lp_KEYDevice_Run1ms(struct _lp_dev_key *ptKEYDevice)
{
	lp_KeyScan10ms();
}



static lp_dev_key g_tKEYDevices[] = {
	
	{lp_KEYDevice_Init, lp_KEYDevice_Read,lp_KEYDevice_Run10ms,lp_KEYDevice_Run1ms},
};



lp_pdev_key lp_getKEYDevice(void)
{
	return &g_tKEYDevices[0];
}






















