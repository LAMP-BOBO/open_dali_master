
lp_project 项目的目标是要成为一个通用的嵌入式应用框架。
它将包含两个语言版本C/C++
目的是为了快速构建一个产品级应用，方便代码移植和扩展。

他将会包含的部分
1.bsp 各种板级底层驱动部分
2.嵌入式行业常用协议栈部分
3.简化上层逻辑处理


lp_app 			用户的运用逻辑层
lp_device 		对全部的设备进行封装抽象
lp_os			是对全部系统层进行抽象
lp_sys			是裸机系统，进行平替的是RTOS或者windows系统调用
lp_log			是给整个系统打印的日志文件

2023.04.25
新增模块
lp_sys/config
lp_config.h			用于定义整个工程的配置文件。
lp_define_io.h		用于定义整个工程的硬件IO。
lp_portmacro.h		用于定义跨平台使用的常用数据类型。
lp_projdefs.h		用于定义整个工程的常用宏定义。


lp_sys/logger
lp_log_system.c     用于管理这个系统的log，目前单片机使用rtt，pc端使用printf。


lp_sys/mem
lp_sys_mem.c		用于管理整个工程用户定义的堆内存，这个lp框架的各个组件动态创建的内存都要使用这个函数管理。
tlsf.c				用于实现内存管理的底层函数。


lp_sys/queue
lp_sys_queue.c		用于lp框架的裸机系统的队列，如果是使用操作系统的平台对应使用封装过的函数即可。


lp_sys/sleep_time
lp_sys_sptime.c		用于lp框架的裸机系统的延时函数



lp_sys/soft_timer
lp_sys_sptime.c		用于lp框架的裸机系统的软件定时器



lp_device/lp_led
lp_dev_led.c		指示灯设备
lp_dev_serial		串口设备


新增系统抽象层，目标是为了可以方便的进行跨平台系统移植
lp_os

对象系统底层硬件的封装
lp_sys_os_gpio
lp_sys_os_hwtimer
lp_sys_os_serial
lp_sys_os_random
lp_sys_os_exint

对象系统基础功能的封装
lp_sys_os_tick
lp_sys_os_mem
lp_sys_os_queue
lp_sys_os_sftimer


2023.04.28
新增软件随机数
使用ic uuid + MD5 + 系统时钟 最多可以一次性产生16位随机数
lp_sys_os_random


2023.05.12
新增IC STH750 兼容freertos
bsp中新增h750

完善DALI设备 新增dali主机
2023.05.31

新增lp_protocol_stack文件夹用于存放各种协议栈文件
抽象出dali协议栈
把dali_dev更改成为dali_busx,然后上层再抽象出具体的设备
新增dali_master，实现设备搜索功能

新增lp_com_protocol文件夹主要用于存放通讯用的协议

2023.6.14
新增
lp_sys_os_key文件夹
增加gpio按键
老化测试程序OK















