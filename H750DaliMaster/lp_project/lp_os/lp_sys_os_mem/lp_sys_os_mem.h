

#ifndef _LP_SYS_OS_MEM_H_
#define _LP_SYS_OS_MEM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "config/lp_config.h"



void lp_sys_os_mem_init(void);
void lp_sys_os_men_destroy(void);
void lp_sys_os_men_free(void* ptr);
void*lp_sys_os_men_malloc(size_t bytes);
void lp_sys_os_log_out(void);


#ifdef __cplusplus
}
#endif

#endif
































