


/* define to prevent recursive inclusion -------------------------------------*/
#ifndef _LP_SYS_OS_EXINT_
#define _LP_SYS_OS_EXINT_

#ifdef __cplusplus
extern "C" {
#endif

/* includes ------------------------------------------------------------------*/

#include "config/lp_config.h"

/* exported functions ------------------------------------------------------- */
typedef void (*lp_exint_hook)(void);





typedef enum
{
	LP_EXINT_TRIGGER_RISING_EDGE_E = 0x00,
	LP_EXINT_TRIGGER_FALLING_EDGE_E,
	LP_EXINT_TRIGGER_BOTH_EDGE_E,
	
} lp_exint_trigger_e;


lp_err_t lp_sys_os_exint_init(lp_u8_t exint_register,lp_exint_trigger_e edge);
lp_err_t lp_sys_os_exint_bind(lp_u8_t exint_register,lp_exint_hook funs);

lp_err_t lp_sys_os_exint_enable(lp_u8_t exint_register);
lp_err_t lp_sys_os_exint_disable(lp_u8_t exint_register);


lp_err_t lp_sys_os_exint_hook(lp_u8_t exint_register);


#ifdef __cplusplus
}
#endif

#endif



























