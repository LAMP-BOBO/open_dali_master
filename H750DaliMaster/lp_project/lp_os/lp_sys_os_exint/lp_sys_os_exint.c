

#include "lp_sys_os_exint.h"


#ifdef AT32F421_BOARD
#include "at32f421_aero_dali_board.h"
#endif

#ifdef STM32H750_BOARD
#include "stm32h750/stm32h750_dali_monitor_board.h"
#endif


lp_exint_hook 	lp_exint[LP_EXINT_MAX_E];


lp_err_t lp_sys_os_exint_init(lp_u8_t exint_register,lp_exint_trigger_e edge)
{
	lp_err_t ret = LP_FAIL;
	
	switch(exint_register){
	case LP_EXINT0_E:
		lp_exint[LP_EXINT0_E] = NULL;
		#ifdef AT32F421_BOARD
			at_pa0_exint_init();
		#endif
	break;
	case LP_EXINT15_E:
		#ifdef STM32H750_BOARD
			//main init 
		#endif
	break;	
	
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;

}

lp_err_t lp_sys_os_exint_bind(lp_u8_t exint_register,lp_exint_hook funs)
{
	lp_err_t ret = LP_FAIL;
	
	if(funs == NULL){
		ret = LP_FAIL;
		return ret;
	}
	
	switch(exint_register){
	case LP_EXINT0_E:
		lp_exint[LP_EXINT0_E] = funs;

	break;
	
	case LP_EXINT15_E:
		lp_exint[LP_EXINT15_E] = funs;
	break;
	
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;

}

lp_err_t lp_sys_os_exint_enable(lp_u8_t exint_register)
{
	lp_err_t ret = LP_FAIL;
		
	switch(exint_register){
	case LP_EXINT0_E:
		 
	break;
	
	case LP_EXINT15_E:
		 st_hal_exint10_15_enable(); 
	break;

	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}

lp_err_t lp_sys_os_exint_disable(lp_u8_t exint_register)
{
	lp_err_t ret = LP_FAIL;
		
	switch(exint_register){
	case LP_EXINT0_E:
		 
	break;
	
	case LP_EXINT15_E:
		st_hal_exint10_15_disable();
	break;

	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}


lp_err_t lp_sys_os_exint_hook(lp_u8_t exint_register)
{
	lp_err_t ret = LP_FAIL;
	switch(exint_register){
	case LP_EXINT0_E:
		if(lp_exint[LP_EXINT0_E] == NULL){
			ret = LP_FAIL;
			return ret;
		}
		lp_exint[LP_EXINT0_E]();
	break;
		
	case LP_EXINT15_E:
		if(lp_exint[LP_EXINT15_E] == NULL){
			ret = LP_FAIL;
			return ret;
		}
		lp_exint[LP_EXINT15_E]();
	break;

	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;

}





















