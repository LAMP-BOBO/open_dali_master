

#include "lp_sys_os_flash.h"
#include "eeprom.h"
#include "flash.h"


lp_err_t lp_sys_os_eeprom_init(void)
{
	lp_err_t ret = LP_OK;
	flash_status_type flash_status = FLASH_OPERATE_DONE;
	
	flash_status = flash_ee_init();
	if(flash_status != FLASH_OPERATE_DONE){
		ret = LP_FAIL;
	}
	
	return ret;

}
/*
- 0: data successfully read
- 1: failed to read data
*/
lp_err_t lp_sys_os_eeprom_read(lp_u16_t addr,lp_u16_t* data)
{
	lp_err_t ret = LP_OK;

	if(flash_ee_data_read(addr, data) != 0){
		ret = LP_FAIL;
	}
	
	return ret;
}


lp_err_t lp_sys_os_eeprom_write(lp_u16_t addr,lp_u16_t data)
{
	lp_err_t ret = LP_OK;
	flash_status_type flash_status = FLASH_OPERATE_DONE;
	
	flash_status = flash_ee_data_write(addr,data);
	if(flash_status != FLASH_OPERATE_DONE){
		ret = LP_FAIL;
	}
	
	return ret;

}


#if 0
#define BUF_SIZE               10
uint16_t buf_write[BUF_SIZE] = {0x2000, 0x2001, 0x2002, 0x2003, 0x2004, 0x2005, 0x2006, 0x2007, 0x2008, 0x2009};
uint16_t buf_read[BUF_SIZE];


uint32_t buffer_compare(uint16_t* buffer1, uint16_t* buffer2, uint32_t len)
{
  uint32_t i;
  
  for(i = 0; i < len; i++)
  {
    if(buffer1[i] != buffer2[i])
    {
      return 1;
    }
  }

  return 0;
}

lp_err_t lp_sys_os_eeprom_text(void)
{
	uint16_t i, address;
	lp_err_t ret = LP_OK;
	
	flash_ee_init();
	
	
	/* write data to eeprom */  
	for(i = 0; i < BUF_SIZE; i++)
	{
	address = i;

	flash_ee_data_write(address, buf_write[i]);
	}

	/* read data from eeprom */  
	for(i = 0; i < BUF_SIZE; i++)
	{
	address = i;

	flash_ee_data_read(address, &buf_read[i]);
	}

	/* compare data */
	if(buffer_compare(buf_write, buf_read, BUF_SIZE) == 0) 
	{
	//at32_led_on(LED3);
	  LOG_OUT("flash  read data equation!\r\n");
	  LOG_OUT("read data:0x%4x 0x%4x 0x%4x 0x%4x 0x%4x!\r\n",buf_read[0],buf_read[1],buf_read[2],buf_read[3],buf_read[4]);
	}
	else
	{ 
	//at32_led_off(LED3);
	  LOG_OUT("flash  read data no equation!\r\n");
	}   
	return ret;

}
#endif

  
lp_err_t lp_sys_os_flash_read(lp_u32_t addr,lp_u16_t* data,lp_u32_t number)
{
	lp_err_t ret = LP_OK;

	flash_read(addr, data, number);
	
	return ret;

}


lp_err_t lp_sys_os_flash_write(lp_u32_t addr,lp_u16_t* data,lp_u32_t number)
{
	lp_err_t ret = LP_OK;

	flash_write(addr, data, number);
	

	return ret;

}

uint32_t buffer_compare(uint16_t* buffer1, uint16_t* buffer2, uint32_t len)
{
  uint32_t i;
  
  for(i = 0; i < len; i++)
  {
    if(buffer1[i] != buffer2[i])
    {
      return 1;
    }
  }

  return 0;
}

#define BUF_SIZE               10
uint16_t buf_write[BUF_SIZE] = {0x2000, 0x2001, 0x2002, 0x2003, 0x2004, 0x2005, 0x2006, 0x2007, 0x2008, 0x2009};
uint16_t buf_read[BUF_SIZE];

lp_err_t lp_sys_os_flash_text(void)
{

	lp_err_t ret = LP_OK;
	
	/* write data to flash */ 
	flash_write(0x8000000 + 1024 * 50, buf_write, BUF_SIZE);

	/* read data from flash */   
	flash_read(0x8000000 + 1024 * 50, buf_read, BUF_SIZE);

	/* compare data */
	if(buffer_compare(buf_write, buf_read, BUF_SIZE) == 0) 
	{
	//at32_led_on(LED3);
	  LOG_OUT("flash  read data equation!\r\n");
	  LOG_OUT("read data:0x%4x 0x%4x 0x%4x 0x%4x 0x%4x!\r\n",buf_read[0],buf_read[1],buf_read[2],buf_read[3],buf_read[4]);
	}
	else
	{ 
	//at32_led_off(LED3);
	  LOG_OUT("flash  read data no equation!\r\n");
	}   
	
	return ret;
}






















