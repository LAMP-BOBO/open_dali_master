



/* define to prevent recursive inclusion -------------------------------------*/
#ifndef _LP_SYS_OS_FLASH_
#define _LP_SYS_OS_FLASH_

#ifdef __cplusplus
extern "C" {
#endif

/* includes ------------------------------------------------------------------*/

#include "config/lp_config.h"

/* exported functions ------------------------------------------------------- */
lp_err_t lp_sys_os_eeprom_init(void);
lp_err_t lp_sys_os_eeprom_read(lp_u16_t addr,lp_u16_t* data);
lp_err_t lp_sys_os_eeprom_write(lp_u16_t addr,lp_u16_t data);

lp_err_t lp_sys_os_flash_read(lp_u32_t addr,lp_u16_t* data,lp_u32_t number);
lp_err_t lp_sys_os_flash_write(lp_u32_t addr,lp_u16_t* data,lp_u32_t number);
lp_err_t lp_sys_os_flash_text(void);

#ifdef __cplusplus
}
#endif

#endif

























