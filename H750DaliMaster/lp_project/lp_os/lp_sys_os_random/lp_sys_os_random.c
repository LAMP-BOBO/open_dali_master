

#include "lp_sys_os_random.h"
#include "lp_sys_os_tick/lp_sys_os_tick.h"
#include <stdlib.h>
#include "at32f421_aero_dali_board.h"
#include "md5/md5.h"


/*MCU UUID is 12 Byte*/
/*
*AT32 MCU内部包含PID和UID编码。
*UID:UID总共为96bit，基地址为0x1FFFF7E8。
*PID:基地址为0xE0042000
*这里只需要最高地址的一个Byte的数据，则地址为0x1FFFF7F3，
*该地址中的内容可以识别AT32系列MCU为403A系列或其他系列。
*
*该芯片只有软件伪随机数，把uuid异或上系统时间作为md5输入获得三位dali随机数
*/

static MD5_CTX md5;
 lp_u8_t md5_out[16];
 lp_u8_t ic_uuid[12];

lp_err_t lp_sys_os_random_init(void)
{
	lp_err_t ret = LP_OK;
	
	srand(lp_get_sys_os_tick());
	MD5Init(&md5);
	at_get_ic_uuid(ic_uuid);
	//LOG_HEX(ic_uuid,sizeof(ic_uuid));
	
	return ret;
}



lp_err_t lp_sys_os_random_produce(void)
{
	lp_err_t ret = LP_OK;
	lp_u32_t systick = lp_get_sys_os_tick();
	
	srand(lp_get_sys_os_tick());
	lp_u8_t index1 = (lp_u8_t)rand()%12;
	lp_u8_t index2 = (lp_u8_t)rand()%12;
	lp_u8_t index3 = (lp_u8_t)rand()%12;
	
	ic_uuid[index1] ^= (lp_u8_t)systick;
	ic_uuid[index2] ^= (lp_u8_t)systick;
	ic_uuid[index3] ^= (lp_u8_t)systick;
	
	MD5Update(&md5,ic_uuid,sizeof(ic_uuid));
	MD5Final(&md5,md5_out);
	
	//LOG_HEX(md5_out,sizeof(md5_out));
	return ret;
}


/*
*一次最多获取16个随机数
*/
lp_err_t lp_sys_os_random_read(lp_u8_t* data,lp_u16_t len)
{
	lp_err_t ret = LP_OK;
	if(data == NULL){
		ret = LP_FAIL;
		return ret;
	}
	
	memcpy(data,md5_out,len);
	return ret;
}



















