


/* define to prevent recursive inclusion -------------------------------------*/
#ifndef _LP_SYS_OS_RANDOM_
#define _LP_SYS_OS_RANDOM_

#ifdef __cplusplus
extern "C" {
#endif

/* includes ------------------------------------------------------------------*/

#include "config/lp_config.h"

/* exported functions ------------------------------------------------------- */
lp_err_t lp_sys_os_random_init(void);
lp_err_t lp_sys_os_random_produce(void);
lp_err_t lp_sys_os_random_read(lp_u8_t* data,lp_u16_t len);

#ifdef __cplusplus
}
#endif

#endif


































