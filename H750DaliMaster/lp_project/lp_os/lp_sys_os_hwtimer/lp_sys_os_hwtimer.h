


/* define to prevent recursive inclusion -------------------------------------*/
#ifndef _LP_SYS_OS_HWTIMER_
#define _LP_SYS_OS_HWTIMER_

#ifdef __cplusplus
extern "C" {
#endif

/* includes ------------------------------------------------------------------*/

#include "config/lp_config.h"

/* exported functions ------------------------------------------------------- */
typedef void (*lp_hwtimer_hook)(void);


lp_err_t lp_sys_os_hwtimer_init(lp_u8_t hwtimer_register,lp_u32_t period,lp_u32_t arr);
lp_err_t lp_sys_os_hwtimer_bind(lp_u8_t hwtimer_register,lp_hwtimer_hook funs);

lp_err_t lp_sys_os_hwtimer_enable(lp_u8_t hwtimer_register);
lp_err_t lp_sys_os_hwtimer_disable(lp_u8_t hwtimer_register);
lp_err_t lp_sys_os_hwtimer_set_counter(lp_u8_t hwtimer_register,lp_u32_t counter);
lp_err_t lp_sys_os_hwtimer_get_counter(lp_u8_t hwtimer_register,lp_u32_t* counter);

lp_err_t lp_sys_os_hwtimer_hook(lp_u8_t hwtimer_register);


#ifdef __cplusplus
}
#endif

#endif



























