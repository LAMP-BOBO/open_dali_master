

#include "lp_sys_os_hwtimer.h"

#ifdef AT32F421_BOARD
#include "at32f421_aero_dali_board.h"
#endif

#ifdef STM32H750_BOARD
#include "stm32h750/stm32h750_dali_monitor_board.h"
#endif


lp_hwtimer_hook lp_hwtimer[LP_HWTIMER_MAX_E];


/*
*
*period: is reality period,min:1831
*		 fre/period < 0xff
*arr:	 is u16	max:0xffff
*/
lp_err_t lp_sys_os_hwtimer_init(lp_u8_t hwtimer_register,lp_u32_t period,lp_u32_t arr)
{
	lp_err_t ret = LP_FAIL;
	switch(hwtimer_register){
	case LP_HWTIMER14_E:
		lp_hwtimer[LP_HWTIMER14_E] = NULL;
		#ifdef AT32F421_BOARD
			at_tim14_init(period,arr);
		#endif
	break;
	
	case LP_HWTIMER15_E:
		lp_hwtimer[LP_HWTIMER15_E] = NULL;
		#ifdef AT32F421_BOARD
			at_tim15_init(period,arr);
		#endif
	break;
	
	case LP_HWTIMER5_E:
		#ifdef STM32H750_BOARD
			//mian init
		#endif
	break;
	
	case LP_HWTIMER12_E:
		#ifdef STM32H750_BOARD
			//mian init
		#endif
	break;
	
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}



lp_err_t lp_sys_os_hwtimer_bind(lp_u8_t hwtimer_register,lp_hwtimer_hook funs)
{
	lp_err_t ret = LP_FAIL;
	
	if(funs == NULL){
		ret = LP_FAIL;
		return ret;
	}
		
	switch(hwtimer_register){
	case LP_HWTIMER14_E:
		lp_hwtimer[LP_HWTIMER14_E] = funs;
	break;
	
	case LP_HWTIMER15_E:
		lp_hwtimer[LP_HWTIMER15_E] = funs;
	break;
	
	case LP_HWTIMER5_E:
		lp_hwtimer[LP_HWTIMER5_E] = funs;
	break;
	
	case LP_HWTIMER12_E:
		lp_hwtimer[LP_HWTIMER12_E] = funs;
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}




lp_err_t lp_sys_os_hwtimer_enable(lp_u8_t hwtimer_register)
{
	lp_err_t ret = LP_FAIL;
		
	switch(hwtimer_register){
	case LP_HWTIMER14_E:
		#ifdef AT32F421_BOARD
			at_tim14_enable();
		#endif
	break;
	
	case LP_HWTIMER15_E:
		#ifdef AT32F421_BOARD
			at_tim15_enable();
		#endif
	break;
	
	case LP_HWTIMER5_E:
		#ifdef STM32H750_BOARD
			st_hal_tim5_enable();
		#endif
	break;
	
	case LP_HWTIMER12_E:
		#ifdef STM32H750_BOARD
			st_hal_tim12_enable();
			//LOG_OUT("st_hal_tim12_enable\r\n");
		#endif
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}

lp_err_t lp_sys_os_hwtimer_disable(lp_u8_t hwtimer_register)
{
	lp_err_t ret = LP_FAIL;
		
	switch(hwtimer_register){
	case LP_HWTIMER14_E:
		#ifdef AT32F421_BOARD
			at_tim14_disable();
		#endif
	break;
	
	case LP_HWTIMER15_E:
		#ifdef AT32F421_BOARD
			at_tim15_disable();
		#endif
	break;
	
	case LP_HWTIMER5_E:
		#ifdef STM32H750_BOARD
			st_hal_tim5_disable();
		#endif
	break;
	
	case LP_HWTIMER12_E:
		#ifdef STM32H750_BOARD
			 st_hal_tim12_disable();
		#endif
	break;
	
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}

lp_err_t lp_sys_os_hwtimer_set_counter(lp_u8_t hwtimer_register,lp_u32_t counter)
{
	lp_err_t ret = LP_FAIL;
		
	switch(hwtimer_register){
	case LP_HWTIMER14_E:
		#ifdef AT32F421_BOARD
			at_tim14_counter_set(counter);
		#endif	 
	break;
	
	case LP_HWTIMER15_E:
		#ifdef AT32F421_BOARD
			 at_tim15_counter_set(counter);
		#endif		
	break;
	
	case LP_HWTIMER5_E:
		#ifdef STM32H750_BOARD
			 st_hal_tim5_counter_set(counter);
		#endif
	break;
	
	case LP_HWTIMER12_E:
		#ifdef STM32H750_BOARD
			 st_hal_tim12_counter_set(counter);
		#endif
	break;
	
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}

lp_err_t lp_sys_os_hwtimer_get_counter(lp_u8_t hwtimer_register,lp_u32_t* counter)
{
	lp_err_t ret = LP_FAIL;
		
	switch(hwtimer_register){
	case LP_HWTIMER14_E:
		#ifdef AT32F421_BOARD
		 *counter = at_tim14_counter_get();
		#endif
		
	break;
	
	case LP_HWTIMER15_E:
		#ifdef AT32F421_BOARD
		 *counter = at_tim15_counter_get();
		#endif
		
	break;
	
	case LP_HWTIMER5_E:
		#ifdef STM32H750_BOARD
			*counter =  st_hal_tim5_counter_get();
		#endif
	break;
	
	case LP_HWTIMER12_E:
		#ifdef STM32H750_BOARD
			*counter =  st_hal_tim12_counter_get();
		#endif
	break;
	
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}


lp_err_t lp_sys_os_hwtimer_hook(lp_u8_t hwtimer_register)
{
	lp_err_t ret = LP_FAIL;
	switch(hwtimer_register){
	case LP_HWTIMER14_E:
		if(lp_hwtimer[LP_HWTIMER14_E] == NULL){
			ret = LP_FAIL;
			return ret;
		}
		lp_hwtimer[LP_HWTIMER14_E]();
	break;
	
	case LP_HWTIMER15_E:
		if(lp_hwtimer[LP_HWTIMER15_E] == NULL){
			ret = LP_FAIL;
			return ret;
		}
		lp_hwtimer[LP_HWTIMER15_E]();
	break;
		
	case LP_HWTIMER5_E:
		if(lp_hwtimer[LP_HWTIMER5_E] == NULL){
			ret = LP_FAIL;
			return ret;
		}
		lp_hwtimer[LP_HWTIMER5_E]();
	break;
	
	case LP_HWTIMER12_E:
		if(lp_hwtimer[LP_HWTIMER12_E] == NULL){
			ret = LP_FAIL;
			return ret;
		}
		lp_hwtimer[LP_HWTIMER12_E]();
	break;	
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;

}






















