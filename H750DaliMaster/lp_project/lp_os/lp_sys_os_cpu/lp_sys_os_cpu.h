


/* define to prevent recursive inclusion -------------------------------------*/
#ifndef _LP_SYS_OS_CPU_
#define _LP_SYS_OS_CPU_

#ifdef __cplusplus
extern "C" {
#endif

/* includes ------------------------------------------------------------------*/

#include "config/lp_config.h"

/* exported functions ------------------------------------------------------- */
lp_err_t lp_sys_os_SystemReset(void);
lp_err_t lp_sys_os_disableINT(void);
lp_err_t lp_sys_os_enableINT(void);
lp_err_t lp_sys_os_set_vector_offset(lp_u32_t base, lp_u32_t offset);

#ifdef __cplusplus
}
#endif

#endif






















