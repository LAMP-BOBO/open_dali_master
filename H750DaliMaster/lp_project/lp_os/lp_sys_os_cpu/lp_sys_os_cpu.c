

#include "lp_sys_os_cpu.h"


#ifdef STM32H750_BOARD
#include "stm32h750/stm32h750_dali_monitor_board.h"
#endif

#ifdef AT32F421_BOARD
#include "at32f421_aero_dali_board.h"
#endif



lp_err_t lp_sys_os_SystemReset(void)
{
	lp_err_t ret = LP_OK;
	NVIC_SystemReset();
	return ret;
}


lp_err_t lp_sys_os_disableINT(void)
{
	lp_err_t ret = LP_OK;
	__set_PRIMASK(1);
	return ret;
}


lp_err_t lp_sys_os_enableINT(void)
{
	lp_err_t ret = LP_OK;
	__set_PRIMASK(0);
	return ret;
}

lp_err_t lp_sys_os_set_vector_offset(lp_u32_t base, lp_u32_t offset)
{
  lp_err_t ret = LP_OK;
  /* config vector table offset */
	#ifdef AT32F421_BOARD
		nvic_vector_table_set(base, offset);
	#endif
  
  return ret;
}












