


#ifndef _LP_SYS_OS_SERIAL_H
#define _LP_SYS_OS_SERIAL_H

#ifdef __cplusplus
extern "C" {
#endif


#include "config/lp_config.h"

#ifdef BOOT_APP
#define LP_SYS_OS_SERIAL_DATA_LEN           512
#else
#define LP_SYS_OS_SERIAL_DATA_LEN	32
#endif



typedef struct _lp_sys_os_serial_data {
	
	lp_u8_t   which;
	lp_u16_t  len;
	lp_u8_t	  data[LP_SYS_OS_SERIAL_DATA_LEN];
	
}lp_sys_os_serial_data, *lp_psys_os_serial_data;

lp_err_t lp_sys_os_serial_init(lp_u8_t serial_register,lp_u32_t baud,lp_u8_t data_bit,lp_u8_t stop_bit);
lp_err_t lp_sys_os_serial_read(lp_u8_t serial_register,lp_u8_t*buf,lp_u16_t*len);
lp_err_t lp_sys_os_serial_wirte(lp_u8_t serial_register,lp_u8_t* data,lp_u16_t len);
lp_err_t lp_sys_os_serial_open(lp_u8_t serial_register,lp_u16_t delay);
lp_err_t lp_sys_os_serial_close(lp_u8_t serial_register);

lp_err_t lp_sys_os_serial_rev_data_complete_hook(lp_u8_t serial_register,lp_u8_t* data,lp_u16_t len);


#ifdef __cplusplus
}
#endif

#endif























