

#include "lp_sys_os_serial.h"
#include "bsp_at32f421_uart.h"
#include "lp_sys_os_queue/lp_sys_os_queue.h"
#include "lp_sys_os_sftimer/lp_sys_os_sftimer.h"


#ifdef BOOT_APP
#define LP_SYS_SERIAL_QUEUE_NUM 	2
#else
#define LP_SYS_SERIAL_QUEUE_NUM 	6
#endif


lp_sys_os_queue_t serial_queue_read[LP_UART_MAX_E];
lp_sys_os_queue_t serial_queue_wirte[LP_UART_MAX_E];

lp_err_t lp_sys_os_serial_init(lp_u8_t serial_register,lp_u32_t baud,lp_u8_t data_bit,lp_u8_t stop_bit)
{
	lp_err_t ret = LP_FAIL;
	switch(serial_register){
	case LP_UART1_E:
		ret = bsp_at32_uart1_init(baud,data_bit,stop_bit);
		serial_queue_read[LP_UART1_E] = lp_sys_os_queue_create(sizeof(lp_sys_os_serial_data),LP_SYS_SERIAL_QUEUE_NUM,false);
		serial_queue_wirte[LP_UART1_E] = lp_sys_os_queue_create(sizeof(lp_sys_os_serial_data),LP_SYS_SERIAL_QUEUE_NUM,false);
		if(serial_queue_wirte[LP_UART1_E] == NULL || serial_queue_read[LP_UART1_E] == NULL){
			ret = LP_FAIL;
			LOG_OUT("serial_queue creat fail\r\n");
			return ret;
		}
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}


lp_err_t lp_sys_os_serial_read(lp_u8_t serial_register,lp_u8_t*buf,lp_u16_t*len)
{
	lp_err_t ret = LP_FAIL;
	lp_sys_os_serial_data serial_data;
	
	
	switch(serial_register){
	case LP_UART1_E:
		if(lp_sys_os_queue_empty(serial_queue_read[LP_UART1_E]) != LP_OK){
			ret = lp_sys_os_queue_pop(serial_queue_read[LP_UART1_E],&serial_data);
			*len = serial_data.len;
			memcpy(buf,serial_data.data,serial_data.len);
			//LOG_OUT("lp_sys_os_serial_read ret %d len %d buf %s!\r\n",ret,serial_data.len,serial_data.data);
		}
		
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	return ret;
}

lp_err_t lp_sys_os_serial_wirte(lp_u8_t serial_register,lp_u8_t* data,lp_u16_t len)
{
	lp_err_t ret = LP_FAIL;
	lp_sys_os_serial_data serial_data;
	
	if(data == NULL)
	{
		ret = LP_FAIL;
		return ret;
	}
	
	switch(serial_register){
	case LP_UART1_E:
		 if(serial_queue_wirte[LP_UART1_E] == NULL){
			ret = LP_FAIL;
			return ret;
		 }
		 serial_data.which = LP_UART1_E;
		 serial_data.len = len;
		 memcpy(serial_data.data,data,len);
		 ret  = lp_sys_os_queue_push(serial_queue_wirte[LP_UART1_E],&serial_data);
		 //LOG_OUT("lp_sys_os_serial_wirte ret %d len %d buf %s!\r\n",ret,serial_data.len,serial_data.data);
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}

struct lp_timer* serial_send_timer_task[LP_UART_MAX_E];


void serial_send_task_timeout_callback(struct lp_timer* timer)
{
	lp_err_t ret = LP_FAIL;
	lp_sys_os_serial_data serial_data;
	if(timer == serial_send_timer_task[LP_UART1_E])
	{
		
		if(bsp_at32_uart1_send_is_busy() == LP_OK)
		{
			if(lp_sys_os_queue_empty(serial_queue_wirte[LP_UART1_E]) != LP_OK){
				ret = lp_sys_os_queue_pop(serial_queue_wirte[LP_UART1_E],&serial_data);
				//LOG_OUT("serial_send_task_timeout_callback ret %d len %d buf %s!\r\n",ret,serial_data.len,serial_data.data);
				if(ret == LP_OK){
						ret = bsp_at32_uart1_send(serial_data.data,serial_data.len);
				}
			}
		}
	}
	
}

static lp_err_t lp_sys_os_serial_send_task_init(lp_u8_t serial_register)
{
	lp_err_t ret = LP_FAIL;
	
	serial_send_timer_task[serial_register] = lp_sys_os_timer_create(serial_send_task_timeout_callback);
	
    if (serial_send_timer_task[serial_register] == NULL)
    {
        LOG_OUT("serial_send_timer_task create %d error!\n",serial_register);
		ret = LP_FAIL;
        return ret;
    }
	
	return ret;
}

static lp_err_t lp_sys_os_serial_send_task_start(lp_u8_t serial_register,lp_u16_t delay)
{
	lp_err_t ret = LP_FAIL;
	
	if(serial_send_timer_task[serial_register] != NULL){
		
		ret = lp_sys_os_timer_start(serial_send_timer_task[serial_register], TIMER_MODE_LOOP, delay);
	}
	
	return ret;
}

static lp_err_t lp_sys_os_serial_send_task_del(lp_u8_t serial_register)
{
	lp_err_t ret = LP_FAIL;
	
	if(serial_send_timer_task[serial_register] != NULL){
		
		ret = lp_sys_os_timer_delete(serial_send_timer_task[serial_register]);
	}
	
	return ret;
}

lp_err_t lp_sys_os_serial_open(lp_u8_t serial_register,lp_u16_t delay)
{
	lp_err_t ret = LP_FAIL;

	switch(serial_register){
	case LP_UART1_E:
		ret = lp_sys_os_serial_send_task_init(serial_register);
		ret = lp_sys_os_serial_send_task_start(serial_register,delay);
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}

lp_err_t lp_sys_os_serial_close(lp_u8_t serial_register)
{
	lp_err_t ret = LP_FAIL;

	switch(serial_register){
	case LP_UART1_E:
		ret = lp_sys_os_serial_send_task_del(serial_register);
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}

lp_err_t lp_sys_os_serial_rev_data_complete_hook(lp_u8_t serial_register,lp_u8_t* data,lp_u16_t len)
{
	lp_err_t ret = LP_OK;
	lp_sys_os_serial_data serial_data;
	
	
	if(data == NULL)
	{
		ret = LP_FAIL;
		return ret;
	}
	
	switch(serial_register){
	case LP_UART1_E:
		if(serial_queue_wirte[LP_UART1_E] == NULL){
			ret = LP_FAIL;
			return ret;
		 }
		 serial_data.which = LP_UART1_E;
		 serial_data.len = len;
		 memcpy(serial_data.data,data,len);
		 ret  = lp_sys_os_queue_push(serial_queue_read[LP_UART1_E],&serial_data);
		 //LOG_OUT("lp_sys_os_serial_rev_data_complete_hook ret %d len %d buf %s!\r\n",ret,len,data);
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}






























