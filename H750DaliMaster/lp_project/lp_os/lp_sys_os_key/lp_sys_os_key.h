


#ifndef _LP_SYS_OS_KEY_H
#define _LP_SYS_OS_KEY_H

#ifdef __cplusplus
extern "C" {
#endif


#include "config/lp_config.h"


/* unit 10ms */
#define LP_KEY_FILTER_TIME   5
#define LP_KEY_LONG_TIME     100	

/*
	key opration struct
*/
typedef struct
{
	
	lp_u8_t (*IsKeyDownFunc)(void);

	lp_u8_t  Count;			/* Filter counter */
	lp_u16_t LongCount;		/* Long press counter */
	lp_u16_t LongTime;		/* Key hold duration, 0 Do not detect long press */
	lp_u8_t  State;			/* Current state of button (Pressed or unpressed)*/
	lp_u8_t  RepeatSpeed;	/* Continuous key cycle */
	lp_u8_t  RepeatCount;	/* Continuous key counter */
    
}lp_key_t,*lp_pkey_t;


#define KEY_FIFO_SIZE	10
typedef struct
{
	lp_u8_t Buf[KEY_FIFO_SIZE];		/* buf */
	lp_u8_t Read;					/* buf read pointer1 */
	lp_u8_t Write;					/* buf write pointer */
	lp_u8_t Read2;					/* buf read pointer2 */
}lp_keyfifo_t;


typedef struct
{
	
	lp_u8_t gpio_register;
	lp_u8_t ActiveLevel;	/* activation level */
	
}lp_key_io_t;


typedef enum
{
	KEY_NONE = 0,			

	KEY_1_DOWN,				
	KEY_1_UP,				
	KEY_1_LONG,				

	KEY_2_DOWN,				
	KEY_2_UP,				
	KEY_2_LONG,				

	KEY_3_DOWN,				
	KEY_3_UP,				
	KEY_3_LONG,				

	KEY_4_DOWN,				
	KEY_4_UP,				
	KEY_4_LONG,				

	KEY_5_DOWN,				
	KEY_5_UP,				
	KEY_5_LONG,				

	KEY_6_DOWN,			
	KEY_6_UP,				
	KEY_6_LONG,	
}KEY_ENUM;


/* Exported functions prototypes ---------------------------------------------*/
lp_err_t lp_sys_os_key_init(void);
void lp_KeyScan10ms(void);
void lp_KeyScan1ms(void);
void lp_PutKey(lp_u8_t _KeyCode);
lp_u8_t lp_GetKey(void);
lp_u8_t lp_GetKey2(void);
lp_u8_t lp_GetKeyState(lp_u8_t _ucKeyID);
void lp_SetKeyParam(lp_u8_t _ucKeyID, lp_u16_t _LongTime, lp_u8_t  _RepeatSpeed);
void lp_ClearKey(void);


#ifdef __cplusplus
}
#endif

#endif



































