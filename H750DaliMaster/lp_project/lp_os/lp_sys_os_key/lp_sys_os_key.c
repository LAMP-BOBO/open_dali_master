

#include "lp_sys_os_key/lp_sys_os_key.h"
#include "lp_sys_os_gpio/lp_sys_os_gpio.h"


#define LP_HARD_KEY_NUM	    	2	   				/* Number of physical keys */
#define LP_KEY_COUNT   	 	(LP_HARD_KEY_NUM + 1)	/* Number of physical keys + Combination of buttons */



lp_key_t 	 s_tBtn[LP_KEY_COUNT] = {0};
lp_keyfifo_t s_tKey;		        /* Key FIFO variable, structure */


#define LP_KEYID_K1		0
#define LP_KEYID_K2		1

static lp_key_io_t s_gpio_list[LP_HARD_KEY_NUM]={
	{LP_GPIO_K1_E, 0},	/* K1 */
	{LP_GPIO_K2_E, 0},	/* K2 */

};


/*
*********************************************************************************************************
*                                Function Declaration
*********************************************************************************************************
*/
static void lp_InitKeyVar(void);


#define KEY_PIN_ACTIVE(id)



/*
*********************************************************************************************************
* Function_Name : lp_sys_os_key_init
* Note    		: all key init
* Parameter     : none
* Return    	: LP_OK is success
*********************************************************************************************************
*/
lp_err_t lp_sys_os_key_init(void)
{
	lp_err_t ret;
	
	lp_InitKeyVar();
	for(int i=0;i<LP_HARD_KEY_NUM;i++)
	{
		ret = lp_sys_os_gpio_init(s_gpio_list[i].gpio_register);
	}

	return ret;
}



/*
*********************************************************************************************************
* Function_Name : KeyPinActive
* Note    		: Determine whether a key is pressed
* Parameter     : _id is key id ,s_gpio_list read  pin state
* Return    	: uint8_t 1 press , 0 no press
*********************************************************************************************************
*/
static lp_u8_t lp_KeyPinActive(lp_u8_t _id)
{
	lp_u8_t level;
	lp_u8_t out_value;
	lp_err_t ret;
	
	ret = lp_sys_os_gpio_read(s_gpio_list[_id].gpio_register,&out_value);
	
	if (out_value == 0)
	{
		level = 0;
	}
	else
	{
		level = 1;
	}

	if (level == s_gpio_list[_id].ActiveLevel)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}



/*
*********************************************************************************************************
* Function_Name : bsp_InitKeyHard
* Note    		: Initialize key variables
* Parameter     : NC
* Return    	: NC
*********************************************************************************************************
*/
static void lp_InitKeyVar(void)
{
	lp_u8_t i;

	
	s_tKey.Read = 0;
	s_tKey.Write = 0;
	s_tKey.Read2 = 0;

	
	for (i = 0; i < LP_KEY_COUNT; i++)
	{
		s_tBtn[i].LongTime = LP_KEY_LONG_TIME;			
		s_tBtn[i].Count = LP_KEY_FILTER_TIME / 2;		
		s_tBtn[i].State = 0;							
		s_tBtn[i].RepeatSpeed = 0;						
		s_tBtn[i].RepeatCount = 0;						
	}

	/* If you need to change the parameters of a key separately, you can reassign them separately here*/
	
}


/*
*********************************************************************************************************
* Function_Name : IsKeyDownFunc
* Note    		: Determine whether a key is pressed,Single-key events do not allow other keys to be pressed
* Parameter     : _id is key id ,s_gpio_list read  pin state
* Return    	: uint8_t 1 press , 0 no press
*********************************************************************************************************
*/
static lp_u8_t lp_IsKeyDownFunc(lp_u8_t _id)
{
	/* Physical buttons */
	if (_id < LP_HARD_KEY_NUM)
	{
		lp_u8_t i;
		lp_u8_t count = 0;
		lp_u8_t save = 255;
		
		
		for (i = 0; i < LP_HARD_KEY_NUM; i++)
		{
			if (lp_KeyPinActive(i)) 
			{
				count++;
				save = i;
			}
		}
		
		if (count == 1 && save == _id)
		{
			return 1;
		}		

		return 0;
	}
	
	/* Combination key K1K2 */
	if (_id == LP_HARD_KEY_NUM + 0)
	{
		if (lp_KeyPinActive(LP_KEYID_K1) && lp_KeyPinActive(LP_KEYID_K2))
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	return 0;
}


/*
*********************************************************************************************************
* Function_Name : lp_PutKey
* Note    		: Press 1 key into the key FIFO buffer.Can be used to simulate a key
* Parameter     : _KeyCode : The key code
* Return    	: NC
*********************************************************************************************************
*/
void lp_PutKey(lp_u8_t _KeyCode)
{
	s_tKey.Buf[s_tKey.Write] = _KeyCode;

	if (++s_tKey.Write  >= KEY_FIFO_SIZE)
	{
		s_tKey.Write = 0;
	}
}


/*
*********************************************************************************************************
* Function_Name : bsp_GetKey
* Note    		: Reads a key value from the key FIFO buffer.
* Parameter     : NC
* Return    	: uint8_t The key code
*********************************************************************************************************
*/
lp_u8_t lp_GetKey(void)
{
	lp_u8_t ret;

	if (s_tKey.Read == s_tKey.Write)
	{
		return KEY_NONE;
	}
	else
	{
		ret = s_tKey.Buf[s_tKey.Read];

		if (++s_tKey.Read >= KEY_FIFO_SIZE)
		{
			s_tKey.Read = 0;
		}
		return ret;
	}
}


/*
*********************************************************************************************************
* Function_Name : lp_GetKey2
* Note    		: Reads a key value from the key FIFO buffer.Independent read pointer
* Parameter     : NC
* Return    	: uint8_t The key code
*********************************************************************************************************
*/
lp_u8_t lp_GetKey2(void)
{
	lp_u8_t ret;

	if (s_tKey.Read2 == s_tKey.Write)
	{
		return KEY_NONE;
	}
	else
	{
		ret = s_tKey.Buf[s_tKey.Read2];

		if (++s_tKey.Read2 >= KEY_FIFO_SIZE)
		{
			s_tKey.Read2 = 0;
		}
		return ret;
	}
}


/*
*********************************************************************************************************
* Function_Name : lp_GetKeyState
* Note    		: Reads the state of the key
* Parameter     : _ucKeyID: indicates the ID of a key. The value starts from 0
* Return    	: uint8_t 1 indicates that the button is pressed, 0 indicates that the button is not pressed
*********************************************************************************************************
*/
lp_u8_t lp_GetKeyState(lp_u8_t _ucKeyID)
{
	return s_tBtn[_ucKeyID].State;
}


/*
*********************************************************************************************************
* Function_Name : lp_SetKeyParam
* Note    		: Setting key Parameters
* Parameter     : _ucKeyID : indicates the ID of a key. The value starts from 0
*                 _LongTime : Long press the event time
*			      _RepeatSpeed : Running speed   
* Return    	: uint8_t 1 indicates that the button is pressed, 0 indicates that the button is not pressed
*********************************************************************************************************
*/
void lp_SetKeyParam(lp_u8_t _ucKeyID, lp_u16_t _LongTime, lp_u8_t  _RepeatSpeed)
{
	s_tBtn[_ucKeyID].LongTime = _LongTime;			
	s_tBtn[_ucKeyID].RepeatSpeed = _RepeatSpeed;			
	s_tBtn[_ucKeyID].RepeatCount = 0;						
}


/*
*********************************************************************************************************
* Function_Name : bsp_ClearKey
* Note    		: Clear the key FIFO buffer
* Parameter     : NC  
* Return    	: NC
*********************************************************************************************************
*/
void lp_ClearKey(void)
{
	s_tKey.Read = s_tKey.Write;
}


/*
*********************************************************************************************************
* Function_Name : bsp_DetectKey
* Note    		: Detect a key.Non-blocking state, which must be called periodically.
* Parameter     : IO ID, encoded from 0  
* Return    	: NC
*********************************************************************************************************
*/
static void bsp_DetectKey(uint8_t i)
{
	lp_key_t *pBtn;

	pBtn = &s_tBtn[i];
	if (lp_IsKeyDownFunc(i))
	{
		if (pBtn->Count < LP_KEY_FILTER_TIME)
		{
			pBtn->Count = LP_KEY_FILTER_TIME;
		}
		else if(pBtn->Count < 2 * LP_KEY_FILTER_TIME)
		{
			pBtn->Count++;
		}
		else
		{
			if (pBtn->State == 0)
			{
				pBtn->State = 1;

				lp_PutKey((uint8_t)(3 * i + 1));
			}

			if (pBtn->LongTime > 0)
			{
				if (pBtn->LongCount < pBtn->LongTime)
				{

					if (++pBtn->LongCount == pBtn->LongTime)
					{

						lp_PutKey((uint8_t)(3 * i + 3));
					}
				}
				else
				{
					if (pBtn->RepeatSpeed > 0)
					{
						if (++pBtn->RepeatCount >= pBtn->RepeatSpeed)
						{
							pBtn->RepeatCount = 0;
							
							lp_PutKey((uint8_t)(3 * i + 1));
						}
					}
				}
			}
		}
	}
	else
	{
		if(pBtn->Count > LP_KEY_FILTER_TIME)
		{
			pBtn->Count = LP_KEY_FILTER_TIME;
		}
		else if(pBtn->Count != 0)
		{
			pBtn->Count--;
		}
		else
		{
			if (pBtn->State == 1)
			{
				pBtn->State = 0;

				lp_PutKey((uint8_t)(3 * i + 2));
			}
		}

		pBtn->LongCount = 0;
		pBtn->RepeatCount = 0;
	}
}


/*
*********************************************************************************************************
* Function_Name : bsp_DetectFastIO
* Note    		: Detect the high speed input IO. 1ms refresh once
* Parameter     : IO ID, encoded from 0  
* Return    	: NC
*********************************************************************************************************
*/
static void bsp_DetectFastIO(uint8_t i)
{
	lp_key_t *pBtn;

	pBtn = &s_tBtn[i];
	if (lp_IsKeyDownFunc(i))
	{
		if (pBtn->State == 0)
		{
			pBtn->State = 1;

			
			lp_PutKey((uint8_t)(3 * i + 1));
		}

		if (pBtn->LongTime > 0)
		{
			if (pBtn->LongCount < pBtn->LongTime)
			{
				
				if (++pBtn->LongCount == pBtn->LongTime)
				{
					
					lp_PutKey((uint8_t)(3 * i + 3));
				}
			}
			else
			{
				if (pBtn->RepeatSpeed > 0)
				{
					if (++pBtn->RepeatCount >= pBtn->RepeatSpeed)
					{
						pBtn->RepeatCount = 0;
						
						lp_PutKey((uint8_t)(3 * i + 1));
					}
				}
			}
		}
	}
	else
	{
		if (pBtn->State == 1)
		{
			pBtn->State = 0;

			
			lp_PutKey((uint8_t)(3 * i + 2));
		}

		pBtn->LongCount = 0;
		pBtn->RepeatCount = 0;
	}
}


/*
*********************************************************************************************************
* Function_Name : lp_KeyScan10ms
* Note    		: Scan all keys.Non-blocking, interrupted by Systick periodically called, 10ms once
* Parameter     : NC  
* Return    	: NC
*********************************************************************************************************
*/
void lp_KeyScan10ms(void)
{
	lp_u8_t i;

	for (i = 0; i < LP_KEY_COUNT; i++)
	{
		bsp_DetectKey(i);
	}
}




/*
*********************************************************************************************************
* Function_Name : lp_KeyScan1ms
* Note    		: Scan all keys.Non-blocking, interrupted by Systick periodically called, 1ms at a time.
* Parameter     : NC  
* Return    	: NC
*********************************************************************************************************
*/
void lp_KeyScan1ms(void)
{
	lp_u8_t i;

	for (i = 0; i < LP_KEY_COUNT; i++)
	{
		bsp_DetectFastIO(i);
	}
}


