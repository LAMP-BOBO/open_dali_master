


#include "lp_sys_os_gpio/lp_sys_os_gpio.h"

#ifdef STM32H750_BOARD
#include "stm32h750/stm32h750_dali_monitor_board.h"
#endif

#ifdef AT32F421_BOARD
#include "at32f421_aero_dali_board.h"
#endif


lp_err_t lp_sys_os_gpio_init(lp_u8_t gpio_register)
{
	lp_err_t ret = LP_OK;
	switch(gpio_register){
		case LP_GPIO_LED_R_E:
			#ifdef AT32F421_BOARD
				at32_led_init(LED1);
			#endif
		
			#ifdef STM32H750_BOARD
				//main init
			#endif
		break;
		case LP_GPIO_DALI_RE_E:
			#ifdef AT32F421_BOARD
				at_dali_re_init();
			#endif
			
			#ifdef STM32H750_BOARD
				////main init
			#endif
		break;
		case LP_GPIO_DALI_TR_E:
			#ifdef AT32F421_BOARD
				at_dali_tr_init();
			#endif
		
			#ifdef STM32H750_BOARD
				////main init
			#endif
		break;	
		case LP_GPIO_K1_E:
			//main init
		break;	
		
		case LP_GPIO_K2_E:
			//main init
		break;
		
		default:
			ret =  LP_FAIL;
			break;
	}
	return ret;
}




lp_err_t lp_sys_os_gpio_write(lp_u8_t gpio_register,lp_u8_t gpio_state)
{
	lp_err_t ret = LP_OK;
	switch(gpio_register){
		case LP_GPIO_LED_R_E:
			if(gpio_state == LP_GPIO_LOW_E){
				#ifdef AT32F421_BOARD
					at32_led_on(LED1);
				#endif
				
				#ifdef STM32H750_BOARD
					st_hal_led_r_on();
				#endif
			}
			else{
				#ifdef AT32F421_BOARD
					at32_led_off(LED1);
				#endif
				
				#ifdef STM32H750_BOARD
					st_hal_led_r_off();
				#endif
			}
			
		break;


		case LP_GPIO_DALI_TR_E:
			if(gpio_state == LP_GPIO_LOW_E){
				#ifdef AT32F421_BOARD
					at_dali_tr_low();
				#endif
				
				#ifdef STM32H750_BOARD
					st_hal_dali_bus0_tr_low();
				#endif
			}
			else{
				#ifdef AT32F421_BOARD
					at_dali_tr_high();
				#endif
				
				#ifdef STM32H750_BOARD
					st_hal_dali_bus0_tr_high();
				#endif
			}
			
			
		break;
		default:
			ret =  LP_FAIL;
			break;
	}
	return ret;
}

lp_err_t lp_sys_os_gpio_read(lp_u8_t gpio_register,lp_u8_t* out_value)
{
	lp_err_t ret = LP_OK;
	switch(gpio_register){

		case LP_GPIO_DALI_RE_E:
			#ifdef AT32F421_BOARD
				*out_value = at_dali_re_read();
			#endif
			
			#ifdef STM32H750_BOARD
				*out_value = st_hal_dali_bus0_re_read();
			#endif
		break;
		
		case LP_GPIO_K1_E:
			#ifdef STM32H750_BOARD
				*out_value = st_hal_key1_read();
			#endif
		break;	
		
		case LP_GPIO_K2_E:
			#ifdef STM32H750_BOARD
				*out_value = st_hal_key2_read();
			#endif
		break;
		
		default:
			ret =  LP_FAIL;
			break;
	}
	return ret;
}


lp_err_t lp_sys_os_gpio_toggout(lp_u8_t gpio_register)
{
	lp_err_t ret = LP_OK;
	switch(gpio_register){
		case LP_GPIO_LED_R_E:
			#ifdef AT32F421_BOARD
				at32_led_toggle(LED1);
			#endif
			
			#ifdef STM32H750_BOARD
				st_hal_led_r_toggle();
			#endif
		break;
		default:
			ret =  LP_FAIL;
			break;
	}
	return ret;
}















