


#ifndef _LP_SYS_OS_GPIO_H
#define _LP_SYS_OS_GPIO_H

#ifdef __cplusplus
extern "C" {
#endif


#include "config/lp_config.h"





typedef enum
{
	LP_GPIO_LOW_E = 0x00,
	LP_GPIO_HIGH_E= 0x01,
	
} lp_gpio_state_e;



lp_err_t lp_sys_os_gpio_init(lp_u8_t gpio_register);
lp_err_t lp_sys_os_gpio_write(lp_u8_t gpio_register,lp_u8_t gpio_state);
lp_err_t lp_sys_os_gpio_read(lp_u8_t gpio_register,lp_u8_t* out_value);
lp_err_t lp_sys_os_gpio_toggout(lp_u8_t gpio_register);

#ifdef __cplusplus
}
#endif

#endif






























