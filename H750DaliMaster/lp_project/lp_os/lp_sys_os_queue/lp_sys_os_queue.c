



#include "lp_sys_os_queue/lp_sys_os_queue.h"




#if (LP_CMSIS_OS)&&(LP_RTOS)

//  ==== Message Queue Management Functions ==== LINE 669

lp_sys_os_queue_t lp_sys_os_queue_create(lp_u16_t queue_size, lp_u16_t max_queues, lp_bool_t keep_fresh)
{
	
	 return osMessageQueueNew(max_queues,queue_size,NULL);
}

lp_err_t lp_sys_os_queue_delete(lp_sys_os_queue_t queue)
{
	osStatus_t ret = osOK;
	
	ret = osMessageQueueDelete(queue);
	if(ret == osOK){
		return LP_OK;
	}else{
		return LP_FAIL;
	}
}

lp_err_t lp_sys_os_queue_push(lp_sys_os_queue_t queue, void* pval)
{
	osStatus_t ret = osOK;
	ret = osMessageQueuePut (queue,pval,NULL,0);
	if(ret == osOK){
		return LP_OK;
	}else{
		return LP_FAIL;
	}
}

lp_err_t lp_sys_os_queue_push_block(lp_sys_os_queue_t queue, void* pval)
{
	osStatus_t ret = osOK;
	ret = osMessageQueuePut (queue,pval,NULL,osWaitForever);
	if(ret == osOK){
		return LP_OK;
	}else{
		return LP_FAIL;
	}
}


lp_err_t lp_sys_os_queue_pop(lp_sys_os_queue_t queue, void* pval)
{
	osStatus_t ret = osOK;
	ret = osMessageQueueGet (queue,pval,NULL,0);
	if(ret == osOK){
		return LP_OK;
	}else{
		return LP_FAIL;
	}
}

lp_err_t lp_sys_os_queue_pop_block(lp_sys_os_queue_t queue, void* pval)
{
	osStatus_t ret = osOK;
	ret = osMessageQueueGet (queue,pval,NULL,osWaitForever);
	if(ret == osOK){
		return LP_OK;
	}else{
		return LP_FAIL;
	}
}

lp_u16_t lp_sys_os_queue_curr_len(lp_sys_os_queue_t queue)
{
	return (lp_u16_t)osMessageQueueGetCount(queue);
}

lp_err_t lp_sys_os_queue_clean(lp_sys_os_queue_t queue)
{
	osStatus_t ret = osOK;
	ret =  osMessageQueueReset (queue);
	if(ret == osOK){
		return LP_OK;
	}else{
		return LP_FAIL;
	}
}

lp_err_t lp_sys_os_queue_empty(lp_sys_os_queue_t queue)
{
	
	if(lp_sys_os_queue_curr_len(queue)==0){
		return LP_OK;
	}
	else{
		return LP_FAIL;
	}
}

lp_err_t lp_sys_os_queue_full(lp_sys_os_queue_t queue)
{
	if(osMessageQueueGetSpace (queue)==0){
		return LP_OK;
	}
	else{
		return LP_FAIL;
	}
}

#endif





#if (LP_FREE_RTOS)&&(LP_RTOS)

lp_sys_os_queue_t lp_sys_os_queue_create(lp_u16_t queue_size, lp_u16_t max_queues, lp_bool_t keep_fresh)
{
}

lp_err_t lp_sys_os_queue_delete(lp_sys_os_queue_t queue)
{
}

lp_err_t lp_sys_os_queue_init(lp_sys_os_queue_t queue, void* queuepool, lp_u16_t pool_size, lp_u16_t queue_size, lp_bool_t keep_fresh)
{
}

lp_err_t lp_sys_os_queue_detach(lp_sys_os_queue_t queue)
{
}

lp_err_t lp_sys_os_queue_clean(lp_sys_os_queue_t queue)
{
}

lp_err_t lp_sys_os_queue_empty(lp_sys_os_queue_t queue)
{
}

lp_err_t lp_sys_os_queue_full(lp_sys_os_queue_t queue)
{
}

lp_err_t lp_sys_os_queue_peep(lp_sys_os_queue_t queue, void* pval)
{
}

lp_err_t lp_sys_os_queue_remove(lp_sys_os_queue_t queue)
{
}

lp_err_t lp_sys_os_queue_push(lp_sys_os_queue_t queue, void* pval)
{
}

lp_err_t lp_sys_os_queue_pop(lp_sys_os_queue_t queue, void* pval)
{
}

lp_u16_t lp_sys_os_queue_curr_len(lp_sys_os_queue_t queue)
{
}

lp_u16_t lp_sys_os_queue_push_multi(lp_sys_os_queue_t queue, void* pval, lp_u16_t len)
{
}

lp_u16_t lp_sys_os_queue_pop_multi(lp_sys_os_queue_t queue, void* pval, lp_u16_t len)
{
}

#endif


#if (!LP_RTOS)
/**
 * @brief 动态创建队列
 * 
 * @param queue_size 队列元素大小(单位字节) 
 * @param max_queues 最大队列个数
 * @param keep_fresh  是否为保持最新模式,true：保持最新 false：默认(存满不能再存)
 * @return struct tk_queue* 创建的队列对象,NULL创建失败
 */
lp_sys_os_queue_t lp_sys_os_queue_create(lp_u16_t queue_size, lp_u16_t max_queues, lp_bool_t keep_fresh)
{
	return lp_queue_create(queue_size,max_queues,keep_fresh);
}

lp_err_t lp_sys_os_queue_delete(lp_sys_os_queue_t queue)
{
	return lp_queue_delete(queue);
}

lp_err_t lp_sys_os_queue_init(lp_sys_os_queue_t queue, void* queuepool, lp_u16_t pool_size, lp_u16_t queue_size, lp_bool_t keep_fresh)
{
	return lp_queue_init(queue,queuepool,pool_size,queue_size,keep_fresh);
}

lp_err_t lp_sys_os_queue_detach(lp_sys_os_queue_t queue)
{
	return lp_queue_detach(queue);
}

lp_err_t lp_sys_os_queue_clean(lp_sys_os_queue_t queue)
{
    return lp_queue_clean(queue);
}

lp_err_t lp_sys_os_queue_empty(lp_sys_os_queue_t queue)
{
    return lp_queue_empty(queue);
}

lp_err_t lp_sys_os_queue_full(lp_sys_os_queue_t queue)
{
    return lp_queue_full(queue);
}

lp_err_t lp_sys_os_queue_peep(lp_sys_os_queue_t queue, void* pval)
{
	return lp_queue_peep(queue,pval);
}

lp_err_t lp_sys_os_queue_remove(lp_sys_os_queue_t queue)
{
    return lp_queue_remove(queue);
}

lp_err_t lp_sys_os_queue_push(lp_sys_os_queue_t queue, void* pval)
{
    return lp_queue_push(queue,pval);
}

lp_err_t lp_sys_os_queue_pop(lp_sys_os_queue_t queue, void* pval)
{
    return lp_queue_pop(queue,pval);
}

lp_u16_t lp_sys_os_queue_curr_len(lp_sys_os_queue_t queue)
{
    return lp_queue_curr_len(queue);
}

lp_u16_t lp_sys_os_queue_push_multi(lp_sys_os_queue_t queue, void* pval, lp_u16_t len)
{
    return lp_queue_push_multi(queue,pval,len);
}

lp_u16_t lp_sys_os_queue_pop_multi(lp_sys_os_queue_t queue, void* pval, lp_u16_t len)
{
    return lp_queue_pop_multi(queue,pval,len);
}

#endif

























