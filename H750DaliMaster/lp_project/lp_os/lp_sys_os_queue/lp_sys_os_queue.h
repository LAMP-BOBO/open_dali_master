

#ifndef _LP_SYS_OS_QUEUE_H_
#define _LP_SYS_OS_QUEUE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "config/lp_config.h"

#if (LP_CMSIS_OS)&&(LP_RTOS)
#include "cmsis_os2.h"
	typedef osMessageQueueId_t lp_sys_os_queue_t;

	lp_err_t lp_sys_os_queue_push_block(lp_sys_os_queue_t queue, void* pval);
	lp_err_t lp_sys_os_queue_pop_block(lp_sys_os_queue_t queue, void* pval);
#endif

#if (!LP_RTOS)
	typedef  lp_queue_t lp_sys_os_queue_t;
	lp_err_t lp_sys_os_queue_init(lp_sys_os_queue_t queue, void* queuepool, lp_u16_t pool_size, lp_u16_t queue_size, lp_bool_t keep_fresh);
    lp_err_t lp_sys_os_queue_detach(lp_sys_os_queue_t queue);
	
	lp_err_t lp_sys_os_queue_peep(lp_sys_os_queue_t queue, void* pval);
    lp_err_t lp_sys_os_queue_remove(lp_sys_os_queue_t queue);
	
	    lp_u16_t lp_sys_os_queue_push_multi(lp_sys_os_queue_t queue, void* pval, lp_u16_t len);
    lp_u16_t lp_sys_os_queue_pop_multi(lp_sys_os_queue_t queue, void* pval, lp_u16_t len);
#endif

    lp_sys_os_queue_t lp_sys_os_queue_create(lp_u16_t queue_size, lp_u16_t max_queues, lp_bool_t keep_fresh);
    lp_err_t lp_sys_os_queue_delete(lp_sys_os_queue_t queue);

    lp_err_t lp_sys_os_queue_push(lp_sys_os_queue_t queue, void* pval);
    lp_err_t lp_sys_os_queue_pop(lp_sys_os_queue_t queue, void* pval);
	
    lp_err_t lp_sys_os_queue_clean(lp_sys_os_queue_t queue);
	lp_u16_t lp_sys_os_queue_curr_len(lp_sys_os_queue_t queue);
	
    lp_err_t lp_sys_os_queue_empty(lp_sys_os_queue_t queue);
    lp_err_t lp_sys_os_queue_full(lp_sys_os_queue_t queue);


#ifdef __cplusplus
}
#endif

#endif
































