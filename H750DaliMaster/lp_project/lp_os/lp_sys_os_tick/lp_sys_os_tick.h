


#ifndef _LP_SYS_OS_TICK_H
#define _LP_SYS_OS_TICK_H

#ifdef __cplusplus
extern "C" {
#endif


#include "config/lp_config.h"


lp_err_t lp_sys_os_tick_init(void);
void lp_sys_os_tick_hook(void);
lp_u32_t lp_get_sys_os_tick(void);

#ifdef __cplusplus
}
#endif

#endif


























