


#include "lp_sys_os_dali.h"
#include "lp_sys_os_exint/lp_sys_os_exint.h"
#include "lp_sys_os_hwtimer/lp_sys_os_hwtimer.h"
#include "lp_sys_os_gpio/lp_sys_os_gpio.h"




#define LP_SYS_DALI_QUEUE_NUM 	3

lp_sys_os_dali_bus dali_bus[LP_DALI_BUS_MAX_E];


static void lp_dali_get_send_bit(lp_psys_os_dali_bus dali_bus)
{
	lp_sys_os_dali_data dali_data;
	lp_u8_t returnData[3];
	
	if(lp_sys_os_queue_empty(dali_bus->queue_wirte) != LP_OK){
			lp_sys_os_queue_pop(dali_bus->queue_wirte,&dali_data);
			memcpy(returnData,dali_data.data,dali_data.len);
			dali_bus->daliSendbit_len = 0;
			dali_bus->daliSendbit_Status = 0;
		    memset(dali_bus->daliSendbit_buff,0x00,sizeof(dali_bus->daliSendbit_buff));
	}
	
	for(int j=0;j<dali_data.len;j++){
		for(int i=0;i<16;i+=2)
		{
			dali_bus->daliSendbit_len += 2;
			if(((returnData[j]>>(7-(i>>1)))&0x01) == 0x01)
			{
				dali_bus->daliSendbit_buff[j*16+i] = 0x00;
				dali_bus->daliSendbit_buff[j*16+i+1] = 0x01;
			}
			else
			{
				dali_bus->daliSendbit_buff[j*16+i] = 0x01;
				dali_bus->daliSendbit_buff[j*16+i+1] = 0x00;							
			}
		}
	}
    
}

#if 0 //旧的写法
void lp_dali_bus0_re_hook(void)
{
	lp_u32_t highTimerCount = 0;
	lp_u32_t lowTimerCount = 0;
	lp_u8_t rx_value = 0;
	lp_err_t ret = 	LP_FAIL;
	
	//进入中断读电平判断是上升沿还是下降沿
	lp_sys_os_gpio_read(LP_GPIO_DALI_RE_E,&rx_value);
	
	if(rx_value == 0)
	{
		if(dali_bus[LP_DALI_BUS0_E].dali_rev_state == 0){
			
			//下降沿
			dali_bus[LP_DALI_BUS0_E].dali_rev_state = 1;
			
			lp_sys_os_hwtimer_set_counter(dali_bus[LP_DALI_BUS0_E].rev_time_index,0);
			lp_sys_os_hwtimer_enable(dali_bus[LP_DALI_BUS0_E].rev_time_index);
			ret = lp_sys_os_timer_start(dali_bus[LP_DALI_BUS0_E].timer_dali_re,TIMER_MODE_SINGLE,2);
		}
		else
		{
			lp_sys_os_hwtimer_get_counter(dali_bus[LP_DALI_BUS0_E].rev_time_index,&highTimerCount);
			
			if(highTimerCount > 300 && highTimerCount < 550)// 416 +- 10%
			{
				dali_bus[LP_DALI_BUS0_E].pulseFlag[dali_bus[LP_DALI_BUS0_E].pulseIndex++] = 1;
			}
			else if(highTimerCount > 650 && highTimerCount < 1000)// 833 +- 10%
			{
				dali_bus[LP_DALI_BUS0_E].pulseFlag[dali_bus[LP_DALI_BUS0_E].pulseIndex++] = 1;
				dali_bus[LP_DALI_BUS0_E].pulseFlag[dali_bus[LP_DALI_BUS0_E].pulseIndex++] = 1;
			}
			lp_sys_os_hwtimer_set_counter(dali_bus[LP_DALI_BUS0_E].rev_time_index,0);
		}	
	}
	else
	{
		//上升沿
		lp_sys_os_hwtimer_get_counter(dali_bus[LP_DALI_BUS0_E].rev_time_index,&lowTimerCount);
		if(lowTimerCount > 300 && lowTimerCount < 550)//416 +- 10%
		{
			dali_bus[LP_DALI_BUS0_E].pulseFlag[dali_bus[LP_DALI_BUS0_E].pulseIndex++] = 0;
		}
		else if(lowTimerCount > 700 && lowTimerCount < 950)
		{
			dali_bus[LP_DALI_BUS0_E].pulseFlag[dali_bus[LP_DALI_BUS0_E].pulseIndex++] = 0;
			dali_bus[LP_DALI_BUS0_E].pulseFlag[dali_bus[LP_DALI_BUS0_E].pulseIndex++] = 0;
		}
		
		lp_sys_os_hwtimer_set_counter(dali_bus[LP_DALI_BUS0_E].rev_time_index,0);
	}

	ret = lp_sys_os_timer_restart(dali_bus[LP_DALI_BUS0_E].timer_dali_re);

	//LOG_OUT("highTimerCount=%d,lowTimerCount=%d ret=%d\r\n",highTimerCount,lowTimerCount,ret);
}

void dali_bus0_re_timeout_callback(lp_sys_os_timer_t timer)
{
	lp_err_t ret = LP_OK;
	lp_sys_os_dali_data dali_data = {0};
	lp_u8_t daliRData[3];
	lp_u8_t daliBitData[24];
	lp_u8_t daliBitDataIndex = 0;
	
	dali_bus[LP_DALI_BUS0_E].dali_rev_state = 0;
	lp_sys_os_hwtimer_set_counter(dali_bus[LP_DALI_BUS0_E].rev_time_index,0);
	lp_sys_os_hwtimer_disable(dali_bus[LP_DALI_BUS0_E].rev_time_index);
	
	//a pack data 1 beyt, 18 bit , 2bit start signal
	if(dali_bus[LP_DALI_BUS0_E].pulseIndex == 17) 
	{
		dali_bus[LP_DALI_BUS0_E].pulseFlag[dali_bus[LP_DALI_BUS0_E].pulseIndex++] = 1;
	}
	//a pack data 2 beyt, 34 bit , 2bit start signal
	else if(dali_bus[LP_DALI_BUS0_E].pulseIndex == 33) 
	{
		dali_bus[LP_DALI_BUS0_E].pulseFlag[dali_bus[LP_DALI_BUS0_E].pulseIndex++] = 1;
	}
	//a pack data 3 beyt, 50 bit , 2bit start signal
	else if(dali_bus[LP_DALI_BUS0_E].pulseIndex == 49)
	{
		dali_bus[LP_DALI_BUS0_E].pulseFlag[dali_bus[LP_DALI_BUS0_E].pulseIndex++] = 1;
	}
	
	for(int i=2;i<dali_bus[LP_DALI_BUS0_E].pulseIndex;i+=2)
	{
		if(dali_bus[LP_DALI_BUS0_E].pulseFlag[i]==0x00 && dali_bus[LP_DALI_BUS0_E].pulseFlag[i+1]==0x01)
		{
			daliBitData[daliBitDataIndex++] = 0x01;
			
		}
		else if(dali_bus[LP_DALI_BUS0_E].pulseFlag[i]==0x01 && dali_bus[LP_DALI_BUS0_E].pulseFlag[i+1]==0x00)
		{
			daliBitData[daliBitDataIndex++] = 0x00;
		}
		else
		{
			dali_bus[LP_DALI_BUS0_E].pulseIndex = 0;
			//error pulseIndex
			return;
		}
	}
	LOG_OUT("pulseIndex=%d daliBitDataIndex=%d\r\n",dali_bus[LP_DALI_BUS0_E].pulseIndex,daliBitDataIndex);
	dali_bus[LP_DALI_BUS0_E].pulseIndex = 0;
	
	if(daliBitDataIndex == 8) //other device ack signal
	{
		daliRData[0] = (daliBitData[0]<<7)|(daliBitData[1]<<6)|(daliBitData[2]<<5)|(daliBitData[3]<<4)|(daliBitData[4]<<3)|(daliBitData[5]<<2)|(daliBitData[6]<<1)|(daliBitData[7]);
		
		dali_data.data_type = LP_DALI_RX_BIT8_E;
		dali_data.len = 1;
		memcpy(dali_data.data,daliRData,dali_data.len);
	}
	else if(daliBitDataIndex == 16)// master signal
	{
		daliRData[0] = (daliBitData[0]<<7)|(daliBitData[1]<<6)|(daliBitData[2]<<5)|(daliBitData[3]<<4)|(daliBitData[4]<<3)|(daliBitData[5]<<2)|(daliBitData[6]<<1)|(daliBitData[7]);
		daliRData[1] = (daliBitData[8]<<7)|(daliBitData[9]<<6)|(daliBitData[10]<<5)|(daliBitData[11]<<4)|(daliBitData[12]<<3)|(daliBitData[13]<<2)|(daliBitData[14]<<1)|(daliBitData[15]);
	
		dali_data.data_type = LP_DALI_RX_BIT16_E;
		dali_data.len = 2;
		memcpy(dali_data.data,daliRData,dali_data.len);
		
	}
	else if(daliBitDataIndex == 24)
	{
		daliRData[0] = (daliBitData[0]<<7)|(daliBitData[1]<<6)|(daliBitData[2]<<5)|(daliBitData[3]<<4)|(daliBitData[4]<<3)|(daliBitData[5]<<2)|(daliBitData[6]<<1)|(daliBitData[7]);
		daliRData[1] = (daliBitData[8]<<7)|(daliBitData[9]<<6)|(daliBitData[10]<<5)|(daliBitData[11]<<4)|(daliBitData[12]<<3)|(daliBitData[13]<<2)|(daliBitData[14]<<1)|(daliBitData[15]);
		daliRData[2] = (daliBitData[16]<<7)|(daliBitData[17]<<6)|(daliBitData[18]<<5)|(daliBitData[19]<<4)|(daliBitData[20]<<3)|(daliBitData[21]<<2)|(daliBitData[22]<<1)|(daliBitData[23]);
		
		dali_data.data_type = LP_DALI_RX_BIT24_E;
		dali_data.len = 3;
		memcpy(dali_data.data,daliRData,dali_data.len);
	}
	
	ret = lp_sys_os_queue_push(dali_bus[LP_DALI_BUS0_E].queue_read,&dali_data);
	if(ret != LP_OK){
		LOG_OUT("DALI RE push queue fali\n");
	}

}
#endif

void lp_dali_bus0_re_hook(void)
{

	lp_u32_t TimerCount = 0;
	lp_u8_t rx_value = 0;
	lp_err_t ret = 	LP_FAIL;
	
	if(dali_bus[LP_DALI_BUS0_E].daliSendbit_Status == 0xff){ 
		lp_sys_os_gpio_read(LP_GPIO_DALI_RE_E,&rx_value);
		lp_sys_os_hwtimer_get_counter(dali_bus[LP_DALI_BUS0_E].rev_time_index,&TimerCount);
		lp_sys_os_hwtimer_set_counter(dali_bus[LP_DALI_BUS0_E].rev_time_index,0);
		if((rx_value == 0) && (dali_bus[LP_DALI_BUS0_E].dali_rev_state == 0)){
			//空闲是高电平，第一次进入肯定是低电平，开启超时定时器
			dali_bus[LP_DALI_BUS0_E].dali_rev_state = 1;
			dali_bus[LP_DALI_BUS0_E].pulseIndex = 0;
			
			dali_bus[LP_DALI_BUS0_E].pulseTime_len = 0;
			memset(dali_bus[LP_DALI_BUS0_E].pulseTime,0x00,sizeof(dali_bus[LP_DALI_BUS0_E].pulseTime));
			ret = lp_sys_os_timer_start(dali_bus[LP_DALI_BUS0_E].timer_dali_re,TIMER_MODE_SINGLE,2);
			return;
		}
		dali_bus[LP_DALI_BUS0_E].pulseTime[dali_bus[LP_DALI_BUS0_E].pulseIndex] = TimerCount;
		dali_bus[LP_DALI_BUS0_E].pulseIndex++;
		dali_bus[LP_DALI_BUS0_E].pulseTime_len++;
		

		ret = lp_sys_os_timer_restart(dali_bus[LP_DALI_BUS0_E].timer_dali_re);
		//LOG_OUT("TimerCount=%d\r\n",TimerCount);
	}
	
}

void dali_bus0_re_timeout_callback(lp_sys_os_timer_t timer)
{
	lp_err_t ret = LP_OK;
	lp_sys_os_dali_data dali_data = {0};
	
	bool ioState[64];
    bool ioData[64];
    uint8_t ioStateIndex = 0;
	uint8_t ioDataIndex = 0;
	
	lp_u8_t daliRData[3]={0};

	
	dali_bus[LP_DALI_BUS0_E].dali_rev_state = 0;
	

	for(int i = 0;i < dali_bus[LP_DALI_BUS0_E].pulseIndex;i++) //先高再低  //忽略0
    {
        if(dali_bus[LP_DALI_BUS0_E].pulseTime[i] > 300 && dali_bus[LP_DALI_BUS0_E].pulseTime[i] < 550)// 416 +- 10%   自身的误差+其他设备的误差
        {
            ioState[ioStateIndex++] = (i % 2)?true:false;
        }
        else if(dali_bus[LP_DALI_BUS0_E].pulseTime[i] > 650 && dali_bus[LP_DALI_BUS0_E].pulseTime[i] < 1000)// 833 +- 10%
        {
            ioState[ioStateIndex++] = (i % 2)?true:false;
            ioState[ioStateIndex++] = (i % 2)?true:false;
        }
        else
        {
            dali_data.data_type = LP_DALI_RX_ERROR_PLUSE_E;//脉宽异常
            dali_data.len = 0;
			LOG_OUT("LP_DALI_RX_ERROR_PLUSE_E %d\r\n",dali_bus[LP_DALI_BUS0_E].pulseIndex);
            goto code_end;
        }
    }
	
	if(ioStateIndex % 2)//补最后一个高电平
    {	//a pack data 1 beyt, 18 bit , 2bit start signal
		//a pack data 2 beyt, 34 bit , 2bit start signal
		//a pack data 3 beyt, 50 bit , 2bit start signal
        ioState[ioStateIndex++] = true;
    }
	
	//LOG_OUT("pulseIndex:%d ioStateIndex:%d \r\n",dali_bus[LP_DALI_BUS0_E].pulseIndex,ioStateIndex);
    for(int i = 2;i < ioStateIndex;i += 2)//起始位
    {
        if(!ioState[i] && ioState[i+1])
        {
            ioData[ioDataIndex++] = true;
        }
        else if(ioState[i] && !ioState[i+1])
        {
            ioData[ioDataIndex++] = false;
        }                
        else
        {
            dali_data.data_type = LP_DALI_RX_ERROR_DECODE_E;//解码异常
            dali_data.len = 0;
			LOG_OUT("LP_DALI_RX_ERROR_DECODE_E %d \r\n",dali_bus[LP_DALI_BUS0_E].pulseIndex);
            goto code_end;
        }
    }
	

    if(ioDataIndex == 8)
    {
        daliRData[0] = (ioData[0]<<7)|(ioData[1]<<6)|(ioData[2]<<5)|(ioData[3]<<4)|(ioData[4]<<3)|(ioData[5]<<2)|(ioData[6]<<1)|(ioData[7]);
        dali_data.data_type = LP_DALI_RX_BIT8_E;
		dali_data.len = 1;
		memcpy(dali_data.data,daliRData,dali_data.len);
        LOG_OUT("LP_DALI_RX_BIT8_E \r\n");
    }
    else if(ioDataIndex == 16)
    {
        daliRData[0] = (ioData[0]<<7)|(ioData[1]<<6)|(ioData[2]<<5)|(ioData[3]<<4)|(ioData[4]<<3)|(ioData[5]<<2)|(ioData[6]<<1)|(ioData[7]);
        daliRData[1] = (ioData[8]<<7)|(ioData[9]<<6)|(ioData[10]<<5)|(ioData[11]<<4)|(ioData[12]<<3)|(ioData[13]<<2)|(ioData[14]<<1)|(ioData[15]);	
        
        dali_data.data_type = LP_DALI_RX_BIT16_E;
		dali_data.len = 2;
		memcpy(dali_data.data,daliRData,dali_data.len);
		LOG_OUT("LP_DALI_RX_BIT16_E \r\n");
    }
    else if(ioDataIndex == 24)
    {
        daliRData[0] = (ioData[0]<<7)|(ioData[1]<<6)|(ioData[2]<<5)|(ioData[3]<<4)|(ioData[4]<<3)|(ioData[5]<<2)|(ioData[6]<<1)|(ioData[7]);
        daliRData[1] = (ioData[8]<<7)|(ioData[9]<<6)|(ioData[10]<<5)|(ioData[11]<<4)|(ioData[12]<<3)|(ioData[13]<<2)|(ioData[14]<<1)|(ioData[15]);	
        daliRData[2] = (ioData[16]<<7)|(ioData[17]<<6)|(ioData[18]<<5)|(ioData[19]<<4)|(ioData[20]<<3)|(ioData[21]<<2)|(ioData[22]<<1)|(ioData[23]);
        
        dali_data.data_type = LP_DALI_RX_BIT24_E;
		dali_data.len = 3;
		memcpy(dali_data.data,daliRData,dali_data.len);
		LOG_OUT("LP_DALI_RX_BIT24_E \r\n");
    }
    else if(ioDataIndex == 25)
    {
        daliRData[0] = (ioData[1]<<7)|(ioData[2]<<6)|(ioData[3]<<5)|(ioData[4]<<4)|(ioData[5]<<3)|(ioData[6]<<2)|(ioData[7]<<1)|(ioData[8]);
        daliRData[1] = (ioData[9]<<7)|(ioData[10]<<6)|(ioData[11]<<5)|(ioData[12]<<4)|(ioData[13]<<3)|(ioData[14]<<2)|(ioData[15]<<1)|(ioData[16]);	
        daliRData[2] = (ioData[17]<<7)|(ioData[18]<<6)|(ioData[19]<<5)|(ioData[20]<<4)|(ioData[21]<<3)|(ioData[22]<<2)|(ioData[23]<<1)|(ioData[24]);
        
        dali_data.data_type = LP_DALI_RX_BIT25_E;
		dali_data.len = 3;
		memcpy(dali_data.data,daliRData,dali_data.len);
		LOG_OUT("LP_DALI_RX_BIT25_E \r\n");
    }
    else
    {
        dali_data.data_type = LP_DALI_RX_UNKNOWN_BIT_E;
		dali_data.len = 0;
		LOG_OUT("LP_DALI_RX_UNKNOWN_BIT_E \r\n");
    }
	
code_end:
	
	dali_bus[LP_DALI_BUS0_E].pulseTime_len = dali_bus[LP_DALI_BUS0_E].pulseIndex;
	dali_data.pulseTime_len = dali_bus[LP_DALI_BUS0_E].pulseTime_len; 
	
	LOG_OUT("pulseTime_len %d \r\n",dali_data.pulseTime_len);
	memcpy(dali_data.pulseTime,dali_bus[LP_DALI_BUS0_E].pulseTime,sizeof(dali_bus[LP_DALI_BUS0_E].pulseTime));
	ret = lp_sys_os_queue_push(dali_bus[LP_DALI_BUS0_E].queue_read,&dali_data);
	if(ret != LP_OK){
		LOG_OUT("DALI RE push queue fali\n");
	}

}


void lp_dali_bus0_tr_hook(void)
{
	lp_u8_t start_time_l = 0;
	lp_u8_t start_time_h = 1;
	lp_u8_t data_time_low = start_time_h+1;
	lp_u8_t data_time_high = start_time_h + dali_bus[LP_DALI_BUS0_E].daliSendbit_len;//33
	lp_u8_t stop_bit_time_low = data_time_high + 1;
	lp_u8_t stop_bit_time_high = stop_bit_time_low + 1;
	
	//LOG_OUT("lp_dali_bus0_tr_hook\r\n");
	if(dali_bus[LP_DALI_BUS0_E].daliSendbit_Status == start_time_l) //发送数据的计时，8为一个bit，一共2bit起始信号+16bit数据+2bit空闲
	{
		lp_sys_os_gpio_write(LP_GPIO_DALI_TR_E,LP_GPIO_HIGH_E);
	}
	else if(dali_bus[LP_DALI_BUS0_E].daliSendbit_Status == start_time_h)
	{
		lp_sys_os_gpio_write(LP_GPIO_DALI_TR_E,LP_GPIO_LOW_E);
	}
	else if(dali_bus[LP_DALI_BUS0_E].daliSendbit_Status >= data_time_low && dali_bus[LP_DALI_BUS0_E].daliSendbit_Status <= data_time_high)//>=2 && <= 17
	{
		
		if(dali_bus[LP_DALI_BUS0_E].daliSendbit_buff[dali_bus[LP_DALI_BUS0_E].daliSendbit_Status - data_time_low])
			lp_sys_os_gpio_write(LP_GPIO_DALI_TR_E,LP_GPIO_LOW_E);
		else
			lp_sys_os_gpio_write(LP_GPIO_DALI_TR_E,LP_GPIO_HIGH_E);
	}
	else if(dali_bus[LP_DALI_BUS0_E].daliSendbit_Status >= stop_bit_time_low && dali_bus[LP_DALI_BUS0_E].daliSendbit_Status < stop_bit_time_high)
	{
		lp_sys_os_gpio_write(LP_GPIO_DALI_TR_E,LP_GPIO_LOW_E);
	}

	dali_bus[LP_DALI_BUS0_E].daliSendbit_Status++;
	if(dali_bus[LP_DALI_BUS0_E].daliSendbit_Status > stop_bit_time_high)
	{
		dali_bus[LP_DALI_BUS0_E].daliSendbit_Status = 0xff;
		lp_sys_os_hwtimer_disable(dali_bus[LP_DALI_BUS0_E].send_time_index);
		//lp_sys_os_exint_enable(LP_EXINT15_E);
	}
}


lp_err_t lp_sys_os_dali_read(lp_u8_t dali_register,lp_u8_t*buf,lp_u16_t*len)
{
	lp_err_t ret = LP_FAIL;
	lp_sys_os_dali_data dali_data;
	
	
	switch(dali_register){
	case LP_DALI_BUS0_E:
		if(lp_sys_os_queue_empty(dali_bus[LP_DALI_BUS0_E].queue_read) != LP_OK){
			ret = lp_sys_os_queue_pop(dali_bus[LP_DALI_BUS0_E].queue_read,&dali_data);
			*len = dali_data.len;
			memcpy(buf,dali_data.data,dali_data.len);
		}
		
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	return ret;
}

lp_err_t lp_sys_os_daliData_read(lp_u8_t dali_register,lp_sys_os_dali_data * pdali_data)
{
	lp_err_t ret = LP_FAIL;

	switch(dali_register){
	case LP_DALI_BUS0_E:
		if(lp_sys_os_queue_empty(dali_bus[LP_DALI_BUS0_E].queue_read) != LP_OK){
			ret = lp_sys_os_queue_pop(dali_bus[LP_DALI_BUS0_E].queue_read,pdali_data);
		}
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	return ret;
}


lp_err_t lp_sys_os_dali_write(lp_u8_t dali_register,lp_u8_t* data,lp_u16_t len)
{
	lp_err_t ret = LP_FAIL;
	lp_sys_os_dali_data dali_data;
	
	if(data == NULL)
	{
		ret = LP_FAIL;
		return ret;
	}
	
	switch(dali_register){
	case LP_DALI_BUS0_E:
		 if(dali_bus[LP_DALI_BUS0_E].queue_wirte == NULL){
			ret = LP_FAIL;
			return ret;
		 }
		 
		 dali_data.len = len;
		 memcpy(dali_data.data,data,len);
		 ret  = lp_sys_os_queue_push(dali_bus[LP_DALI_BUS0_E].queue_wirte,&dali_data);
		 lp_dali_get_send_bit(&dali_bus[LP_DALI_BUS0_E]);
		 //LOG_OUT("SEND DALI LEN:%d\r\n",dali_bus[LP_DALI_BUS0_E].daliSendbit_len);
		 
		 //主机在发送数据期间关闭接收
		 //lp_sys_os_exint_disable(LP_EXINT15_E);
		 lp_sys_os_hwtimer_enable(dali_bus[LP_DALI_BUS0_E].send_time_index);
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}




lp_err_t lp_sys_os_dali_init(lp_u8_t dali_register)
{
	lp_err_t ret = LP_FAIL;
	switch(dali_register){
	case LP_DALI_BUS0_E:
		dali_bus[LP_DALI_BUS0_E].which = 	LP_DALI_BUS0_E;
		dali_bus[LP_DALI_BUS0_E].send_time_index = LP_HWTIMER12_E;
		dali_bus[LP_DALI_BUS0_E].rev_time_index = LP_HWTIMER5_E;
	
		memset(dali_bus[LP_DALI_BUS0_E].daliSendbit_buff,0x00,sizeof(dali_bus[LP_DALI_BUS0_E].daliSendbit_buff));
		dali_bus[LP_DALI_BUS0_E].daliSendbit_len = 0;
		dali_bus[LP_DALI_BUS0_E].daliSendbit_Status = 0xff;

		dali_bus[LP_DALI_BUS0_E].dali_rev_state = 0;
		dali_bus[LP_DALI_BUS0_E].pulseIndex = 0;
		memset(dali_bus[LP_DALI_BUS0_E].pulseFlag,0x00,sizeof(dali_bus[LP_DALI_BUS0_E].pulseFlag));
	
		lp_sys_os_gpio_init(LP_GPIO_DALI_RE_E);
		lp_sys_os_gpio_init(LP_GPIO_DALI_TR_E);
	
		lp_sys_os_exint_init(LP_EXINT15_E,LP_EXINT_TRIGGER_BOTH_EDGE_E);
	    lp_sys_os_exint_bind(LP_EXINT15_E,lp_dali_bus0_re_hook);
		lp_sys_os_exint_enable(LP_EXINT15_E);
	
		lp_sys_os_hwtimer_init(dali_bus[LP_DALI_BUS0_E].rev_time_index,1000000,0xffff); //1us
	
		lp_sys_os_hwtimer_init(dali_bus[LP_DALI_BUS0_E].send_time_index,1000000,416);
		lp_sys_os_hwtimer_bind(dali_bus[LP_DALI_BUS0_E].send_time_index,lp_dali_bus0_tr_hook);
	
		lp_sys_os_hwtimer_enable(dali_bus[LP_DALI_BUS0_E].rev_time_index);
		dali_bus[LP_DALI_BUS0_E].timer_dali_re = lp_sys_os_timer_create(dali_bus0_re_timeout_callback);
		if(dali_bus[LP_DALI_BUS0_E].timer_dali_re == NULL)
		{
			LOG_OUT("timer_dali_re create error!\n");
			ret = LP_FAIL;
			return ret;
		}
		
		dali_bus[LP_DALI_BUS0_E].queue_read = lp_sys_os_queue_create(sizeof(lp_sys_os_dali_data),LP_SYS_DALI_QUEUE_NUM,true);
		dali_bus[LP_DALI_BUS0_E].queue_wirte = lp_sys_os_queue_create(sizeof(lp_sys_os_dali_data),LP_SYS_DALI_QUEUE_NUM,true);
		if(dali_bus[LP_DALI_BUS0_E].queue_read == NULL || dali_bus[LP_DALI_BUS0_E].queue_wirte == NULL){
			ret = LP_FAIL;
			LOG_OUT("dali queue creat fail\r\n");
			return ret;
		}
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}













