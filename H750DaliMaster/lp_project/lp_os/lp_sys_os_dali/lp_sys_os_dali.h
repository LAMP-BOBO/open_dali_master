

/* define to prevent recursive inclusion -------------------------------------*/
#ifndef _LP_SYS_OS_DALI_
#define _LP_SYS_OS_DALI_

#ifdef __cplusplus
extern "C" {
#endif

/* includes ------------------------------------------------------------------*/

#include "config/lp_config.h"
#include "lp_sys_os_queue/lp_sys_os_queue.h"
#include "lp_sys_os_sftimer/lp_sys_os_sftimer.h"

typedef enum{
    LP_DALI_RX_NULL_E,
    LP_DALI_RX_BIT8_E,
    LP_DALI_RX_BIT16_E,
    LP_DALI_RX_BIT24_E,
    LP_DALI_RX_BIT25_E,
    LP_DALI_RX_UNKNOWN_BIT_E,
    LP_DALI_RX_ERROR_PLUSE_E,
    LP_DALI_RX_ERROR_DECODE_E,
    LP_DALI_RX_END_E
}lp_dali_rx_type_e;

#define LP_SYS_OS_DALI_DATA_LEN	3

typedef struct _lp_sys_os_dali_data {
	
	lp_u8_t   data_type;
	lp_u16_t  len;
	lp_u8_t	  data[LP_SYS_OS_DALI_DATA_LEN];
	
	lp_u8_t  pulseTime_len;
	lp_u16_t pulseTime[64]; //如果要使用主机电平抓包功能需要打开这个
	
}lp_sys_os_dali_data, *lp_psys_os_dali_data;


typedef struct _lp_sys_os_dali_bus {
	
	lp_u8_t   which;
	lp_u8_t   send_time_index;
	lp_u8_t   rev_time_index;
	
	lp_sys_os_queue_t queue_read;
	lp_sys_os_queue_t queue_wirte;
	lp_sys_os_timer_t timer_dali_re;
	
	//private----------
	//dali send
	lp_u8_t daliSendbit_buff[64];
	lp_u8_t daliSendbit_len;
	lp_u8_t daliSendbit_Status; //发送状态，避免自己发送的数据被自己接受到.0xff的时候才可以接受数据
								//空闲状态是0xff
	
	//dali rev
	lp_u8_t  dali_rev_state; //0 为空闲状态 ， 1接收状态
	lp_u8_t  pulseIndex;
	lp_u8_t  pulseFlag[64]; //旧的写法
	lp_u8_t  pulseTime_len;
	lp_u16_t pulseTime[64];
	
}lp_sys_os_dali_bus, *lp_psys_os_dali_bus;


/* exported functions ------------------------------------------------------- */
lp_err_t lp_sys_os_dali_init(lp_u8_t dali_register);
lp_err_t lp_sys_os_dali_read(lp_u8_t dali_register,lp_u8_t*buf,lp_u16_t*len);
lp_err_t lp_sys_os_daliData_read(lp_u8_t dali_register,lp_sys_os_dali_data * dali_data);
lp_err_t lp_sys_os_dali_write(lp_u8_t dali_register,lp_u8_t* data,lp_u16_t len);

#ifdef __cplusplus
}
#endif

#endif

























