



#include "lp_sys_os_sftimer.h"
#include "lp_sys_os_tick/lp_sys_os_tick.h"


#if 0

lp_sys_os_timer_t lp_sys_os_timer_create_rtos(void(*timeout_callback)(lp_sys_os_timer_t timer),void *argument,lp_timer_mode mode)
{
	if(mode == TIMER_MODE_SINGLE){
		return osTimerNew(timeout_callback,osTimerOnce,argument,NULL);
	}
	else if(mode == TIMER_MODE_LOOP){
		return osTimerNew(timeout_callback,osTimerPeriodic,argument,NULL);
	}
	
	return NULL;
}

lp_err_t lp_sys_os_timer_delete(lp_sys_os_timer_t timer)
{
	osStatus_t ret = osOK;
	
	ret = osTimerDelete (timer);
	
	if(ret == osOK){
		return LP_OK;
	}else{
		return LP_FAIL;
	}
}

lp_err_t lp_sys_os_timer_start(lp_sys_os_timer_t timer, lp_timer_mode mode, lp_u32_t delay_tick)
{
	osStatus_t ret = osOK;
	ret =  osTimerStart(timer,delay_tick);
	if(ret == osOK){
		return LP_OK;
	}else{
		return LP_FAIL;
	}
}

lp_err_t lp_sys_os_timer_stop(lp_sys_os_timer_t timer)
{
	osStatus_t ret = osOK;
	ret = osTimerStop(timer);
	
	if(ret == osOK){
		return LP_OK;
	}else{
		return LP_FAIL;
	}
}

lp_err_t lp_sys_os_timer_restart(lp_sys_os_timer_t timer,lp_u32_t ticks)
{
	osStatus_t ret = osOK;
	
	ret =  osTimerStart(timer,ticks);
	if(ret == osOK){
		return LP_OK;
	}else{
		return LP_FAIL;
	}
}


#endif


#if 1
lp_err_t lp_sys_os_timer_func_init(void)
{
	return lp_timer_func_init(lp_get_sys_os_tick);
}

struct lp_timer* lp_sys_os_timer_create(void(*timeout_callback)(struct lp_timer* timer))
{
	return lp_timer_create(timeout_callback);
}

lp_err_t lp_sys_os_timer_delete(struct lp_timer* timer)
{
    return lp_timer_delete(timer);
}

lp_err_t lp_sys_os_timer_start(struct lp_timer* timer, lp_timer_mode mode, lp_u32_t delay_tick)
{
	return lp_timer_start(timer,mode,delay_tick);
}

lp_err_t lp_sys_os_timer_stop(struct lp_timer* timer)
{
	return lp_timer_stop(timer);
}

lp_err_t lp_sys_os_timer_continue(struct lp_timer* timer)
{
	return lp_timer_continue(timer);
}

lp_err_t lp_sys_os_timer_restart(struct lp_timer* timer)
{
	return lp_timer_restart(timer);
}


lp_err_t lp_sys_os_timer_loop_handler(void)
{
	return lp_timer_loop_handler();
}
#endif














