

#ifndef _LP_SYS_OS_SFTIMER_H_
#define _LP_SYS_OS_SFTIMER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "config/lp_config.h"


//#if (LP_CMSIS_OS)&&(LP_RTOS)
//rtos 自带的软件定时器无法在中断中使用
#if 0
#include "cmsis_os2.h"
//  ==== Timer Management Functions ==== line 489
// Timer callback function.
//typedef void (*osTimerFunc_t) (void *argument);

typedef  osTimerId_t lp_sys_os_timer_t;
lp_sys_os_timer_t lp_sys_os_timer_create_rtos(void(*timeout_callback)(lp_sys_os_timer_t timer),void *argument,lp_timer_mode mode);
lp_err_t lp_sys_os_timer_restart(lp_sys_os_timer_t timer,lp_u32_t ticks);
#endif


//#if (!LP_RTOS)
#if 1
	lp_err_t lp_sys_os_timer_func_init(void);
	lp_err_t lp_sys_os_timer_loop_handler(void);
	typedef  lp_timer_t lp_sys_os_timer_t;
	
	lp_sys_os_timer_t lp_sys_os_timer_create(void(*timeout_callback)(lp_sys_os_timer_t timer));
	lp_err_t lp_sys_os_timer_restart(lp_sys_os_timer_t timer);
	lp_err_t lp_sys_os_timer_continue(lp_sys_os_timer_t timer);
#endif

    lp_err_t lp_sys_os_timer_delete(lp_sys_os_timer_t timer);
    lp_err_t lp_sys_os_timer_start(lp_sys_os_timer_t timer, lp_timer_mode mode, lp_u32_t delay_tick);
    lp_err_t lp_sys_os_timer_stop(lp_sys_os_timer_t timer);

    

#ifdef __cplusplus
}
#endif

#endif
































