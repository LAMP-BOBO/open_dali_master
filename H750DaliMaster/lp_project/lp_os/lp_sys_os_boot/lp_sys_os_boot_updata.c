

#include "lp_sys_os_boot_updata.h"
#include "lp_sys_os_flash/lp_sys_os_flash.h"
#include "string.h"


lp_u8_t  flashWriteBuff[PAGE_SIZE];
lp_u16_t flashWriteIndex;
lp_u8_t  flashWriteCount;
lp_u32_t flashWriteSizeCount;
lp_u16_t flashWriteEndSize;
lp_u32_t flashWriteLastIndex = 0xFFFFFFFF;

static void dataInit(void)
{
	flashWriteLastIndex=0xFFFFFFFF;
	flashWriteIndex=0;
    flashWriteCount=0;
}        


lp_u8_t isEnterBootloder(lp_u8_t *p,lp_u16_t length)
{
	if(length == 7)
	{
			if((p[0]==0xAA)&&(p[1]==0xAF)&&(p[2]==0x0B)&&(p[3]==0x02)&&(p[4]==0x09))
			{
					lp_u8_t frame = p[0]+p[1]+p[2]+p[3]+p[4]+p[5];
					if(frame == p[6])
					{
						return 1;
					}
			}
	}
	return 0;
}


lp_u8_t lp_boot_revDataHandle(lp_u8_t *data, lp_u16_t length)
{
    if(memcmp(data,"Aero",4)==0)
    {
        uint16_t dataLen = (data[4]<<8)|data[5];
		uint8_t pcbID = data[6];
		
        if(dataLen == length)
        {
            uint16_t dataFrame = (data[length-2]<<8)|data[length-1];
            uint16_t frame = 0;
            for(int i=0;i<length-2;i++)
            {
                frame += data[i];
            }
            if(frame == dataFrame)
            {
                uint32_t fileIndex = (data[7]<<24)|(data[8]<<16)|(data[9]<<8)|data[10];
				if(flashWriteLastIndex != fileIndex)
				{
					flashWriteLastIndex = fileIndex;
					for(int i=11;i<length-2;i++)
					{
						flashWriteBuff[flashWriteIndex++] = data[i];
						if(flashWriteIndex >= PAGE_SIZE)
						{
							
							lp_sys_os_flash_write(APP_FIRMWARE_ADDR+PAGE_SIZE*flashWriteCount,(uint16_t *)flashWriteBuff,PAGE_SIZE);
							
							flashWriteCount++;
							flashWriteSizeCount+=PAGE_SIZE;
							flashWriteIndex = 0;
						}
					}
					
					if(fileIndex == 0xFFFFFFFF)
					{
						
						lp_sys_os_flash_write(APP_FIRMWARE_ADDR+PAGE_SIZE*flashWriteCount,(uint16_t *)flashWriteBuff,flashWriteIndex);
						
						flashWriteSizeCount+=flashWriteIndex;
						flashWriteCount++;
						flashWriteEndSize = flashWriteIndex;
						return LP_PACKAGE_FINISH_E;
					}
				}
            }
            else
            {
				dataInit();
                return LP_PACKAGE_FRAME_ERROR_E;
            }
        }   
        else
        {
			dataInit();
            return LP_PACKAGE_LENGTH_ERROR_E;
        }
    }
	else if(isEnterBootloder(data,length))
    {
		dataInit();
		if(data[5] == PCB_ID)
		{
				return LP_PACKAGE_WAIT_E;
		}
		return LP_PACKAGE_PCB_ERROR_E;
    }
    else if(memcmp(data,"Reset",5)==0)
    {
		return LP_PACKAGE_RESET_E;
    }
    else
    {
		dataInit();
        return LP_PACKAGE_HEAD_ERROR_E;
    }
	
    return LP_PACKAGE_OK_E;
}































