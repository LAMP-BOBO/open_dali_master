
/************************************************* 
Copyright : Aero-Light Co., Ltd. 2021-2031. All rights reserved.
File name:bootjump.c
Author：lamp_bobo
Version:V0.1
Description: boot jump app ,app reset up firmware;
Others: base on MDK AC5
Log:2021.11.17
*************************************************/


#include "lp_sys_os_boot.h"
#include "lp_sys_os_cpu/lp_sys_os_cpu.h"




/*must set keil tool*/
#if defined (__ICCARM__)  /* IAR */

#pragma location = ".NoInit"  
uint32_t g_JumpInit;

#elif defined(__CC_ARM)   /* MDK AC5 */

uint32_t lp_JumpInit __attribute__((at(0x20003C00), zero_init));
 
#elif  (defined (__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)) /* MDK AC6 */

uint32_t g_JumpInit __attribute__( ( section( ".bss.NoInit"))); 

#elif  defined(__GNUC__)      /* GCC */

__attribute__((section( ".NoInit"))) uint32_t g_JumpInit;

#endif 





lp_iapfun jump_app;
lp_err_t iap_load_app(lp_u32_t appxaddr)
{
    lp_err_t ret = LP_OK;
    
	if(((*(volatile lp_u32_t*)appxaddr)&0x2FFE0000)==0x20000000)
	{ 
		jump_app = (lp_iapfun)*(volatile lp_u32_t*)(appxaddr+4);		
		__set_MSP(*(volatile lp_u32_t*)appxaddr);
        
		jump_app();									
	}
    else{
         
        ret =  LP_FAIL;
    }
    
    return  ret;
}


/*boot use*/
lp_err_t lp_isUpgradeFirmware(lp_u32_t appxaddr)
{
	lp_err_t ret = LP_FAIL;
	
	if (lp_JumpInit == 0xAA553344)
	{	
        //jump app
		ret = iap_load_app(appxaddr);
	}
	else if (lp_JumpInit == 0x5AA51234)	    
	{
		//up firmware mode
        return ret;
	}
	else
	{
        //jump app
		ret = iap_load_app(appxaddr);
	}
	
    return ret;
}


/*app use*/
void lp_app_jumpboot_system_reset(void)
{
    
    lp_sys_os_disableINT();
    lp_JumpInit = 0x5AA51234;
    lp_sys_os_enableINT();
    NVIC_SystemReset();
}
    

/*boot updata complete system reset*/
void lp_iap_system_reset(void)
{
    lp_sys_os_disableINT();
    lp_JumpInit = 0xAA553344;
    lp_sys_os_enableINT();
	NVIC_SystemReset();
}















