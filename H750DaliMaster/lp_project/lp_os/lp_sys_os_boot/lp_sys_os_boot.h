


/* define to prevent recursive inclusion -------------------------------------*/
#ifndef _LP_SYS_OS_BOOT_
#define _LP_SYS_OS_BOOT_

#ifdef __cplusplus
extern "C" {
#endif

/* includes ------------------------------------------------------------------*/

#include "config/lp_config.h"

/* exported functions ------------------------------------------------------- */
typedef  void (*lp_iapfun)(void);

lp_err_t iap_load_app(lp_u32_t appxaddr);
lp_err_t lp_isUpgradeFirmware(lp_u32_t appxaddr);
void lp_app_jumpboot_system_reset(void);
void lp_iap_system_reset(void);

#ifdef __cplusplus
}
#endif

#endif































