



/* define to prevent recursive inclusion -------------------------------------*/
#ifndef _LP_SYS_OS_BOOT_UPDATA_
#define _LP_SYS_OS_BOOT_UPDATA_

#ifdef __cplusplus
extern "C" {
#endif

/* includes ------------------------------------------------------------------*/

#include "config/lp_config.h"

#define PCB_ID	2
#define PAGE_SIZE	1024

#define FLASH_BASE          ((uint32_t)0x08000000)
#define BOOTLOADER_SIZE		(16*1024)
#define APP_FIRMWARE_ADDR	(FLASH_BASE + BOOTLOADER_SIZE)

enum{
    LP_PACKAGE_OK_E,
    LP_PACKAGE_FINISH_E,
    LP_PACKAGE_WAIT_E,
	LP_PACKAGE_RESET_E,
    LP_PACKAGE_HEAD_ERROR_E,
    LP_PACKAGE_FRAME_ERROR_E,
    LP_PACKAGE_LENGTH_ERROR_E,
	LP_PACKAGE_PCB_ERROR_E,
    LP_PACKAGE_UNKNOWN_ERROR_E
};
/* exported functions ------------------------------------------------------- */
lp_u8_t lp_boot_revDataHandle(lp_u8_t *data, lp_u16_t length);

#ifdef __cplusplus
}
#endif

#endif















































