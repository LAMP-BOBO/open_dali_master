


#ifndef _LP_DALI_BUSX_H
#define _LP_DALI_BUSX_H

#ifdef __cplusplus
extern "C" {
#endif


#include "config/lp_config.h"
#include "lp_sys_os_queue/lp_sys_os_queue.h"
#include "lp_sys_os_sftimer/lp_sys_os_sftimer.h"


#define DALI_BUS0 	0


//__attribute__((packed))

typedef struct _lp_dali_log {

	lp_u8_t  pulseTime_len;
	lp_u16_t pulseTime[64];	

}lp_dali_log_t,*lp_pdali_log_t;


typedef struct _lp_dali_busx {
	
	lp_u8_t  which; 
	
	lp_err_t (*Init)(struct _lp_dali_busx *ptDALI_BUSx);
	lp_err_t (*Open)(struct _lp_dali_busx *ptDALI_BUSx);
	lp_err_t (*Read)(struct _lp_dali_busx *ptDALI_BUSx,lp_u8_t* data_type,lp_u8_t *data,lp_u16_t* len);
	lp_err_t (*Write)(struct _lp_dali_busx *ptDALI_BUSx,lp_u8_t *data,lp_u16_t len);
	lp_err_t (*Close)(struct _lp_dali_busx *ptDALI_BUSx);
	
	lp_err_t (*Read_log)(struct _lp_dali_busx *ptDALI_BUSx,lp_u16_t *data,lp_u16_t* len);
	lp_sys_os_queue_t log_queue; //用于主机打log
	
}lp_dali_busx, *lp_pdali_busx;


lp_pdali_busx lp_getDALI_BUS(lp_u8_t which);

#ifdef __cplusplus
}
#endif

#endif





























