




#ifndef _LP_DALI_MASTER_H
#define _LP_DALI_MASTER_H

#ifdef __cplusplus
extern "C" {
#endif


#include "config/lp_config.h"
#include "lp_sys_os_queue/lp_sys_os_queue.h"
#include "lp_sys_os_sftimer/lp_sys_os_sftimer.h"
#include "lp_dali_stack/lp_dali_busx.h"

#define LP_DALI_MASTER0 	0


typedef enum
{
	LP_DALI_TYPE_MASTER_E = 0x00,
	LP_DALI_TYPE_GEAR_E,
	LP_DALI_TYPE_CONTROL_E,

} lp_dali_device_type_e;

#define DALI_INVALID_DATA 	0xFF
#define DALI_SEND_NEED_ACK  0x01
#define DALI_SEND_NO_ACK  	0x00

#define DALI_DATA_8BIT(a) 		(((lp_u8_t)a)|(1<<24))
#define DALI_DATA_16BIT(a,b) 	(((lp_u8_t)a)|(((lp_u8_t)b)<<8)|(2<<24))
#define DALI_DATA_24BIT(a,b,c) 	(((lp_u8_t)a)|(((lp_u8_t)b)<<8)|(((lp_u8_t)c)<<24)|(3<<24))

//search_running

typedef enum
{
	LP_DALI_SEARCH_STATE_STOP_E = 0x00,
	LP_DALI_SEARCH_STATE_CHECK_E,
	LP_DALI_SEARCH_STATE_SEARCH_AGAIN_E,
	LP_DALI_SEARCH_STATE_SEARCH_END_E,
	LP_DALI_SEARCH_STATE_ONLY_CHECK_E,
	

} lp_dali_search_state_e;



typedef struct _lp_dali_devices_data_t {
    lp_u8_t addr;
    lp_u8_t device_type;//设备类型
    lp_u8_t status;//设备状态
    lp_u8_t lightness;//设备亮度
    lp_u8_t version_number;
    lp_u8_t min_level;
    lp_u8_t max_level;
}lp_dali_devices_data_t,*lp_pdali_devices_data_t;

typedef struct _lp_dali_packet_t{
	
    lp_u8_t  send_type;
    lp_u8_t  send_count;//发送次数
    lp_u8_t  send_length;//发送长度
    lp_u8_t  send_need_ack;//需要应答?
    lp_u32_t send_data;//发送数据
	
}lp_dali_packet_t,*lp_pdali_packet_t;

typedef struct _lp_dali_master_data_t{
    lp_u8_t device_num;//设备总数
	lp_u8_t search_running;
    lp_u8_t search_addr_confirm;//搜索有回复标志
    lp_u8_t search_reply_count;//搜索重发计数
    lp_u32_t master_search_addr:24;//二分法中间值,地址
    lp_u32_t master_search_last_addr:24;//保存上一次传给设备的地址
    lp_u32_t master_search_addr_min:24;//二分法最小值
    lp_u32_t master_search_addr_max:24;//二分法最大值
    lp_u32_t device_list[BITFIELD_BLOCK_COUNT(64)];

    lp_dali_devices_data_t device_info[64];//64个设备信息. 先用着后面再优化
}lp_dali_master_data_t,*lp_pdali_master_data_t;




typedef struct _lp_dali_master_t{
	//publish
	//搜索设备
	//获取设备信息
	//读取抓包数据
	lp_u8_t  which;
	lp_err_t (*Init)(struct _lp_dali_master_t *master);
	lp_err_t (*check_device)(struct _lp_dali_master_t* master);
	lp_err_t (*search_device)(struct _lp_dali_master_t* master,lp_bool_t clear_addr_flag);
	lp_err_t (*get_device_info)(struct _lp_dali_master_t* master,lp_u8_t addr);
	lp_err_t (*read_log)(struct _lp_dali_master_t* master);
	lp_err_t (*read_dev_info)(struct _lp_dali_master_t* master,lp_u8_t addr,lp_pdali_devices_data_t pdev_info);
	lp_err_t (*write_dev_info)(struct _lp_dali_master_t* master,lp_u8_t addr,lp_pdali_devices_data_t pdev_info);
	lp_err_t (*active_maxlevel)(struct _lp_dali_master_t* master,lp_u8_t addr);
	lp_err_t (*active_turnoff)(struct _lp_dali_master_t* master,lp_u8_t addr);
	lp_err_t (*set_dacp)(struct _lp_dali_master_t* master,lp_u8_t addr,lp_u8_t level);
	lp_err_t (*send_cmd)(struct _lp_dali_master_t *master,lp_u8_t addr,lp_u8_t cmd,lp_u8_t send_count,lp_u8_t ack);
	//private
	lp_u8_t revDali_ack;
	lp_u8_t revDali_len;
	lp_u8_t revDali_data[3];
	lp_pdali_busx	  dali_bus;
	lp_dali_master_data_t master_data;
	lp_sys_os_timer_t timer_dali_send;
	lp_sys_os_queue_t queue_dali_send; //待发送队列
	lp_dali_packet_t  current_packet;//当前处理的包
	
	

}lp_dali_master_t,*lp_pdali_master_t;



lp_pdali_master_t lp_getDALI_master(lp_u8_t which);


#ifdef __cplusplus
}
#endif

#endif

