
#include "lp_communi_task.h"
#include "lp_dali/aero_dali_command.h"
#include "lp_storage_task.h"
#include "lp_dali_task.h"
#include "stdio.h"

/*协议:IEC62386-102
*10 Declaration of variables
*/
dali_data_t daliData;//保存的数据
dali_scene_data_t daliSceneData;//保存场景数据

//static uint8_t randomAddrTemp[3];//临时保存的随机地址
static uint8_t returnData=0x08;//返回的数据 3250,436,797,437,386,431,694,436,386,853,790,435,388,436,381,435 
static uint8_t returnFlag;//是否要返回数据
static volatile uint8_t daliRegister = 0x00;//RAM 临时数据 DT0 DT1 DT2
static volatile uint8_t daliRegister1= 0x00;
static volatile uint8_t daliRegister2= 0x00;
static uint8_t searchH = 0xff;
static uint8_t searchM = 0xff;
static uint8_t searchL = 0xff;
static uint8_t initialisationState = DISABLED;
static uint8_t writeEnableState = DISABLED;
static uint16_t temporaryTc = PHY_WARM_MIREK;
static uint8_t daliAddrSaveFlag=false;
static uint8_t daliDataSaveFlag=false;
static uint8_t daliDimmerFlag=false;
static uint8_t daliDataSceneSaveFlag=false;

//DT8----------------------------
//bit 1 Colour temperature TC out of range; '0' = No
//bit 5 Colour type colour temperature TC active; '0' = No
static uint8_t dali_colour_status = 0;
static uint8_t dali_device_type = DEVICE_TYPE;

//----------------------------



static dali_bank0_t dali_bank0={
	.address = 0x1a,		
	.Reserved0 = 0xff,
	.Number = 0x01,			
	.GTIN0= 0x0,		
	.GTIN1= 0x0,
	.GTIN2= 0x0,
	.GTIN3= 0x0,
	.GTIN4= 0x0,
	.GTIN5= 0x0,			
	.Firmware_version0= 0x02,	
	.Firmware_version1= 0x00,
	.Identification0= 0x0,
	.Identification1= 0x0,
	.Identification2= 0x0,
	.Identification3= 0x0,
	.Identification4= 0x0,
	.Identification5= 0x0,
	.Identification6= 0x0,
	.Identification7= 0x0,
	.Hardware_version0= 0x01,
	.Hardware_version1= 0x00,
	.version_number101=DEVICE_DALI_VERSION,
	.version_number102=DEVICE_DALI_VERSION,
	.version_number103= 0xff,
	.logical_control_device= 0x00,	 //?
	.logical_control_gear= 0x01,
	.logical_control_gear_index= 0x00,
};

static dali_bank1_t dali_bank1={
	.address=0x10,		
	.Indicator=0x0e,		
	.lock= 0xff,		
	.OEM_GTIN0= 0x0,	 
	.OEM_GTIN1= 0x0,
	.OEM_GTIN2= 0x0,
	.OEM_GTIN3= 0x0,
	.OEM_GTIN4= 0x0,
	.OEM_GTIN5= 0x0,
	.OEM_identification0= 0x0,
	.OEM_identification1= 0x0,
	.OEM_identification2= 0x0,
	.OEM_identification3= 0x0,
	.OEM_identification4= 0x0,
	.OEM_identification5= 0x0,
	.OEM_identification6= 0x0,
	.OEM_identification7= 0x0,
};

void dali_bank0_reset(void)
{
	dali_bank0.address = 0x1a;	
	dali_bank0.Reserved0 = 0xff;
	dali_bank0.Number = 0x01;			
	dali_bank0.GTIN0= 0x0;	
	dali_bank0.GTIN1= 0x0;
	dali_bank0.GTIN2= 0x0;
	dali_bank0.GTIN3= 0x0;
	dali_bank0.GTIN4= 0x0;
	dali_bank0.GTIN5= 0x0;		
	dali_bank0.Firmware_version0= 0x02;
	dali_bank0.Firmware_version1= 0x00;
	dali_bank0.Identification0= 0x0;
	dali_bank0.Identification1= 0x0;
	dali_bank0.Identification2= 0x0;
	dali_bank0.Identification3= 0x0;
	dali_bank0.Identification4= 0x0;
	dali_bank0.Identification5= 0x0;
	dali_bank0.Identification6= 0x0;
	dali_bank0.Identification7= 0x0;
	dali_bank0.Hardware_version0= 0x01;
	dali_bank0.Hardware_version1= 0x00;
	dali_bank0.version_number101=DEVICE_DALI_VERSION;
	dali_bank0.version_number102=DEVICE_DALI_VERSION;
	dali_bank0.version_number103= 0xff;
	dali_bank0.logical_control_device= 0x00;	 
	dali_bank0.logical_control_gear= 0x01;
	dali_bank0.logical_control_gear_index= 0x00;
}

void dali_bank1_reset(void)
{
	dali_bank1.address=0x10;	
	dali_bank1.Indicator=0x0e;		
	dali_bank1.lock= 0xff;	
	dali_bank1.OEM_GTIN0= 0x0; 
	dali_bank1.OEM_GTIN1= 0x0;
	dali_bank1.OEM_GTIN2= 0x0;
	dali_bank1.OEM_GTIN3= 0x0;
	dali_bank1.OEM_GTIN4= 0x0;
	dali_bank1.OEM_GTIN5= 0x0;
	dali_bank1.OEM_identification0= 0x0;
	dali_bank1.OEM_identification1= 0x0;
	dali_bank1.OEM_identification2= 0x0;
	dali_bank1.OEM_identification3= 0x0;
	dali_bank1.OEM_identification4= 0x0;
	dali_bank1.OEM_identification5= 0x0;
	dali_bank1.OEM_identification6= 0x0;
	dali_bank1.OEM_identification7= 0x0;

}


pdali_data_t dali_get_pdata(void)
{
	return &daliData;
}

pdali_scene_data_t dali_get_pscene(void)
{
	return &daliSceneData;
}

void dali_set_short_addr(uint8_t addr)
{
	daliData.address      = addr;
	lp_dali_addr_updata();
}

uint8_t dali_get_short_addr(void)
{
	return daliData.address;
}

uint8_t dali_get_level(void)
{
	return daliData.level;
}

void aero_dali_reset_data(void)
{
	
	daliData.address      = 0xff;
	daliData.group        = 0x0000;
	daliData.dimmerCurve  = 0x00;
	daliData.maxLevel     = 0xFE;
	daliData.minLevel     = 0x05;
	daliData.poweronLevel = 0x05;
	daliData.errorLevel   = 0xFE;
	daliData.fade         = 0x11;
	daliData.extended_fade = 0x00;
	daliData.level        = 0xFE;
	daliData.tempTC       = PHY_WARM_MIREK;//0x0142;
	daliData.powerOnTC    = PHY_WARM_MIREK;
	daliData.invalidTC    = PHY_WARM_MIREK;
	daliData.warm         = PHY_WARM_MIREK;//0x0172; //Mirek =1000000÷T = 1000000/2700k = 370
	daliData.warmest	  = PHY_WARM_MIREK;//0x0172;
	daliData.cool         = PHY_COOL_MIREK;//0x00C8; //Mirek =1000000÷T = 1000000/5000k = 200
	daliData.coolest	  = PHY_COOL_MIREK;//0x00C8;
	daliData.randomAddrH  = 0xFF;
	daliData.randomAddrM  = 0xFF;
	daliData.randomAddrL  = 0xFF;
	daliData.mode         = 0x80; //0x80 to 0xFF: manufacturer specific modes
    for(int i=0;i<16;i++)
    {
        daliSceneData.level[i] = 0xFF;
        daliSceneData.tempTC[i]= 0xFFFF;
    }
}

void dali_set_random_addr(uint8_t h, uint8_t m, uint8_t l)
{
//	randomAddrTemp[0] = l;
//	randomAddrTemp[1] = m;
//	randomAddrTemp[2] = h;
	daliData.randomAddrH  = h;
	daliData.randomAddrM  = m;
	daliData.randomAddrL  = l;
	
}

uint8_t dali_get_returnData(void)
{
	return returnData;
}

void dali_debug_printf(uint8_t *p,uint8_t length)
{
	//printf("%s",p);
	//HAL_UART_Transmit_DMA(&huart1,p,length);
	//LOG_OUT("%s",p);
}

static void dali_commond_add_to_group(uint8_t num)
{
    daliData.group |= (1<<num);
	//dali_debug_printf("FUNC:dali_commond_add_to_group\r\n",sizeof("FUNC:dali_commond_add_to_group\r\n"));
    daliDataSaveFlag = true;
}

static void dali_commond_remove_from_group(uint8_t num)
{
    daliData.group &= ~(1<<num);
	//dali_debug_printf("FUNC:dali_commond_remove_from_group\r\n",sizeof("FUNC:dali_commond_remove_from_group\r\n"));
    daliDataSaveFlag = true;
}

static void dali_commond_query_device_type(void)//返回类型
{
    returnFlag = true;
    returnData = DEVICE_TYPE;
    //dali_debug_printf("FUNC:dali_commond_query_device_type\r\n",sizeof("FUNC:dali_commond_query_device_type\r\n"));
}

static void dali_commond_query_lamp_failure(void)//返回则是ERROR状态
{
    returnFlag = false;
    returnData = 0x00;
    //dali_debug_printf("FUNC:dali_commond_query_lamp_failure\r\n",sizeof("FUNC:dali_commond_query_lamp_failure\r\n"));
}

static void dali_commond_query_lamp_power_on(void)//
{
    if(daliData.level != 0x00)
        returnFlag = true;
    returnData = 0xFF;
    //dali_debug_printf("FUNC:dali_commond_query_lamp_power_on\r\n",sizeof("FUNC:dali_commond_query_lamp_power_on\r\n"));
}

static void dali_commond_query_group_0_7(void)
{
    returnFlag = true;
    returnData = daliData.group&0x00ff;
    //dali_debug_printf("FUNC:dali_commond_query_group_0_7\r\n",sizeof("FUNC:dali_commond_query_group_0_7\r\n"));
}

static void dali_commond_query_group_8_15(void)
{
    returnFlag = true;
    returnData = (daliData.group>>8)&0x00ff;
    //dali_debug_printf("FUNC:dali_commond_query_group_8_15\r\n",sizeof("FUNC:dali_commond_query_group_8_15\r\n"));
}

static void dali_commond_query_version_number(void)//返回的值跟上位机有关      0x08是DALI2.0，0x01是DALI1.0 
{
    returnFlag = true;
    returnData = DEVICE_DALI_VERSION;
    //dali_debug_printf("FUNC:dali_commond_query_version_number\r\n",sizeof("FUNC:dali_commond_query_version_number\r\n"));
}

//2023.05.08
//根据查询的dtr0 dtr1 查询数据
static void dali_commond_read_memory_location(void)
{
	uint8_t * p = NULL;
	if(daliRegister1 == 0x00){
		if(daliRegister>dali_bank0.address){
			returnFlag = false;
			returnData = 0x00;
		}
		else{
			returnFlag = true;
			p = (uint8_t*)&dali_bank0.address;
			returnData = p[daliRegister];
		}
	}
	else if(daliRegister1 == 0x01)
	{
		if(daliRegister>dali_bank1.address){
			returnFlag = false;
			returnData = 0x00;
		}
		else{
			returnFlag = true;
			p = (uint8_t*)&dali_bank1.address;
			returnData = p[daliRegister];
		}
	}
	else{
		returnFlag = false;
		returnData = 0x00;
	}
	
	if(daliRegister1<0xff){
		daliRegister++;
	}

    //dali_debug_printf("FUNC:dali_commond_read_memory_location\r\n",sizeof("FUNC:dali_commond_read_memory_location\r\n"));
}

static void dali_spec_commond_dtr0(uint8_t data)
{
    daliRegister = data;
    //dali_debug_printf("FUNC:dali_spec_commond_dtr0\r\n",sizeof("FUNC:dali_spec_commond_dtr0\r\n"));
}

static void dali_spec_commond_save_to_dtr1(uint8_t data)
{
    daliRegister1 = data;
    //dali_debug_printf("FUNC:dali_spec_commond_save_to_dtr1\r\n",sizeof("FUNC:dali_spec_commond_save_to_dtr1\r\n"));
}

static void dali_spec_commond_save_to_dtr2(uint8_t data)
{
    daliRegister2 = data;
    //dali_debug_printf("FUNC:dali_spec_commond_save_to_dtr2\r\n",sizeof("FUNC:dali_spec_commond_save_to_dtr2\r\n"));
}

//DALI_SPECIAL_COMMOND_WRITE_MEMORY_LOCATION
static void dali_spec_commond_write_memory_location(uint8_t data)
{
    if(writeEnableState == DISABLED){
		returnFlag = false;
		return;
	}
	else{
		//uint8_t * p = NULL;
		if(daliRegister1 == 0x00){
			if(daliRegister>dali_bank0.address){
				returnFlag = false;
				returnData = 0x00;
			}
			else{
				returnFlag = true;
				//p = (uint8_t*)&dali_bank0.address;
				//returnData = p[daliRegister];
				returnData = data;
			}
		}
		else if(daliRegister1 == 0x01)
		{
			if(daliRegister>dali_bank1.address){
				returnFlag = false;
				returnData = 0x00;
			}
			else{
				returnFlag = true;
				//p = (uint8_t*)&dali_bank1.address;
				//returnData = p[daliRegister];
				returnData = data;
			}
		}
		else{
			returnFlag = false;
			returnData = 0x00;
		}
	}
    //dali_debug_printf("FUNC:dali_spec_commond_write_memory_location\r\n",sizeof("FUNC:dali_spec_commond_write_memory_location\r\n"));
}

//DALI_SPECIAL_COMMOND_WRITE_MEMORY_LOCATION_NO_REPLY
static void dali_spec_commond_write_memory_location_no_reply(uint8_t data)
{
    dali_spec_commond_write_memory_location(data);
	returnFlag = false;
    //dali_debug_printf("FUNC:dali_spec_commond_write_memory_location_no_reply\r\n",sizeof("FUNC:dali_spec_commond_write_memory_location_no_reply\r\n"));
}


static void dali_commond_set_dimmer_curve(void)
{
    if(daliData.dimmerCurve != daliRegister)
    {
        daliData.dimmerCurve = daliRegister;
        daliDimmerFlag = true;
    }
    //dali_debug_printf("FUNC:DALI_COMMOND_SET_DIMMER_CURVE\r\n",sizeof("FUNC:DALI_COMMOND_SET_DIMMER_CURVE\r\n"));
    daliDataSaveFlag = true;
}

static void dali_commond_query_dimmer_curve(void)
{
    returnFlag = true;
    returnData =  daliData.dimmerCurve;
    //dali_debug_printf("FUNC:DALI_COMMOND_QUERY_DIMMER_CURVE\r\n",sizeof("FUNC:DALI_COMMOND_QUERY_DIMMER_CURVE\r\n"));
}

static void dali_commond_query_physical_minimum(void)//查询物理最小功率等级
{
    returnFlag = true;
    returnData = DEVICE_PHYSICS_MIN_LEVEL;
    //dali_debug_printf("FUNC:DALI_COMMOND_RETUEN_PHY_MIN\r\n",sizeof("FUNC:DALI_COMMOND_RETUEN_PHY_MIN\r\n"));
}

static void dali_commond_set_max_level(void)
{
    daliData.maxLevel = daliRegister;
    //dali_debug_printf("FUNC:dali_commond_set_max_level\r\n",sizeof("FUNC:dali_commond_set_max_level\r\n"));
    daliDataSaveFlag = true;
}

static void dali_commond_set_min_level(void)
{
    daliData.minLevel = daliRegister;
    //dali_debug_printf("FUNC:dali_commond_set_min_level\r\n",sizeof("FUNC:dali_commond_set_min_level\r\n"));
    daliDataSaveFlag = true;
}

static void dali_commond_recall_max_level(void)
{
    daliData.level = daliData.maxLevel;
    //dali_debug_printf("FUNC:DALI_COMMOND_LIMIT_MAX\r\n",sizeof("FUNC:DALI_COMMOND_LIMIT_MAX\r\n"));
    daliDataSaveFlag = true;
    daliDimmerFlag = true;
}

static void dali_commond_step_down_and_off(void)
{
    if(daliData.level == 0)
        return ;
    if(daliData.level == daliData.minLevel)
    {
        daliData.level = 0; 
    }
    else
    {
        daliData.level--;
    }
    //dali_debug_printf("FUNC:DALI_COMMOND_STEP_DOWN\r\n",sizeof("FUNC:DALI_COMMOND_STEP_DOWN\r\n"));
    daliDataSaveFlag = true;
    daliDimmerFlag = true;
}

static void dali_commond_on_and_step_up(void)
{
    if(daliData.level == daliData.maxLevel)
        return;
    if(daliData.level == 0)
      daliData.level = daliData.minLevel;
    else
      daliData.level++;
    //dali_debug_printf("FUNC:DALI_COMMOND_STEP_UP\r\n",sizeof("FUNC:DALI_COMMOND_STEP_UP\r\n"));
    daliDataSaveFlag = true;
    daliDimmerFlag = true;
}

//DALI_STANDARD_COMMOND_ENABLE_DAPC_SEQUENCE
static void dali_commond_enable_dapc_sequence(void)
{
	returnFlag = false;
}

static void dali_commond_recall_min_level(void)
{
    daliData.level = daliData.minLevel;
    //dali_debug_printf("FUNC:DALI_COMMOND_LIMIE_MIN\r\n",sizeof("FUNC:DALI_COMMOND_LIMIE_MIN\r\n"));
    daliDataSaveFlag = true;
    daliDimmerFlag = true;
}

/******************************************
协议:IEC62386-102
编号:3
指令代码:YAAA AAA1 0000 0003
详细:11.3.5
描述:如果level=0或者level=maxLevel,不变化
     快速变化,不需要渐变
******************************************/
static void dali_commond_step_up(void)
{
    //dali_debug_printf("FUNC:DALI_COMMOND_UPGRADE\r\n",sizeof("FUNC:DALI_COMMOND_UPGRADE\r\n"));
    if((daliData.level == 0) || (daliData.level == daliData.maxLevel))
        return;
    daliData.level++;
    daliDataSaveFlag = true;
    daliDimmerFlag = true;
}

/******************************************
协议:IEC62386-102
编号:4
指令代码:YAAA AAA1 0000 0004
详细:11.3.6
描述:如果level=0或者level=minLevel,不变化
     快速变化,不需要渐变
******************************************/
static void dali_commond_step_down(void)
{
    //dali_debug_printf("FUNC:DALI_COMMOND_DEGRADE\r\n",sizeof("FUNC:DALI_COMMOND_DEGRADE\r\n"));
    if((daliData.level == 0) || (daliData.level == daliData.minLevel))
        return;
    daliData.level--;
    daliDataSaveFlag = true;
    daliDimmerFlag = true;
}

static void dali_commond_query_content_dtr0(void)
{
    returnFlag = true;
    returnData = daliRegister;
    //dali_debug_printf("FUNC:DALI_COMMOND_READ_DTR_DATA\r\n",sizeof("FUNC:DALI_COMMOND_READ_DTR_DATA\r\n"));
}

static void dali_commond_query_content_dtr1(void)
{
    returnFlag = true;
    returnData = daliRegister1;
    //dali_debug_printf("FUNC:dali_commond_query_content_dtr1\r\n",sizeof("FUNC:dali_commond_query_content_dtr1\r\n"));
}

static void dali_commond_query_content_dtr2(void)
{
    returnFlag = true;
    returnData = daliRegister2;
    //dali_debug_printf("FUNC:dali_commond_query_content_dtr2\r\n",sizeof("FUNC:dali_commond_query_content_dtr2\r\n"));
}

//case DALI_STANDARD_COMMOND_QUERY_OPEARTING_MODE:break;
static void dali_commond_query_opearting_mode(void)
{
    returnFlag = true;
    returnData = daliData.mode;
    //dali_debug_printf("FUNC:dali_commond_query_opearting_mode\r\n",sizeof("FUNC:dali_commond_query_opearting_mode\r\n"));
}
//case DALI_STANDARD_COMMOND_QUERY_LIGHT_SOURCE_TYPE:break;
static void dali_commond_query_light_source_type(void)
{
    returnFlag = true;
    returnData = LIGHT_SOURCE_TYPE;
    //dali_debug_printf("FUNC:dali_commond_query_light_source_type\r\n",sizeof("FUNC:dali_commond_query_light_source_type\r\n"));
}


static void dali_commond_query_actual_level(void)
{
    returnFlag = true;
    returnData = daliData.level;
    temporaryTc = daliData.tempTC;
    //dali_debug_printf("FUNC:dali_commond_query_actual_level\r\n",sizeof("FUNC:dali_commond_query_actual_level\r\n"));
}

static void dali_commond_query_power_on_level(void)
{
    returnFlag = true;
    returnData = daliData.poweronLevel;
    temporaryTc = daliData.powerOnTC;
    //dali_debug_printf("FUNC:dali_commond_query_power_on_level\r\n",sizeof("FUNC:dali_commond_query_power_on_level\r\n"));
}

static void dali_commond_query_system_failure_level(void)
{
    returnFlag = true;
    returnData = daliData.errorLevel;
    temporaryTc = daliData.invalidTC;
    //dali_debug_printf("FUNC:dali_commond_query_system_failure_level\r\n",sizeof("FUNC:dali_commond_query_system_failure_level\r\n"));
}

static void dali_commond_query_max_level(void)
{
    returnFlag = true;
    returnData =  daliData.maxLevel;
    //dali_debug_printf("FUNC:dali_commond_query_max_level\r\n",sizeof("FUNC:dali_commond_query_max_level\r\n"));
}

static void dali_commond_query_min_level(void)
{
    returnFlag = true;
    returnData =  daliData.minLevel;
    //dali_debug_printf("FUNC:dali_commond_query_min_level\r\n",sizeof("FUNC:dali_commond_query_min_level\r\n"));
}

static void dali_commond_query_scene_level(uint8_t sceneIndex)
{
    returnFlag = true;
    returnData = daliSceneData.level[sceneIndex];
    temporaryTc = daliSceneData.tempTC[sceneIndex];
    //dali_debug_printf("FUNC:dali_commond_query_scene_level\r\n",sizeof("FUNC:dali_commond_query_scene_level\r\n"));
}

static void dali_commond_go_to_scene(uint8_t sceneIndex)
{
	if(daliSceneData.level[sceneIndex] != 0xFF)
	{
		daliData.level = daliSceneData.level[sceneIndex];
	}
	if(daliSceneData.tempTC[sceneIndex] != 0xFFFF)
	{
		daliData.tempTC = daliSceneData.tempTC[sceneIndex];
	}
	daliDimmerFlag = true;
	daliDataSaveFlag = true;
    //dali_debug_printf("FUNC:DALI_COMMOND_SCENE\r\n",sizeof("FUNC:DALI_COMMOND_SCENE\r\n"));
} 

static void dali_commond_set_scene(uint8_t sceneIndex)
{
    daliSceneData.level[sceneIndex]  = daliRegister;
    daliSceneData.tempTC[sceneIndex] = temporaryTc;
    //dali_debug_printf("FUNC:DALI_COMMOND_SET_DTR_SCENE_LEVEL\r\n",sizeof("FUNC:DALI_COMMOND_SET_DTR_SCENE_LEVEL\r\n"));
    daliDataSceneSaveFlag = true;
}

static void dali_commond_remove_from_scene(uint8_t sceneIndex)
{
    daliSceneData.level[sceneIndex] = 0xFF;
    //dali_debug_printf("FUNC:dali_commond_remove_from_scene\r\n",sizeof("FUNC:dali_commond_remove_from_scene\r\n"));
    daliDataSceneSaveFlag = true;
}

static void dali_commond_set_system_failure_level(void)
{
    daliData.errorLevel = daliRegister;
    daliData.invalidTC = temporaryTc;
    //dali_debug_printf("FUNC:dali_commond_set_system_failure_level\r\n",sizeof("FUNC:dali_commond_set_system_failure_level\r\n"));
    daliDataSaveFlag = true;
}

//case DALI_STANDARD_COMMOND_QUERY_MANUFACTURER_SPECIFIC_MODE:break;
static void dali_commond_query_manufacturer_specific_mode(void)
{

    //dali_debug_printf("FUNC:dali_commond_query_manufacturer_specific_mode\r\n",sizeof("FUNC:dali_commond_query_manufacturer_specific_mode\r\n"));
    //daliDataSaveFlag = true;
	returnFlag = true;
	returnData = 0xff;
}


//case DALI_STANDARD_COMMOND_QUERY_NEXT_DEVICE_TYPE:break;
static void dali_commond_query_next_device_type(void)
{
	returnFlag = false;
    //dali_debug_printf("FUNC:dali_commond_set_system_failure_level\r\n",sizeof("FUNC:dali_commond_set_system_failure_level\r\n"));
}

//case DALI_STANDARD_COMMOND_QUERY_EXTENDED_FADE_TIME:break;
static void dali_commond_query_extended_fade_time(void)
{
    //dali_debug_printf("FUNC:dali_commond_query_extended_fade_time\r\n",sizeof("FUNC:dali_commond_query_extended_fade_time\r\n"));
    returnFlag = true;
	returnData = daliData.extended_fade;
}

//case DALI_STANDARD_COMMOND_QUERY_CONTROL_GEAR_FAILURE:break;
static void dali_commond_query_control_grar_failuer(void)
{
	returnFlag = false;
    //dali_debug_printf("FUNC:dali_commond_query_control_grar_failuer\r\n",sizeof("FUNC:dali_commond_query_control_grar_failuer\r\n"));
}

static void dali_commond_set_power_on_level(void)
{
    daliData.poweronLevel = daliRegister;
    daliData.powerOnTC    = temporaryTc;
    //dali_debug_printf("FUNC:DALI_COMMOND_SET_DTR_POWER_ON_LEVEL\r\n",sizeof("FUNC:DALI_COMMOND_SET_DTR_POWER_ON_LEVEL\r\n"));
    daliDataSaveFlag = true;
}

static void dali_commond_query_fade_time_rate(void)
{
    returnFlag = true;
    returnData = daliData.fade;
    //dali_debug_printf("FUNC:dali_commond_query_fade_time_rate\r\n",sizeof("FUNC:dali_commond_query_fade_time_rate\r\n"));
}


static void dali_commond_set_fade_time(void)
{
    daliData.fade &= 0x0F;
    daliData.fade |= (daliRegister<<4)&0xF0;
    //dali_debug_printf("FUNC:dali_commond_set_fade_time\r\n",sizeof("FUNC:dali_commond_set_fade_time\r\n"));	
    daliDataSaveFlag = true;
}

static void dali_commond_set_fade_rate(void)
{
    daliData.fade &= 0xF0;
    daliData.fade |= daliRegister&0x0F;
    //dali_debug_printf("FUNC:dali_commond_set_fade_rate\r\n",sizeof("FUNC:dali_commond_set_fade_rate\r\n"));	
    daliDataSaveFlag = true;
}

//DALI_STANDARD_COMMOND_SET_EXTENDED_FADE_TIME
static void dali_commond_set_extended_fade_time(void)
{
	//Fade time = extendedFadeTimeBase * extendedFadeTimeMultiplier
	//extendedFadeTimeBase = daliRegister&0x0f
	//extendedFadeTimeMultiplier = (daliRegister>>4)&0x0f
	/*
		000b  0 ms 
		001b  100 ms 
		010b  1 s
		011b  10 s 
		100b  1 min 
	*/
	if(daliRegister > 0x4f){
		daliData.extended_fade = 0x00;
	}
	
	daliData.extended_fade = daliRegister;
	//dali_debug_printf("FUNC:dali_commond_set_extended_fade_time\r\n",sizeof("FUNC:dali_commond_set_extended_fade_time\r\n"));	
	daliDataSaveFlag = true;
}


static void dali_commond_set_short_address(void)
{
   // returnFlag = true;
    daliData.address = daliRegister>>1;
    returnData = 0x00;
    //dali_debug_printf("FUNC:dali_commond_set_short_address\r\n",sizeof("FUNC:dali_commond_set_short_address\r\n"));	
    daliDataSaveFlag = true;
//	creatRandomAddrStart();
}

static void dali_commond_enable_write_memory(void)
{
    returnFlag = false;
	writeEnableState = ENABLED;
    //dali_debug_printf("FUNC:dali_commond_query_fade_time_rate\r\n",sizeof("FUNC:dali_commond_query_fade_time_rate\r\n"));
}

static void dali_commond_query_missing_short_address(void)
{
    if(daliData.address > 63)
      returnFlag = true;
    returnData = 0x00;
    //dali_debug_printf("FUNC:dali_commond_query_missing_short_address\r\n",sizeof("FUNC:dali_commond_query_missing_short_address\r\n"));
}

static void dali_commond_store_actual_level_in_dtr0(void)
{
    daliRegister = daliData.level;
    //dali_debug_printf("FUNC:dali_commond_store_actual_level_in_dtr0\r\n",sizeof("FUNC:dali_commond_store_actual_level_in_dtr0\r\n"));
}

//DALI_STANDARD_COMMOND_SAVE_PERSISTENT_VARIABLES:;
static void dali_commond_save_persistent_variables(void)
{
	returnFlag = false;
	daliAddrSaveFlag = true;
	daliDataSaveFlag = true;
}

//DALI_STANDARD_COMMOND_SET_OPERATING_MODE:;
static void dali_commond_set_operating_mode(void)
{
	returnFlag = false;
}

//DALI_STANDARD_COMMOND_RESET_MEMORY_BANK:; 
static void dali_commond_reset_memory_bank(void)
{
	returnFlag = false;
	//RESET
	dali_bank0_reset();
	dali_bank1_reset();
}

//DALI_STANDARD_COMMOND_IDENTIFY_DEVICE:;
static void dali_commond_identify_device(void)
{
	returnFlag = false; //可以增加呼吸指令
}

static void dali_commond_query_limit_error(void)
{
    //uint8_t temp = getLightLevelFromPwm();
    //returnData = 0xFF;
    //if((temp < daliData.minLevel) || (temp > daliData.maxLevel) || (temp > daliData.errorLevel) || (temp < DEVICE_PHYSICS_MIN_LEVEL))
    //    returnFlag = true;
    //__LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "dali_commond_query_limit_error\r\n");
}

static void dali_commond_query_control_gear_present(void)
{
	returnData = 0xFF;
    returnFlag = true;
    //dali_debug_printf("FUNC:dali_commond_query_control_gear_present\r\n",sizeof("FUNC:dali_commond_query_control_gear_present\r\n"));
}

//
static void dali_commond_query_status(void)
{
    returnFlag = true;
    returnData = 0x00;
    //dali_debug_printf("FUNC:DALI_COMMOND_QUERY_STATUS\r\n",sizeof("FUNC:DALI_COMMOND_QUERY_STATUS\r\n"));
}

static void dali_commond_query_reset_state(void)
{
    returnFlag = false;
    returnData = 0x00;
    //dali_debug_printf("FUNC:dali_commond_query_reset_state\r\n",sizeof("FUNC:dali_commond_query_reset_state\r\n"));
}

static void dali_commond_reset(void)
{
    #if SAVE_ENABLE
		aero_dali_reset_data();
    #endif
    daliDataSaveFlag = true;
    daliDataSceneSaveFlag = false;
}

//DALI_EXTENDED_COMMOND_QUERY_COLOUR_TYPE_FEATURES
static void dali_commond_query_colour_type_features(void)
{
	if(DEVICE_TYPE == 0x08){
		returnData = COLOUR_TYPE_FEATURES;
		returnFlag = true;
	}
	
}

static void dali_commond_query_colour_value(void)
{
    if(daliRegister >= 0x80 && daliRegister<=0x83)
    {
        uint16_t tempData = 0x0000;
        switch(daliRegister) //COLOUR TEMPERATURE TC COOLEST
        {
            case 0x80:tempData = daliData.coolest;break;
            case 0x81:tempData = daliData.cool;break;
            case 0x82:tempData = daliData.warmest;break;
            case 0x83:tempData = daliData.warm;break;
        }
        returnData = tempData>>8;
        daliRegister = (tempData&0xFF);
        returnFlag = true;
    }
    else if(daliRegister == 0xE2)//REPORT COLOUR TEMPERATURE TC
    {
        returnData = temporaryTc>>8;
        daliRegister = temporaryTc&0xff;
        returnFlag = true;
    }
    else if(daliRegister == 0xF0) //REPORT COLOUR TYPE
    {
        returnData = 0x20;
        daliRegister = 0x20;
        returnFlag = true;
    }
}

static void dali_commond_set_color_temp(void)//设置临时色温值
{
    temporaryTc = (daliRegister1<<8)|daliRegister;
}

static void dali_commond_activate(void)
{
    daliData.tempTC = temporaryTc;
    if(daliData.tempTC > daliData.warmest)
        daliData.tempTC = daliData.warmest;
    if(daliData.tempTC < daliData.coolest)
        daliData.tempTC = daliData.coolest;
    daliDataSaveFlag = true;
    //dali_debug_printf("FUNC:DALI_COMMOND_ACTIVATE\r\n",sizeof("FUNC:DALI_COMMOND_ACTIVATE\r\n"));
    daliDimmerFlag = true;
}

static void dali_commond_set_color_temp_limit(void)
{
    switch(daliRegister2)
    {
        case 0x03:daliData.warm = ((daliRegister1<<8)|daliRegister);daliDataSaveFlag=true;break;
        case 0x02:daliData.cool	= ((daliRegister1<<8)|daliRegister);daliDataSaveFlag=true;break;
        case 0x01:daliData.warmest = ((daliRegister1<<8)|daliRegister);daliDataSaveFlag=true;break;
        case 0x00:daliData.coolest = ((daliRegister1<<8)|daliRegister);daliDataSaveFlag=true;break;
    }
    //dali_debug_printf("FUNC:DALI_COMMOND_SET_COLOR_TEMP_LIMIT\r\n",sizeof("FUNC:DALI_COMMOND_SET_COLOR_TEMP_LIMIT\r\n"));
}

static void dali_commond_query_random_address_H(void)
{
    returnFlag = true;
    returnData = daliData.randomAddrH;
    //dali_debug_printf("FUNC:dali_commond_query_random_address_H\r\n",sizeof("FUNC:dali_commond_query_random_address_H\r\n"));
}

static void dali_commond_query_random_address_M(void)
{
    returnFlag = true;
    returnData = daliData.randomAddrM;
    //dali_debug_printf("FUNC:dali_commond_query_random_address_M\r\n",sizeof("FUNC:dali_commond_query_random_address_M\r\n"));
}

static void dali_commond_query_random_address_L(void)
{
    returnFlag = true;
    returnData = daliData.randomAddrL;
    //dali_debug_printf("FUNC:dali_commond_query_random_address_L\r\n",sizeof("FUNC:dali_commond_query_random_address_L\r\n"));
}

static void dali_commond_query_extended_version_number(void)
{
	if(DEVICE_TYPE == 0x08)
		returnFlag = true;
	else if(DEVICE_TYPE == 0x06)
		returnFlag = false;
    returnData = DEVICE_TYPE;
    //dali_debug_printf("FUNC:dali_commond_query_extended_version_number\r\n",sizeof("FUNC:dali_commond_query_extended_version_number\r\n"));
}

/******************************************
协议:IEC62386-102
编号:0
指令代码:YAAA AAA1 0000 0000
详细:11.3.2
描述:直接关闭,而且要迅速,所以不需要太长的渐变时间
******************************************/
static void dali_commond_off(void)
{
    daliData.level = 0;
	aero_dali_practical_dimmer(daliData.level,daliData.tempTC);
    //dali_debug_printf("FUNC:DALI_COMMOND_OFF\r\n",sizeof("FUNC:DALI_COMMOND_OFF\r\n"));
}

/******************************************
协议:IEC62386-102
编号:1
指令代码:YAAA AAA1 0000 0001
详细:11.3.3
描述:先level++,再根据设置的淡入淡出(fade)调高200ms,
      如果level=maxLevel或者level=0,那么level不更改.
******************************************/
static void dali_commond_up(void)
{ 
    if((daliData.level != daliData.maxLevel) && (daliData.level!=0x00))
    {
        daliData.level++;
		daliDataSaveFlag = true;
		daliDimmerFlag = true;
    }
    //dali_debug_printf("FUNC:DALI_COMMOND_UP_200MS\r\n",sizeof("FUNC:DALI_COMMOND_UP_200MS\r\n"));
}

/******************************************
协议:IEC62386-102
编号:2
指令代码:YAAA AAA1 0000 0002
详细:11.3.4
描述:先level--,再根据设置的淡入淡出(fade)调低200ms,
      如果level=minLevel或者level=0,那么level不更改.
******************************************/
static void dali_commond_down(void)
{
    if((daliData.level != daliData.minLevel) && (daliData.level!=0x00))
    {
        daliData.level--;
		daliDataSaveFlag = true;
		daliDimmerFlag = true;
    }
    //dali_debug_printf("FUNC:DALI_COMMOND_DOWN_200MS\r\n",sizeof("FUNC:DALI_COMMOND_DOWN_200MS\r\n"));
}

static void dali_commond_query_power_failure(void)
{
    if(daliData.level == 0)
    {
        returnFlag = true;
        returnData = 0xFF;
    }
}

static void dali_spec_commond_randomise(void)
{
    static uint8_t revRandomCommondCount = 0;
    if(initialisationState != DISABLED)
    {
        revRandomCommondCount++;
        if(revRandomCommondCount>=2)
        {
            revRandomCommondCount = 0;
			lp_dali_reset_random_addr();
//            daliData.randomAddrH = randomAddrTemp[0];
//            daliData.randomAddrM = randomAddrTemp[1];
//            daliData.randomAddrL = randomAddrTemp[2];
            daliDataSaveFlag = true;
//            creatRandomAddrStart();
			
        }
    }
}

//启动设备类型
static void dali_spec_commond_enable_device_type(uint8_t data)
{
    if(data > 0xFD)
      return ;
	if(0x08 == data)
	{
//		temporaryTc = (daliRegister1<<8)|daliRegister;
//		dali_commond_activate();
		//2023.05.10
		dali_device_type = data;
	}
	returnFlag = false;
    returnData = DEVICE_TYPE;
    //dali_debug_printf("FUNC:DALI_SPEC_COMMOND_ENABLE_DEVICE_TYPE\r\n",sizeof("FUNC:DALI_SPEC_COMMOND_ENABLE_DEVICE_TYPE\r\n"));
}

/*
    data :0x00  所有设备都响应
         :0AAA AAA1  地址为AAAAAA的设备响应
         :0xFF  没有地址的设备响应
*/
static void dali_spec_commond_initialise(uint8_t data)
{
    static uint8_t revInitCommondCount = 0;
    revInitCommondCount++;
    if(revInitCommondCount >= 2)
    {
        revInitCommondCount = 0;
        returnData = 0xFF;
        if(data == 0x00)
        {
            returnFlag = false;
            initialisationState = ENABLED;
        }
        else if(data == 0xFF)
        {
            if(daliData.address>63) //没有短地址的设备才会响应
            {
                returnFlag = false;
                initialisationState = ENABLED;
            }
        }
        else if(!(data&0x80) && (data&0x01))//8bit=0    1bit=1
        {
            if(daliData.address == (data>>1))
            {
                returnFlag = false;
                initialisationState = ENABLED;
            }
        }
    }
    //dali_debug_printf("FUNC:DALI_SPEC_COMMOND_ADDRESS_INIT\r\n",sizeof("FUNC:DALI_SPEC_COMMOND_ADDRESS_INIT\r\n"));
}

static void dali_spec_commond_searchAddrH(uint8_t data)
{
    if(initialisationState != DISABLED)
      searchH = data;
}

static void dali_spec_commond_searchAddrM(uint8_t data)
{
    if(initialisationState != DISABLED)
      searchM = data;
}

static void dali_spec_commond_searchAddrL(uint8_t data)
{
    if(initialisationState != DISABLED)
      searchL = data;
}

static void dali_spec_commond_compare(void)
{
	returnFlag = false;
    if(initialisationState == ENABLED)
    {
        uint32_t randomAddr = (daliData.randomAddrH<<16)|(daliData.randomAddrM<<8)|daliData.randomAddrL;
        uint32_t searchAddr = (searchH<<16)|(searchM<<8)|searchL;
		LOG_OUT("-------------searchAddr 0x%x,randomAddr 0x%x\r\n",searchAddr,randomAddr);
		if(searchAddr >= randomAddr)
		{
			returnFlag = true;
			returnData = 0xFF;
			
		}
        
    }
}


/*
    data : 0AAA AAA1   将设备地址设置为AAAAAA
           0xFF   删除设备地址
    无论data是啥, 都将随机地址赋值为搜索随机地址,即random_address[H:M:L] = search_addr[H:M:L]
*/
static void dali_spec_commond_program_short_address(uint8_t data)
{
    if((initialisationState==ENABLED) || (initialisationState==WITHDRAWN))
    {
        uint32_t randomAddr = (daliData.randomAddrH<<16)|(daliData.randomAddrM<<8)|daliData.randomAddrL;
        uint32_t searchAddr = (searchH<<16)|(searchM<<8)|searchL;
        if(randomAddr == searchAddr)
        {
            if(data == 0xFF)
            {
                daliData.address = 0xFF;
                daliDataSaveFlag = true;
				daliAddrSaveFlag = true;
            }
            else if(!(data&0x80) && (data&0x01))
            {
                daliData.address = data>>1;
                daliDataSaveFlag = true;
				daliAddrSaveFlag = true;
            }
        }
    }
    //dali_debug_printf("FUNC:dali_spec_commond_program_short_address\r\n",sizeof("FUNC:dali_spec_commond_program_short_address\r\n"));
}

static void dali_spec_commond_query_short_address(void)
{
    if(initialisationState != DISABLED)
    {
        uint32_t randomAddr = (daliData.randomAddrH<<16)|(daliData.randomAddrM<<8)|daliData.randomAddrL;
        uint32_t searchAddr = (searchH<<16)|(searchM<<8)|searchL;
        if(searchAddr == randomAddr)
        {
            if(daliData.address < 64)
            {
                returnFlag = true;
                returnData = (daliData.address<<1)|0x01;
            }
            else
            {
                returnFlag = true;
                returnData = 0xFF;
            }
        }
    }
    //dali_debug_printf("FUNC:dali_spec_commond_query_short_address\r\n",sizeof("FUNC:dali_spec_commond_query_short_address\r\n"));
}

static void dali_spec_commond_withraw(void)
{
    if(initialisationState == ENABLED)
    {
        uint32_t randomAddr = (daliData.randomAddrH<<16)|(daliData.randomAddrM<<8)|daliData.randomAddrL;
        uint32_t searchAddr = (searchH<<16)|(searchM<<8)|searchL;
        if(searchAddr == randomAddr)
        {
            initialisationState = WITHDRAWN;
            //dali_debug_printf("FUNC:searchAddr == randomAddr\r\n",sizeof("FUNC:searchAddr == randomAddr\r\n"));
        }
    }
    //dali_debug_printf("FUNC:dali_spec_commond_withraw\r\n",sizeof("FUNC:dali_spec_commond_withraw\r\n"));
}

static void dali_spec_commond_verify_short_address(uint8_t data)
{
    if(initialisationState != DISABLED)
    {
		//2023.03.13
        //returnFlag = true;
        //returnData = (daliData.address == data?0x00:0xff);
		returnFlag = false;
		if(daliData.address == (data>>1))
		{
			returnFlag = true;
			returnData = 0xff;
		}
		
    }
    //dali_debug_printf("FUNC:dali_spec_commond_verify_short_address\r\n",sizeof("FUNC:dali_spec_commond_verify_short_address\r\n"));
}

static void dali_spec_commond_terminate(void)
{
    initialisationState = DISABLED;
    //dali_debug_printf("FUNC:dali_spec_commond_terminate\r\n",sizeof("FUNC:dali_spec_commond_terminate\r\n"));
}

/******************************************
协议:IEC62386-102
指令代码:YAAA AAA0 XXXX XXXX
详细:11.3.1
描述:调节亮度
******************************************/
static void dali_handle_dimmer_commond(uint8_t data)
{
    daliData.level = data;
    daliDataSaveFlag = true;
    daliDimmerFlag = true;
}

static void dali_handle_standard_commond(uint8_t address,uint8_t commond)
{
    switch(commond)
    {
        case DALI_STANDARD_COMMOND_OFF:dali_commond_off();break;
        case DALI_STANDARD_COMMOND_UP:dali_commond_up();break;
        case DALI_STANDARD_COMMOND_DOWN:dali_commond_down();break;
        case DALI_STANDARD_COMMOND_STEP_UP:dali_commond_step_up();break;
        case DALI_STANDARD_COMMOND_STEP_DOWN:dali_commond_step_down();break;
        case DALI_STANDARD_COMMOND_RECALL_MAX_LEVEL:dali_commond_recall_max_level();break;
        case DALI_STANDARD_COMMOND_RECALL_MIN_LEVEL:dali_commond_recall_min_level();break;
        case DALI_STANDARD_COMMOND_STEP_DOWN_AND_OFF:dali_commond_step_down_and_off();break;
        case DALI_STANDARD_COMMOND_ON_AND_STEP_UP:dali_commond_on_and_step_up();break;
		case DALI_STANDARD_COMMOND_ENABLE_DAPC_SEQUENCE:dali_commond_enable_dapc_sequence();break;
        case DALI_STANDARD_COMMOND_RESET:dali_commond_reset();break;
        case DALI_STANDARD_COMMOND_STORE_ACTUAL_LEVEL_IN_DTR0:dali_commond_store_actual_level_in_dtr0();break;
		
		case DALI_STANDARD_COMMOND_SAVE_PERSISTENT_VARIABLES:dali_commond_save_persistent_variables();break;   
		case DALI_STANDARD_COMMOND_SET_OPERATING_MODE:dali_commond_set_operating_mode();break; 
		case DALI_STANDARD_COMMOND_RESET_MEMORY_BANK:dali_commond_reset_memory_bank();break;    
		case DALI_STANDARD_COMMOND_IDENTIFY_DEVICE:dali_commond_identify_device();break;   
		
        case DALI_STANDARD_COMMOND_SET_MAX_LEVEL:dali_commond_set_max_level();break;
        case DALI_STANDARD_COMMOND_SET_MIN_LEVEL:dali_commond_set_min_level();break;
        case DALI_STANDARD_COMMOND_SET_SYSTEM_FAILURE_LEVEL:dali_commond_set_system_failure_level();break;
        case DALI_STANDARD_COMMOND_SET_POWER_ON_LEVEL:dali_commond_set_power_on_level();break;
        case DALI_STANDARD_COMMOND_SET_FADE_TIME:dali_commond_set_fade_time();break;
        case DALI_STANDARD_COMMOND_SET_FADE_RATE:dali_commond_set_fade_rate();break;
		case DALI_STANDARD_COMMOND_SET_EXTENDED_FADE_TIME:dali_commond_set_extended_fade_time();break;
        case DALI_STANDARD_COMMOND_SET_SHORT_ADDRESS:dali_commond_set_short_address();break;
		case DALI_STANDARD_COMMOND_ENABLE_WRITE_MEMORY:dali_commond_enable_write_memory();break;
		
        case DALI_STANDARD_COMMOND_QUERY_STATUS:dali_commond_query_status();break;
        case DALI_STANDARD_COMMOND_QUERY_CONTROL_GEAR_PRESENT:dali_commond_query_control_gear_present();break;
        case DALI_STANDARD_COMMOND_QUERY_LAMP_FAILURE:dali_commond_query_lamp_failure();break;
        case DALI_STANDARD_COMMOND_QUERY_LAMP_POWER_ON:dali_commond_query_lamp_power_on();break;
        case DALI_STANDARD_COMMOND_QUERY_LIMIT_ERROR:dali_commond_query_limit_error();break;
        case DALI_STANDARD_COMMOND_QUERY_RESET_STATE:dali_commond_query_reset_state();break;
        case DALI_STANDARD_COMMOND_QUERY_MISSING_SHORT_ADDRESS:dali_commond_query_missing_short_address();break;
        case DALI_STANDARD_COMMOND_QUERY_VERSION_NUMBER:dali_commond_query_version_number();break;
        case DALI_STANDARD_COMMOND_QUERY_CONTENT_DTR0:dali_commond_query_content_dtr0();break;
        case DALI_STANDARD_COMMOND_QUERY_CONTENT_DTR1:dali_commond_query_content_dtr1();break;
        case DALI_STANDARD_COMMOND_QUERY_CONTENT_DTR2:dali_commond_query_content_dtr2();break;
		
		case DALI_STANDARD_COMMOND_QUERY_OPEARTING_MODE:dali_commond_query_opearting_mode();break;
		case DALI_STANDARD_COMMOND_QUERY_LIGHT_SOURCE_TYPE:dali_commond_query_light_source_type();break;
		
        case DALI_STANDARD_COMMOND_QUERY_DEVICE_TYPE:dali_commond_query_device_type();break;
        case DALI_STANDARD_COMMOND_QUERY_PHYSICAL_MINIMUM:dali_commond_query_physical_minimum();break;
        case DALI_STANDARD_COMMOND_QUERY_POWER_FAILURE:dali_commond_query_power_failure();break;
        case DALI_STANDARD_COMMOND_QUERY_ACTUAL_LEVEL:dali_commond_query_actual_level();break;
        case DALI_STANDARD_COMMOND_QUERY_MAX_LEVEL:dali_commond_query_max_level();break;
        case DALI_STANDARD_COMMOND_QUERY_MIN_LEVEL:dali_commond_query_min_level();break;
        case DALI_STANDARD_COMMOND_QUERY_POWER_ON_LEVEL:dali_commond_query_power_on_level();break;
        case DALI_STANDARD_COMMOND_QUERY_SYSTEM_FAILURE_LEVEL:dali_commond_query_system_failure_level();break;
        case DALI_STANDARD_COMMOND_QUERY_FADE_TIME_RATE:dali_commond_query_fade_time_rate();break;
		
		case DALI_STANDARD_COMMOND_QUERY_MANUFACTURER_SPECIFIC_MODE:dali_commond_query_manufacturer_specific_mode();break;
		case DALI_STANDARD_COMMOND_QUERY_NEXT_DEVICE_TYPE:dali_commond_query_next_device_type();break;
		case DALI_STANDARD_COMMOND_QUERY_EXTENDED_FADE_TIME:dali_commond_query_extended_fade_time();break;
		case DALI_STANDARD_COMMOND_QUERY_CONTROL_GEAR_FAILURE:dali_commond_query_control_grar_failuer();break;
		
        case DALI_STANDARD_COMMOND_QUERY_GROUP_0_7:dali_commond_query_group_0_7();break;
        case DALI_STANDARD_COMMOND_QUERY_GROUP_8_15:dali_commond_query_group_8_15();break;
        case DALI_STANDARD_COMMOND_QUERY_RANDOM_ADDRESS_H:dali_commond_query_random_address_H();break;
        case DALI_STANDARD_COMMOND_QUERY_RANDOM_ADDRESS_M:dali_commond_query_random_address_M();break;
        case DALI_STANDARD_COMMOND_QUERY_RANDOM_ADDRESS_L:dali_commond_query_random_address_L();break;
        case DALI_STANDARD_COMMOND_READ_MEMORY_LOCATION:dali_commond_read_memory_location();break;
        case DALI_EXTENDED_COMMOND_QUERY_DIMMER_CURVE:dali_commond_query_dimmer_curve();break;
        case DALI_EXTENDED_COMMOND_SET_DIMMER_CURVE:dali_commond_set_dimmer_curve();break;
		
		case DALI_EXTENDED_COMMOND_QUERY_COLOUR_TYPE_FEATURES:dali_commond_query_colour_type_features();break;
        case DALI_EXTENDED_COMMOND_QUERY_COLOUR_VALUE:dali_commond_query_colour_value();break;
		
        case DALI_EXTENDED_COMMOND_SET_COLOR_TEMP:dali_commond_set_color_temp();break;
        case DALI_EXTENDED_COMMOND_ACTIVATE:dali_commond_activate();break;
        case DALI_EXTENDED_COMMOND_SET_COLOR_TEMP_LIMIT:dali_commond_set_color_temp_limit();break;
        case DALI_STANDARD_COMMOND_QUERY_EXTENDED_VERSION_NUMBER:dali_commond_query_extended_version_number();break;
        //case DALI_STANDARD_COMMOND_GO_TO_SCENE...DALI_STANDARD_COMMOND_GO_TO_SCENE+0x0F:dali_commond_go_to_scene(commond-DALI_STANDARD_COMMOND_GO_TO_SCENE);break;
        //case DALI_STANDARD_COMMOND_SET_SCENE...DALI_STANDARD_COMMOND_SET_SCENE+0x0F:dali_commond_set_scene(commond-DALI_STANDARD_COMMOND_SET_SCENE);break;
        //case DALI_STANDARD_COMMOND_REMOVE_FROM_SCENE...DALI_STANDARD_COMMOND_REMOVE_FROM_SCENE+0x0F:dali_commond_remove_from_scene(commond-DALI_STANDARD_COMMOND_REMOVE_FROM_SCENE);break;
        //case DALI_STANDARD_COMMOND_QUERY_SCENE_LEVEL...DALI_STANDARD_COMMOND_QUERY_SCENE_LEVEL+0x0F:dali_commond_query_scene_level(commond-DALI_STANDARD_COMMOND_QUERY_SCENE_LEVEL);break;
        //case DALI_STANDARD_COMMOND_ADD_TO_GROUP...DALI_STANDARD_COMMOND_ADD_TO_GROUP+0x0F:dali_commond_add_to_group(commond-DALI_STANDARD_COMMOND_ADD_TO_GROUP);break;
        //case DALI_STANDARD_COMMOND_REMOVE_FROM_GROUP...DALI_STANDARD_COMMOND_REMOVE_FROM_GROUP+0x0F:dali_commond_remove_from_group(commond-DALI_STANDARD_COMMOND_REMOVE_FROM_GROUP);break;
        default:;break;
    }
	
	if((commond >= DALI_STANDARD_COMMOND_GO_TO_SCENE) && (commond <= DALI_STANDARD_COMMOND_GO_TO_SCENE+0x0F))
	{
		dali_commond_go_to_scene(commond-DALI_STANDARD_COMMOND_GO_TO_SCENE);
	}
	else if((commond >= DALI_STANDARD_COMMOND_SET_SCENE) && (commond <= DALI_STANDARD_COMMOND_SET_SCENE+0x0F))
	{
		dali_commond_set_scene(commond-DALI_STANDARD_COMMOND_SET_SCENE);
	}
	else if((commond >= DALI_STANDARD_COMMOND_REMOVE_FROM_SCENE) && (commond <= DALI_STANDARD_COMMOND_REMOVE_FROM_SCENE+0x0F))
	{
		dali_commond_remove_from_scene(commond-DALI_STANDARD_COMMOND_REMOVE_FROM_SCENE);
	}
	else if((commond >= DALI_STANDARD_COMMOND_QUERY_SCENE_LEVEL) && (commond <= DALI_STANDARD_COMMOND_QUERY_SCENE_LEVEL+0x0F))
	{
		dali_commond_query_scene_level(commond-DALI_STANDARD_COMMOND_QUERY_SCENE_LEVEL);
	}
	else if((commond >= DALI_STANDARD_COMMOND_ADD_TO_GROUP) && (commond <= DALI_STANDARD_COMMOND_ADD_TO_GROUP+0x0F))
	{
		dali_commond_add_to_group(commond-DALI_STANDARD_COMMOND_ADD_TO_GROUP);
	}
	else if((commond >= DALI_STANDARD_COMMOND_REMOVE_FROM_GROUP) && (commond <= DALI_STANDARD_COMMOND_REMOVE_FROM_GROUP+0x0F))
	{
		dali_commond_remove_from_group(commond-DALI_STANDARD_COMMOND_REMOVE_FROM_GROUP);
	}
}

static void dali_handle_spec_commond(uint8_t address,uint8_t commond)
{
    switch(address)
    {
        case DALI_SPECIAL_COMMOND_TERMINATE:dali_spec_commond_terminate();break;
        case DALI_SPECIAL_COMMOND_DTR0:dali_spec_commond_dtr0(commond);break;
        case DALI_SPECIAL_COMMOND_INITIALISE:dali_spec_commond_initialise(commond);break;
        case DALI_SPECIAL_COMMOND_RANDOMISE:dali_spec_commond_randomise();break;
        case DALI_SPECIAL_COMMOND_COMPARE:dali_spec_commond_compare();break;
        case DALI_SPECIAL_COMMOND_WITHRAW:dali_spec_commond_withraw();break;
        case DALI_SPECIAL_COMMOND_SEARCHADDRH:dali_spec_commond_searchAddrH(commond);break;
        case DALI_SPECIAL_COMMOND_SEARCHADDRM:dali_spec_commond_searchAddrM(commond);break;
        case DALI_SPECIAL_COMMOND_SEARCHADDRL:dali_spec_commond_searchAddrL(commond);break;
        case DALI_SPECIAL_COMMOND_PROGRAM_SHORT_ADDRESS:dali_spec_commond_program_short_address(commond);break;
        case DALI_SPECIAL_COMMOND_VERIFY_SHORT_ADDRESS:dali_spec_commond_verify_short_address(commond);break;
        case DALI_SPECIAL_COMMOND_QUERY_SHORT_ADDRESS:dali_spec_commond_query_short_address();break;
        //case DALI_SPECIAL_COMMOND_INTO_PHY_MODE:dali_spec_commond_into_phy_mode();break;//进入物理选择模式
        case DALI_SPECIAL_COMMOND_DTR1:dali_spec_commond_save_to_dtr1(commond);break;
        case DALI_SPECIAL_COMMOND_DTR2:dali_spec_commond_save_to_dtr2(commond);break;
		case DALI_SPECIAL_COMMOND_WRITE_MEMORY_LOCATION:dali_spec_commond_write_memory_location(commond);break;
		case DALI_SPECIAL_COMMOND_WRITE_MEMORY_LOCATION_NO_REPLY:dali_spec_commond_write_memory_location_no_reply(commond);break;
		
        case DALI_SPECIAL_COMMOND_ENABLE_DEVICE_TYPE:dali_spec_commond_enable_device_type(commond);break;
       // default:////dali_debug_printf("FUNC:Not found The Spec Command or The Spec Command undefined\r\n",sizeof("FUNC:Not found The Spec Command or The Spec Command undefined\r\n"));break;
    }
    	
}

static void dali_handle_commond(uint8_t address, uint8_t commond)
{
    if((address&0x01) == 0x00)//调光
    {
        dali_handle_dimmer_commond(commond);
    }
    else//DALI命令
    {
        dali_handle_standard_commond(address, commond);
    }
}

/*  
    address : 8 high bit
    commond : 8 low bit
    address type     :  describe
    short address       0AAA AAAS   AAAAAA=(0-63)
    Group address       100A AAAS   AAAA=(0-15)
    Broadcast address   1111 111S
    Special command     101C CCC1   CCCC=(command code)
    IEC 62386-209  command:272  1100 0001 0000 1000
*/
uint8_t dali_decode(uint8_t address, uint8_t commond)
{
    returnFlag = false;
    if((address&0x80) == 0x80)//组地址  广播地址  特殊命令 
    {
      if((address>>5) == 0x04)//组地址
      {
          uint8_t groupAddress = (address>>1)&0x0F;
          if((daliData.group>>groupAddress)&0x0001)
          {
              dali_handle_commond(address, commond);  
          }
      }
      else if((address>>1) == 0x7F)//广播地址
      {             
          dali_handle_commond(address, commond);
      }
      else if(((address&0xE0) == 0xA0)||((address&0xE0) == 0xC0))//特殊命令  
      {             
          dali_handle_spec_commond(address, commond);
      }
    }
    else
    {	
        uint8_t shortAddress = address>>1;
        if(shortAddress == daliData.address)
        {
          dali_handle_commond(address, commond);
        }
    }
	
    if(daliDimmerFlag)
    {
        daliDimmerFlag = false;
        daliData.mode = 0x00;
		aero_dali_time_dimmer(daliData.level, (daliData.fade>>4)&0x0F,daliData.tempTC);
    }
	if(daliAddrSaveFlag)
	{
		lp_dali_addr_updata();
		daliAddrSaveFlag = false;
	}
	
    if(daliDataSceneSaveFlag)
    {
		lp_dali_scene_updata();
        daliDataSceneSaveFlag = false;
    }
    if(daliDataSaveFlag)
    {
		lp_dali_data_updata();
        daliDataSaveFlag = false;
    }
	
    return returnFlag;
}

