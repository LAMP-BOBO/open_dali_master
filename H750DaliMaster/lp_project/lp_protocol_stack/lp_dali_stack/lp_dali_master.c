

#include "lp_dali_stack/lp_dali_master.h"
#include "lp_dali_stack/lp_dali_102_command.h"
#include "lp_dali_stack/lp_dali_207_command.h"
#include "lp_dali_stack/lp_dali_209_command.h"
#include "lp_sys_os_dali/lp_sys_os_dali.h"
#include "lp_pc_protocol/lp_pc_protocol.h"

//----------------funs statement---------------
static lp_err_t dali_load_send_data(lp_u32_t data,lp_u8_t send_count,lp_u8_t ack,lp_pdali_master_t master);
lp_err_t lp_DALIMaster_get_device_info(lp_pdali_master_t master,lp_u8_t addr);
//--------------------------------------------- 

static void dali_current_packet_clear(lp_pdali_packet_t packet)
{
	packet->send_count = 0;
	packet->send_data = 0x00;
	packet->send_length = 0x00;
	packet->send_need_ack = DALI_SEND_NO_ACK;
}

static void dali_search_next_device(lp_pdali_master_t master)
{
    if(master->master_data.search_running == 2)
    {
        master->master_data.master_search_addr = 0xFFFFFF;
        master->master_data.master_search_last_addr = 0xFFFFFF;
        master->master_data.master_search_addr_min = 0x000000;
        master->master_data.master_search_addr_max = 0xFFFFFF;
        master->master_data.search_addr_confirm = 0;
        master->master_data.search_reply_count++;
        if(master->master_data.search_reply_count > 10) //重复搜索10次.有应答 但是搜不到设备情况()
        {
            master->master_data.search_running = 3;
            LOG_OUT("--------------------------------------------------");
            dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_TERMINATE, 0x00),1,DALI_SEND_NEED_ACK,master);
            LOG_OUT("device num = %d\r\n",master->master_data.device_num);
            for(uint32_t i = bitfield_next_get(master->master_data.device_list, 64, 0);
                 i != 64;
                 i = bitfield_next_get(master->master_data.device_list, 64, i + 1))
            {
                LOG_OUT("device address = %d\r\n",i);
            }
            LOG_OUT("--------------------------------------------------\r\n");
        }
        else
        {
            dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_SEARCHADDRH, (master->master_data.master_search_addr&0xFF)),1,DALI_SEND_NO_ACK,master);
            dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_SEARCHADDRM, ((master->master_data.master_search_addr>>8)&0xFF)),1,DALI_SEND_NO_ACK,master);
            dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_SEARCHADDRL, ((master->master_data.master_search_addr>>16)&0xFF)),1,DALI_SEND_NO_ACK,master);
            dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_COMPARE, 0x00),1,DALI_SEND_NEED_ACK,master);
			LOG_OUT("start DALI_SPECIAL_COMMOND_COMPARE\r\n");
			
        }
    }
    else if(master->master_data.search_running == 1)
    {
        uint8_t *send_data = (uint8_t *)&master->current_packet.send_data;
        uint8_t short_address = send_data[0]>>1;
        if(short_address == 63)//扫描到最后一个地址
        {
            master->master_data.search_running = 2;
            dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_TERMINATE,0x00),1,DALI_SEND_NO_ACK,master);
            dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_INITIALISE,0xFF),2,DALI_SEND_NO_ACK,master);//地址初始化
            dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_RANDOMISE,0x00),2,DALI_SEND_NO_ACK,master);//产生随机地址
            dali_search_next_device(master);
            //比较地址后.会找到一个设备.怎样继续寻找下一台设备呢? 重新将搜索地址设置为0xFFFFFF.再进行对比. 没有数据 说明没有设备
        }
        else
        {
            dali_load_send_data(DALI_DATA_16BIT(((short_address+1)<<1)|1, DALI_STANDARD_COMMOND_QUERY_CONTROL_GEAR_PRESENT),1,DALI_SEND_NEED_ACK,master);
        }
    }
	else if(master->master_data.search_running == LP_DALI_SEARCH_STATE_ONLY_CHECK_E)
	{
		uint8_t *send_data = (uint8_t *)&master->current_packet.send_data;
        uint8_t short_address = send_data[0]>>1;
		
        dali_load_send_data(DALI_DATA_16BIT(((short_address+1)<<1)|1, DALI_STANDARD_COMMOND_QUERY_CONTROL_GEAR_PRESENT),1,DALI_SEND_NEED_ACK,master);
        if(short_address == 63)//扫描到最后一个地址,上报搜索结果
		{
			master->master_data.search_running = LP_DALI_SEARCH_STATE_STOP_E;
			send_dali_searched_dev((lp_u8_t*)master->master_data.device_list,sizeof(master->master_data.device_list));
		}
	}
}

lp_err_t lp_dali_last_packet_handle(lp_pdali_master_t master)
{
	lp_err_t ret = LP_OK;
	lp_dali_packet_t daliPacket = master->current_packet;
	
	if(daliPacket.send_need_ack == DALI_SEND_NO_ACK){
        return ret;
    }
	
	lp_u8_t *rev_data = master->revDali_data;
    lp_u8_t *send_data = (lp_u8_t *)&daliPacket.send_data;
	
	if(send_data[0] == 0xFF && send_data[1] == DALI_STANDARD_COMMOND_QUERY_CONTROL_GEAR_PRESENT)//查询灯具响应
    {
        if(master->revDali_ack == LP_DALI_RX_NULL_E) //什么脉冲都没有收到
        {
            LOG_OUT("not find dali device\n"); //清空
			dali_current_packet_clear(&master->current_packet);
            master->master_data.device_num = 0;
            bitfield_clear_all(master->master_data.device_list,64);
        }
        else //接收到脉冲,不用管什么数据
        {
            if(master->master_data.search_running == 1)
            {
                dali_load_send_data(DALI_DATA_16BIT(0x01,DALI_STANDARD_COMMOND_QUERY_CONTROL_GEAR_PRESENT),1,DALI_SEND_NEED_ACK,master);
            }
        }
    }
	else if(((send_data[0]&0x01) == 0x01) && (send_data[0] < 0x80)) //单播地址
    {
        uint8_t short_address = (send_data[0]>>1);
        if(send_data[1]  == DALI_STANDARD_COMMOND_QUERY_CONTROL_GEAR_PRESENT)
        {
            if(master->revDali_ack == LP_DALI_RX_BIT8_E && rev_data[0] == 0xFF)
            {
                master->master_data.device_num++;
                bitfield_set(master->master_data.device_list, short_address);
                lp_DALIMaster_get_device_info(master,send_data[0]);
            }
            else if(master->revDali_ack != LP_DALI_RX_NULL_E) //可能存在两个相同的地址. 要把他们短地址清掉
            {
                dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_DTR0,0xFF),1,DALI_SEND_NO_ACK,master);
                dali_load_send_data(DALI_DATA_16BIT(send_data[0],DALI_STANDARD_COMMOND_SET_SHORT_ADDRESS),2,DALI_SEND_NO_ACK,master);
                dali_search_next_device(master);
            }
            else if(master->revDali_ack == LP_DALI_RX_NULL_E)
            {
                dali_search_next_device(master);
            }
        }
        else if(send_data[1]  == DALI_STANDARD_COMMOND_QUERY_VERSION_NUMBER)
        {
            if(master->revDali_ack == LP_DALI_RX_BIT8_E)
            {
                master->master_data.device_info[short_address].version_number = rev_data[0];
				LOG_OUT("short_address = %d,version_number = %d\r\n",short_address,rev_data[0]);
            }
        }
        else if(send_data[1]  == DALI_STANDARD_COMMOND_QUERY_DEVICE_TYPE)
        {
            if(master->revDali_ack == LP_DALI_RX_BIT8_E)
            {
                master->master_data.device_info[short_address].device_type = rev_data[0];
				LOG_OUT("short_address = %d,device_type = %d\r\n",short_address,rev_data[0]);
            }
        }
        else if(send_data[1]  == DALI_STANDARD_COMMOND_QUERY_STATUS)
        {
            if(master->revDali_ack == LP_DALI_RX_BIT8_E)
            {
                master->master_data.device_info[short_address].status = rev_data[0];
				LOG_OUT("short_address = %d,status = %d\r\n",short_address,rev_data[0]);
            }
			if(master->master_data.search_running != 0)
			{
				dali_search_next_device(master);
			}
        }
        else if(send_data[1]  == DALI_STANDARD_COMMOND_QUERY_ACTUAL_LEVEL)
        {
            if(master->revDali_ack == LP_DALI_RX_BIT8_E)
            {
                master->master_data.device_info[short_address].lightness = rev_data[0];
				LOG_OUT("short_address = %d,lightness = %d\r\n",short_address,rev_data[0]);
            }
        }
        else if(send_data[1]  == DALI_STANDARD_COMMOND_QUERY_MAX_LEVEL)
        {
            if(master->revDali_ack == LP_DALI_RX_BIT8_E)
            {
                master->master_data.device_info[short_address].max_level = rev_data[0];
				LOG_OUT("short_address = %d,max_level = %d\r\n",short_address,rev_data[0]);
            }
        }
        else if(send_data[1]  == DALI_STANDARD_COMMOND_QUERY_MIN_LEVEL)
        {
            if(master->revDali_ack == LP_DALI_RX_BIT8_E)
            {
                master->master_data.device_info[short_address].min_level = rev_data[0];
				LOG_OUT("short_address = %d,min_level = %d\r\n",short_address,rev_data[0]);
            }
        }
    }
	else if(send_data[0] == DALI_SPECIAL_COMMOND_COMPARE && send_data[1] == 0x00)//比较命令
    {
        lp_u8_t res = 0;
        if(master->revDali_ack == LP_DALI_RX_NULL_E) //什么脉冲都没有收到
        {
            if(master->master_data.master_search_addr == 0xFFFFFF && (master->master_data.search_addr_confirm == 0))//第一次对比0xFFFFFF就没有数据返回.说明没有在初始化里的设备
            {
                dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_TERMINATE, 0x00),1,DALI_SEND_NO_ACK,master);
				LOG_OUT("device num = %d\r\n",master->master_data.device_num);
                for(uint32_t i = bitfield_next_get(master->master_data.device_list, 64, 0);
                     i != 64;
                     i = bitfield_next_get(master->master_data.device_list, 64, i + 1))
                {
                     // Do something to the bit here.
					LOG_OUT("device address = %d\r\n",i);
                }
				send_dali_searched_dev((lp_u8_t*)master->master_data.device_list,sizeof(master->master_data.device_list));
                return ret;   
            }
        }
        else //接收到脉冲,不用管什么数据  搜索地址>=随机地址
        {
            res = 1;
        }
		
        if(master->master_data.search_addr_confirm)
        {
            if(res) //有数据回复，搜索地址>= 随机地址
            {
                master->master_data.master_search_addr_max = ((master->master_data.master_search_addr_min+master->master_data.master_search_addr_max)>>1);
            }
            else //没有数据回复，搜索地址< 随机地址
            {
                master->master_data.master_search_addr_min = ((master->master_data.master_search_addr_min+master->master_data.master_search_addr_max)>>1);
            }
        }
        else
        {
            master->master_data.search_addr_confirm = 1;
        }
        master->master_data.master_search_addr = ((master->master_data.master_search_addr_min + master->master_data.master_search_addr_max)>>1);
        if(master->master_data.master_search_last_addr == master->master_data.master_search_addr)
        {
            if(master->master_data.master_search_addr != 0x000000)
            {
                master->master_data.master_search_addr++;
                dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_SEARCHADDRL, (master->master_data.master_search_addr&0xFF)),1,DALI_SEND_NO_ACK,master);
                if((master->master_data.master_search_addr & 0xFF) == 0x00)//0xHHMMFF + 1 的情况
                {
                    dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_SEARCHADDRM, ((master->master_data.master_search_addr>>8)&0xFF)),1,DALI_SEND_NO_ACK,master);
                    if(((master->master_data.master_search_addr>>8)&0xFF) == 0x00)//0xHHFFFF + 1 的情况
                    {
                        dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_SEARCHADDRH, ((master->master_data.master_search_addr>>16)&0xFF)),1,DALI_SEND_NO_ACK,master);
                    }
                }
            }
            lp_u8_t find_empty_index = DALI_INVALID_DATA;//无效数据
            for(lp_u8_t i=0;i<64;i++)
            {
                if(bitfield_get(master->master_data.device_list, i) == 0)
                {
                    find_empty_index = i;
                    break;
                }
            }
            if(find_empty_index != DALI_INVALID_DATA)
            {
                dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_PROGRAM_SHORT_ADDRESS, (find_empty_index<<1)|1),1,DALI_SEND_NO_ACK,master);
                dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_QUERY_SHORT_ADDRESS, 0x00),1,DALI_SEND_NEED_ACK,master);
            }
            else //无法增加额外的设备了. 总线超过64设备情况
            {
                dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_TERMINATE, 0x00),1,DALI_SEND_NEED_ACK,master);
            }
			LOG_OUT("find rand address = 0x%06X\r\n",master->master_data.master_search_addr);
            return ret;
        }

        if(((master->master_data.master_search_last_addr>>16)&0xFF) != ((master->master_data.master_search_addr>>16)&0xFF))
        {
            dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_SEARCHADDRH, ((master->master_data.master_search_addr>>16)&0xFF)),1,DALI_SEND_NO_ACK,master);
        }
        if(((master->master_data.master_search_last_addr>>8)&0xFF) != ((master->master_data.master_search_addr>>8)&0xFF))
        {
            dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_SEARCHADDRM, ((master->master_data.master_search_addr>>8)&0xFF)),1,DALI_SEND_NO_ACK,master);
        }
        if(((master->master_data.master_search_last_addr)&0xFF) != ((master->master_data.master_search_addr)&0xFF))
        {
            dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_SEARCHADDRL, (master->master_data.master_search_addr&0xFF)),1,DALI_SEND_NO_ACK,master);
        }
        dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_COMPARE, 0x00),1,DALI_SEND_NEED_ACK,master);//比较地址
        master->master_data.master_search_last_addr = master->master_data.master_search_addr;
    }
	else if(send_data[0] == DALI_SPECIAL_COMMOND_QUERY_SHORT_ADDRESS && send_data[1] == 0x00)
    {
        if(master->revDali_ack == LP_DALI_RX_BIT8_E && ((rev_data[0]>>1)<0x40))
        {
            uint8_t addr = (rev_data[0]>>1);
            bitfield_set(master->master_data.device_list, addr);
            dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_WITHRAW, 0x00),1,DALI_SEND_NO_ACK,master);
            master->master_data.device_num++;
            master->master_data.search_reply_count = 0;
            master->master_data.device_info[(rev_data[0]>>1)].addr = (rev_data[0]>>1);
            lp_DALIMaster_get_device_info(master,rev_data[0]);//获取设备信息
            //继续寻找下一个设备
            return ret;
        }
        dali_search_next_device(master);
    }
	else if(send_data[0] == DALI_SPECIAL_COMMOND_TERMINATE && send_data[1] == 0x00)
    {
        if(master->master_data.search_running == 3)
        {
			master->master_data.search_running = 0;
			LOG_OUT("Search Stop\r\n");
        }
    }	
	
	
	return ret;
}

void dali_bus0_master_send_timeout_callback(lp_sys_os_timer_t timer)
{
	lp_err_t ret = LP_OK;
	lp_pdali_master_t master = lp_getDALI_master(LP_DALI_MASTER0);
	lp_u8_t ack_type = 0;
	lp_u8_t ack_data[3] = {0};
	lp_u16_t ack_len = 0;
	
	 //从缓存中读取数据
	 ret = master->dali_bus->Read(master->dali_bus,&ack_type,ack_data,&ack_len);
	 if(ret == LP_OK){
		LOG_OUT("master read data ok\r\n");
		if(ack_type == LP_DALI_RX_BIT8_E || \
				    ack_type == LP_DALI_RX_BIT16_E|| \
					ack_type == LP_DALI_RX_BIT24_E|| \
					ack_type == LP_DALI_RX_BIT25_E ){
						master->revDali_ack = ack_type;
						master->revDali_len = ack_len;
						memcpy(master->revDali_data,ack_data,ack_len);
					}
					else{
						master->revDali_ack = ack_type;
						master->revDali_len = 0;
						memset(master->revDali_data,0x00,3);
					}
	 }
	 else{
			master->revDali_ack = LP_DALI_RX_NULL_E;
			master->revDali_len = 0;
			memset(master->revDali_data,0x00,3);
	 }
	
	 lp_dali_last_packet_handle(master);
	 //获取上一包处理结果
	 if(master->current_packet.send_count > 0){
		ret = master->dali_bus->Write(master->dali_bus,(lp_u8_t*)&master->current_packet.send_data,master->current_packet.send_length);
		master->current_packet.send_count--;
		 
		send_dali_master_command((lp_u8_t*)&master->current_packet.send_data,master->current_packet.send_length);
	 }
	 else{
		ret = lp_sys_os_queue_pop(master->queue_dali_send,&master->current_packet);
		if(ret == LP_OK){
			//LOG_OUT("master send data=0x%x,send_need_ack=%d,send_count=%d\r\n",master->current_packet.send_data,master->current_packet.send_need_ack,master->current_packet.send_count);
			ret = master->dali_bus->Write(master->dali_bus,(lp_u8_t*)&master->current_packet.send_data,master->current_packet.send_length);
			master->current_packet.send_count--;
			
			send_dali_master_command((lp_u8_t*)&master->current_packet.send_data,master->current_packet.send_length);
			
		} 
	 }
	 
}


lp_err_t lp_dali_master_init(struct _lp_dali_master_t *master)
{
	lp_err_t ret = LP_OK;
	
	switch(master->which){
	case LP_DALI_MASTER0:
			master->dali_bus =  lp_getDALI_BUS(DALI_BUS0);
			master->dali_bus->Init(master->dali_bus);

			master->master_data.device_num = 0;
			master->master_data.search_running = 0;
			master->master_data.search_addr_confirm = 0;
			master->master_data.search_reply_count = 0;
			master->master_data.master_search_addr = 0xFFFFFF;
			master->master_data.master_search_last_addr = 0xFFFFFF;
			master->master_data.master_search_addr_min = 0x000000;
			master->master_data.master_search_addr_max = 0xFFFFFF;
			
			master->current_packet.send_count = 0;
			master->current_packet.send_type = 0;
			master->current_packet.send_length = 0;
			master->current_packet.send_need_ack = DALI_SEND_NO_ACK;
			master->current_packet.send_data = 0x00;
			
			master->timer_dali_send = lp_sys_os_timer_create(dali_bus0_master_send_timeout_callback);
			if(master->timer_dali_send == NULL)
			{
				LOG_OUT("ptDALIDevice->dali_master.timer_dali_send error!\n");
				ret = LP_FAIL;
				return ret;
			}
			master->queue_dali_send = lp_sys_os_queue_create(sizeof(lp_dali_packet_t),12,true);
			if(master->queue_dali_send == NULL)
			{	
				LOG_OUT("ptDALIDevice->dali_master.queue_dali_send error!\n");
				ret = LP_FAIL;
				return ret;
			}
		
			//master send 13.5ms - 75ms
			ret = lp_sys_os_timer_start(master->timer_dali_send,TIMER_MODE_LOOP,50);
		
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;


}


static lp_err_t dali_load_send_data(lp_u32_t data,lp_u8_t send_count,lp_u8_t ack,lp_pdali_master_t master)
{
	lp_err_t ret = LP_OK;
    lp_dali_packet_t packet;
	
    packet.send_type = 0;
    packet.send_data = data;
    packet.send_count = send_count;
    packet.send_length = (data>>24)&0xFF;
    packet.send_need_ack = ack;

	ret  = lp_sys_os_queue_push(master->queue_dali_send,&packet);
    if(ret == LP_FAIL)
    {
        LOG_OUT("dali_load_send_data fail\r\n");
		ret  = LP_FAIL;
    }
	
	return ret;
}


lp_err_t lp_DALIMaster_check_device(lp_pdali_master_t master)
{
	lp_err_t ret = LP_OK;
	
	master->master_data.search_running = LP_DALI_SEARCH_STATE_ONLY_CHECK_E;
	ret = dali_load_send_data(DALI_DATA_16BIT(0x01,DALI_STANDARD_COMMOND_QUERY_CONTROL_GEAR_PRESENT),1,DALI_SEND_NEED_ACK,master);
	
	return ret;
}

lp_err_t lp_DALIMaster_start_search_devices(lp_pdali_master_t master,lp_bool_t clear_addr_flag)
{
	lp_err_t ret = LP_OK;
	
    bitfield_clear_all(master->master_data.device_list,64);
    for(uint8_t i=0;i<64;i++)
    {
        master->master_data.device_info[i].addr = 0xFF;
    }
    master->master_data.device_num = 0;
	master->master_data.search_running = 1;
    
    ret = dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_INITIALISE,0x00),2,DALI_SEND_NO_ACK,master);
    if(clear_addr_flag)
    {
        ret = dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_DTR0,0xFF),1,DALI_SEND_NO_ACK,master);//设置DTR0值
        ret = dali_load_send_data(DALI_DATA_16BIT(0xFF,DALI_STANDARD_COMMOND_SET_SHORT_ADDRESS),2,DALI_SEND_NO_ACK,master);//广播设置地址 全部设置为0xFF
    }
    ret = dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_INITIALISE,0x00),2,DALI_SEND_NO_ACK,master);
    ret = dali_load_send_data(DALI_DATA_16BIT(0xFF,DALI_STANDARD_COMMOND_QUERY_CONTROL_GEAR_PRESENT),1,DALI_SEND_NEED_ACK,master);
	
	return ret;
}

lp_err_t lp_DALIMaster_get_device_info(lp_pdali_master_t master,lp_u8_t addr)
{
	lp_err_t ret = LP_OK;
	
	ret = dali_load_send_data(DALI_DATA_16BIT(addr,DALI_STANDARD_COMMOND_QUERY_VERSION_NUMBER),1,DALI_SEND_NEED_ACK,master);
    ret = dali_load_send_data(DALI_DATA_16BIT(addr,DALI_STANDARD_COMMOND_QUERY_DEVICE_TYPE),1,DALI_SEND_NEED_ACK,master);
    ret = dali_load_send_data(DALI_DATA_16BIT(addr,DALI_STANDARD_COMMOND_QUERY_ACTUAL_LEVEL),1,DALI_SEND_NEED_ACK,master);
    ret = dali_load_send_data(DALI_DATA_16BIT(addr,DALI_STANDARD_COMMOND_QUERY_MIN_LEVEL),1,DALI_SEND_NEED_ACK,master);
    ret = dali_load_send_data(DALI_DATA_16BIT(addr,DALI_STANDARD_COMMOND_QUERY_MAX_LEVEL),1,DALI_SEND_NEED_ACK,master);
    ret = dali_load_send_data(DALI_DATA_16BIT(addr,DALI_STANDARD_COMMOND_QUERY_STATUS),1,DALI_SEND_NEED_ACK,master);

	return ret;
}

lp_err_t lp_DALIMaster_read_device_info(lp_pdali_master_t master,lp_u8_t addr,lp_pdali_devices_data_t pdev_info)
{
	lp_err_t ret = LP_OK;
	
	if(pdev_info == NULL || master==NULL){
		ret = LP_FAIL;
		return ret;
	}
	
	pdev_info->addr = addr;
	pdev_info->device_type = master->master_data.device_info[addr].device_type;
	pdev_info->lightness = master->master_data.device_info[addr].lightness;
	pdev_info->max_level = master->master_data.device_info[addr].max_level;
	pdev_info->min_level = master->master_data.device_info[addr].min_level;
	pdev_info->status = master->master_data.device_info[addr].status;
	pdev_info->version_number = master->master_data.device_info[addr].version_number;
	
	return ret;
}

lp_err_t lp_DALIMaster_write_device_info(lp_pdali_master_t master,lp_u8_t addr,lp_pdali_devices_data_t pdev_info)
{
	//仅仅能设置最大亮度和最小亮度
	lp_err_t ret = LP_OK;
	lp_u8_t set_addr = (addr<<1)|0X01;
	
	if(pdev_info == NULL || master==NULL){
		ret = LP_FAIL;
		return ret;
	}
	
	//将DTR中的值，设置为预订的最大亮度
	ret = dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_DTR0, pdev_info->max_level),1,DALI_SEND_NO_ACK,master);
	ret = dali_load_send_data(DALI_DATA_16BIT(set_addr,DALI_STANDARD_COMMOND_SET_MAX_LEVEL),1,DALI_SEND_NO_ACK,master);
	ret = dali_load_send_data(DALI_DATA_16BIT(DALI_SPECIAL_COMMOND_DTR0,pdev_info->min_level),1,DALI_SEND_NO_ACK,master);
    ret = dali_load_send_data(DALI_DATA_16BIT(set_addr,DALI_STANDARD_COMMOND_SET_MIN_LEVEL),1,DALI_SEND_NO_ACK,master);

	return ret;
}

lp_err_t lp_DALIMaster_active_maxlevel(lp_pdali_master_t master,lp_u8_t addr)
{
	lp_err_t ret = LP_OK;
	lp_u8_t set_addr = (addr<<1)|0X01;
	
	if(master == NULL){
		ret = LP_FAIL;
		return ret;
	}
	
	ret = dali_load_send_data(DALI_DATA_16BIT(set_addr,DALI_STANDARD_COMMOND_RECALL_MAX_LEVEL),1,DALI_SEND_NO_ACK,master);
	return ret;
}

lp_err_t lp_DALIMaster_active_turnoff(lp_pdali_master_t master,lp_u8_t addr)
{
	lp_err_t ret = LP_OK;
	lp_u8_t set_addr = (addr<<1)|0X01;
	
	if(master == NULL){
		ret = LP_FAIL;
		return ret;
	}
	
	ret = dali_load_send_data(DALI_DATA_16BIT(set_addr,DALI_STANDARD_COMMOND_OFF),1,DALI_SEND_NO_ACK,master);
	return ret;
}

lp_err_t lp_DALIMaster_set_dacp(lp_pdali_master_t master,lp_u8_t addr,lp_u8_t level)
{
	lp_err_t ret = LP_OK;
	lp_u8_t set_addr = (addr<<1);
	set_addr &= ~(1u << 0);
	
	if(master == NULL){
		ret = LP_FAIL;
		return ret;
	}
	//Address byte 
	//DALI_STANDARD_COMMOND_DAPC 
	ret = dali_load_send_data(DALI_DATA_16BIT(set_addr,level),1,DALI_SEND_NO_ACK,master);
	return ret;
}

//lp_err_t lp_DALIMaster_auto_test(lp_pdali_master_t master)
//{
//	
//	lp_err_t ret = LP_OK;
//	//grup0 0x81
//	//grup1 0x83
//	ret = dali_load_send_data(DALI_DATA_16BIT(0x81,DALI_STANDARD_COMMOND_OFF),1,DALI_SEND_NO_ACK,master);
//	ret = dali_load_send_data(DALI_DATA_16BIT(0x83,DALI_STANDARD_COMMOND_RECALL_MAX_LEVEL),1,DALI_SEND_NO_ACK,master);
//	return ret;
//}

lp_err_t lp_DALIMaster_send_cmd(struct _lp_dali_master_t *master,lp_u8_t addr,lp_u8_t cmd,lp_u8_t send_count,lp_u8_t ack)
{
	lp_err_t ret = LP_OK;
	
	ret = dali_load_send_data(DALI_DATA_16BIT(addr,cmd),send_count,ack,master);
	return ret;
}



lp_err_t lp_DALIMaster_read_log(lp_pdali_master_t master)
{
	lp_err_t ret = LP_OK;
	lp_u16_t len = 0;
	lp_u16_t buf[64] = {0};
	
	ret = master->dali_bus->Read_log(master->dali_bus,buf,&len);
	if(ret == LP_OK){
		send_dali_log_command((lp_u8_t*)buf,len*2);
		//LOG_OUT("READ log len:%d\r\n",len);
		//LOG_HEX((lp_u8_t*)buf,len);
		//LOG_U16_ARRAY((lp_u16_t*)buf,len);
	}
	
	return ret;
}

lp_dali_master_t g_tDALI_masterx[] = {
	
	{LP_DALI_MASTER0,lp_dali_master_init,lp_DALIMaster_check_device,lp_DALIMaster_start_search_devices,lp_DALIMaster_get_device_info,lp_DALIMaster_read_log,\
	lp_DALIMaster_read_device_info,lp_DALIMaster_write_device_info,lp_DALIMaster_active_maxlevel,lp_DALIMaster_active_turnoff,\
	lp_DALIMaster_set_dacp,lp_DALIMaster_send_cmd},
};

lp_pdali_master_t lp_getDALI_master(lp_u8_t which)
{
	if (which >= LP_DALI_MASTER0 && which <= LP_DALI_MASTER0)
		return &g_tDALI_masterx[which];
	else
		return NULL;
}


























