

#include "lp_dali_stack/lp_dali_busx.h"
#include "lp_dali_stack/lp_dali_102_command.h"
#include "lp_sys_os_dali/lp_sys_os_dali.h"

//----------------funs statement---------------
lp_err_t lp_DALI_BUSx_Init(struct _lp_dali_busx *ptDALI_BUSx);
lp_err_t lp_DALI_BUSx_Open(struct _lp_dali_busx *ptDALI_BUSx);
lp_err_t lp_DALI_BUSx_Read(struct _lp_dali_busx *ptDALI_BUSx,lp_u8_t* data_type,lp_u8_t *data,lp_u16_t* len);
lp_err_t lp_DALI_BUSx_Write(struct _lp_dali_busx *ptDALI_BUSx,lp_u8_t *data,lp_u16_t len);
lp_err_t lp_DALI_BUSx_Close(struct _lp_dali_busx *ptDALI_BUSx);
lp_err_t lp_DALI_BUSx_Read_log(struct _lp_dali_busx *ptDALI_BUSx,lp_u16_t *data,lp_u16_t* len);

//--------------------------------------------- 

static lp_dali_busx g_tDALI_BUSx[] = {
	
	{DALI_BUS0,lp_DALI_BUSx_Init,lp_DALI_BUSx_Init,lp_DALI_BUSx_Read,lp_DALI_BUSx_Write,lp_DALI_BUSx_Close,lp_DALI_BUSx_Read_log},
};


lp_pdali_busx lp_getDALI_BUS(lp_u8_t which)
{
	if (which >= DALI_BUS0 && which <= DALI_BUS0)
		return &g_tDALI_BUSx[which];
	else
		return NULL;
}

lp_err_t lp_DALI_BUSx_Init(struct _lp_dali_busx *ptDALI_BUSx)
{
	lp_err_t ret = LP_OK;
	switch(ptDALI_BUSx->which){
	case DALI_BUS0:
		ret =  lp_sys_os_dali_init(DALI_BUS0);
	
	    ptDALI_BUSx->log_queue = lp_sys_os_queue_create(sizeof(lp_dali_log_t),6,true);
		if(ptDALI_BUSx->log_queue == NULL ){
			ret = LP_FAIL;
			LOG_OUT("dali log queue creat fail\r\n");
			return ret;
		}
		
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}
	
lp_err_t lp_DALI_BUSx_Open(struct _lp_dali_busx *ptDALI_BUSx)
{
	lp_err_t ret = LP_OK;
	switch(ptDALI_BUSx->which){
	case DALI_BUS0:

	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}

lp_err_t lp_DALI_BUSx_Read(struct _lp_dali_busx *ptDALI_BUSx,lp_u8_t* data_type,lp_u8_t *data,lp_u16_t* len)
{
	lp_err_t ret = LP_FAIL;
	lp_sys_os_dali_data dali_data_temp = {0};
	lp_dali_log_t dali_log_temp = {0};

	if(data == NULL || len==NULL ){
		ret = LP_FAIL;
		return ret;
	}
	
	switch(ptDALI_BUSx->which){
	case DALI_BUS0:
			ret = lp_sys_os_daliData_read(DALI_BUS0,&dali_data_temp);
			if(ret == LP_OK){
				
				dali_log_temp.pulseTime_len = dali_data_temp.pulseTime_len;
				//memcpy(dali_log_temp.pulseTime,dali_data_temp.pulseTime,dali_log_temp.pulseTime_len);
				for(int i=0;i<dali_log_temp.pulseTime_len;i++){
					dali_log_temp.pulseTime[i] = dali_data_temp.pulseTime[i];
				}
				
				ret  = lp_sys_os_queue_push(ptDALI_BUSx->log_queue,&dali_log_temp);
				if(ret != LP_OK){
					LOG_OUT("dali log queue_push fail\r\n");
				}
				else{
					LOG_OUT("dali_log_temp.pulseTime_len: %d\r\n",dali_log_temp.pulseTime_len);
				}
				
				if( dali_data_temp.data_type == LP_DALI_RX_BIT8_E || \
				    dali_data_temp.data_type == LP_DALI_RX_BIT16_E|| \
					dali_data_temp.data_type == LP_DALI_RX_BIT24_E|| \
					dali_data_temp.data_type == LP_DALI_RX_BIT25_E ){
						*len = dali_data_temp.len;
						memcpy(data,dali_data_temp.data,dali_data_temp.len);
						*data_type = dali_data_temp.data_type;
					
					}
					else{
						*data_type = dali_data_temp.data_type;
					}
				
			}

	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}

lp_err_t lp_DALI_BUSx_Read_log(struct _lp_dali_busx *ptDALI_BUSx,lp_u16_t *data,lp_u16_t* len)
{
	lp_err_t ret = LP_FAIL;
	lp_dali_log_t dali_log_temp = {0};

	if(data == NULL || len==NULL ){
		ret = LP_FAIL;
		return ret;
	}
	
	switch(ptDALI_BUSx->which){
	case DALI_BUS0:
			if(lp_sys_os_queue_empty(ptDALI_BUSx->log_queue) != LP_OK){
				ret = lp_sys_os_queue_pop(ptDALI_BUSx->log_queue,&dali_log_temp);
				if(ret == LP_OK){
					*len = dali_log_temp.pulseTime_len;
					//memcpy(data,dali_log_temp.pulseTime,dali_log_temp.pulseTime_len);
					for(int i=0;i<dali_log_temp.pulseTime_len;i++){
						data[i] = dali_log_temp.pulseTime[i];
					}
				}
				else{
					ret = LP_FAIL;
				}
			}
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;

}

lp_err_t lp_DALI_BUSx_Write(struct _lp_dali_busx *ptDALI_BUSx,lp_u8_t *data,lp_u16_t len)
{
	lp_err_t ret = LP_OK;
	
	if(data == NULL || len==NULL ){
		ret = LP_FAIL;
		return ret;
	}
	
	switch(ptDALI_BUSx->which){
	case DALI_BUS0:
		ret = lp_sys_os_dali_write(DALI_BUS0,data,len);
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}

lp_err_t lp_DALI_BUSx_Close(struct _lp_dali_busx *ptDALI_BUSx)
{
	lp_err_t ret = LP_OK;
	switch(ptDALI_BUSx->which){
	case DALI_BUS0:
			
	break;
	default:
		ret = LP_FAIL;
		break;
	}
	
	return ret;
}


























