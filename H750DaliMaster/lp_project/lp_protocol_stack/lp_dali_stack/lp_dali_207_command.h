






/* define to prevent recursive inclusion -------------------------------------*/
#ifndef _LP_DALI_207_COMMAND_
#define _LP_DALI_207_COMMAND_

#ifdef __cplusplus
extern "C" {
#endif

/******************************************
协议:IEC62386-207
设备:DT6类型
0xE0 to 0xFF 
******************************************/
#define DALI_EXTENDED_COMMOND_QUERY_DIMMER_CURVE		0xEE //查询调光曲线
#define DALI_EXTENDED_COMMOND_SET_DIMMER_CURVE		    0xE3 //选择调光曲线


#ifdef __cplusplus
}
#endif

#endif
























