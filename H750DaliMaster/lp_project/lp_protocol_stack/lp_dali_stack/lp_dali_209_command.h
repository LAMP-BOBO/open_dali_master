






/* define to prevent recursive inclusion -------------------------------------*/
#ifndef _LP_DALI_209_COMMAND_
#define _LP_DALI_209_COMMAND_

#ifdef __cplusplus
extern "C" {
#endif

/******************************************
协议:IEC62386-209
设备:DT8类型
0xE0 to 0xFF 
xy chromaticity
colour temperature
//故障色温和上电色温都没实现
******************************************/
#define DALI_EXTENDED_COMMOND_SET_TEMPORARY_X_COORDINATE			0xE0
#define DALI_EXTENDED_COMMOND_SET_TEMPORARY_Y_COORDINATE			0xE1

#define DALI_EXTENDED_COMMOND_ACTIVATE                  0xE2 //激活
#define DALI_EXTENDED_COMMOND_SET_COLOR_TEMP			0xE7 //设置临时色温
#define DALI_EXTENDED_COMMOND_SET_COLOR_TEMP_LIMIT		0xF2 //保存色温限制值


#define DALI_EXTENDED_COMMOND_QUERY_GEAR_FEATURES			0xF7//查询设备特征
#define DALI_EXTENDED_COMMOND_QUERY_COLOUR_STATUS			0xF8//查询颜色状态
#define DALI_EXTENDED_COMMOND_QUERY_COLOUR_TYPE_FEATURES	0xF9 //查询颜色类型特征
#define DALI_EXTENDED_COMMOND_QUERY_COLOUR_VALUE			0xFA //查询颜色值


#ifdef __cplusplus
}
#endif

#endif
























