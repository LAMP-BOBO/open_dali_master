

#ifndef _LP_SYS_MEM_H_
#define _LP_SYS_MEM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "config/lp_config.h"

#ifdef LP_USING_SYS_MEM

typedef struct {
	size_t free_node_num;		//空闲节点个数
	size_t used_node_num;		//分配节点个数
	size_t max_free_node_size;	//最大空闲节点内存
	size_t min_free_node_size;	//最小空闲节点内存
	size_t max_used_node_size;	//最大已用节点内存
	size_t min_used_node_size;	//最小已用节点内存
	size_t total_size;			//总内存
	size_t free_size;			//空闲内存
	size_t used_size;			//使用内存
}Heap_State;


void lp_sys_mem_init(void);
void lp_sys_men_destroy(void);
void lp_sys_men_free(void* ptr);
void*lp_sys_men_malloc(size_t bytes);
void lp_sys_log_out(void);

#endif

#ifdef __cplusplus
}
#endif

#endif
































