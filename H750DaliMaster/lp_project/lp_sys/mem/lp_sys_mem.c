


#include "tlsf.h"
#include "string.h"
#include "lp_sys_mem.h"

#ifdef LP_USING_SYS_MEM

static tlsf_t sys_tlsf;

#define POOL_SIZE 1024 * 8
lp_u8_t lp_align(4) buf[POOL_SIZE];

static void heap_state_tlfs_walk_func(void* ptr, size_t size, lp_int_t used, void* user) {
	Heap_State* pstate = (Heap_State*)user;
	pstate->total_size += size;
	if (used) {
		pstate->used_size += size;
		pstate->used_node_num++;
		if (pstate->max_used_node_size == 0 || pstate->max_used_node_size < size)
			pstate->max_used_node_size = size;
		if (pstate->min_used_node_size == 0 || pstate->min_used_node_size > size)
			pstate->min_used_node_size = size;
	}
	else {
		pstate->free_size += size;
		pstate->free_node_num++;
		if (pstate->max_free_node_size == 0 || pstate->max_free_node_size < size) {
			pstate->max_free_node_size = size;
		}
		if (pstate->min_free_node_size == 0 || pstate->min_free_node_size > size) {
			pstate->min_free_node_size = size;
		}
	}
}

static void tlfs_get_heap_state(Heap_State* pstate, tlsf_t tlsf_handle) {
	memset(pstate, 0, sizeof(Heap_State));
	tlsf_walk_pool(tlsf_get_pool(tlsf_handle), heap_state_tlfs_walk_func, pstate);

}

static void tlfs_print_heap_state(tlsf_t tlsf_handle) {
	Heap_State buf_state;
	tlfs_get_heap_state(&buf_state, tlsf_handle);
	LOG_OUT("free_node_num:%d\r\n"
		"used_node_num:%d\r\n"
		"max_free_node_size:%d\r\n"
		"min_free_node_size:%d\r\n"
		"max_used_node_size:%d\r\n"
		"min_used_node_size:%d\r\n"
		"total_size:%d\r\n"
		"free_size:%d\r\n"
		"used_size:%d\r\n",
		buf_state.free_node_num,
		buf_state.used_node_num,
		buf_state.max_free_node_size,
		buf_state.min_free_node_size,
		buf_state.max_used_node_size,
		buf_state.min_used_node_size,
		buf_state.total_size,
		buf_state.free_size,
		buf_state.used_size);
	LOG_OUT("remain percent:%.2f%%\r\n", (float)buf_state.free_size / (float)buf_state.total_size * 100.0);
}

void lp_sys_mem_init(void)
{
	sys_tlsf = tlsf_create_with_pool(buf,POOL_SIZE);
	tlsf_check(sys_tlsf);
	tlfs_print_heap_state(sys_tlsf);
}

void* lp_sys_men_malloc(size_t bytes)
{
	return tlsf_malloc(sys_tlsf,bytes);
}

void lp_sys_men_free(void* ptr)
{
	tlsf_free(sys_tlsf,ptr);
}

void lp_sys_men_destroy(void)
{
	tlsf_destroy(sys_tlsf);
}

void lp_sys_log_out(void)
{
	tlfs_print_heap_state(sys_tlsf);
}

#endif

#if 0
int* ptest[100];

void My_tlsf_test(void)
{
	
	test_tlsf = tlsf_create_with_pool(buf,POOL_SIZE);
	tlsf_check(test_tlsf);
	tlfs_print_heap_state(test_tlsf);
	
	for (int i = 0; i < 100;i++) {
		ptest[i] = (int*)tlsf_malloc(test_tlsf,10);

		if(ptest[i] == NULL){
			tlfs_print_heap_state(test_tlsf);
			printf("error%d\n",i);
			return;
		}
	}

	tlfs_print_heap_state(test_tlsf);
	for (int i = 0; i < 100; i++) {
		 tlsf_free(test_tlsf, ptest[i]);
	}

	tlfs_print_heap_state(test_tlsf);
}
#endif
