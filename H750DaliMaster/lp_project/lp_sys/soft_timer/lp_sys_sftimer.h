
#ifndef _LP_SYS_SFTIMER_H_
#define _LP_SYS_SFTIMER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "config/lp_config.h"

#define LOG_OUT_QUEUE    LOG_OUT
//#define	LOG_OUT_QUEUE(...)

/* lp timer */
#ifdef LP_USING_TIMER

    struct lp_timer;

    typedef enum
    {
        TIMER_STATE_RUNNING = 0,
        TIMER_STATE_STOP,
        TIMER_STATE_TIMEOUT,
    } lp_timer_state;

    typedef enum
    {
        TIMER_MODE_SINGLE = 0,
        TIMER_MODE_LOOP,
    } lp_timer_mode;

    struct lp_timer
    {
        lp_bool_t enable;
        lp_timer_state state;
        lp_timer_mode mode;
        lp_u32_t delay_tick;
        lp_u32_t timer_tick_timeout;
        struct lp_timer* prev;
        struct lp_timer* next;
#ifdef LP_TIMER_USING_TIMEOUT_CALLBACK
        void(*timeout_callback)(struct lp_timer* timer);
#endif /* LP_TIMER_USING_TIMEOUT_CALLBACK */
    };
    typedef struct lp_timer* lp_timer_t;

    lp_err_t lp_timer_func_init(lp_u32_t(*get_tick_func)(void));

#ifdef LP_TIMER_USING_CREATE
    struct lp_timer* lp_timer_create(void(*timeout_callback)(struct lp_timer* timer));
    lp_err_t lp_timer_delete(struct lp_timer* timer);
#endif /* LP_TIMER_USING_CREATE */
    lp_err_t lp_timer_init(struct lp_timer* timer, void(*timeout_callback)(struct lp_timer* timer));
    lp_err_t lp_timer_detach(struct lp_timer* timer);

    lp_err_t lp_timer_start(struct lp_timer* timer, lp_timer_mode mode, lp_u32_t delay_tick);
    lp_err_t lp_timer_stop(struct lp_timer* timer);
    lp_err_t lp_timer_continue(struct lp_timer* timer);
    lp_err_t lp_timer_restart(struct lp_timer* timer);
    lp_timer_mode lp_timer_get_mode(struct lp_timer* timer);
    lp_timer_state lp_timer_get_state(struct lp_timer* timer);
    lp_err_t lp_timer_loop_handler(void);
#endif /* LP_USING_TIMER */

#ifdef __cplusplus
}
#endif

#endif

