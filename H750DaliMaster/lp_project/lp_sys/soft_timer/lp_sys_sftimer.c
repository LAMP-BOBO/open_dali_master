
#include "lp_sys_sftimer.h"


#ifdef LP_USING_TIMER
typedef lp_u32_t(*lp_timer_get_tick_callback)(void);
static lp_timer_get_tick_callback lp_timer_get_tick = NULL;

#ifndef LP_TIMER_USING_CREATE
static struct lp_timer lp_timer_node;
#endif /* LP_TIMER_USING_CREATE */
static struct lp_timer* lp_timer_head_node = NULL;

/**
 * @brief 查找定时器链表尾节点(内部函数)
 *
 * @param lp_timer_start_node 开始查找的首节点
 * @return struct lp_timer* 尾节点地址
 */
static struct lp_timer* _lp_timer_find_node_tail(struct lp_timer* lp_timer_start_node)
{
    LP_ASSERT(lp_timer_start_node);
    struct lp_timer* node_curr = lp_timer_start_node;
    struct lp_timer* node_next = lp_timer_start_node;
    do
    {
        node_curr = node_next;
        node_next = node_curr->next;
    } while (node_next != NULL);
    return node_curr;
}

/**
 * @brief 插入定时器到链表(内部函数)
 *
 * @param lp_timer_node 定时器对象
 * @return LP_OK 插入成功
 * @return LP_FAIL 插入失败
 */
static lp_err_t _lp_timer_insert_node_to_list(struct lp_timer* lp_timer_node)
{
    LP_ASSERT(lp_timer_node);
    struct lp_timer* node_tail = _lp_timer_find_node_tail(lp_timer_head_node);
    node_tail->next = lp_timer_node;
    lp_timer_node->prev = node_tail;
    return LP_OK;
}

/**
 * @brief 软件定时器功能初始化
 *
 * @param get_tick_func 获取系统tick回调函数
 * @return LP_OK 初始化成功
 * @return LP_FAIL 初始失败
 * LP_SYS_MALLOC
 */
lp_err_t lp_timer_func_init(lp_u32_t(*get_tick_func)(void))
{
    LP_ASSERT(get_tick_func);
#ifdef LP_TIMER_USING_CREATE
    struct lp_timer* lp_timer_node;
    lp_timer_node = (struct lp_timer*)LP_SYS_MALLOC(sizeof(struct lp_timer));
    if (lp_timer_node == NULL)
        return LP_FAIL;
    lp_timer_head_node = lp_timer_node;
#else
    lp_timer_head_node = &lp_timer_node;
#endif /* LP_TIMER_USING_CREATE */
    lp_timer_head_node->prev = NULL;
    lp_timer_head_node->next = NULL;
    lp_timer_get_tick = get_tick_func;
    return LP_OK;
}

/**
 * @brief 静态初始化定时器
 *
 * @param timer 要初始化的定时器对象
 * @param timeout_callback 定时器超时回调函数，不使用可配置为NULL
 * @return LP_OK 初始化成功
 * @return LP_FAIL 初始化失败
 */
lp_err_t lp_timer_init(struct lp_timer* timer, void (*timeout_callback)(struct lp_timer* timer))
{
    LP_ASSERT(lp_timer_head_node);
    LP_ASSERT(lp_timer_get_tick);
    LP_ASSERT(timer);
    if (lp_timer_head_node == NULL || lp_timer_get_tick == NULL)
        return LP_FAIL;
    timer->enable = false;
    timer->mode = TIMER_MODE_LOOP;
    timer->state = TIMER_STATE_STOP;
    timer->delay_tick = 0;
    timer->timer_tick_timeout = 0;
    timer->prev = NULL;
    timer->next = NULL;
#ifdef LP_TIMER_USING_TIMEOUT_CALLBACK
    timer->timeout_callback = timeout_callback;
#endif /* LP_TIMER_USING_TIMEOUT_CALLBACK */
    lp_err_t result = _lp_timer_insert_node_to_list(timer);
    return result;
}

/**
 * @brief 静态脱离定时器
 *
 * @param timer 要脱离的定时器对象
 * @return LP_OK 脱离成功
 * @return LP_FAIL 脱离失败
 */
lp_err_t lp_timer_detach(struct lp_timer* timer)
{
    LP_ASSERT(lp_timer_head_node);
    LP_ASSERT(timer);
    timer->prev->next = timer->next;
    if (timer->next != NULL)
        timer->next->prev = timer->prev;
    return LP_OK;
}

#ifdef LP_TIMER_USING_CREATE
/**
 * @brief 动态创建定时器
 *
 * @param timeout_callback 定时器超时回调函数，不使用可配置为NULL
 * @return struct lp_timer* 创建的定时器对象，NULL为创建失败
 * LP_SYS_MALLOC
 */
struct lp_timer* lp_timer_create(void (*timeout_callback)(struct lp_timer* timer))
{
    LP_ASSERT(lp_timer_head_node);
    LP_ASSERT(lp_timer_get_tick);
    struct lp_timer* timer;
    if (lp_timer_head_node == NULL || lp_timer_get_tick == NULL)
        return NULL;
    if ((timer = LP_SYS_MALLOC(sizeof(struct lp_timer))) == NULL)
        return NULL;
    timer->enable = false;
    timer->mode = TIMER_MODE_LOOP;
    timer->state = TIMER_STATE_STOP;
    timer->delay_tick = 0;
    timer->timer_tick_timeout = 0;
    timer->prev = NULL;
    timer->next = NULL;
#ifdef LP_TIMER_USING_TIMEOUT_CALLBACK
    timer->timeout_callback = timeout_callback;
#endif /* LP_TIMER_USING_TIMEOUT_CALLBACK */
    _lp_timer_insert_node_to_list(timer);
    return timer;
}

/**
 * @brief 动态删除定时器
 *
 * @param timer 要删除的定时器对象
 * @return LP_OK 删除成功
 * @return LP_FAIL 删除失败
 * LP_SYS_FREE
 */
lp_err_t lp_timer_delete(struct lp_timer* timer)
{
    LP_ASSERT(timer);
    if (lp_timer_detach(timer) == LP_OK)
    {
        LP_SYS_FREE(timer);
        return LP_OK;
    }
    return LP_FAIL;
}
#endif /* LP_TIMER_USING_CREATE */

/**
 * @brief 设置定时器启动参数(内部函数)
 *
 * @param timer 定时器对象
 * @return LP_OK 成功
 * @return LP_FAIL 失败
 */
static lp_err_t _lp_timer_set_start_param(struct lp_timer* timer)
{
    LP_ASSERT(timer);
    if (timer->delay_tick == 0 || lp_timer_get_tick == NULL)
        return LP_FAIL;
    timer->timer_tick_timeout = lp_timer_get_tick() + timer->delay_tick;
    timer->enable = true;
    timer->state = TIMER_STATE_RUNNING;
    return LP_OK;
}

/**
 * @brief 定时器启动
 *
 * @param timer 要启动的定时器对象
 * @param mode 模式: 单次TIMER_MODE_SINGLE; 循环TIMER_MODE_LOOP
 * @param delay_tick 定时器时长(单位tick)
 * @return LP_OK 启动成功
 * @return LP_FAIL 启动失败
 */
lp_err_t lp_timer_start(struct lp_timer* timer, lp_timer_mode mode, lp_u32_t delay_tick)
{
    LP_ASSERT(timer);
    LP_ASSERT(delay_tick);
    timer->mode = mode;
    timer->delay_tick = delay_tick;
    lp_err_t result = _lp_timer_set_start_param(timer);
    return result;
}

/**
 * @brief 定时器停止
 *
 * @param timer 要停止的定时器对象
 * @return LP_OK 停止成功
 * @return LP_FAIL 停止失败
 */
lp_err_t lp_timer_stop(struct lp_timer* timer)
{
    LP_ASSERT(timer);
    timer->enable = false;
    timer->state = TIMER_STATE_STOP;
    return LP_OK;
}

/**
 * @brief 定时器继续
 *
 * @param timer 要继续的定时器对象
 * @return LP_OK 继续成功
 * @return LP_FAIL 继续失败
 */
lp_err_t lp_timer_continue(struct lp_timer* timer)
{
    LP_ASSERT(timer);
    timer->enable = true;
    timer->state = TIMER_STATE_RUNNING;
    return LP_OK;
}

/**
 * @brief 定时器重启
 *
 * @param timer 要重启的定时器对象
 * @return LP_OK 重启成功
 * @return LP_FAIL 重启失败
 */
lp_err_t lp_timer_restart(struct lp_timer* timer)
{
    LP_ASSERT(timer);
    lp_err_t result = _lp_timer_set_start_param(timer);
    return result;
}

/**
 * @brief 获取定时器模式
 *
 * @param timer 要获取的定时器对象
 * @return lp_timer_mode 定时器模式：单次TIMER_MODE_SINGLE; 循环TIMER_MODE_LOOP
 */
lp_timer_mode lp_timer_get_mode(struct lp_timer* timer)
{
    LP_ASSERT(timer);
    return timer->mode;
}

/**
 * @brief 获取定时器状态
 *
 * @param timer 要获取的定时器对象
 * @return lp_timer_state 定时器状态
 * TIMER_STATE_RUNNING、TIMER_STATE_STOP、TIMER_STATE_TIMEOUT
 */
lp_timer_state lp_timer_get_state(struct lp_timer* timer)
{
    LP_ASSERT(timer);
    return timer->state;
}

/**
 * @brief 定时器处理
 *
 * @return LP_OK 正常
 * @return LP_FAIL 异常
 */
lp_err_t lp_timer_loop_handler(void)
{
    struct lp_timer* timer = NULL;

    if (lp_timer_head_node != NULL)
        timer = lp_timer_head_node->next;
    else
        return LP_FAIL;

    while (timer != NULL)
    {
        if (timer->enable && (lp_timer_get_tick() - timer->timer_tick_timeout) < (LP_U32_MAX / 2))
        {
            timer->enable = false;
            timer->state = TIMER_STATE_TIMEOUT;
#ifndef LP_TIMER_USING_INTERVAL
            if (timer->mode == TIMER_MODE_LOOP)
                lp_timer_restart(timer);
#endif /* LP_TIMER_USING_INTERVAL */
#ifdef LP_TIMER_USING_TIMEOUT_CALLBACK
            if (timer->timeout_callback != NULL)
                timer->timeout_callback(timer);
#endif /* LP_TIMER_USING_TIMEOUT_CALLBACK */
#ifdef LP_TIMER_USING_INTERVAL
            if (timer->mode == TIMER_MODE_LOOP)
                lp_timer_restart(timer);
#endif /* LP_TIMER_USING_INTERVAL */
        }
        timer = timer->next;
    }

    return LP_OK;
}

#endif /* TOOLKIT_USING_TIMER */









