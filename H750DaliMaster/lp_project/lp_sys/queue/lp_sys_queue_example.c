#include <stdio.h>
#include <string.h>
#include "string.h"
#include "queue/lp_sys_queue.h"
#include "mem/lp_sys_mem.h"

/* 打印缓冲区 */
void printf_buffer(char* str, void* buffer, uint16_t buf_len)
{
	uint8_t* buff = buffer;
	printf("%s (%d):", str, buf_len);
	while (buf_len--)
	{
		printf("%d ", *buff);
		buff++;
	}
	printf("\n");
}

struct test
{
	uint8_t a;
	uint8_t b;
	uint16_t c;
};

#define QUEUE1_POOL_SIZE 50
#define QUEUE2_POOL_SIZE 50
#define QUEUE3_ELEMENT_NUM 10

struct lp_queue queue1;
uint8_t queue1_pool[QUEUE1_POOL_SIZE] = {0};

int main(int argc, char* argv[])
{
	int i = 0;
	uint8_t temp[100] = {0};
	int ret = 0;

	struct test test_a = {
	.a = 1,
	.b = 1,
	.c = 1,
	};
	struct test test_b = {
	.a = 2,
	.b = 2,
	.c = 2,
	};
	struct test test_c = {
	.a = 3,
	.b = 3,
	.c = 3,
	};
	struct test test_temp = {0};

	lp_sys_mem_init();
	lp_queue_init(&queue1, queue1_pool, sizeof(queue1_pool), sizeof(queue1_pool[0]), true);

	for (i = 0; i < sizeof(temp) / sizeof(temp[0]); i++)
		temp[i] = i;

	printf_buffer("queue1_push_before", queue1.queue_pool, QUEUE1_POOL_SIZE);
	lp_queue_push_multi(&queue1, temp, QUEUE1_POOL_SIZE + 10);
	printf_buffer("queue1_push_after", queue1.queue_pool, QUEUE1_POOL_SIZE);

	/* 拿出10个数据后打印 */
	int pop_len = lp_queue_pop_multi(&queue1, temp, 10);
	printf_buffer("queue1_pop", temp, pop_len);
	lp_queue_detach(&queue1);
	printf("\n");
	printf("\n");

	/* 动态方式创建一个循环队列"queue2",空间为50字节,模式为正常模式 */
	lp_queue_t queue2 = lp_queue_create(sizeof(uint8_t), QUEUE2_POOL_SIZE, false);

	for (i = 0; i < sizeof(temp) / sizeof(temp[0]); i++)
		temp[i] = i;

	printf_buffer("queue2_push_before", (uint8_t*)queue2->queue_pool, QUEUE2_POOL_SIZE);

	lp_queue_push_multi(queue2, temp, QUEUE2_POOL_SIZE + 10);
	printf_buffer("queue2_push_after", (uint8_t*)queue2->queue_pool, QUEUE2_POOL_SIZE);

	pop_len = lp_queue_pop_multi(queue2, temp, 10);
	printf_buffer("queue2_pop", temp, pop_len);

	lp_queue_delete(queue2);

	printf("\n");
	printf("\n");

	lp_queue_t queue3 = lp_queue_create(sizeof(struct test), QUEUE3_ELEMENT_NUM* sizeof(struct test), false);
	lp_queue_push(queue3,&test_a);
	lp_queue_push(queue3,&test_b);
	lp_queue_push(queue3,&test_c);
	printf_buffer("queue3_push_before", (uint8_t*)queue3->queue_pool, QUEUE3_ELEMENT_NUM * sizeof(struct test));

	ret = lp_queue_pop(queue3,&test_temp);
	printf("ret %d test_temp %d %d %d\r\n", ret, test_temp.a, test_temp.b, test_temp.c);
	ret = lp_queue_pop(queue3, &test_temp);
	printf("ret %d test_temp %d %d %d\r\n", ret, test_temp.a, test_temp.b, test_temp.c);
	ret = lp_queue_pop(queue3, &test_temp);
	printf("ret %d test_temp %d %d %d\r\n", ret, test_temp.a, test_temp.b, test_temp.c);
	printf_buffer("queue3_pop", (uint8_t*)queue3->queue_pool, QUEUE3_ELEMENT_NUM * sizeof(struct test));
	ret = lp_queue_pop(queue3, &test_temp);
	printf("ret %d test_temp %d %d %d\r\n",ret,test_temp.a, test_temp.b, test_temp.c);

	getchar();
	return 0;
}