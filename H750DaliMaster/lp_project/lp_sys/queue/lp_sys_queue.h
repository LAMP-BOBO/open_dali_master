
#ifndef _LP_SYS_QUEUE_H_
#define _LP_SYS_QUEUE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "config/lp_config.h"

#define LOG_OUT_QUEUE    LOG_OUT
//#define	LOG_OUT_QUEUE(...)

#ifdef LP_USING_QUEUE
    struct lp_queue
    {
        lp_bool_t keep_fresh;
        void* queue_pool;
        lp_u16_t queue_size;
        lp_u16_t max_queues;
        lp_u16_t front;
        lp_u16_t rear;
        lp_u16_t len;
    };
    typedef struct lp_queue* lp_queue_t;

#ifdef LP_QUEUE_USING_CREATE
    struct lp_queue* lp_queue_create(lp_u16_t queue_size, lp_u16_t max_queues, lp_bool_t keep_fresh);
    lp_err_t lp_queue_delete(struct lp_queue* queue);
#endif

    lp_err_t lp_queue_init(struct lp_queue* queue, void* queuepool, lp_u16_t pool_size, lp_u16_t queue_size, lp_bool_t keep_fresh);
    lp_err_t lp_queue_detach(struct lp_queue* queue);
    lp_err_t lp_queue_clean(struct lp_queue* queue);
    lp_err_t lp_queue_empty(struct lp_queue* queue);
    lp_err_t lp_queue_full(struct lp_queue* queue);
    lp_err_t lp_queue_peep(struct lp_queue* queue, void* pval);
    lp_err_t lp_queue_remove(struct lp_queue* queue);
    lp_err_t lp_queue_push(struct lp_queue* queue, void* pval);
    lp_err_t lp_queue_pop(struct lp_queue* queue, void* pval);

    lp_u16_t lp_queue_curr_len(struct lp_queue* queue);
    lp_u16_t lp_queue_push_multi(struct lp_queue* queue, void* pval, lp_u16_t len);
    lp_u16_t lp_queue_pop_multi(struct lp_queue* queue, void* pval, lp_u16_t len);

#endif /* TOOLKIT_USING_QUEUE */

#ifdef __cplusplus
}
#endif

#endif



