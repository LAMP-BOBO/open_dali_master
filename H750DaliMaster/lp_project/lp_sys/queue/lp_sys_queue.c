
#include "lp_sys_queue.h"

#ifdef LP_USING_QUEUE

/**
 * @brief 静态初始化队列
 *
 * @param queue 队列对象
 * @param queuepool 队列缓存区
 * @param pool_size 缓存区大小(单位字节)
 * @param queue_size 队列元素大小(单位字节)
 * @param keep_fresh 是否为保持最新模式,true：保持最新 false：默认(存满不能再存)
 * @return LP_OK 初始化成功
 * @return LP_FAIL 初始化失败
 */
lp_err_t lp_queue_init(struct lp_queue* queue, void* queuepool, lp_u16_t pool_size, lp_u16_t queue_size, lp_bool_t keep_fresh)
{
    LP_ASSERT(queue);
    LP_ASSERT(queuepool);
    LP_ASSERT(queue_size);
    LP_ASSERT(pool_size);

    if (queue == NULL || queuepool == NULL) {
        return LP_FAIL;
    }
       
    queue->keep_fresh = keep_fresh;
    queue->queue_pool = queuepool;
    queue->queue_size = queue_size;
    queue->max_queues = pool_size / queue->queue_size;
    queue->front = 0;
    queue->rear = 0;
    queue->len = 0;
    return LP_OK;
}

/**
 * @brief 静态脱离队列
 * 会使缓存区脱离与队列的关联。必须为静态方式创建的队列对象。
 *
 * @param queue 要脱离的队列对象
 * @return LP_OK 脱离成功
 * @return LP_FAIL 脱离失败
 */
lp_err_t lp_queue_detach(struct lp_queue* queue)
{
    LP_ASSERT(queue);
    if (queue == NULL) {
        return LP_FAIL;
    }
    queue->queue_pool = NULL;
    queue->front = 0;
    queue->rear = 0;
    queue->len = 0;

    return LP_OK;
}

#ifdef LP_QUEUE_USING_CREATE
/**
 * @brief 动态创建队列
 *
 * @param queue_size 队列元素大小(单位字节)
 * @param max_queues 最大队列个数
 * @param keep_fresh  是否为保持最新模式,true：保持最新 false：默认(存满不能再存)
 * @return struct lp_queue* 创建的队列对象,NULL创建失败
 * LP_SYS_MALLOC
 */
struct lp_queue* lp_queue_create(lp_u16_t queue_size, lp_u16_t max_queues, lp_bool_t keep_fresh)
{
    LP_ASSERT(queue_size);
    struct lp_queue* queue = NULL;

    queue = (struct lp_queue*)LP_SYS_MALLOC(sizeof(struct lp_queue));
    if (queue == NULL) {
        LOG_OUT_QUEUE("queue == NULL malloc fail %d\r\n",sizeof(struct lp_queue));
        return NULL;
    } 
    queue->queue_size = queue_size;
    queue->max_queues = max_queues;
    size_t total_size = queue->queue_size * queue->max_queues; //SIZE_MAX
    if (queue->queue_size != 0 && (total_size / queue->queue_size != queue->max_queues)) {
        LOG_OUT_QUEUE("total_size == NULL error total_size %d max_queues %d queue_size %d\r\n", total_size, queue->max_queues, queue->queue_size);
        LP_SYS_FREE(queue);
        return NULL; 
    }
    queue->queue_pool = LP_SYS_MALLOC(total_size);
    if (queue->queue_pool == NULL)
    {
        LP_SYS_FREE(queue);
        LOG_OUT_QUEUE("queue->queue_pool == NULL malloc fail %d %d %d\r\n",total_size,queue->queue_size,queue->max_queues);
        return NULL;
    }
    memset((lp_u8_t*)queue->queue_pool,0x00, total_size);
    queue->keep_fresh = keep_fresh;
    queue->front = 0;
    queue->rear = 0;
    queue->len = 0;
    return queue;
}

/**
 * @brief 动态删除队列
 *
 * @param queue 要删除的队列对象
 * @return LP_OK 删除成功
 * @return LP_FAIL 删除失败
 * LP_SYS_FREE
 */
lp_err_t lp_queue_delete(struct lp_queue* queue)
{
    LP_ASSERT(queue);
    if (queue == NULL)
        return LP_FAIL;
    LP_SYS_FREE(queue->queue_pool);
    LP_SYS_FREE(queue);
    return LP_OK;
}
#endif /* LP_QUEUE_USING_CREATE */

/**
 * @brief 清空队列
 *
 * @param queue 要清空的队列对象
 * @return LP_OK 清除成功
 * @return LP_FAIL 清除失败
 */
lp_err_t lp_queue_clean(struct lp_queue* queue)
{
    LP_ASSERT(queue);
    if (queue == NULL)
        return LP_FAIL;
    queue->front = 0;
    queue->rear = 0;
    queue->len = 0;
    return LP_OK;
}

/**
 * @brief 查询队列当前数据长度
 *
 * @param queue 要查询的队列对象
 * @return lp_u16_t 队列数据当前长度(元素个数)
 */
lp_u16_t lp_queue_curr_len(struct lp_queue* queue)
{
    LP_ASSERT(queue);
    if (queue == NULL)
        return LP_U16_MIN;
    return (queue->len);
}

/**
 * @brief 判断队列是否为空
 *
 * @param queue 要查询的队列对象
 * @return LP_OK 空
 * @return LP_FAIL 不为空
 */
lp_err_t lp_queue_empty(struct lp_queue* queue)
{
    LP_ASSERT(queue);
    if (queue->len == 0) {
        return LP_OK;
    }
    return LP_FAIL;
}

/**
 * @brief 判断队列是否已满
 *
 * @param queue 要查询的队列对象
 * @return LP_OK 满
 * @return LP_FAIL 不为满
 */
lp_err_t lp_queue_full(struct lp_queue* queue)
{
    LP_ASSERT(queue);
    if (queue->len >= queue->max_queues) {
        return LP_OK;
    }
    return LP_FAIL;
}

/**
 * @brief 向队列压入(入队)1个元素数据
 *
 * @param queue 要压入的队列对象
 * @param val 压入值
 * @return LP_OK 成功
 * @return LP_FAIL 失败
 */
lp_err_t lp_queue_push(struct lp_queue* queue, void* val)
{
    LP_ASSERT(queue);
    LP_ASSERT(queue->queue_pool);
    if (lp_queue_full(queue) == LP_OK)
    {
        if (queue->keep_fresh == true)
        {
            memcpy((uint8_t*)queue->queue_pool + (queue->rear * queue->queue_size),
                (uint8_t*)val, queue->queue_size);

            queue->rear = (queue->rear + 1) % queue->max_queues;
            queue->front = (queue->front + 1) % queue->max_queues;
            queue->len = queue->max_queues;
            return LP_OK;
        }
        return LP_FAIL;
    }
    else
    {
        memcpy((uint8_t*)queue->queue_pool + (queue->rear * queue->queue_size),
            (uint8_t*)val, queue->queue_size);

        queue->rear = (queue->rear + 1) % queue->max_queues;
        queue->len++;
    }
    return LP_OK;
}

/**
 * @brief 从队列弹出(出队)1个元素数据
 *
 * @param queue 要弹出的队列对象
 * @param pval 弹出值
 * @return LP_OK 成功
 * @return LP_FAIL 失败
 */
lp_err_t lp_queue_pop(struct lp_queue* queue, void* pval)
{
    LP_ASSERT(queue);
    LP_ASSERT(queue->queue_pool);
    if (lp_queue_empty(queue)== LP_OK)
    {
        return LP_FAIL;
    }
    else
    {
        memcpy((uint8_t*)pval,
            (uint8_t*)queue->queue_pool + (queue->front * queue->queue_size),
            queue->queue_size);

        queue->front = (queue->front + 1) % queue->max_queues;
        queue->len--;
    }
    return LP_OK;
}

/**
 * @brief 向队列压入(入队)多个元素数据
 *
 * @param queue 要压入的队列对象
 * @param pval 压入元素首地址
 * @param len 压入元素个数
 * @return lp_u16_t 实际压入个数
 */
lp_u16_t lp_queue_push_multi(struct lp_queue* queue, void* pval, lp_u16_t len)
{
    LP_ASSERT(queue);
    LP_ASSERT(queue->queue_pool);
    uint8_t* u8pval = pval;
    lp_u16_t push_len = 0;
    while (len-- && lp_queue_push(queue, u8pval) == LP_OK)
    {
        push_len++;
        u8pval += queue->queue_size;
    }

    return push_len;
}

/**
 * @brief 从队列弹出(出队)多个元素数据
 *
 * @param queue 要弹出的队列对象
 * @param pval 存放弹出元素的首地址
 * @param len 希望弹出的元素个数
 * @return lp_u16_t 实际弹出个数
 */
lp_u16_t lp_queue_pop_multi(struct lp_queue* queue, void* pval, lp_u16_t len)
{
    LP_ASSERT(queue);
    LP_ASSERT(queue->queue_pool);
    lp_u16_t pop_len = 0;
    uint8_t* u8pval = pval;
    if (lp_queue_empty(queue) == true)
        return false;
    while (len-- && lp_queue_pop(queue, u8pval) == LP_OK)
    {
        pop_len++;
        u8pval += queue->queue_size;
    }
    return pop_len;
}

/**
 * @brief 从队列中读取一个元素(不从队列中删除)
 *
 * @param queue 队列对象
 * @param pval 读取值地址
 * @return LP_OK 读取成功
 * @return LP_FAIL 读取失败
 */
lp_err_t lp_queue_peep(struct lp_queue* queue, void* pval)
{
    LP_ASSERT(queue);
    LP_ASSERT(queue->queue_pool);
    if (lp_queue_empty(queue))
    {
        return LP_FAIL;
    }
    else
    {
        memcpy((uint8_t*)pval,
            (uint8_t*)queue->queue_pool + (queue->front * queue->queue_size),
            queue->queue_size);
    }
    return LP_OK;
}

/**
 * @brief 移除一个元素
 *
 * @param queue  要移除元素的对象
 * @return LP_OK 移除成功
 * @return false 移除失败
 */
lp_err_t lp_queue_remove(struct lp_queue* queue)
{
    if (lp_queue_empty(queue) == LP_OK)
    {
        return LP_OK;
    }
    queue->front = (queue->front + 1) % queue->max_queues;
    queue->len--;

    return LP_OK;
}
#endif










