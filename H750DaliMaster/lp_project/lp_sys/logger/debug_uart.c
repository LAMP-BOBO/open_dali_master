

#include "log_system.h"
#include "debug_uart.h"
#include "kal_debug_uart.h"


int DebugUartDeviceInit(void)
{
	KAL_DebugUartDeviceInit();
	
	return 0;
}


int DebugUart_Log_printf(const char* format, va_list args_list)
{

    KAL_DebugUart_Log_printf(format, args_list);
	
	return 0;
}


int DebugUart_log_float(float valve)
{
	KAL_DebugUart_log_float(valve);
	
	return 0;
}





static LogDevice g_tUartDevice = {
	.name = "log_uart",
	.DeviceInit = DebugUartDeviceInit,
	.DeviceExit = NULL,
	.Log_printf = DebugUart_Log_printf,
	.log_float	= DebugUart_log_float,
};



void AddLogDeviceUart(void)
{
	LogDeviceRegister(&g_tUartDevice);
}



