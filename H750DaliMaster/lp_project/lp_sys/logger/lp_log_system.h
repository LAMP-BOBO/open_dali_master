

#ifndef _LP_LOG_SYSTEM_H
#define _LP_LOG_SYSTEM_H

#include "config/lp_config.h"
#include <stdarg.h>
#include <string.h>
#include <stdio.h>

#ifdef BOOT_APP
#define LOG_SYSTEM_ENABLE		0
#else
#define LOG_SYSTEM_ENABLE		1
#endif


//printf("%d\n", __LINE__);
//printf("%s\n", __FILE__);

typedef struct LogDevice {
	char *name;
	int (*DeviceInit)(void);
	int (*DeviceExit)(void);
	int (*Log_printf)(const char* format, va_list args_list);
	int (*log_float)(float valve);
	struct LogDevice *ptNext;
}LogDevice, *PLogDevice;



void AddLogDevices(void);
int SelectDefaultLogDevices(char *name);
PLogDevice InitDefaulLogDevices(void);
void LogDeviceRegister(PLogDevice ptLogDevices);

/*外部组件调用下列函数即可使用log system*/
/*但是打印前必须先初始化一次*/
#if LOG_SYSTEM_ENABLE
void Log_init(char *name);
void LOG_FLOAT(float value);
void LOG_OUT(const char *format, ...);
void LOG_HEX(lp_u8_t* data,lp_u16_t len);
void LOG_U16_ARRAY(lp_u16_t* data,lp_u16_t len);
void LOG_U32_ARRAY(lp_u32_t* data,lp_u16_t len);
#else
#define	LOG_FLOAT(a)
#define	LOG_OUT(...)
#endif

#endif


