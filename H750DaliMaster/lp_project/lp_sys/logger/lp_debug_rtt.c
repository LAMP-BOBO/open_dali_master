
#include "lp_log_system.h"
#include "lp_debug_rtt.h"
#include "SEGGER_RTT.h"


#define RTTBUFFER_INDEX    0

int DebugRttDeviceInit(void)
{
	/* 配置通道0，上行配置*/
	SEGGER_RTT_ConfigUpBuffer(RTTBUFFER_INDEX, "RTTUP", NULL, 0, SEGGER_RTT_MODE_NO_BLOCK_SKIP);
	
	/* 配置通道0，下行配置*/	
	SEGGER_RTT_ConfigDownBuffer(RTTBUFFER_INDEX, "RTTDOWN", NULL, 0, SEGGER_RTT_MODE_NO_BLOCK_SKIP);
	
	return 0;
}

extern  int SEGGER_RTT_vprintf(unsigned BufferIndex, const char * sFormat, va_list * pParamList);
int DebugRtt_Log_printf(const char* format, va_list args_list)
{

    SEGGER_RTT_vprintf(RTTBUFFER_INDEX, format, &args_list);
	return 0;
}


int DebugRtt_log_float(float valve)
{
    char float_str[10];
 
    sprintf(float_str, "%f\n", valve); 
 
	SEGGER_RTT_printf(RTTBUFFER_INDEX, "%s\r\n",float_str);
    
	return 0;
}



/* 做一个简单的回环功能 */
//		if (SEGGER_RTT_HasKey()) 
//		{
//			GetKey = SEGGER_RTT_GetKey();
//			SEGGER_RTT_SetTerminal(0);
//			SEGGER_RTT_printf(0, "SEGGER_RTT_GetKey = %c\r\n", GetKey);
//		}

static LogDevice g_tRttDevice = {
	.name = "log_rtt",
	.DeviceInit = DebugRttDeviceInit,
	.DeviceExit = NULL,
	.Log_printf = DebugRtt_Log_printf,
	.log_float	= DebugRtt_log_float,
};



void AddLogDeviceRtt(void)
{
	LogDeviceRegister(&g_tRttDevice);
}



