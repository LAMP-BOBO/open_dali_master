


#include "lp_log_system.h"
//#include "lp_debug_uart.h"
#include "lp_debug_rtt.h"
#include <string.h>



static PLogDevice g_ptLogDevices = NULL;



void LogDeviceRegister(PLogDevice ptLogDevices)
{
	ptLogDevices->ptNext = g_ptLogDevices;
	g_ptLogDevices = ptLogDevices;
}



void AddLogDevices(void)
{
	AddLogDeviceRtt();
	//AddLogDeviceUart();
}

int SelectDefaultLogDevices(char *name)
{
	PLogDevice pTmp = g_ptLogDevices;
	while (pTmp) 
	{
		if (strcmp(name, pTmp->name) == 0)
		{
			g_ptLogDevices = pTmp;
			return 0;
		}

		pTmp = pTmp->ptNext;
	}

	return -1;
}

PLogDevice InitDefaulLogDevices(void)
{
	if(g_ptLogDevices != NULL){
		
		g_ptLogDevices->DeviceInit();
		
		return g_ptLogDevices;
	}
	
	return NULL;
}


/*再一次的封装log函数，让所有外部组件都能直接调用打印 */
static PLogDevice gp_log;

void Log_init(char *name)
{
	AddLogDevices();
	SelectDefaultLogDevices(name);
	gp_log = InitDefaulLogDevices();
}

#if LOG_SYSTEM_ENABLE
void LOG_OUT(const char *format, ...)
{
	va_list marker;
	va_start(marker, format);
	
	gp_log->Log_printf(format,marker);
	
    va_end(marker);
}

void LOG_FLOAT(float value)
{

	gp_log->log_float(value);
	
}

void LOG_HEX(lp_u8_t* data,lp_u16_t len)
{
	if(data == NULL)return;
	
	for(int i=0;i<len;i++)
	{
		LOG_OUT("0x%x ",data[i]);
	}
	LOG_OUT("\r\n");
}

void LOG_U16_ARRAY(lp_u16_t* data,lp_u16_t len)
{
	if(data == NULL)return;
	
	for(int i=0;i<len;i++)
	{
		LOG_OUT("%d ",data[i]);
	}
	LOG_OUT("\r\n");
}

void LOG_U32_ARRAY(lp_u32_t* data,lp_u16_t len)
{
	if(data == NULL)return;
	
	for(int i=0;i<len;i++)
	{
		LOG_OUT("%d ",data[i]);
	}
	LOG_OUT("\r\n");
}
#endif



