

#ifndef _LP_SYS_SPTIME_H_
#define _LP_SYS_SPTIME_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "config/lp_config.h"

	/*this time no os block*/
	lp_err_t lp_sys_delay_us(lp_u32_t ustime);
	lp_err_t lp_sys_delay_ms(lp_u32_t mstime);

#ifdef __cplusplus
}
#endif

#endif




















