

#include "sleep_time/lp_sys_sptime.h"



#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>

lp_err_t lp_sys_delay_us(lp_u32_t ustime)
{
    HANDLE timer;
    LARGE_INTEGER ft; //100ns
    lp_long_t time_tmep = (lp_long_t)ustime;
    //QueryPerformanceFrequency(&ft);
    //ft.QuadPart /= 1000000;

    timer = CreateWaitableTimer(NULL, FALSE, NULL);
    if (timer == NULL) {
        return LP_FAIL;
    }
    ft.QuadPart = -10LL* time_tmep;
    SetWaitableTimer(timer, &ft, 0, NULL, NULL, FALSE);
    WaitForSingleObject(timer, INFINITE); //block
    //WaitForMultipleObjects 
    CloseHandle(timer);

    return LP_OK;
}

lp_err_t lp_sys_delay_ms(lp_u32_t mstime)
{
	 Sleep(mstime);
     return LP_OK;
}

#elif defined(__ARMCC_VERSION)  
lp_err_t lp_sys_delay_us(lp_u32_t ustime)
{

    return LP_OK;
}

lp_err_t lp_sys_delay_ms(lp_u32_t mstime)
{
    return LP_OK;
}
#endif











