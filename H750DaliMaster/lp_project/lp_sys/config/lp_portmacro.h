

#ifndef _LP_PORTMACRO_H
#define _LP_PORTMACRO_H

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "stddef.h"
#include <stdbool.h>

/* Type definitions. */
typedef unsigned int                    lp_u32_t;
typedef signed int                      lp_s32_t;
typedef unsigned short                  lp_u16_t;
typedef signed short                    lp_s16_t;
typedef unsigned char                   lp_u8_t;
typedef signed char                     lp_s8_t;
typedef int                             lp_int_t;
typedef bool                            lp_bool_t;
typedef long                            lp_long_t;
typedef float                           lp_float_t;
typedef double                          lp_double_t;

typedef int                             lp_err_t;

#define LP_NULL                        ((void *)0)

#define LP_U32_MAX                     (0xffffffffU)
#define LP_U32_MIN                     (0U)
#define LP_U16_MAX                     (0xffffU)
#define LP_U16_MIN                     (0U)
#define LP_U8_MAX                      (0xffU)
#define LP_U8_MIN                      (0U)

/* Compiler Related Definitions */
#if defined(__ARMCC_VERSION)           /* ARM Compiler */

    #define lp_section(x)              __attribute__((section(x)))
    #define lp_used                    __attribute__((used))
    #define lp_align(n)                __attribute__((aligned(n)))
    #define lp_weak                    __attribute__((weak))
    #define lp_inline                  static __inline

#elif defined (__GNUC__)                /* GNU GCC Compiler */

    #define lp_section(x)              __attribute__((section(x)))
    #define lp_used                    __attribute__((used))
    #define lp_align(n)                __attribute__((aligned(n)))
    #define lp_weak                    __attribute__((weak))
    #define lp_inline                  static __inline

#elif defined (__IAR_SYSTEMS_ICC__)     /* for IAR Compiler */

    #define lp_section(x)              @ x
    #define lp_used                    __root
    #define lp_align(n)                PRAGMA(data_alignment=n)
    #define lp_weak                    __weak
    #define lp_inline                  static inline
	
#elif defined(_WIN32) || defined(_WIN64) 
	#define lp_section(x)              __declspec((section(x)))
    #define lp_used                    __declspec((used))
    #define lp_align(n)                __declspec(align(n))
    #define lp_weak                    __declspec(weak)
    #define lp_inline                  static __inline
#else
    #error "The current compiler is not supported. "
#endif




#ifdef __cplusplus
}
#endif

#endif




















