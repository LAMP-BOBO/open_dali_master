

#ifndef _LP_DEFINE_IO_H
#define _LP_DEFINE_IO_H


#ifdef __cplusplus
extern "C" {
#endif

/**
* @brief gpio sys register
*/
typedef enum
{
	LP_GPIO_LED_R_E = 0x00,
	LP_GPIO_DALI_RE_E,
	LP_GPIO_DALI_TR_E,
	LP_GPIO_K1_E,
	LP_GPIO_K2_E,
		
} lp_gpio_register_e;


/**
* @brief sys serial sys register
*/
typedef enum
{
	LP_UART1_E = 0x00,
	
	LP_UART_MAX_E,	
} lp_serial_register_e;


/**
* @brief sys dali sys register
*/
typedef enum
{
	LP_DALI_BUS0_E = 0x00,
	
	LP_DALI_BUS_MAX_E,	
} lp_dali_register_e;

/**
* @brief sys hwtimer sys register
*/
typedef enum
{
	LP_HWTIMER14_E = 0x00,
	LP_HWTIMER15_E,
	LP_HWTIMER5_E,
	LP_HWTIMER12_E,
	
	LP_HWTIMER_MAX_E,	
} lp_hwtimer_register_e;


/**
* @brief sys exint register
*/
typedef enum
{
	LP_EXINT0_E = 0x00,
	LP_EXINT1_E,
	LP_EXINT15_E,
	
	LP_EXINT_MAX_E,	
} lp_exint_register_e;



/* KEY ID, mian use bsp_KeyState()  Parameter*/
typedef enum
{
	LP_KID_K0 = 0,
	LP_KID_K1,
	LP_KID_K2,
	LP_KID_WK_UP,
	
}lp_key_id_e;


#ifdef __cplusplus
}
#endif
#endif




























