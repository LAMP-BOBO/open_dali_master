#ifndef _LP_CONFIG_H
#define _LP_CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "stdbool.h"
#include "stdlib.h"

/*lp sys include*/
#include "config/lp_projdefs.h"
#include "config/lp_portmacro.h"
#include "config/lp_define_io.h"

/*lp common*/
#include "bit/bitfield.h"

//2023.04.23
//2023.05.15
#define LP_SW_VERSION    "0.1.1"

#ifndef NULL
#define NULL (void *)0
#endif

#define LP_USING_ASSERT
#define LP_USING_SYS_MEM

#define LP_USING_QUEUE
#define LP_QUEUE_USING_CREATE

#define LP_USING_TIMER
#define LP_TIMER_USING_TIMEOUT_CALLBACK
#define LP_TIMER_USING_CREATE

#ifdef 	LP_USING_SYS_MEM
#include "mem/lp_sys_mem.h"
#define LP_SYS_MALLOC   lp_sys_men_malloc
#define LP_SYS_FREE     lp_sys_men_free
#else
#define LP_SYS_MALLOC   malloc
#define LP_SYS_FREE     free
#endif


#ifdef LP_QUEUE_USING_CREATE
#include "queue/lp_sys_queue.h"
#endif
#ifdef LP_USING_TIMER
#include "soft_timer/lp_sys_sftimer.h"
#endif

#if defined(_WIN32) || defined(_WIN64)

#define LOG_OUT(...)  printf(__VA_ARGS__)
#define LP_DEBUG(...) printf(__VA_ARGS__)
 
#elif defined(__ARMCC_VERSION)    
#define LP_DEBUG 		LOG_OUT

#define LP_RTOS			   1
#define CONFIG_NOOS 	   0
#define CONFIG_SUPPORT_HAL 0

#define LP_FREE_RTOS	    0
#define LP_CMSIS_OS	   		1

#if (LP_FREE_RTOS)&&(LP_RTOS)
#include "FreeRTOS.h"
#endif

#if (LP_CMSIS_OS)&&(LP_RTOS)
//ARM 提供的通用 CMSIS-OS 封装层
#include "cmsis_os2.h"
#endif

//#define CHIP_ST_F0
//#define CHIP_ST_F1
//#define CHIP_ST_F4
//#define CHIP_AT_F4
#define CHIP_ST_H7

#ifdef  CHIP_ST_F0
#include "stm32f0xx_hal.h"
#endif

#ifdef  CHIP_ST_F1
#include "stm32f1xx_hal.h"
#endif

#ifdef  CHIP_ST_F4
#include "stm32f4xx_hal.h"
#endif

#ifdef  CHIP_ST_H7
#include "stm32h7xx_hal.h"
#define STM32H750_BOARD

#endif

#ifdef  CHIP_AT_F4
#include "at32f421.h"
#define AT32F421_BOARD
#endif

#include "logger/lp_log_system.h"


#endif


/* LP_USING_ASSERT */
#ifdef LP_USING_ASSERT

#define LP_ASSERT(EXPR)                                                   \
    if (!(EXPR))                                                          \
    {                                                                     \
        LP_DEBUG("(%s) has assert failed at %s.\n", #EXPR, __FUNCTION__); \
        while (1)                                                         \
            ;                                                             \
    }
#else
#define LP_ASSERT(EXPR) (void)EXPR
#endif
/* LP_USING_ASSERT */
	
	

#ifdef __cplusplus
}
#endif

#endif /* __CONFIG_H */

