

#ifndef _LP_PROJDEFS_H
#define _LP_PROJDEFS_H

//#pragma once
#ifdef __cplusplus
extern "C" {
#endif


typedef void (*lp_TaskFunction_t)(void*);

/* Definitions for error constants. */
#define LP_OK          0       /*!< esp_err_t value indicating success (no error) */
#define LP_FAIL        -1      /*!< Generic esp_err_t code indicating failure */

#define LP_ERR_NO_MEM              0x101   /*!< Out of memory */
#define LP_ERR_INVALID_ARG         0x102   /*!< Invalid argument */
#define LP_ERR_INVALID_STATE       0x103   /*!< Invalid state */
#define LP_ERR_INVALID_SIZE        0x104   /*!< Invalid size */
#define LP_ERR_NOT_FOUND           0x105   /*!< Requested resource not found */
#define LP_ERR_NOT_SUPPORTED       0x106   /*!< Operation or feature not supported */
#define LP_ERR_TIMEOUT             0x107   /*!< Operation timed out */
#define LP_ERR_INVALID_RLPONSE     0x108   /*!< Received response was invalid */
#define LP_ERR_INVALID_CRC         0x109   /*!< CRC or checksum was invalid */
#define LP_ERR_INVALID_VERSION     0x10A   /*!< Version was invalid */
#define LP_ERR_INVALID_MAC         0x10B   /*!< MAC address was invalid */
#define LP_ERR_NOT_FINISHED        0x10C   /*!< There are items remained to retrieve */


#define LP_ERR_WIFI_BASE           0x3000  /*!< Starting number of WiFi error codes */
#define LP_ERR_MESH_BASE           0x4000  /*!< Starting number of MESH error codes */
#define LP_ERR_FLASH_BASE          0x6000  /*!< Starting number of flash error codes */
#define LP_ERR_HW_CRYPTO_BASE      0xc000  /*!< Starting number of HW cryptography module error codes */
#define LP_ERR_MEMPROT_BASE        0xd000  /*!< Starting number of Memory Protection API error codes */

#ifdef __cplusplus
}
#endif

#endif



























