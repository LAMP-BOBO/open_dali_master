/**
  **************************************************************************
  * @file     flash.c
  * @version  v2.0.0
  * @date     2020-11-02
  * @brief    the driver library of the flash
  **************************************************************************
  *                       Copyright notice & Disclaimer
  *
  * The software Board Support Package (BSP) that is made available to 
  * download from Artery official website is the copyrighted work of Artery. 
  * Artery authorizes customers to use, copy, and distribute the BSP 
  * software and its related documentation for the purpose of design and 
  * development in conjunction with Artery microcontrollers. Use of the 
  * software is governed by this copyright notice and the following disclaimer.
  *
  * THIS SOFTWARE IS PROVIDED ON "AS IS" BASIS WITHOUT WARRANTIES,
  * GUARANTEES OR REPRESENTATIONS OF ANY KIND. ARTERY EXPRESSLY DISCLAIMS,
  * TO THE FULLEST EXTENT PERMITTED BY LAW, ALL EXPRESS, IMPLIED OR
  * STATUTORY OR OTHER WARRANTIES, GUARANTEES OR REPRESENTATIONS,
  * INCLUDING BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
  *
  **************************************************************************
  */

#include "flash.h"
#include <stdio.h>

/* at32 flash size infromation */
#define FLASH_SIZE                        (*(uint16_t *)0x1FFFF7E0)  

/* flash data buffer */
uint16_t flash_buf[FLASH_SECTOR_SIZE / 2];

/**
  * @brief  flash read halfword.
  * @param  address: flash address.
  * @retval the data of assign address
  */
uint16_t flash_half_word_read(uint32_t address)
{
  return (*(__IO uint16_t*)address); 
}

/**
  * @brief  write single sector data.
  * @param  address: address start.
  * @param  pdata: data point.
  * @param  number: halfword data count.
  * @retval none
  */
void flash_sector_write(uint32_t address, uint16_t *pdata, uint32_t number)   
{
  uint32_t start_addres;
  uint32_t offset_addres;
  uint32_t erase_num, i;
  
  /* calculate sector start addres */
  start_addres = address / FLASH_SECTOR_SIZE * FLASH_SECTOR_SIZE;
  
  /* calculate sector offset addres */
  offset_addres = (address % FLASH_SECTOR_SIZE) / 2;
  
  /* read sector data */
  flash_read(start_addres, flash_buf, FLASH_SECTOR_SIZE / 2);
  
  /* check whether the area to be progammed is erased */
  for (erase_num = 0; erase_num < number; erase_num++)
  {
    if (flash_buf[offset_addres + erase_num] != 0xFFFF)
    {
      break;
    }
  }
  
  /* the area to be progammed is not erased */
  if (erase_num != number)
  {
    /* copy the data to be programmed to the buffer */
    for (i = 0; i < number; i++)
    {
      flash_buf[offset_addres + i] = pdata[i];
    }    
    
    /* erase sector */
    flash_sector_erase(start_addres);
    
    /* program the entire sector data */
    for (i = 0; i < (FLASH_SECTOR_SIZE / 2); i++)
    {
      /* program data */
      flash_halfword_program(start_addres, flash_buf[i]);

      /* address add 2 */
      start_addres += 2;
    } 
  }
  /* the area to be progammed is erased */
  else
  {
    /* no need to erase sectors, directly program data */
    for (i = 0; i < number; i++)
    {
      /* program data */
      flash_halfword_program(address, pdata[i]);

      /* address add 2 */
      address += 2;
    }   
  }
} 

/**
  * @brief  flash write function.
  * @param  address: address start.
  * @param  pdata: data point.
  * @param  number: halfword data count.
  * @retval none
  */
void flash_write(uint32_t address, uint16_t *pdata, uint32_t number)  
{
  uint32_t sector_number = 0;
  uint32_t sector_last = 0;
  uint32_t single_write = 0;  
  uint32_t write_address = 0;  
  
  /* address 2-byte alignment */  
  address = address >> 1;
  address = address << 1;
  
  /* illegal address direct return */  
  if (address < (FLASH_BASE + FLASH_CODE_SIZE) || (address >= (FLASH_BASE + 1024 * FLASH_SIZE)))
  {
    return ;  
  }
    
  /* unlock the flash controller */
  flash_unlock();

  /* calculate the number of data programred for the first time, and the subsequent program address will be aligned */
  single_write = (address / FLASH_SECTOR_SIZE * FLASH_SECTOR_SIZE + FLASH_SECTOR_SIZE - address) / 2;
  
  /* first program address */
  write_address = address;
  
  if (number < single_write)
  {
    single_write = number;
  }
  
  /* program data */ 
  flash_sector_write(write_address, pdata, single_write);  
  
  /* subtract the number of data programed for the first time */
  number -= single_write;
  
  /* address increase */  
  write_address += (single_write * 2);

  /* pointer increase */  
  pdata += single_write;
  
  /* if the remaining quantity is 0, it means the program is complete */
  if (number == 0)
  {
    return ;
  }

  /* calculate the number of bytes in less than one sector */
  sector_last = number % (FLASH_SECTOR_SIZE / 2);  
    
  /* calculate the number of full sectors */
  sector_number = number / (FLASH_SECTOR_SIZE / 2);    
 
  /* program an integer number of sectors of data */
  while(sector_number > 0)
  {
    /* program data */
    flash_sector_write(write_address, pdata, (FLASH_SECTOR_SIZE / 2));  
    
    /* sector number subtract*/  
    sector_number--;
    
    /* address increase */  
    write_address += FLASH_SECTOR_SIZE;

    /* pointer increase */  
    pdata += (FLASH_SECTOR_SIZE / 2);
  }
  
  /* programring less than one sector of data */
  if (sector_last)
  {
    /* program data */
    flash_sector_write(write_address, pdata, sector_last);  
  }
  
  /* lock the flash controller */
  flash_lock();
}

/**
  * @brief  flash read function.
  * @param  address: address start(address must be even).
  * @param  pdata: data point.
  * @param  number: halfword data count.
  * @retval none
  */
void flash_read(uint32_t address, uint16_t *pdata, uint32_t number)     
{
  uint32_t i;
  
  /* address 2-byte alignment */  
  address = address >> 1;
  address = address << 1;
  
  for (i = 0; i < number; i++)
  {
    /* read data */
    pdata[i] = flash_half_word_read(address);

    /* address add 2 */
    address += 2;  
  }
}

