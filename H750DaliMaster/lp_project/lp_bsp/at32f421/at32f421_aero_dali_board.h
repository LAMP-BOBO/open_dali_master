

#ifndef __AT32F421_AERO_DALI_BOARD_H
#define __AT32F421_AERO_DALI_BOARD_H

#ifdef __cplusplus
extern "C" {
#endif

#include "stdio.h"
#include "at32f421.h"
#include "config/lp_config.h"

#if !defined (AT_AERO_DALI_F421_V1)
#error "please select first the board at-start device used in your application (in at32f421_board.h file)"
#endif

/******************** define led ********************/
typedef enum
{
  LED1                                   = 0,

} led_type;

#define LED_NUM                          1

#if defined (AT_AERO_DALI_F421_V1)

#define LED1_PIN                         GPIO_PINS_4
#define LED1_GPIO                        GPIOA
#define LED1_GPIO_CRM_CLK                CRM_GPIOA_PERIPH_CLOCK

#define DALI_RE_PIN                      GPIO_PINS_0
#define DALI_RE_GPIO                     GPIOA
#define DALI_RE_GPIO_CRM_CLK             CRM_GPIOA_PERIPH_CLOCK

#define DALI_TR_PIN                      GPIO_PINS_1
#define DALI_TR_GPIO                     GPIOA
#define DALI_TR_GPIO_CRM_CLK             CRM_GPIOA_PERIPH_CLOCK

#endif


#define LOG_UART		0

#if LOG_UART
/**************** define print uart ******************/
#define PRINT_UART                       USART1
#define PRINT_UART_CRM_CLK               CRM_USART1_PERIPH_CLOCK
#define PRINT_UART_TX_PIN                GPIO_PINS_9
#define PRINT_UART_TX_GPIO               GPIOA
#define PRINT_UART_TX_GPIO_CRM_CLK       CRM_GPIOA_PERIPH_CLOCK
#define PRINT_UART_TX_PIN_SOURCE         GPIO_PINS_SOURCE9
#define PRINT_UART_TX_PIN_MUX_NUM        GPIO_MUX_1

/* printf uart init function */
void uart_print_init(uint32_t baudrate);
#endif


#if 0
/******************* define button *******************/
typedef enum
{
  USER_BUTTON                            = 0,
  NO_BUTTON                              = 1
} button_type;

#define USER_BUTTON_PIN                  GPIO_PINS_0
#define USER_BUTTON_PORT                 GPIOA
#define USER_BUTTON_CRM_CLK              CRM_GPIOA_PERIPH_CLOCK

/* button operation function */
void at32_button_init(void);
button_type at32_button_press(void);
uint8_t at32_button_state(void);
#endif


/**
  * @}
  */

/** @defgroup BOARD_exported_functions
  * @{ A key task is to get pupils to perceive for themselves the relationship between success and effort.
  */

/******************** functions ********************/
void at32_board_init(void);

/* led operation function */
void at32_led_init(led_type led);
void at32_led_on(led_type led);
void at32_led_off(led_type led);
void at32_led_toggle(led_type led);


/* delay function */
void delay_init(void);
void delay_us(uint32_t nus);
void delay_ms(uint16_t nms);
void delay_sec(uint16_t sec);

void at_get_ic_uuid(lp_u8_t *data);

void at_tim14_init(lp_u32_t period,lp_u32_t arr);
void at_tim14_enable(void);
void at_tim14_disable(void);
lp_u32_t at_tim14_counter_get(void);
void at_tim14_counter_set(lp_u32_t counter);

void at_tim15_init(lp_u32_t period,lp_u32_t arr);
void at_tim15_enable(void);
void at_tim15_disable(void);
lp_u32_t at_tim15_counter_get(void);
void at_tim15_counter_set(lp_u32_t counter);

void at_pa0_exint_init(void);

void at_dali_re_init(void);
lp_u8_t at_dali_re_read(void);
void at_dali_tr_init(void);
void at_dali_tr_high(void);
void at_dali_tr_low(void);




/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */




#ifdef __cplusplus
}
#endif

#endif
















