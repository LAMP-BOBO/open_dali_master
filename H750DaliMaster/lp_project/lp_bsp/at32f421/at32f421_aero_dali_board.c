

#include "at32f421_aero_dali_board.h"



/** @addtogroup AT32F421_board
  * @{
  */

/** @defgroup BOARD
  * @brief onboard periph driver
  * @{
  */

/* delay macros */
#define STEP_DELAY_MS                    50

/* at-start led resouce array */
gpio_type *led_gpio_port[LED_NUM]        = {LED1_GPIO};
uint16_t led_gpio_pin[LED_NUM]           = {LED1_PIN};
crm_periph_clock_type led_gpio_crm_clk[LED_NUM] = {LED1_GPIO_CRM_CLK};

/* delay variable */
static volatile uint32_t fac_us;
static volatile uint32_t fac_ms;


#if LOG_UART
/* support printf function, usemicrolib is unnecessary */
#if (__ARMCC_VERSION > 6000000)
  __asm (".global __use_no_semihosting\n\t");
  void _sys_exit(int x)
  {
    x = x;
  }
  /* __use_no_semihosting was requested, but _ttywrch was */
  void _ttywrch(int ch)
  {
    ch = ch;
  }
  FILE __stdout;
#else
 #ifdef __CC_ARM
  #pragma import(__use_no_semihosting)
  struct __FILE
  {
    int handle;
  };
  FILE __stdout;
  void _sys_exit(int x)
  {
    x = x;
  }
  /* __use_no_semihosting was requested, but _ttywrch was */
  void _ttywrch(int ch)
  {
    ch = ch;
  }
 #endif
#endif

#if defined (__GNUC__) && !defined (__clang__)
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif

/**
  * @brief  retargets the c library printf function to the usart.
  * @param  none
  * @retval none
  */
PUTCHAR_PROTOTYPE
{
  while(usart_flag_get(PRINT_UART, USART_TDBE_FLAG) == RESET);
  usart_data_transmit(PRINT_UART, ch);
  return ch;
}

#if defined (__GNUC__) && !defined (__clang__)
int _write(int fd, char *pbuffer, int size)
{
  for(int i = 0; i < size; i ++)
  {
    __io_putchar(*pbuffer++);
  }

  return size;
}
#endif



/**
  * @brief  initialize uart
  * @param  baudrate: uart baudrate
  * @retval none
  */
void uart_print_init(uint32_t baudrate)
{
  gpio_init_type gpio_init_struct;

#if defined (__GNUC__) && !defined (__clang__)
  setvbuf(stdout, NULL, _IONBF, 0);
#endif

  /* enable the uart and gpio clock */
  crm_periph_clock_enable(PRINT_UART_CRM_CLK, TRUE);
  crm_periph_clock_enable(PRINT_UART_TX_GPIO_CRM_CLK, TRUE);

  gpio_default_para_init(&gpio_init_struct);

  /* configure the uart tx pin */
  gpio_init_struct.gpio_drive_strength = GPIO_DRIVE_STRENGTH_STRONGER;
  gpio_init_struct.gpio_out_type  = GPIO_OUTPUT_PUSH_PULL;
  gpio_init_struct.gpio_mode = GPIO_MODE_MUX;
  gpio_init_struct.gpio_pins = PRINT_UART_TX_PIN;
  gpio_init_struct.gpio_pull = GPIO_PULL_NONE;
  gpio_init(PRINT_UART_TX_GPIO, &gpio_init_struct);

  gpio_pin_mux_config(PRINT_UART_TX_GPIO, PRINT_UART_TX_PIN_SOURCE, PRINT_UART_TX_PIN_MUX_NUM);

  /* configure uart param */
  usart_init(PRINT_UART, baudrate, USART_DATA_8BITS, USART_STOP_1_BIT);
  usart_transmitter_enable(PRINT_UART, TRUE);
  usart_enable(PRINT_UART, TRUE);
}
#endif
/**
  * @brief  board initialize interface init led and button
  * @param  none
  * @retval none
  */
void at32_board_init()
{
	nvic_priority_group_config(NVIC_PRIORITY_GROUP_4);
	/* initialize delay function */
	delay_init();
	void at_sys_tick_init(void);
	at_sys_tick_init();

	/* configure led in at_start_board */
	at32_led_init(LED1);
	
}

#if 0
/**
  * @brief  configure button gpio
  * @param  button: specifies the button to be configured.
  * @retval none
  */
void at32_button_init(void)
{
  gpio_init_type gpio_init_struct;

  /* enable the button clock */
  crm_periph_clock_enable(USER_BUTTON_CRM_CLK, TRUE);

  /* set default parameter */
  gpio_default_para_init(&gpio_init_struct);

  /* configure button pin as input with pull-up/pull-down */
  gpio_init_struct.gpio_drive_strength = GPIO_DRIVE_STRENGTH_STRONGER;
  gpio_init_struct.gpio_out_type  = GPIO_OUTPUT_PUSH_PULL;
  gpio_init_struct.gpio_mode = GPIO_MODE_INPUT;
  gpio_init_struct.gpio_pins = USER_BUTTON_PIN;
  gpio_init_struct.gpio_pull = GPIO_PULL_DOWN;
  gpio_init(USER_BUTTON_PORT, &gpio_init_struct);
}

/**
  * @brief  returns the selected button state
  * @param  none
  * @retval the button gpio pin value
  */
uint8_t at32_button_state(void)
{
  return gpio_input_data_bit_read(USER_BUTTON_PORT, USER_BUTTON_PIN);
}

/**
  * @brief  returns which button have press down
  * @param  none
  * @retval the button have press down
  */
button_type at32_button_press()
{
  static uint8_t pressed = 1;
  /* get button state in at_start board */
  if((pressed == 1) && (at32_button_state() != RESET))
  {
    /* debounce */
    pressed = 0;
    delay_ms(10);
    if(at32_button_state() != RESET)
      return USER_BUTTON;
  }
  else if(at32_button_state() == RESET)
  {
    pressed = 1;
  }
  return NO_BUTTON;
}

#endif

/**
  * @brief  configure led gpio
  * @param  led: specifies the led to be configured.
  * @retval none
  */
void at32_led_init(led_type led)
{
  gpio_init_type gpio_init_struct;

  /* enable the led clock */
  crm_periph_clock_enable(led_gpio_crm_clk[led], TRUE);

  /* set default parameter */
  gpio_default_para_init(&gpio_init_struct);

  /* configure the led gpio */
  gpio_init_struct.gpio_drive_strength = GPIO_DRIVE_STRENGTH_STRONGER;
  gpio_init_struct.gpio_out_type  = GPIO_OUTPUT_PUSH_PULL;
  gpio_init_struct.gpio_mode = GPIO_MODE_OUTPUT;
  gpio_init_struct.gpio_pins = led_gpio_pin[led];
  gpio_init_struct.gpio_pull = GPIO_PULL_NONE;
  gpio_init(led_gpio_port[led], &gpio_init_struct);
}

/**
  * @brief  turns selected led on.
  * @param  led: specifies the led to be set on.
  *   this parameter can be one of following parameters:
  *     @arg LED2
  *     @arg LED3
  *     @arg LED4
  * @retval none
  */
void at32_led_on(led_type led)
{
  if(led > (LED_NUM - 1))
    return;
  if(led_gpio_pin[led])
    led_gpio_port[led]->clr = led_gpio_pin[led];
}

/**
  * @brief  turns selected led off.
  * @param  led: specifies the led to be set off.
  *   this parameter can be one of following parameters:
  *     @arg LED2
  *     @arg LED3
  *     @arg LED4
  * @retval none
  */
void at32_led_off(led_type led)
{
  if(led > (LED_NUM - 1))
    return;
  if(led_gpio_pin[led])
    led_gpio_port[led]->scr = led_gpio_pin[led];
}

/**
  * @brief  turns selected led toggle.
  * @param  led: specifies the led to be set off.
  *   this parameter can be one of following parameters:
  *     @arg LED2
  *     @arg LED3
  *     @arg LED4
  * @retval none
  */
void at32_led_toggle(led_type led)
{
  if(led > (LED_NUM - 1))
    return;
  if(led_gpio_pin[led])
    led_gpio_port[led]->odt ^= led_gpio_pin[led];
}

/**
  * @brief  initialize delay function
  * @param  none
  * @retval none
  */
void delay_init()
{
  /* configure systick */
  systick_clock_source_config(SYSTICK_CLOCK_SOURCE_AHBCLK_NODIV);
  fac_us = system_core_clock / (1000000U);
  fac_ms = fac_us * (1000U);
}

/**
  * @brief  inserts a delay time.
  * @param  nus: specifies the delay time length, in microsecond.
  * @retval none
  */
void delay_us(uint32_t nus)
{
  uint32_t temp = 0;
  SysTick->LOAD = (uint32_t)(nus * fac_us);
  SysTick->VAL = 0x00;
  SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk ;
  do
  {
    temp = SysTick->CTRL;
  }while((temp & 0x01) && !(temp & (1 << 16)));

  SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
  SysTick->VAL = 0x00;
}

/**
  * @brief  inserts a delay time.
  * @param  nms: specifies the delay time length, in milliseconds.
  * @retval none
  */
void delay_ms(uint16_t nms)
{
  uint32_t temp = 0;
  while(nms)
  {
    if(nms > STEP_DELAY_MS)
    {
      SysTick->LOAD = (uint32_t)(STEP_DELAY_MS * fac_ms);
      nms -= STEP_DELAY_MS;
    }
    else
    {
      SysTick->LOAD = (uint32_t)(nms * fac_ms);
      nms = 0;
    }
    SysTick->VAL = 0x00;
    SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
    do
    {
      temp = SysTick->CTRL;
    }while((temp & 0x01) && !(temp & (1 << 16)));

    SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
    SysTick->VAL = 0x00;
  }
}

/**
  * @brief  inserts a delay time.
  * @param  sec: specifies the delay time, in seconds.
  * @retval none
  */
void delay_sec(uint16_t sec)
{
  uint16_t index;
  for(index = 0; index < sec; index++)
  {
    delay_ms(500);
    delay_ms(500);
  }
}

void at_sys_tick_init(void)
{
  crm_clocks_freq_type crm_clocks_freq_struct = {0};
	/* get system clock */
  crm_clocks_freq_get(&crm_clocks_freq_struct);
  /* enable tmr1 clock */
  crm_periph_clock_enable(CRM_TMR6_PERIPH_CLOCK, TRUE);

  /* tmr1 configuration */
  /* time base configuration */
  /* systemclock/120/1000 = 1ms */
  tmr_base_init(TMR6, 1000-1, (crm_clocks_freq_struct.apb1_freq /1000000) - 1);
  tmr_cnt_dir_set(TMR6, TMR_COUNT_UP);

  /* overflow interrupt enable */
  tmr_interrupt_enable(TMR6, TMR_OVF_INT, TRUE);

  /* tmr6 overflow interrupt nvic init */
 /*!< 4 bits for preemption priority, 0 bits for subpriority */
  //nvic_priority_group_config(NVIC_PRIORITY_GROUP_4);
  nvic_irq_enable(TMR6_GLOBAL_IRQn,15, 0);

  /* enable tmr6 */
  tmr_counter_enable(TMR6, TRUE);
}

void at_tim14_init(lp_u32_t period,lp_u32_t arr)
{
  crm_clocks_freq_type crm_clocks_freq_struct = {0};
	/*get system clock */
  crm_clocks_freq_get(&crm_clocks_freq_struct);
  /* enable tmr14 clock */
  crm_periph_clock_enable(CRM_TMR14_PERIPH_CLOCK, TRUE);

  /* tmr1 configuration */
  /* time base configuration */
  /* systemclock/120/416 = 416us */
  tmr_base_init(TMR14, arr-1, (crm_clocks_freq_struct.apb1_freq /period) - 1);
  tmr_cnt_dir_set(TMR14, TMR_COUNT_UP);

  /* overflow interrupt enable */
  tmr_interrupt_enable(TMR14, TMR_OVF_INT, TRUE);

  /* tmr14 overflow interrupt nvic init */
  /*!< 4 bits for preemption priority, 0 bits for subpriority */
  //nvic_priority_group_config(NVIC_PRIORITY_GROUP_4);
  //nvic_irq_enable(TMR14_GLOBAL_IRQn,15, 0);

  tmr_counter_enable(TMR14, FALSE);
}

void at_tim14_enable(void)
{
	tmr_counter_enable(TMR14, TRUE);
}

void at_tim14_disable(void)
{
	tmr_counter_enable(TMR14, FALSE);
}

void at_tim14_counter_set(lp_u32_t counter)
{
	tmr_counter_value_set(TMR14,counter); 
}

lp_u32_t at_tim14_counter_get(void)
{
	return tmr_counter_value_get(TMR14); 
}

void at_tim15_init(lp_u32_t period,lp_u32_t arr)
{
  crm_clocks_freq_type crm_clocks_freq_struct = {0};
	/*get system clock */
  crm_clocks_freq_get(&crm_clocks_freq_struct);
  /* enable tmr15 clock */
  crm_periph_clock_enable(CRM_TMR15_PERIPH_CLOCK, TRUE);

  /* tmr1 configuration */
  /* time base configuration */
  /* systemclock/120/416 = 416us */
  tmr_base_init(TMR15, arr-1, (crm_clocks_freq_struct.apb2_freq /period) - 1);
  //LOG_OUT("APB2:%d %ld\r\n",arr-1,(crm_clocks_freq_struct.apb2_freq /period) - 1);
  tmr_cnt_dir_set(TMR15, TMR_COUNT_UP);

  /* overflow interrupt enable */
  tmr_interrupt_enable(TMR15, TMR_OVF_INT, TRUE);

  /* tmr14 overflow interrupt nvic init */
  /*!< 4 bits for preemption priority, 0 bits for subpriority */
  //nvic_priority_group_config(NVIC_PRIORITY_GROUP_4);
  nvic_irq_enable(TMR15_GLOBAL_IRQn,12, 0);

  tmr_counter_enable(TMR15, FALSE);
}

void at_tim15_enable(void)
{
	tmr_counter_enable(TMR15, TRUE);
}

void at_tim15_disable(void)
{
	tmr_counter_enable(TMR15, FALSE);
}

void at_tim15_counter_set(lp_u32_t counter)
{
	tmr_counter_value_set(TMR15,counter); 
}

lp_u32_t at_tim15_counter_get(void)
{
	return tmr_counter_value_get(TMR15); 
}

/* at32 mcu pid/uid base address */
#define UUID_ID_ADDR  0x1FFFF7E8
#define PID_ID_ADDR   0xE0042000

/*96bit = 12 byte*/
void at_get_ic_uuid(lp_u8_t *data)
{
	lp_u8_t *p_uuid = (lp_u8_t*)UUID_ID_ADDR;
	
	memcpy(data,p_uuid,12);
}

/**
  * @brief  configure dali re exint
  * @param  none
  * @retval none
  */
void at_pa0_exint_init(void)
{
  exint_init_type exint_init_struct;

  crm_periph_clock_enable(CRM_GPIOA_PERIPH_CLOCK, TRUE);
  crm_periph_clock_enable(CRM_SCFG_PERIPH_CLOCK, TRUE);

  scfg_exint_line_config(SCFG_PORT_SOURCE_GPIOA, SCFG_PINS_SOURCE0);

  exint_default_para_init(&exint_init_struct);
  exint_init_struct.line_enable = TRUE;
  exint_init_struct.line_mode = EXINT_LINE_INTERRUPUT;
  exint_init_struct.line_select = EXINT_LINE_0;
  exint_init_struct.line_polarity = EXINT_TRIGGER_BOTH_EDGE;
  exint_init(&exint_init_struct);

  //nvic_priority_group_config(NVIC_PRIORITY_GROUP_4);
  nvic_irq_enable(EXINT1_0_IRQn,5, 0);
}

//void at_pa0_exint_enable(void)
//{
//	nvic_irq_enable(EXINT1_0_IRQn,9, 0);
//}

//void at_pa0_exint_disable(void)
//{
//	nvic_irq_disable(EXINT1_0_IRQn);
//}

void at_dali_re_init(void)
{
  gpio_init_type gpio_init_struct;

  /* enable clock */
  crm_periph_clock_enable(DALI_RE_GPIO_CRM_CLK, TRUE);

  /* set default parameter */
  gpio_default_para_init(&gpio_init_struct);

  /* configure  gpio */
  gpio_init_struct.gpio_drive_strength = GPIO_DRIVE_STRENGTH_STRONGER;
  gpio_init_struct.gpio_mode = GPIO_MODE_INPUT;
  gpio_init_struct.gpio_pins = DALI_RE_PIN;
  gpio_init_struct.gpio_pull = GPIO_PULL_NONE;
  gpio_init(DALI_RE_GPIO, &gpio_init_struct);
}

lp_u8_t at_dali_re_read(void)
{
	 if(gpio_input_data_bit_read(DALI_RE_GPIO,DALI_RE_PIN) == RESET)
	 {
		return 0;
	 }
	 else
	 {
		return 1;
	 }
}

void at_dali_tr_init(void)
{
  gpio_init_type gpio_init_struct;

  /* enable clock */
  crm_periph_clock_enable(DALI_TR_GPIO_CRM_CLK, TRUE);

  /* set default parameter */
  gpio_default_para_init(&gpio_init_struct);

  /* configure  gpio */
  gpio_init_struct.gpio_drive_strength = GPIO_DRIVE_STRENGTH_STRONGER;
  gpio_init_struct.gpio_out_type = GPIO_OUTPUT_PUSH_PULL;
  gpio_init_struct.gpio_mode = GPIO_MODE_OUTPUT;
  gpio_init_struct.gpio_pins = DALI_TR_PIN;
  gpio_init_struct.gpio_pull = GPIO_PULL_NONE;
  gpio_init(DALI_TR_GPIO, &gpio_init_struct);
}

void at_dali_tr_high(void)
{
	gpio_bits_set(DALI_TR_GPIO,DALI_TR_PIN);
}

void at_dali_tr_low(void)
{
	gpio_bits_reset(DALI_TR_GPIO,DALI_TR_PIN);
}



/**
  * @}
  */

/**
  * @}
  */






























