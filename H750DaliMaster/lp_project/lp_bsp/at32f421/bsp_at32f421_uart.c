

#include "bsp_at32f421_uart.h"




uint8_t usart1_tx_buffer[USART1_BUFFER_SIZE] ={0};
uint8_t usart1_rx_buffer[USART1_BUFFER_SIZE] ={0};

volatile uint8_t usart1_tx_dma_status = 0;
volatile uint8_t usart1_rx_dma_status = 0;
/**
  * @brief  config usart1 dma mode
  * @param  none
  * @retval none
  */
void usart1_configuration(lp_u32_t baud,usart_data_bit_num_type data_bit, usart_stop_bit_num_type stop_bit)
{
  gpio_init_type gpio_init_struct;

  /* enable the usart1 and gpio clock */
  crm_periph_clock_enable(CRM_USART1_PERIPH_CLOCK, TRUE);
  crm_periph_clock_enable(CRM_GPIOA_PERIPH_CLOCK, TRUE);

  gpio_default_para_init(&gpio_init_struct);

  /* configure the usart1 tx/rx pin */
  gpio_init_struct.gpio_drive_strength = GPIO_DRIVE_STRENGTH_STRONGER;
  gpio_init_struct.gpio_out_type = GPIO_OUTPUT_PUSH_PULL;
  gpio_init_struct.gpio_mode = GPIO_MODE_MUX;
  gpio_init_struct.gpio_pins = GPIO_PINS_9 | GPIO_PINS_10;
  gpio_init_struct.gpio_pull = GPIO_PULL_NONE;
  gpio_init(GPIOA, &gpio_init_struct);

  /* config usart1 iomux */
  gpio_pin_mux_config(GPIOA, GPIO_PINS_SOURCE9, GPIO_MUX_1);
  gpio_pin_mux_config(GPIOA, GPIO_PINS_SOURCE10, GPIO_MUX_1);

  /* config usart nvic interrupt */
  nvic_irq_enable(USART1_IRQn,9, 0);
	
  /* configure usart1 param */
  usart_init(USART1, baud, data_bit, stop_bit);
  usart_transmitter_enable(USART1, TRUE);
  usart_receiver_enable(USART1, TRUE);
  usart_dma_transmitter_enable(USART1, TRUE);
  usart_dma_receiver_enable(USART1, TRUE);
  usart_interrupt_enable(USART1, USART_IDLE_INT, TRUE);
  usart_enable(USART1, TRUE);
}

/**
  * @brief  config dma for usart2 and usart1
  * @param  none
  * @retval none
  */
void uart1_dma_configuration(void)
{
  dma_init_type dma_init_struct;

  /* enable dma1 clock */
  crm_periph_clock_enable(CRM_DMA1_PERIPH_CLOCK, TRUE);

  /* dma1 channel2 for usart1 tx configuration */
  dma_reset(DMA1_CHANNEL2);
  dma_default_para_init(&dma_init_struct);
  dma_init_struct.buffer_size = USART1_BUFFER_SIZE;
  dma_init_struct.direction = DMA_DIR_MEMORY_TO_PERIPHERAL;
  dma_init_struct.memory_base_addr = (uint32_t)usart1_tx_buffer;
  dma_init_struct.memory_data_width = DMA_MEMORY_DATA_WIDTH_BYTE;
  dma_init_struct.memory_inc_enable = TRUE;
  dma_init_struct.peripheral_base_addr = (uint32_t)&USART1->dt;
  dma_init_struct.peripheral_data_width = DMA_PERIPHERAL_DATA_WIDTH_BYTE;
  dma_init_struct.peripheral_inc_enable = FALSE;
  dma_init_struct.priority = DMA_PRIORITY_MEDIUM;
  dma_init_struct.loop_mode_enable = FALSE;
  dma_init(DMA1_CHANNEL2, &dma_init_struct);

  /* enable transfer full data intterrupt */
  dma_interrupt_enable(DMA1_CHANNEL2, DMA_FDT_INT, TRUE);

  /* dma1 channel2 interrupt nvic init */
  nvic_irq_enable(DMA1_Channel3_2_IRQn, 10, 0);

  /* dma1 channel3 for usart1 rx configuration */
  dma_reset(DMA1_CHANNEL3);
  dma_default_para_init(&dma_init_struct);
  dma_init_struct.buffer_size = USART1_BUFFER_SIZE;
  dma_init_struct.direction = DMA_DIR_PERIPHERAL_TO_MEMORY;
  dma_init_struct.memory_base_addr = (uint32_t)usart1_rx_buffer;
  dma_init_struct.memory_data_width = DMA_MEMORY_DATA_WIDTH_BYTE;
  dma_init_struct.memory_inc_enable = TRUE;
  dma_init_struct.peripheral_base_addr = (uint32_t)&USART1->dt;
  dma_init_struct.peripheral_data_width = DMA_PERIPHERAL_DATA_WIDTH_BYTE;
  dma_init_struct.peripheral_inc_enable = FALSE;
  dma_init_struct.priority = DMA_PRIORITY_MEDIUM;
  dma_init_struct.loop_mode_enable = FALSE;
  dma_init(DMA1_CHANNEL3, &dma_init_struct);

  /* enable transfer full data intterrupt */
  dma_interrupt_enable(DMA1_CHANNEL3, DMA_FDT_INT, TRUE);

  /* dma1 channel3 interrupt nvic init */
  nvic_irq_enable(DMA1_Channel3_2_IRQn, 10, 0);

  dma_channel_enable(DMA1_CHANNEL3, TRUE); /* usart1 rx begin dma receiving */
  dma_channel_enable(DMA1_CHANNEL2, FALSE); /* usart1 tx begin dma transmitting */
}




lp_err_t bsp_at32_uart1_init(lp_u32_t baud,lp_u8_t data_bit,lp_u8_t stop_bit)
{
	usart1_configuration(baud,data_bit,stop_bit);
	uart1_dma_configuration();
	
	return LP_OK;
}



lp_err_t bsp_at32_uart1_send(lp_u8_t *data,lp_u16_t len)
{
	
	if(usart1_tx_dma_status != 0)
	{
		return LP_FAIL;
	}
	memset(usart1_tx_buffer,0x00,USART1_BUFFER_SIZE);
	memcpy(usart1_tx_buffer,data,len);
	usart1_tx_dma_status = 1;
	dma_data_number_set(DMA1_CHANNEL2,len);
	dma_channel_enable(DMA1_CHANNEL2, TRUE);
	
	return LP_OK;
}


lp_err_t bsp_at32_uart1_send_is_busy(void)
{
	
	if(usart1_tx_dma_status == 0)
	{
		return LP_OK;
	}

	return LP_FAIL;
}






