/**
  **************************************************************************
  * @file     flash.h
  * @version  v2.0.0
  * @date     2020-11-02
  * @brief    flash libray header file
  **************************************************************************
  *                       Copyright notice & Disclaimer
  *
  * The software Board Support Package (BSP) that is made available to 
  * download from Artery official website is the copyrighted work of Artery. 
  * Artery authorizes customers to use,  copy,  and distribute the BSP 
  * software and its related documentation for the purpose of design and 
  * development in conjunction with Artery microcontrollers. Use of the 
  * software is governed by this copyright notice and the following disclaimer.
  *
  * THIS SOFTWARE IS PROVIDED ON "AS IS" BASIS WITHOUT WARRANTIES, 
  * GUARANTEES OR REPRESENTATIONS OF ANY KIND. ARTERY EXPRESSLY DISCLAIMS, 
  * TO THE FULLEST EXTENT PERMITTED BY LAW,  ALL EXPRESS,  IMPLIED OR
  * STATUTORY OR OTHER WARRANTIES,  GUARANTEES OR REPRESENTATIONS, 
  * INCLUDING BUT NOT LIMITED TO,  THE IMPLIED WARRANTIES OF MERCHANTABILITY, 
  * FITNESS FOR A PARTICULAR PURPOSE,  OR NON-INFRINGEMENT.
  *
  **************************************************************************
  */

/*!< define to prevent recursive inclusion -------------------------------------*/
#ifndef __FLASH_H
#define __FLASH_H

#ifdef __cplusplus
extern "C" {
#endif

/* includes ------------------------------------------------------------------*/
#include "at32f421_aero_dali_board.h"

/*

 FLASH_BASE                      FLASH_DATA_BASE                               FLASH_SIZE
 0x8000000                       0x8000000 + FLASH_CODE_SIZE 
    +-------------------------------+----------------------------------------------+
    |                               |                                              | 
    |         program area          |                 data area                    |
    |                               |                                              |
    +-------------------------------+----------------------------------------------+
    |<------ FLASH_CODE_SIZE ------>|
    
*/ 
 
#define FLASH_SECTOR_SIZE                ((uint32_t)(1024 * 1))   /*!< flash sector size (byte) */
#define FLASH_CODE_SIZE                  ((uint32_t)(1024 * 10)) /*!< flash code size (byte) */


void flash_write(uint32_t address, uint16_t *pdata, uint32_t number);
void flash_read(uint32_t address, uint16_t *pdata, uint32_t number);


#ifdef __cplusplus
}
#endif

#endif


