


#ifndef _BSP_AT32F421_UART_H
#define _BSP_AT32F421_UART_H

#ifdef __cplusplus
extern "C" {
#endif


#include "at32f421_aero_dali_board.h"

#ifdef BOOT_APP
#define USART1_BUFFER_SIZE           512
#else
#define USART1_BUFFER_SIZE           32
#endif

extern uint8_t usart1_tx_buffer[USART1_BUFFER_SIZE];
extern uint8_t usart1_rx_buffer[USART1_BUFFER_SIZE];

lp_err_t bsp_at32_uart1_init(lp_u32_t baud,lp_u8_t data_bit,lp_u8_t stop_bit);
lp_err_t bsp_at32_uart1_send(lp_u8_t *data,lp_u16_t len);
/*
*return :
*LP_OK is no busy 
*LP_FAIL is busy
*/
lp_err_t bsp_at32_uart1_send_is_busy(void);

#ifdef __cplusplus
}
#endif

#endif


































