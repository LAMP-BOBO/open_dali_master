


#include "stm32h750_dali_monitor_board.h"
#include "main.h"



extern TIM_HandleTypeDef htim5;
extern TIM_HandleTypeDef htim12;



void st_hal_tim5_enable(void)
{
	
	__HAL_TIM_ENABLE(&htim5);
}

void st_hal_tim12_enable(void)
{
	__HAL_TIM_ENABLE(&htim12);
}


void st_hal_tim5_disable(void)
{
	__HAL_TIM_DISABLE(&htim5);
}


void st_hal_tim12_disable(void)
{
	__HAL_TIM_DISABLE(&htim12);
}

void st_hal_tim5_counter_set(lp_u32_t counter)
{
	__HAL_TIM_SET_COUNTER(&htim5,counter);
}

void st_hal_tim12_counter_set(lp_u32_t counter)
{
	__HAL_TIM_SET_COUNTER(&htim12,counter);
}

lp_u32_t st_hal_tim5_counter_get(void)
{
	return __HAL_TIM_GET_COUNTER(&htim5);
}

lp_u32_t st_hal_tim12_counter_get(void)
{
	return __HAL_TIM_GET_COUNTER(&htim12);
}




void st_hal_led_r_on(void)
{
	HAL_GPIO_WritePin(LED_GPIO_Port,LED_Pin,GPIO_PIN_RESET);
}

void st_hal_led_r_off(void)
{
	HAL_GPIO_WritePin(LED_GPIO_Port,LED_Pin,GPIO_PIN_SET);
}

void st_hal_led_r_toggle(void)
{
	HAL_GPIO_TogglePin(LED_GPIO_Port,LED_Pin);
}

void st_hal_dali_bus0_tr_high(void)
{
	HAL_GPIO_WritePin(DALI_TR_GPIO_Port,DALI_TR_Pin,GPIO_PIN_SET);
}

void st_hal_dali_bus0_tr_low(void)
{
	HAL_GPIO_WritePin(DALI_TR_GPIO_Port,DALI_TR_Pin,GPIO_PIN_RESET);
}

lp_u8_t st_hal_dali_bus0_re_read(void)
{
	
	if(GPIO_PIN_RESET == HAL_GPIO_ReadPin(DALI_RE_GPIO_Port,DALI_RE_Pin))
	{
		return 0;
	}
	 else
	 {
		return 1;
	 }
}

lp_u8_t st_hal_key1_read(void)
{
	if(GPIO_PIN_RESET == HAL_GPIO_ReadPin(K1_GPIO_Port,K1_Pin))
	{
		return 0;
	}
	 else
	 {
		return 1;
	 }
}

lp_u8_t st_hal_key2_read(void)
{
	if(GPIO_PIN_RESET == HAL_GPIO_ReadPin(K2_GPIO_Port,K2_Pin))
	{
		return 0;
	}
	 else
	 {
		return 1;
	 }

}



void st_hal_exint10_15_enable(void)
{
	HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
}

void st_hal_exint10_15_disable(void)
{
	HAL_NVIC_DisableIRQ(EXTI15_10_IRQn);
}








