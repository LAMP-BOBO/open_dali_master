


#ifndef _STM32H750_DALI_MONITOR_BOARD_
#define _STM32H750_DALI_MONITOR_BOARD_

#ifdef __cplusplus
extern "C" {
#endif


#include "config/lp_config.h"


void st_hal_exint10_15_enable(void);
void st_hal_exint10_15_disable(void);


void st_hal_tim5_enable(void);
void st_hal_tim12_enable(void);
void st_hal_tim5_disable(void);
void st_hal_tim12_disable(void);
void st_hal_tim5_counter_set(lp_u32_t counter);
void st_hal_tim12_counter_set(lp_u32_t counter);
lp_u32_t st_hal_tim5_counter_get(void);
lp_u32_t st_hal_tim12_counter_get(void);


void st_hal_led_r_on(void);
void st_hal_led_r_off(void);
void st_hal_led_r_toggle(void);
void st_hal_dali_bus0_tr_high(void);
void st_hal_dali_bus0_tr_low(void);
lp_u8_t st_hal_dali_bus0_re_read(void);


lp_u8_t st_hal_key1_read(void);
lp_u8_t st_hal_key2_read(void);
#ifdef __cplusplus
}
#endif

#endif























