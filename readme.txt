

2023.06.01

调通上位机和下位机的通讯目前可以发送数据到上位机打log

主要问题
1.自定义USB HID设备，只能固定64字节发送数据，应用层数据大了要拆包小了要补位.
2.STM32CubeMX 生成的hid代码不能直接使用
需要设置一些参数 
usbd_desc.c文件中
1. pid vid 用来查找设备

2.
修改将usbd_conf.h
#define USBD_CUSTOMHID_OUTREPORT_BUF_SIZE      64
#define USBD_CUSTOM_HID_REPORT_DESC_SIZE       34
usbd_customhid.h文件中的发送与接收长度为64
#define CUSTOM_HID_EPIN_SIZE                  0x40
#define CUSTOM_HID_EPOUT_SIZE                 0x40

3.添加HID设备描述符
 usbd_custom_hid_if.c 中
0x06, 0x00, 0xff,              // USAGE_PAGE (Vendor Defined Page 1)
0x09, 0x01,                    // USAGE (Vendor Usage 1)
0xa1, 0x01,                    // COLLECTION (Application)
0x09, 0x01,                    //   USAGE (Vendor Usage 1)
0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
0x26, 0xff, 0x00,              //   LOGICAL_MAXIMUM (255)
0x95, 0x40,                    //   REPORT_COUNT (64)
0x75, 0x08,                    //   REPORT_SIZE (8)
0x81, 0x02,                    //   INPUT (Data,Var,Abs)
0x09, 0x01,                    //   USAGE (Vendor Usage 1)
0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
0x26, 0xff, 0x00,              //   LOGICAL_MAXIMUM (255)
0x95, 0x40,                    //   REPORT_COUNT (64)
0x75, 0x08,                    //   REPORT_SIZE (8)
0x91, 0x02,                    //   OUTPUT (Data,Var,Abs)
0xc0                           // END_COLLECTION

改完上述即可使用

qt使用 libusb库的时候注意，必须要使用对应的编译器的库，不然会报错.


2023.06.06
上位机发送完成一直导致程序卡死，由于是下位机程序bug导致.
libusb bug导致 上位机时好时坏，有时候一天都是好的，第二天又不能用
放弃 libusb 更改成libhid.

卡死的bug已经找，主要原因是使用了c++的new去处理不定长数据包，改成使用QByteArray
以后qt尽量使用qt自己的容器来处理数据。
libhid 库的话发送部分第一个字节需要补位设置为 hid报告ID，如果没有报告ID就设置为0x00.

主机中有k1和k2按键
k1用来长按3s进入自带测试模式
指示灯200ms快速闪烁，0组和组1，5s来回交换发送全开全灭指令。










